#!/bin/bash

git pull https://kykylkan@bitbucket.org/intertechcompany/uagent.git master
php7.1 /usr/local/bin/composer install --no-dev --optimize-autoloader
php7.1 artisan migrate
npm i --no-save
npm run prod
php7.1 artisan view:clear
php7.1 artisan cache:clear
php7.1 artisan config:clear