<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Todo auth validation
Route::group([], function () {
	Route::post('policy/validateStep/{step}', 'Api\PolicyController@validateStep');
	Route::post('policy/checkAllRules', 'Api\PolicyController@checkAllRules');
	Route::post('policy/calc/{insurance}/{user}', 'Api\PolicyController@calc');
    Route::post('policy/save/{user}/{policy}/{insurance}', 'Api\PolicyController@savePolicy');
    Route::post('policy/createUpdatePolicy/{policy?}', 'Api\PolicyController@createUpdatePolicy');
	Route::get('/getAllInsurance', 'Api\PolicyController@getAllInsurance')->name('api.policy.insurances');


	Route::get('car/makes', 'Api\Cars\MakeController@index');
	Route::get('cities', 'Api\CitiesController@getCities');
	Route::get('city/{city}', 'Api\CitiesController@getStreets');
	Route::get('car/makes/{make}/models', 'Api\Cars\ModelController@index');
	Route::get('car/categories', 'Api\Cars\CategoriesController@index');
	Route::get('car/types', 'Api\Cars\TypesController@index');
	Route::get('car/purposes', 'Api\Cars\PurposesController@index');
	Route::get('car/check-in-alpha/{model}', 'Api\Cars\ModelController@checkAlpha');

	Route::get('car/detect', 'Api\Cars\DetectController@show');

    // Send message on phone
    Route::post('message', 'Api\SendMessageController@send')->name('message');

	// Address
	Route::get('get/address', 'Api\KladrController@getAddress');

    Route::post('home/question','HomePageController@question')->name('home.question');

    // Avtocad
    Route::get('/avtocode/ping', 'Api\AvtocodeController@ping');
    Route::get('/avtocode/user', 'Api\AvtocodeController@user');
	Route::get('/avtocode/reports', 'Api\AvtocodeController@getReports');
	Route::get('/avtocode/report/{gosNumber}', 'Api\AvtocodeController@findReport');
    Route::post('/avtocode/getReport', 'Api\AvtocodeController@getReport');

    // REST_API for Curators
    Route::get('/curators/index', 'Api\CuratorController@index')->name('api.curators.index');
    Route::get('/curators/show/{id}', 'Api\CuratorController@show')->name('api.curators.show');
    Route::patch('/curators/update/{id}', 'Api\CuratorController@update')->name('api.curators.update');
    Route::post('/curators/store', 'Api\CuratorController@store')->name('api.curators.store');

	Route::get('/agents', 'Api\AgentController@index')->name('api.agents');
});
