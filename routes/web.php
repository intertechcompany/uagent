<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/test-field', function () {
    return view('agent.test-field');
})->name('html');

Route::get('/pay-of-balance', function () {
    return view('agent.policies.pay-of-balance');
})->name('html2');

Route::get('/', 'HomePageController@index')->name('home.index');

Route::middleware('guest')->group(function () {
    Route::post('/', 'Auth\LoginController@login')->name('login');
    Route::post('/register', 'Auth\RegisterController@register')->name('register');
    Route::get('/sendPassword', 'Auth\RegisterController@sendPassword')->name('sendPassword');
    Route::post('/resetPassword/send', 'Auth\ResetPasswordController@send')->name('resetSend');
    Route::get('/newPassword/{token}', 'Auth\ResetPasswordController@showResetForm')->name('showResetForm');
    Route::post('/resetPassword', 'Auth\ResetPasswordController@reset')->name('reset');
    Route::post('/contact','ContactController@store')->name('contact');
});

// Confirm email
Route::get('email/{id}/confirm/{code}', 'Auth\EmailConfirmController')->name('email.confirm');

Route::middleware('auth')->group(function () {
    Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
});

// Settings
Route::group(['middleware' => ['auth', 'role.guest']], function () {
    Route::get('/settings', 'Agent\SettingController@index')->name('settings');
    Route::post('/settings', 'Agent\SettingController@update')->name('settings.update');
    Route::post('/setting/update/field', 'Agent\SettingController@updateField')->name('settings.update.field');
});

// Agents routes
Route::group(['middleware' => ['auth', 'role.agent']], function () {

    Route::get('/home', 'Agent\HomeController@index')->name('home');
    Route::get('/kv', 'Agent\KvController@index')->name('kv');
    Route::get('/support', 'Agent\ChatController@index')->name('support');
    Route::get('/support/chat/{chat}', 'Agent\ChatController@chat')->name('support.chat');
    Route::post('/support/data', 'Agent\ChatController@chats')->name('support.getData');
    Route::post('/support/chat/{chat}', 'Agent\ChatController@messages')->name('support.chat.getData');
    Route::post('/support/chat/{chat}/send', 'Agent\ChatController@newMessage')->name('support.chat.sendData');
    Route::post('/support/create', 'Agent\ChatController@newChat')->name('support.create');

    /**
     * Ajax fixed left admin panel menu
     */
    Route::post('/menu/fixed/{type}','AjaxController@fixedMenu')->name('agent.menu.fixed');

    // Policies
    Route::group(['middleware' => ['canShowPolicies']], function () {
        Route::get('/policies', 'Agent\PolicyController@index')->name('policies');

        Route::group(['middleware' => ['canCreatePolicy']], function () {
            Route::get('/policies/create', 'Agent\PolicyController@create')->name('policies.create');
            Route::get('/policies/reCalc/{policy}/formData', 'Agent\PolicyController@reCalc')->name('policies.reCalc.formData');
            Route::get('/policies/reCalc/{policy}', 'Agent\PolicyController@reCalcForm')->name('policies.reCalc.form');
        });

        Route::get('/policies/print/{policy}/{form}', 'Agent\PolicyController@printDocument')->name('policies.print');
        Route::get('/policies/show/{policy}', 'Agent\PolicyController@show')->name('policies.show');
        Route::post('/policies/set-status/{policy}', 'Agent\PolicyController@setStatus')->name('policies.set.status');

        /**
         * Verify phone
         */
        Route::post('/policies/verify-phone/{policy}', 'Agent\PolicyController@verifyPhone')->name('policies.verify.phone');

        Route::post('/policies/filter', 'Agent\PolicyController@ajaxFilter')->name('policies.filter');
        Route::post('/policies/delete', 'Agent\PolicyController@delete')->name('policies.delete');
        Route::get('/policies/filter/export', 'Agent\PolicyController@ajaxFilterExport')->name('policies.filter.export');
        Route::post('/policy/recount', 'Agent\PolicyController@ajaxRecount')->name('policy.recount');

        Route::get('/policies/{policy}/payment', 'Agent\PolicyController@payment')->name('policies.payment');
        Route::post('/policies/{policy}/payment', 'Agent\PolicyController@paymentFromBalance')->name('policies.payment.submit');
    });

    // Statistic
    Route::get('/statistic', 'Agent\StatisticController@index')->name('statistic');
    Route::any('/statistic/filter', 'Agent\StatisticController@ajaxFilter')->name('statistic.filter');
    Route::get('/statistic/export', 'Agent\StatisticController@export')->name('statistic.export');

    // Withdraws
    Route::get('/withdraws', 'Agent\WithdrawController@index')->name('withdraws');
    Route::get('/withdraw-funds', 'Agent\WithdrawController@funds')->name('withdraws-funds');
    Route::post('/withdraw-funds', 'Agent\WithdrawController@edit')->name('withdraws-funds.edit');
    Route::post('/withdraws/filter', 'Agent\WithdrawController@ajaxFilter')->name('withdraws.filter');
    Route::get('/withdraws/export', 'Agent\WithdrawController@export')->name('withdraws.export');

    // SubAgents
    Route::get('/createPassword', 'Agent\SubAgentController@index')->name('subAgents.pass');
    Route::post('/createPassword', 'Agent\SubAgentController@store')->name('subAgents.store');

    Route::get('/agents', 'Agent\AgentController@index')->name('agents');
    Route::get('/agents/create', 'Agent\AgentController@create')->name('agents.create');
    Route::post('/agents/create', 'Agent\AgentController@store')->name('agents.store');
    Route::get('/agents/{id}/edit', 'Agent\AgentController@edit')->name('agents.edit');
    Route::put('/agents/{id}/update', 'Agent\AgentController@update')->name('agents.update');
    Route::post('/agents/{id}/update/field', 'Agent\AgentController@updateField')->name('agents.update.field');
    Route::get('/agents/{id}/destroy', 'Agent\AgentController@destroy')->name('agents.destroy');
    Route::post('/agents/filter', 'Agent\AgentController@ajaxFilter')->name('agents.filter');
    Route::get('/agents/export', 'Agent\AgentController@export')->name('agents.export');
    Route::post('/agent/{agent}/update/rate', 'Agent\AgentController@updateRates')->name('agents.update.rates');
    Route::post('/agent/update/can-create-agent/{agent}', 'Agent\AgentController@updateCreateAgent')->name('agents.update.canCreateAgent');

    Route::get('/search', 'Agent\SearchController@index')->name('search');
});

Route::group(['prefix' => 'payment'], function () {
    Route::get('/alpha/{status}/{token}', 'Payment\AlphaPaymentController@status')->name('alpha.status');
    Route::get('/ingos/{token}', 'Payment\IngosPaymentController@status')->name('ingos.status');
    Route::get('/rgs/{status}/{token}', 'Payment\RgsPaymentController@status')->name('rgs.status');
});

Route::get('/nasko/{orderId}', 'Payment\NaskoPaymentController@status')->name('nasko.status');

// Admins routes
Route::group(['middleware' => ['auth', 'role.admin', 'role:superadministrator|administrator|curator'], 'prefix' => 'admin'], function () {

    Route::get('/support', 'Admin\ChatController@index')->name('admin.support');
    Route::post('/support/data', 'Admin\ChatController@chats')->name('admin.support.getData');
    Route::post('/support/chat/update/{chat?}', 'Admin\ChatController@update')->name('admin.support.chat.update');
    Route::get('/support/chat/{chat}', 'Admin\ChatController@chat')->name('admin.support.chat');
    Route::post('/support/chat/{chat}', 'Admin\ChatController@messages')->name('admin.support.chat.getData');
    Route::post('/support/chat/{chat}/send', 'Admin\ChatController@newMessage')->name('admin.support.chat.sendData');

    /**
     * Ajax fixed left admin panel menu
     */
    Route::post('/menu/fixed/{type}','AjaxController@fixedMenu')->name('admin.menu.fixed');

    Route::get('/home', 'Admin\DashboardController@index')->name('admin.dashboard.index');
    Route::post('/dashboard/info', 'Admin\DashboardController@ajaxDashboard')->name('dashboard.info');
    Route::get('/dashboard/policies/filter/export', 'Admin\DashboardController@ajaxFilterExport')->name('dashboard.policies.filter.export');

    // Stocks
    Route::get('/stocks', 'Admin\StocksController@index')->name('admin.stocks.index');
    Route::get('/stocks/edit/{stock}', 'Admin\StocksController@edit')->name('admin.stocks.edit');
    Route::post('/stocks/edit/{stock}/ajax', 'Admin\StocksController@editAjax')->name('admin.stocks.edit.ajax');
    Route::put('/stocks/update/{stock}', 'Admin\StocksController@update')->name('admin.stocks.update');
    Route::post('/stocks/update/{stock}/ajax', 'Admin\StocksController@updateAjax')->name('admin.stocks.update.ajax');
    Route::get('/stocks/delete/{stock}', 'Admin\StocksController@delete')->name('admin.stocks.delete');
    Route::get('/stocks/create', 'Admin\StocksController@create')->name('admin.stocks.create');
    Route::post('/stocks/store', 'Admin\StocksController@store')->name('admin.stocks.store');
    Route::post('/stocks/ajax', 'Admin\StocksController@ajaxIndex')->name('admin.stocks.ajax');

    // Agents
    Route::get('/agents', 'Admin\AgentController@index')->name('admin.agent.index');
    Route::get('/agents/create', 'Admin\AgentController@create')->name('admin.agent.create');
    Route::post('/agents/store', 'Admin\AgentController@store')->name('admin.agent.store');
    Route::get('/agents/edit/{agent}', 'Admin\AgentController@edit')->name('admin.agent.edit');
    Route::post('/agents/update/{agent}', 'Admin\AgentController@update')->name('admin.agent.update');
    Route::get('/agents/bonus/{agent}', 'Admin\AgentController@bonus')->name('admin.agent.bonus.add');
    Route::post('/agents/store/bonus/{agent}', 'Admin\AgentController@bonusStore')->name('admin.agent.bonus.store');

    /**
     * Ajax
     */
    Route::post('/agents', 'Admin\AgentController@ajaxAgents')->name('admin.agent.index.ajax');
    Route::post('/agents/edit/{agent}', 'Admin\AgentController@ajaxAgentData')->name('admin.agent.edit.ajax');
    Route::post('/agents/withdraw/edit', 'Admin\AgentController@updateWithdraw')->name('admin.agent.edit.withdraw.ajax');

    Route::group(['middleware' => ['auth', 'role.admin', 'role:superadministrator']], function () {
        //Admins
        Route::get('/admins', 'Admin\AdminsController@index')->name('admin.admins.index');
        Route::get('/admins/create', 'Admin\AdminsController@create')->name('admin.admins.create');
        Route::get('/admins/delete/{user}', 'Admin\AdminsController@delete')->name('admin.admins.delete');
        Route::get('/admins/edit/{user}', 'Admin\AdminsController@edit')->name('admin.admins.edit');
        Route::post('/admins/store', 'Admin\AdminsController@store')->name('admin.admins.store');
        Route::post('/admins/load/ajax', 'Admin\AdminsController@loadAjax')->name('admin.admins.load.ajax');
        Route::post('/admins/update/{user}', 'Admin\AdminsController@update')->name('admin.admins.update');
        Route::post('/admins/change-pass/{user}', 'Admin\AdminsController@onChangePass')->name('admin.admins.changePass');
        /**
         * Ajax
         */
        Route::post('/admins/create/ajax', 'Admin\AdminsController@createAjax')->name('admin.admins.create.ajax');
        Route::post('/admins/store/ajax', 'Admin\AdminsController@store')->name('admin.admins.store.ajax');
        Route::post('/admins/ajax', 'Admin\AdminsController@loadAjax')->name('admin.admins.load.ajax');
        Route::post('/admins/edit/{user}/ajax', 'Admin\AdminsController@editAjax')->name('admin.admins.edit.ajax');
    });

    //Curators
    Route::get('/curators', 'Admin\CuratorsController@index')->name('admin.curators.index');
    Route::get('/curators/edit/{curator}', 'Admin\CuratorsController@edit')->name('admin.curators.edit');
    Route::get('/curators/show/{id}', 'Admin\CuratorsController@show')->name('admin.curators.show');
    Route::put('/curators/update/{curator}', 'Admin\CuratorsController@update')->name('admin.curators.update');
    Route::get('/curators/delete/{curator}', 'Admin\CuratorsController@delete')->name('admin.curators.delete');
    Route::get('/curators/create', 'Admin\CuratorsController@create')->name('admin.curators.create');
    Route::post('/curators/store', 'Admin\CuratorsController@store')->name('admin.curators.store');

    //Rates
    Route::get('/rates', 'Admin\RatesController@index')->name('admin.rates.index');
    Route::get('/rates/edit/{rate}', 'Admin\RatesController@edit')->name('admin.rates.edit');
    Route::post('/rates/update/{rate}', 'Admin\RatesController@update')->name('admin.rates.update');
    Route::get('/rates/delete/{rate}', 'Admin\RatesController@delete')->name('admin.rates.delete');
    Route::get('/rates/create', 'Admin\RatesController@create')->name('admin.rates.create');
    Route::post('/rates/store', 'Admin\RatesController@store')->name('admin.rates.store');

    /**
     * Ajax
     */
    Route::post('/rates/edit/ajax', 'Admin\RatesController@editAjax')->name('admin.rates.create.ajax');
    Route::post('/rates/store/ajax', 'Admin\RatesController@store')->name('admin.rates.store.ajax');
    Route::post('/rates/edit/ajax/{rate}', 'Admin\RatesController@editRateAjax')->name('admin.rates.edit.ajax');

    //Rates Default
    Route::get('/rates-default', 'Admin\RatesDefaultController@index')->name('admin.rates_default.index');
    Route::get('/rates-default/edit/{rate}', 'Admin\RatesDefaultController@edit')->name('admin.rates_default.edit');
    Route::put('/rates-default/update/{rate}', 'Admin\RatesDefaultController@update')->name('admin.rates_default.update');
    Route::get('/rates-default/delete/{rate}', 'Admin\RatesDefaultController@delete')->name('admin.rates_default.delete');
    Route::get('/rates-default/create', 'Admin\RatesDefaultController@create')->name('admin.rates_default.create');
    Route::post('/rates-default/store', 'Admin\RatesDefaultController@store')->name('admin.rates_default.store');
    Route::post('/rates-default/load/ajax', 'Admin\RatesDefaultController@loadDataAjax')->name('admin.rates_default.load.ajax');

    // Withdraws
    Route::get('/withdraws', 'Admin\WithdrawController@index')->name('admin.withdraws.index');
    Route::get('/withdraws/edit/{withdraw}', 'Admin\WithdrawController@edit')->name('admin.withdraws.edit');
    Route::put('/withdraws/edit/{withdraw}', 'Admin\WithdrawController@update')->name('admin.withdraws.update');
    Route::post('/payments/edit/{payment?}', 'Admin\WithdrawController@updatePayment')->name('admin.payments.update');

    /**
     * Ajax
     */
    Route::post('/withdraws/load/ajax', 'Admin\WithdrawController@loadAjax')->name('admin.withdraws.load.ajax');
    Route::post('/withdraws/edit/{withdraw}/ajax', 'Admin\WithdrawController@editAjax')->name('admin.withdraws.edit.ajax');
    Route::post('/admins/status/ajax', 'Admin\WithdrawController@changeStatus')->name('admin.admins.change.status');

    //Rate Cities
    Route::get('/ratecities', 'Admin\RatecitiesController@index')->name('admin.ratecities.index');
    Route::get('/ratecities/edit/{rateCity}', 'Admin\RatecitiesController@edit')->name('admin.ratecities.edit');
    Route::post('/ratecities/update/{rateCity}', 'Admin\RatecitiesController@update')->name('admin.ratecities.update');
    Route::get('/ratecities/delete/{rateCity}', 'Admin\RatecitiesController@delete')->name('admin.ratecities.delete');
    Route::get('/ratecities/create', 'Admin\RatecitiesController@create')->name('admin.ratecities.create');
    Route::post('/ratecities/store', 'Admin\RatecitiesController@store')->name('admin.ratecities.store');

    /**
     * Ajax rate cities
     */
    Route::post('/ratecities/edit/ajax', 'Admin\RatecitiesController@editAjax')->name('admin.ratecities.create.ajax');

    Route::post('/ratecities/store/ajax', 'Admin\RatecitiesController@store')->name('admin.ratecities.store.ajax');

    Route::post('/ratecities/edit/ajax/{rateCity}', 'Admin\RatecitiesController@editRateAjax')->name('admin.ratecities.edit.ajax');

    // Policies
    Route::get('/policy/show/{policy}', 'Admin\PoliciesController@show')->name('admin.policies.show');
    Route::get('/policy/export', 'Admin\DashboardController@exportPolices')->name('admin.policy.export');

    // Ajax

    Route::get('/search/cities','AjaxController@cities');

    // Tabs

    Route::post('/create/{tab}', 'Admin\AgentController@createTab')->name('admin.agent.create.tab');
    Route::get('/{tab}/file/{tabmodel}', 'Admin\AgentController@getTabFile')->name('admin.agent.tab.file');
    Route::post('/{tab}/edit/{tabmodel}', 'Admin\AgentController@editTab')->name('admin.agent.tab.edit');
    Route::get('/{tab}/delete/{tabmodel}', 'Admin\AgentController@deleteTab')->name('admin.agent.tab.delete');
});

if(config('app.env') != 'production') {
    Route::get('/test', function () {
        $data = array(
            'user_id' => 4,
            'make_id' => 42,
            'model_id' => 467,
            'vehicle_registration_year' => '2013',
            'vehicle_id_type' => 'vin',
            'vehicle_document_type' => 'sts',
            'driver_count' => 'specified',
            'drivers' =>
                array(
                    0 =>
                        array(
                            'last_name' => 'УСКОВ',
                            'first_name' => 'ИВАН',
                            'middle_name' => 'АЛЕКСЕЕВИЧ',
                            'dob' => '04-05-1957',
                            'document_serial' => '3609',
                            'document_number' => '054213',
                            'start_date' => '20-03-2000',
                        ),
                    // array (
                    //     'last_name' => 'ТАТАРИНЦЕВ',
                    //     'first_name' => 'МИХАИЛ',
                    //     'middle_name' => 'ВАСИЛЬЕВИЧ',
                    //     'dob' => '17-11-1955',
                    //     'document_serial' => '9903',
                    //     'document_number' => '535457',
                    //     'start_date' => '01-07-1974',
                    // ),
                    // array (
                    //     'last_name' => 'АНОХИН',
                    //     'first_name' => 'СЕРГЕЙ',
                    //     'middle_name' => 'НИКОЛАЕВИЧ',
                    //     'dob' => '06-12-1957',
                    //     'document_serial' => '36ВА',
                    //     'document_number' => '109786',
                    //     'start_date' => '31-12-1975',
                    // ),
                    // array (
                    //     'last_name' => 'СЕЛЯВКИН',
                    //     'first_name' => 'ИВАН',
                    //     'middle_name' => 'МИХАЙЛОВИЧ',
                    //     'dob' => '01-01-1949',
                    //     'document_serial' => '36ВА',
                    //     'document_number' => '100998',
                    //     'start_date' => '01-01-1967',
                    // ),
                    // array (
                    //     'last_name' => 'СТЕПИН',
                    //     'first_name' => 'ЮРИЙ',
                    //     'middle_name' => 'ОЛЕГОВИЧ',
                    //     'dob' => '14-09-1987',
                    //     'document_serial' => '3607',
                    //     'document_number' => '552480',
                    //     'start_date' => '14-05-2008',
                    // ),
                ),
            'owner' =>
                array(
                    'last_name' => 'ХОХАНОВ',
                    'first_name' => 'ИГОРЬ',
                    'middle_name' => 'СЕРГЕЕВИЧ',
                    'dob' => '27-12-1958',
                    'personal_document_serial' => '2004',
                    'personal_document_number' => '139907',
                    'personal_document_issued_by' => 'ЛЕВОБЕРЕЖНЫМ РУВД ГОРОДА ВОРОНЕЖА',
                    'personal_document_date' => '29-12-2003',
                    'place_of_birth' => 'ВОРОНЕЖ',
                    'address' =>
                        array(
                            'value' => 'г Воронеж, ул Красный Октябрь',
                            'unrestricted_value' => 'Воронежская обл, г Воронеж, Левобережный р-н, ул Красный Октябрь',
                            'data' =>
                                array(
                                    'postal_code' => NULL,
                                    'country' => 'Россия',
                                    'region_fias_id' => 'b756fe6b-bbd3-44d5-9302-5bfcc740f46e',
                                    'region_kladr_id' => '3600000000000',
                                    'region_with_type' => 'Воронежская обл',
                                    'region_type' => 'обл',
                                    'region_type_full' => 'область',
                                    'region' => 'Воронежская',
                                    'area_fias_id' => NULL,
                                    'area_kladr_id' => NULL,
                                    'area_with_type' => NULL,
                                    'area_type' => NULL,
                                    'area_type_full' => NULL,
                                    'area' => NULL,
                                    'city_fias_id' => '5bf5ddff-6353-4a3d-80c4-6fb27f00c6c1',
                                    'city_kladr_id' => '3600000100000',
                                    'city_with_type' => 'г Воронеж',
                                    'city_type' => 'г',
                                    'city_type_full' => 'город',
                                    'city' => 'Воронеж',
                                    'city_area' => NULL,
                                    'city_district_fias_id' => NULL,
                                    'city_district_kladr_id' => NULL,
                                    'city_district_with_type' => 'Левобережный р-н',
                                    'city_district_type' => 'р-н',
                                    'city_district_type_full' => 'район',
                                    'city_district' => 'Левобережный',
                                    'settlement_fias_id' => NULL,
                                    'settlement_kladr_id' => NULL,
                                    'settlement_with_type' => NULL,
                                    'settlement_type' => NULL,
                                    'settlement_type_full' => NULL,
                                    'settlement' => NULL,
                                    'street_fias_id' => 'b66fb543-c2bb-4dc2-9cd2-c775ff0f1849',
                                    'street_kladr_id' => '36000001000043000',
                                    'street_with_type' => 'ул Красный Октябрь',
                                    'street_type' => 'ул',
                                    'street_type_full' => 'улица',
                                    'street' => 'Красный Октябрь',
                                    'house_fias_id' => NULL,
                                    'house_kladr_id' => NULL,
                                    'house_type' => NULL,
                                    'house_type_full' => NULL,
                                    'house' => NULL,
                                    'block_type' => NULL,
                                    'block_type_full' => NULL,
                                    'block' => NULL,
                                    'flat_type' => NULL,
                                    'flat_type_full' => NULL,
                                    'flat' => NULL,
                                    'flat_area' => NULL,
                                    'square_meter_price' => NULL,
                                    'flat_price' => NULL,
                                    'postal_box' => NULL,
                                    'fias_id' => 'b66fb543-c2bb-4dc2-9cd2-c775ff0f1849',
                                    'fias_code' => '36000001000000004300000',
                                    'fias_level' => '7',
                                    'fias_actuality_state' => '0',
                                    'kladr_id' => '36000001000043000',
                                    'geoname_id' => NULL,
                                    'capital_marker' => '2',
                                    'okato' => '20401375000',
                                    'oktmo' => '20701000001',
                                    'tax_office' => '3663',
                                    'tax_office_legal' => '3663',
                                    'timezone' => NULL,
                                    'geo_lat' => '51.6293364',
                                    'geo_lon' => '39.2721622',
                                    'beltway_hit' => NULL,
                                    'beltway_distance' => NULL,
                                    'metro' => NULL,
                                    'qc_geo' => '2',
                                    'qc_complete' => NULL,
                                    'qc_house' => NULL,
                                    'history_values' => NULL,
                                    'unparsed_parts' => NULL,
                                    'source' => 'Воронежская обл, г Воронеж, ул Красный Октябрь',
                                    'qc' => NULL,
                                ),
                        ),
                    'postal_code' => '394028',
                    'house' => '88',
                    'address2' => '-',
                    'address3' => '-',
                ),
            'insurer' =>
                array(
                    'last_name' => 'УСКОВ',
                    'first_name' => 'ИВАН',
                    'middle_name' => 'АЛЕКСЕЕВИЧ',
                    'dob' => '04-05-1957',
                    'personal_document_serial' => '2003',
                    'personal_document_number' => '614854',
                    'personal_document_issued_by' => 'КОМИНТЕРНОВСКИМ РУВД ГОРОДА ВОРОНЕЖ',
                    'personal_document_date' => '10-02-2003',
                    'place_of_birth' => 'ВОРОНЕЖ',
                    'address' =>
                        array(
                            "value" => "г Воронеж, ул Газовая, д 20, кв 5",
                            "unrestricted_value" => "Воронежская обл, г Воронеж, Коминтерновский р-н, ул Газовая, д 20, кв 5",
                            "data" => [
                                "postal_code" => "394019",
                                "country" => "Россия",
                                "region_fias_id" => "b756fe6b-bbd3-44d5-9302-5bfcc740f46e",
                                "region_kladr_id" => "3600000000000",
                                "region_with_type" => "Воронежская обл",
                                "region_type" => "обл",
                                "region_type_full" => "область",
                                "region" => "Воронежская",
                                "area_fias_id" => null,
                                "area_kladr_id" => null,
                                "area_with_type" => null,
                                "area_type" => null,
                                "area_type_full" => null,
                                "area" => null,
                                "city_fias_id" => "5bf5ddff-6353-4a3d-80c4-6fb27f00c6c1",
                                "city_kladr_id" => "3600000100000",
                                "city_with_type" => "г Воронеж",
                                "city_type" => "г",
                                "city_type_full" => "город",
                                "city" => "Воронеж",
                                "city_area" => null,
                                "city_district_fias_id" => null,
                                "city_district_kladr_id" => null,
                                "city_district_with_type" => "Коминтерновский р-н",
                                "city_district_type" => "р-н",
                                "city_district_type_full" => "район",
                                "city_district" => "Коминтерновский",
                                "settlement_fias_id" => null,
                                "settlement_kladr_id" => null,
                                "settlement_with_type" => null,
                                "settlement_type" => null,
                                "settlement_type_full" => null,
                                "settlement" => null,
                                "street_fias_id" => "ad19e1a1-9f9a-4840-99c9-7c766d36f244",
                                "street_kladr_id" => "36000001000017700",
                                "street_with_type" => "ул Газовая",
                                "street_type" => "ул",
                                "street_type_full" => "улица",
                                "street" => "Газовая",
                                "house_fias_id" => "77153d87-2625-425e-8499-626e1df99d4e",
                                "house_kladr_id" => "3600000100001770032",
                                "house_type" => "д",
                                "house_type_full" => "дом",
                                "house" => "20",
                                "block_type" => null,
                                "block_type_full" => null,
                                "block" => null,
                                "flat_type" => "кв",
                                "flat_type_full" => "квартира",
                                "flat" => "5",
                                "flat_area" => null,
                                "square_meter_price" => null,
                                "flat_price" => null,
                                "postal_box" => null,
                                "fias_id" => "77153d87-2625-425e-8499-626e1df99d4e",
                                "fias_code" => "36000001000000001770032",
                                "fias_level" => "8",
                                "fias_actuality_state" => "0",
                                "kladr_id" => "3600000100001770032",
                                "geoname_id" => null,
                                "capital_marker" => "2",
                                "okato" => "20401370000",
                                "oktmo" => "20701000001",
                                "tax_office" => "3662",
                                "tax_office_legal" => "3662",
                                "timezone" => null,
                                "geo_lat" => "51.6773494",
                                "geo_lon" => "39.1410974",
                                "beltway_hit" => null,
                                "beltway_distance" => null,
                                "metro" => null,
                                "qc_geo" => "0",
                                "qc_complete" => null,
                                "qc_house" => null,
                                "history_values" => null,
                                "unparsed_parts" => null,
                                "source" => null,
                                "qc" => null,
                            ]
                        ),
                    'postal_code' => '394019',
                    'house' => '88',
                    'address2' => '-',
                    'address3' => '-',
                ),
            'insurer_and_owner_same' => 2,
            'is_electronic' => 1,
            'email' => 'kulbij.intertech@gmail.com',
            'policy_number_series' => NULL,
            'insurance' => 'alpha',
            'is_vehicle_use_trailer' => false,
            'type' => 6,
            'purpose' => 3,
            // 'phone' => '+7-951-304-86-95',
            'phone' => '+7-903-858-87-76',
            'vehicle_plate_number' => 'К603УХ36',
            'vehicle_plate_region' => '36',
            'policy_start_date' => '21-06-2019',
            'power' => '80',
            'vehicle_id' => 'X9L212300D0449668',
            'vehicle_document_serial' => '3608',
            'vehicle_document_number' => '849714',
            'vehicle_document_date' => '26-04-2005',
            'diagnostic_number' => '055990031801281',
            'diagnostic_until_date' => '22-04-2020',
        );

        // dd(
        //     \App\Service\DadataService::suggest([
        //             'query' => 'г Воронеж, УЛ ГАЗОВАЯ  Д 20 КВ 5',
        //             'count' => 1
        //         ])['suggestions']
        // );

        dd(\App\Service\PolicyService\Provider\InsuranceStaticFactory::factory('alpha')->create($data));
    });
}

Route::get('/html/{page}',function (string $page) {
    return view('html.'.$page);
})->name('html.home');

Route::get('/admin/{dir}/{page}/{id?}',function (string $dir,string $page) {
    return view('html.' . $dir . '.'.$page);
})->name('admin');

Route::get('/home-new',function () {
    return view('agent.home-new');
})->name('html');
