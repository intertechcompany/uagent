$('.printType').on('change',function () {
	let form = $(this).val();
	let id = $(this).attr('data-policyId');
	if ($.isNumeric(form)) {
		window.open('/policies/print/' + id + '/' + form, '_blank')
	}
});

$('.policy-file i').on('click',function () {
	let p = $(this);
	let name = $(this).attr('data-name');
	let deleteFile = confirm('Удалить ' + name + ' ?');

	if (deleteFile) {
		let id = $(this).attr('data-file_id');
		$.ajax({
			dataType: 'json',
			type: 'GET',
			url: '/policies/remove-photo/',
			data: 'file='+id,
			success: function(data){
				if (data.status == 1) {
					p.parent().remove();
				}
			}
		});
	}

});


