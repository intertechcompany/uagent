class ajaxBuilder {
    constructor(data, callback = null) {
        this.url = data.url;
        this.method = data.method;
        this.param = data.param;
        this.async = data.async == 'undefined' ? true : data.async;
        this.callback = callback;

        this.ajax();
    }

    ajax() {
        let render = $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'json',
            type: this.method,
            url: this.url,
            data: this.param,
            success: this.callback,
            async: this.async,
        });
    }
}

$(document).ready( function () {
    let rateSelect = $(".js-select2-rates");

    if (rateSelect.length) {
        rateSelect.select2({
            placeholder: "Выберите ставки",
            multiple: true,
            templateSelection: formatState
        });
    }

	let agentsSubAgentsSelect = $(".js-select2-agents-subagents");

	if (agentsSubAgentsSelect.length) {
	    agentsSubAgentsSelect.select2({
            placeholder: "Выберите агентов или субагентов",
            multiple: true,
            templateSelection: formatState
        });
    }

    let select2Cities = $('.js-select2-agents-filter-cities');

	if (select2Cities.length) {
        select2Cities.select2({
            placeholder: "Выберите города и населенные пункты",
            multiple: true,
            minimumInputLength: 2,
            ajax: {
                url: '/admin/search/cities',
                dataType: 'json',
                data: function (params) {
                    return {
                        q: $.trim(params.term)
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },

        });
    }

    let address = $("#address");

	if (address.length) {
        address.suggestions({
            token: "20131cc64a9d46aa2ea1d9437478bfcb1c1c66dc",
            type: "ADDRESS",
            scrollOnFocus: false,
            triggerSelectOnBlur: false,
            triggerSelectOnEnter: false,
            addon: 'none',
            onSelect: function(suggestion) {
                $('input[name="city_id"]').val(suggestion.data.kladr_id);
            }
        });
    }

    if ($('.datepicker-tab').length) {
        $('.datepicker-tab').datepicker({
            dateFormat: 'yyyy-mm-dd',
            language: 'ru'
        });
    }

    if ($('.datepicker-worktime').length) {
        $('.datepicker-worktime').datetimepicker({
            datepicker:false,
            format:'H:i',
        });
    }

    if ($('.datetimepicker').length) {
        $('.datetimepicker').datetimepicker({
            format:'Y-m-d H:i',
        });
    }

    $('#paid, #accrued').on('click', '.create-tab', function() {
        let tab = $(this).data('tab');
        $('.create-form-tab').addClass('hidden');
        let ajaxBuild = new ajaxBuilder({
            'url': $(this).data('url'),
            'method': 'POSt',
            'param': null,
        }, function(data) {
            let formPaid = $('#formCreateTab' + tab);
            formPaid.html(data.view);
            formPaid.toggleClass('hidden');
            
            $('.datepicker-tab').datepicker({
                dateFormat: 'yyyy-mm-dd',
                language: 'ru'
            });
        });
    });

    $('#leftMSlideMenu').on('click', function() {
        let ajaxBuild = new ajaxBuilder({
            'url': $(this).data('url'),
            'method': 'post',
            'param': null,
        }, function(data) {});
    });

    $('#paid, #accrued').on('click', '.edit-tab', function() {
        let tab = $(this).data('tab');
        $('.create-form-tab').addClass('hidden');
        let ajaxBuild = new ajaxBuilder({
            'url': $(this).data('url'),
            'method': 'POSt',
            'param': null,
        }, function(data) {
            let formPaid = $('#formCreateTab' + tab);
            formPaid.html(data.view);
            formPaid.removeClass('hidden');
            
            $('.datepicker-paid').datepicker({
                dateFormat: 'yyyy-mm-dd',
                language: 'ru'
            });
        });
    });

    let datepicker_js =  $('.datepicker-js');

	if (datepicker_js.length) {
        var im = new Inputmask("9999-99-99");
        im.mask(datepicker_js);
        datepicker_js.datepicker({language: 'ru'});
    }

    let allPaid = $('#allPaid');
    if (allPaid.length) {
        allPaid.on('click', function() {
            let elementPaidounts = $(this);
            $('.paidout input[type="checkbox"]').each(function() {
                if (elementPaidounts.is(':checked')) {
                    $(this).attr('checked', true);
                } else {
                    $(this).removeAttr('checked');
                }
            });
        });
    }

    let inputmaskNumberCart = $('#number_cart');
    if (inputmaskNumberCart.length) {
        inputmaskNumberCart.inputmask('9999-9999-9999-9999');
    }

    let inputmaskNCartDate = $('#cart_date');
    if (inputmaskNCartDate.length) {
        inputmaskNCartDate.inputmask('99/99');
    }
});

function formatState (state) {
	return state.element.title;
}
