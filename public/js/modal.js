(function () {

	$.fn.modalPlugin = function(options){
		var options = $.extend({
			'overlayClass':'#overlay',
			'openModalClass': '.js_open_modal',
			'closeClass': '.modal_close, #overlay',
			'modalClass': '.modal_div',
			'showClass': 'show',
			'hideClass': 'hide',
			'attrModal': 'data-modal',
			'bodyNoScroll': 'no-scroll',
			'messageModalClass': '.message_modal',
			'body': 'body'

		}, options);

		var storage = {};
		var methods = {
			init: function(){

				$(options.openModalClass).on('click', function(){
					storage.modal = $(this).attr(options.attrModal);
					methods.open();
					methods.bodyAddClass();
				});


				$(options.closeClass).on('click', function(){
					methods.close();
					methods.bodyRemoveClass();
				});


				$(options.body).keydown(function(eventObject){
					if (eventObject.which === 27){
						methods.close();
						methods.bodyRemoveClass();
					}
				});


				if($(options.modalClass).hasClass('show')){
					methods.open();
					methods.bodyAddClass();
				}

				if (window.location.hash && ~window.location.hash.indexOf('modal')) {
					storage.modal = window.location.hash;
					methods.open();
					methods.bodyAddClass();
				}
			},


			open: function (){
				$(storage.modal).removeClass(options.hideClass);
				$(options.overlayClass).removeClass(options.hideClass);
			},


			close: function (){
				$(options.modalClass).addClass(options.hideClass);
				$(options.overlayClass).addClass(options.hideClass);
				$(options.messageModalClass).removeClass(options.showClass);
			},


			bodyAddClass: function () {
				$(options.body).addClass(options.bodyNoScroll);
			},


			bodyRemoveClass: function () {
				$(options.body).removeClass(options.bodyNoScroll);
			}

		};
		methods.init();
	};
})(jQuery);
