<?php

return [
	'home' => 'Главная',
	'agents' => 'Агенты',
	'curators' => 'Кураторы',
	'rates' => 'Ставки',
	'news' => 'Новости',
	'stocks' => 'Акции',
	'statistic' => 'Статистика',
	'administrators' => 'Администраторы',
	'withdraws' => 'Выплаты',
	'policies' => 'Полисы',
	'roles' => [
		'superadministrator' => 'Суперадминистратор',
		'administrator' => 'Администратор',
		'curator' => 'Куратор',
	],
];