<?php

return [
    'status' => [
        \App\Model\Chat::STATUS_NEW => 'новый',
        \App\Model\Chat::STATUS_IN_WORK => 'в работе',
        \App\Model\Chat::STATUS_CLOSED => 'решен',
    ]
];