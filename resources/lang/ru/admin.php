<?php

return [
    'payments' => [
        'status' => [
            \App\Model\PolicyPayment::STATUS_NEW => 'новый',
            \App\Model\PolicyPayment::STATUS_PAID => 'оплачен',
            \App\Model\PolicyPayment::STATUS_CANCELED => 'отклонен',
        ]
    ]
];