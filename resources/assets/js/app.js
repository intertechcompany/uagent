/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vue from 'vue';


import $ from 'jquery';
import 'select2';
import 'select2/dist/css/select2.css';
import 'element-ui/lib/theme-chalk/index.css';
// import 'semantic-ui-css/semantic.min.css';


import VueResource from 'vue-resource';
import SuiVue from 'semantic-ui-vue';
import vSelect from 'vue-select';
import Tooltip from 'vue-directive-tooltip';
import VueTheMask from 'vue-the-mask'
// window.axios = require('axios');
import axios from 'axios';
import DatePicker from 'element-ui';
import lang from 'element-ui/lib/locale/lang/ru-RU'
import locale from 'element-ui/lib/locale'

locale.use(lang)
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

window.preloader = function () {
    let app = $('#app');
    let preloaderEl = $('#preloader');

    if (app.hasClass('preloader-body')) {
        app.removeClass('preloader-body');
        preloaderEl.addClass('hidden');
    } else {
        app.addClass('preloader-body');
        preloaderEl.removeClass('hidden');
    }
};

window.preloaderOff = () => {
    $('#app').removeClass('preloader-body');
    $('#preloader').addClass('hidden');
};


Vue.use(VueResource);
Vue.use(SuiVue);
Vue.use(axios);
Vue.use(VueTheMask);
Vue.use(DatePicker);


import policyCreateForm from './components/PolicyCreateForm.vue';
import FormError from './components/Common/FormError.vue';
import Statistics from './components/Statistics/Statistics.vue';
import Payment from './components/Payment/Payment.vue';
import policy from './components/PolicyCreateForm/PolicyFilter.vue';
import Agents from './components/Agents/Agents.vue';
import ReCalc from './components/ReCalc/ReCalc.vue';
import PolicyFilter from './components/PolicyFilter.vue';
import ProgressCircle from './components/ProgressCircle.vue';
import TestField from './components/test-field-step.vue';
import TechSupport from './../vue/components/Support/TechSupport.vue';
import ChatSupport from './../vue/components/Support/ChatSupport.vue';

Vue.component('pagination', require('laravel-vue-pagination'));

Vue.filter('to2Fixed', function (value) {
    return Number(value).toFixed(2);
});

const app = new Vue({
    el: '#app',
    components: {
        Tooltip,
        policyCreateForm,
        PolicyFilter,
        FormError,
        vSelect,
        Statistics,
        policy,
        Payment,
        Agents,
        ReCalc,
        ProgressCircle,
        TechSupport,
        ChatSupport,
        TestField
    },
    data: {
        checked: 1
    }
});

var onSubmit = function(token) {
    let phone = $('#modal-sign input[name="phone"]');
    let email = $('#modal-sign input[name="email"]');
    let fieldError = $('#modal-sign .field-error').empty();
    let password = $('#modal-sign input[name="password"]');
    let password2 = $('#modal-sign input[name="password2"]');
    let emailPattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z1]{2,4}$/i;
    let isHaveError = false;

    if (Number($('#modal-sign .form-type:checked').val()) === 1) {
        if (phone.val().trim().length === 0) {
            phone.addClass("error");
            fieldError.addClass('have-error');
            isHaveError = true;
            let oldError = fieldError.html();
            fieldError.html(oldError + 'Укажите телефон <br>');
        }
        if (password.val().trim().length === 0) {
            password.addClass("error");
            fieldError.addClass('have-error');
            isHaveError = true;
            let oldError = fieldError.html();
            fieldError.html(oldError + 'Укажите пароль <br>');
        }
    } else {
        if (email.val().trim().length === 0) {
            email.addClass("error");
            fieldError.addClass('have-error');
            isHaveError = true;
            let oldError = fieldError.html();
            fieldError.html(oldError + 'Укажите почту <br>');
        }
        if (!emailPattern.test(email.val())) {
            email.addClass("error");
            fieldError.addClass('have-error');
            isHaveError = true;
        }
        if (password2.val().trim().length === 0) {
            password2.addClass("error");
            fieldError.addClass('have-error');
            isHaveError = true;
            let oldError = fieldError.html();
            fieldError.html(oldError + 'Укажите пароль <br>');
        }
    }

    if (isHaveError === true) {
        return false;
    }

    $.ajax({
        type: "POST",
        url: "/",
        data: {
            phone: phone.val(),
            email: email.val(),
            password: password.val(),
            password2: password2.val(),
            "g-recaptcha-response": token,
            fileType: $('#modal-sign .form-type:checked').val()
        },
        success: function (response) {
            if (response.status == false) {
                fieldError.addClass('have-error');
                fieldError.html(response.error);
            } else {
                fieldError.removeClass('have-error');
                location.href = response.path;
            }
        }
    });
};

var onSubmit2 = function (token) {
    let fieldError = $('#modal-pass-recovery .field-error').empty();
    let reset_password = $('#reset_password').val();
    let reset_password_confirmation = $('#reset_password_confirmation').val();
    let recoverToken = $('#reset_token').val();
    let isHaveError = false;

    if (reset_password.trim().length === 0) {
        fieldError.addClass('have-error');
        isHaveError = true;
        fieldError.html('Укажите пароль <br>');
    }

    if(reset_password_confirmation.trim().length === 0) {
        fieldError.addClass('have-error');
        isHaveError = true;
        let oldError = fieldError.html();
        fieldError.html(oldError + 'Укажите пароль еще раз <br>');
    }

    if(reset_password.trim()!== reset_password_confirmation.trim()) {
        fieldError.addClass('have-error');
        isHaveError = true;
        let oldError = fieldError.html();
        fieldError.html(oldError + 'Пароли должны совпадать <br>');
    }


    if (isHaveError === true) {
        return false;
    } else {
        fieldError.removeClass('have-error');
        $.ajax({
            type: "POST",
            url: "/resetPassword",
            data: {
                reset_password: reset_password,
                reset_password_confirmation: reset_password_confirmation,
                token: recoverToken,
                "g-recaptcha-response": token
            },
            success: function (response) {
                if (response.status === true) {
                    location.href = response.path;
                } else {
                    fieldError.html(response.error);
                }
            }
        });
    }
}

var onSubmit3 = function (token) {
    let phone = $('#modal-register input[name="phone"]');
    let passwordMess = $('#modal-register input[name="password"]');
    let email = $('#modal-register input[name="email"]');
    let agree = $('#modal-register input[name="i-true"]');
    let agreeCheckbox = $('#modal-register .c-choice__label');
    let fieldError = $('#modal-register .field-error').empty();
    let password = $('#modal-register input[name="reg_password"]');
    let password2 = $('#modal-register input[name="reg_password_confirmation"]');
    let emailPattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
    let isHaveError = false;

    if (Number($('#modal-register .form-type:checked').val()) === 1) {
        if (phone.val().trim().length === 0) {
            phone.addClass("error");
            fieldError.addClass('have-error');
            isHaveError = true;
            let oldError = fieldError.html();
            fieldError.html(oldError + 'Укажите телефон <br>');
        }

        if (agree.prop("checked") === false) {
            agree.addClass("error");
            agreeCheckbox.addClass('have-error');
            isHaveError = true;
        }
    } else {
        if (email.val().trim().length === 0) {
            passwordMess.addClass("error");
            fieldError.addClass('have-error');
            isHaveError = true;
            let oldError = fieldError.html();
            fieldError.html(oldError + 'Укажите почту <br>');
        }
        if (!emailPattern.test(email.val())) {
            email.addClass("error");
            fieldError.addClass('have-error');
            isHaveError = true;
        }
        if (password.val().trim().length < 9) {
            password.addClass("error");
            fieldError.addClass('have-error');
            isHaveError = true;
            let oldError = fieldError.html();
            fieldError.html(oldError + 'Пароль должен быть не менее 9 символов<br>');
        }
        if (!/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?).{8,}$/.test(password.val())) {
            password.addClass("error");
            fieldError.addClass('have-error');
            isHaveError = true;
            let oldError = fieldError.html();
            fieldError.html(oldError + 'Пароль должен содержать символы верхнего и нижнего регистра, а так же не менее одной цифры<br>');
        }
        if (password2.val().trim().length === 0) {
            password2.addClass("error");
            fieldError.addClass('have-error');
            isHaveError = true;
            let oldError = fieldError.html();
            fieldError.html(oldError + 'Укажите подтверждающий пароль <br>');
        }
        if (password2.val().trim() !== password.val().trim()) {
            password2.addClass("error");
            fieldError.addClass('have-error');
            isHaveError = true;
            let oldError = fieldError.html();
            fieldError.html(oldError + 'Пароли не совпадают <br>');
        }
        if (agree.prop("checked") === false) {
            agree.addClass("error");
            agreeCheckbox.addClass('have-error');
            isHaveError = true;
        }
    }



    if (isHaveError === true) {
        return false;
    }

    $.ajax({
        type: "POST",
        url: "/register",
        data: {
            phone: phone.val(),
            email: email.val(),
            password: passwordMess.val(),
            reg_password: password.val(),
            reg_password_confirmation: password2.val(),
            "gRecaptchaResponse": token,
            fileType: $('#modal-register .form-type:checked').val()
        },
        success: function (response) {
            fieldError.removeClass('have-error');
            location.href = response.path;
        },
        error: function (error) {
            fieldError.addClass('have-error');if (error.responseJSON.error.phone !== undefined) {
                phone.addClass("error");
                fieldError.html(error.responseJSON.error.phone);
            } else if (error.responseJSON.error.email !== undefined) {
                email.addClass("error");
                fieldError.html(error.responseJSON.error.email);
            }
        }
    });
};

var feedback = function (token) {
    let fio = $('.fio').val();
    let feedbackPhone = $('.feedback-phone').val();
    let feedbackText = $('.feedback-text').val();

    $.ajax({
        type: "POST",
        url: "/api/home/question",
        data: {
            name: fio,
            phone: feedbackPhone,
            message: feedbackText,
            "g-recaptcha-response": token
        },
        success: function (response) {
            alert('Сообщение отправлено');
            document.location.reload(true);
        }
    });

    return false;
};

window.onSubmit = onSubmit;
window.onSubmit2 = onSubmit2;
window.onSubmit3 = onSubmit3;
window.feedback = feedback;

// var onloadCallback = function() {
//     grecaptcha.render('submit', {
//         'sitekey' : '6LccLKMUAAAAACjrtSptXthc7trpsXAUOmj20H8g',
//         'callback' : onSubmit
//     });
// };

// const tab = new Vue({
//     el: '#tabs',
//     data: {activetab: 1},
// });

function formatState(state) {
    return state.element.title;
}

window.clone = function clone(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
};

Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('[name="csrf-token"]').getAttribute('content');

$.ajaxSetup({
    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
});

$(document).ready(() => {

    if ($('.tech-support__link').attr('data-support') >= 1) {
        $('.tech-support__link').addClass('empty')
    }

    if ($('#counter').attr('data-info') == '+0') {
        $('#counter').removeClass('my-agents__inf');
    } else {
        $('#counter').addClass('my-agents__inf');
    }

    $(".card_number").inputmask('9999 9999 9999 9999');
    $(".yandex_number").inputmask('999999999999999');
    $(".input-phone").inputmask('+7-999-999-99-99');
    $(".tell-new").inputmask('+7 (999)-999-99-99');
    $(".tell-new__settings").inputmask('+9 (999)-999-99-99');
    $('input[name="birthday"]').inputmask('99-99-9999');


    $('.kv-link').on('click', function () {
        let $this = $(this);
        $this.toggleClass('open');
        $this.next().slideToggle(300)
    });

    $("#agentRates").select2({
        placeholder: "Выберите ставки",
        multiple: true,
        templateSelection: formatState
    });
    $('.js-close__local-modal').on('click', function (e) {
        e.preventDefault();
        let $this = $(this);
        $this.closest('.local-modal__generate').remove();
    });

    $('.js-close__notifications-modal').on('click', function (e) {
        e.preventDefault();
        let $this = $(this);
        $this.closest('.modal-notifications__generate').remove();
    });
    $('.js-sidebar-btn').on('click', function () {
        let sidebar = $('.o-page__sidebar');
        let pageContent = $('.o-page__content');
        sidebar.toggleClass('open close');
        pageContent.toggleClass('open close');
        if (sidebar.hasClass('open') === true) {

        } else {


        }
    });
    jQuery('.js-hideshow').on('click', function (e) {
        let $this = $(this),
            icon = $this.find('.arrow-insurance');
        $this.parent().find('.c-card__statistics--tbody').toggle('show');
        $this.toggleClass('border-table');
        icon.toggleClass('fa-caret-up fa-caret-down');
    });

    $('.edit').on('click', function (e) {
        e.preventDefault();
        let $this = $(this),
            col = $this.closest('.col'),
            input = col.find('.settings-input'),
            icon = col.find('i.fas')
        ;
        if (input.prop('readonly') === true) {
            input.prop('readonly', false);
            icon.removeClass('fa-edit').addClass('fa-check-circle');
        } else {
            input.prop('readonly', true);
            icon.addClass('fa-edit').removeClass('fa-check-circle');
        }
    });

    $('.card-input .edit').on('click', function (e) {
        e.preventDefault();
        let $this = $(this),
            col = $this.closest('.card-input'),
            input = col.find('.settings-input'),
            icon = col.find('i.fas')
        ;
        if (input.prop('readonly') === true) {
            input.prop('readonly', false);
            icon.removeClass('fa-edit').addClass('fa-check-circle');
        } else {
            input.prop('readonly', true);
            icon.addClass('fa-edit').removeClass('fa-check-circle');
        }
    });

    $('.edit-settings').on('click', function () {
        let $this = $(this),
            icon = $this.find('i.fas'),
            field = $this.parent().find('input, select');

        field.prop('readonly', !field.prop('readonly'));
        icon.toggleClass('fa-edit fa-check-circle');
        field.focus();
    });

    $('.edit-insurance').on('click', function () {
        let $this = $(this),
            col = $this.closest('.cont-p'),
            inputs = col.find('.insurance-col .settings-input'),
            icon = $this.find('i.fas')
        ;

        inputs.prop('readonly', !inputs.prop('readonly'));
        icon.toggleClass('fa-edit fa-check-circle');
    });
    $('.edit-nasko').on('click', function () {
        let $this = $(this),
            col = $this.closest('.cont-p'),
            inputs = col.find('.nasko-col .settings-input'),
            icon = $this.find('i.fas')
        ;

        inputs.prop('readonly', !inputs.prop('readonly'));
        icon.toggleClass('fa-edit fa-check-circle');
    });
    $('.edit-sibinsurance').on('click', function () {
        let $this = $(this),
            col = $this.closest('.cont-p'),
            inputs = col.find('.sibinsurance-col .settings-input'),
            icon = $this.find('i.fas')
        ;

        inputs.prop('readonly', !inputs.prop('readonly'));
        icon.toggleClass('fa-edit fa-check-circle');
    });

    $('.js-toggle__nsub').on('click', function () {
        let $this = $(this),
            icon = $this.find('.arrow-insurance')
        ;
        $this.parent().find('.toggle-children__body').toggle('show');
        icon.toggleClass('fa-caret-down fa-caret-up');
    });
    $('.js-check').on('click', function () {
        if ($('input#radio-card').is(':checked')) {
            $('.form-card').css("display", "block");
            $('.form-yd').css("display", "none");
            $('.form-pp').css("display", "none");
            $('.bank-card__head').css("background-color", "#f16192");
            $('.bank-pp__head').css("background-color", "#ffedf3");
            $('.bank-yd__head').css("background-color", "#ffedf3");

        }
        if ($('input#radio-yd').is(':checked')) {
            $('.form-card').css("display", "none");
            $('.form-yd').css("display", "block");
            $('.form-pp').css("display", "none");
            $('.bank-yd__head').css("background-color", "#f16192");
            $('.bank-pp__head').css("background-color", "#ffedf3");
            $('.bank-card__head').css("background-color", "#ffedf3");
        }
        if ($('input#radio-pp').is(':checked')) {
            $('.form-card').css("display", "none");
            $('.form-yd').css("display", "none");
            $('.form-pp').css("display", "block");
            $('.bank-pp__head').css("background-color", "#f16192");
            $('.bank-yd__head').css("background-color", "#ffedf3");
            $('.bank-card__head').css("background-color", "#ffedf3");
        }

    });

    $('form.ajax-setting-form .edit-settings').on('click', 'i.fa-check-circle', function () {
        let field = $(this).parent().parent().find('input, select'),
            name = field.attr('name'),
            value = field.val();

        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: $('form.ajax-setting-form').data('url'),
            data: {
                name: name,
                value: value,
            },
            success: function (data) {

            }
        });
    });

    $('#leftMSlideMenu').on('click', function () {
        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: $(this).data('url'),
            success: function (data) {
            }
        });
    });

    $('form.ajax-setting-form .ajax-select-field').on('change', function () {
        let field = $(this).parent().find('select'),
            name = field.attr('name'),
            value = field.val();

        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: $('form.ajax-setting-form').data('url'),
            data: {
                name: name,
                value: value,
            },
            success: function (data) {

            }
        });
    });

    $('form.agent-update-form .ajax-edit-field').on('click', 'i.fa-edit', function () {
        let field = $(this).parent().parent().find('input, select'),
            name = field.attr('name'),
            value = field.val();

        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: $('form.agent-update-form').data('url'),
            data: {
                name: name,
                value: value,
            },
        });
    });

    $('#is_opportunity_sub_agent').on('change', function () {
        let $this = $(this);

        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: $this.data('route'),
            data: {
                value: $this.prop('checked') ? 1 : 0,
            },
        });
    });

    $('form.ajax-setting-form .ajax-edit-field').on('click', function () {
        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: $('form.ajax-setting-form').data('url'),
            data: {
                name: $(this).attr('name'),
                value: $(this).prop('checked') ? 1 : 0
            },
        });
    });

    $('form.ajax-setting-form input.ajax-edit-blocks').on('click', function () {

        let blocks = {};
        $('.ajax-blocks .c-choice--checkbox input').each(function () {
            blocks[$(this).val()] = $(this).prop('checked') ? 1 : 0;
        });

        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: $('form.ajax-setting-form').data('url'),
            data: {name: 'blocks', value: blocks},
        });
    });

    const updateRate = (data, parentBlock) => {
        let route = $('.edit-sub').data('route_rate');

        if (typeof route !== "undefined") {
            $.ajax({
                dataType: 'json',
                type: 'POST',
                url: route,
                data: data,
                success: function (res) {
                    parentBlock.find('.ajax-reate-target').data('commission_id', res.commission);
                }
            });
        }
    };

    $('.ajax-reate-target.rate-you').on('change', function () {
        let $this = $(this),
            parentBlock = $this.closest('.children-body'),
            id = $this.data('commission_id'),
            youRate = Number($this.val()),
            allRate = Number(parentBlock.find('.rate-all').val()),
            agentRate = parentBlock.find('.rate-agent')
        ;

        if (youRate > allRate || youRate < 0) {
            $this.val(allRate.toFixed(2));
            agentRate.val(0);
        } else {
            agentRate.val((allRate - youRate).toFixed(2));
            $this.val(youRate.toFixed(2));
        }

        updateRate({
            id: id,
            you: $this.val(),
            agent: agentRate.val(),
            rate_id: parentBlock.data('id'),
            rate_type: parentBlock.data('rate_type'),
            is_commission: parentBlock.data('is_commission'),
        }, parentBlock);
    });

    $('.ajax-reate-target.rate-agent').on('change', function () {
        let $this = $(this),
            parentBlock = $this.closest('.children-body'),
            id = $this.data('commission_id'),
            agentRate = Number($this.val()),
            allRate = Number(parentBlock.find('.rate-all').val()),
            youRate = parentBlock.find('.rate-you')
        ;

        if (agentRate > allRate || agentRate < 0) {
            $this.val(allRate.toFixed(2));
            youRate.val(0.00);
        } else {
            youRate.val((allRate - agentRate).toFixed(2));
            $this.val(agentRate.toFixed(2));
        }

        updateRate({
            id: id,
            you: youRate.val(),
            agent: $this.val(),
            rate_id: parentBlock.data('id'),
            rate_type: parentBlock.data('rate_type'),
            is_commission: parentBlock.data('is_commission'),
        }, parentBlock);
    });

    jQuery('.custom-modal-js, .md-o').on('click', function () {
        let modal = $(this).data('modal'),
            modalEl = jQuery(`#${modal}`)
        ;
        if ($(this)) {
            $('.modal-js').removeClass('open');
        }
        modalEl.toggleClass('open');
        $('.bg-owerlay').addClass('openBg');
        $('body').css("overflow", "hidden")
    });

    $('.bg-owerlay, .modremove colal-accept__code--close, .js-close__local-modal').on('click', function () {
        $('.modal-js.open').removeClass('open');
        $('.bg-owerlay').removeClass('openBg');
        $('body').css("overflow", "")
    });

    // For modal page POLICY, modal remove col //
        /*jQuery('.js-modal__check').on('click', function () {
            let modal = $(this).data('modal'),
                modalEl = jQuery(`#${modal}`)
            ;
            if ($(this)) {
                $('.modal-js__trans').removeClass('open');
            }
            modalEl.toggleClass('open');
            $('.bg-owerlay2').addClass('openBg');
        });
        $('.bg-owerlay2, .btn-close__modal').on('click', function () {
            $('.modal-js__trans.open').removeClass('open');
            $('.bg-owerlay2').removeClass('openBg');
        });*/
    // For modal page POLICY, modal remove col //

    $('input[type="file"]').on('change', function () {
        $('form.ajax-setting-form').submit();
    });

    $('.selectRecount').on('change', function () {
        let insurance = $(this).val();
        let id = $(this).attr('data-policyId');
        preloader();
        if (insurance.length > 0) {
            $.ajax({
                dataType: 'json',
                type: 'GET',
                url: `/policies/re-calc/${id}/${insurance}`,
                success: function (data) {
                    preloader();
                    window.location = data.link;
                }
            });
        }
    });

    $('.ajax-send-code').on('click', function () {
        let form = $('#modal-accept');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'json',
            type: 'POST',
            url: form.data('url'),
            beforeSend: function (xhr) {
                $('.accept-code').hide();
                $('.beforeSend').show();
            },
            success: function (data) {
                if (data.is_success == true && data.is_verified == true) {
                    return window.location = data.url;
                }

                if (data.is_success == true) {
                    $('.beforeSend').hide();
                    $('.paste-code').show();
                } else {
                    $('.beforeSend').hide();
                    $('.accept-code').show();
                }
            }
        });
    });

    $('.ajax-apply-code, .ajax-pay-code').on('click', function () {
        let form = $('#modal-accept'),
            code = form.find('.paste-code input').val();

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'json',
            data: {
                // is_pay: $(this).data('pay'),
                is_apply: true,
                code: code,
            },
            type: 'POST',
            url: form.data('url'),
            beforeSend: function (xhr) {
                $('.paste-code').hide();
                $('.beforeSend').show();
            },
            success: function (data) {
                if (data.is_success == true && data.is_verified == true && data.is_confirm == true) {
                    return window.location = data.url;
                }

                $('.beforeSend').hide();
                $('.paste-code').show();
            },
            error: function (error) {
                $('.beforeSend').hide();
                $('.paste-code').show();

                $('#success_message').html(error.responseJSON.errors.code[0]);
            }
        });
    });

    let address = $("#address");

    if (address.length) {
        address.suggestions({
            token: "20131cc64a9d46aa2ea1d9437478bfcb1c1c66dc",
            type: "ADDRESS",
            scrollOnFocus: false,
            triggerSelectOnBlur: false,
            triggerSelectOnEnter: false,
            addon: 'none',
            onSelect: function (suggestion) {
                $('input[name="address"]').val(suggestion.value);
            }
        });
    }


    let resetPass = $('#modal-pass-recovery .form-type');
    resetPass.on('change', function () {
        if (Number($(this).val()) === 1) {
            $('#reset-pass__email').hide();
            $('#reset-pass__phone').show();
        } else {
            $('#reset-pass__email').show();
            $('#reset-pass__phone').hide();
        }
    });

    var sendResetMessage = false;

    $('.btn-recovery__pass').click(function (e) {
        let phone = $('#modal-pass-recovery input[name="phone"]');
        let email = $('#modal-pass-recovery input[name="email"]');
        let fieldError = $('#modal-pass-recovery .field-error').empty();
        let emailPattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
        let isHaveError = false;
        let modalRec = $('#modal-pass-recovery');

        if (Number($('#modal-pass-recovery .form-type:checked').val()) === 1) {
            if (phone.val().trim().length < 1) {
                console.log(phone.val());
                phone.addClass("error");
                fieldError.addClass('have-error');
                isHaveError = true;
                let oldError = fieldError.html();
                fieldError.html(oldError + 'Укажите телефон <br>');
            }
        } else {
            if (email.val().trim().length === 0) {
                email.addClass("error");
                fieldError.addClass('have-error');
                isHaveError = true;
                let oldError = fieldError.html();
                fieldError.html(oldError + 'Укажите почту <br>');
            }
            if (!emailPattern.test(email.val())) {
                email.addClass("error");
                fieldError.addClass('have-error');
                isHaveError = true;
            }
        }


        if (isHaveError === true) {
            return false;
        }

        if (sendResetMessage) return;

        $.ajax({
            type: "POST",
            url: "/resetPassword/send",
            data: {
                phone: phone.val(),
                email: email.val(),
                "g-recaptcha-response": $("#g-recaptcha-response").val(),
                fileType: $('#modal-pass-recovery .form-type:checked').val(),
            },
            success: function (response) {
                if (response.status == false) {
                    fieldError.addClass('have-error');
                    fieldError.html(response.error);
                } else {
                    fieldError.addClass('have-error');
                    fieldError.html("Доступы на восстановления пароля Вам отправленны");
                }
            }
        });

        sendResetMessage = true;

    });

    let registerModal = $('#modal-register .form-type');
    registerModal.on('change', function () {
        if (Number($(this).val()) === 1) {
            $('#form-reg-email').hide();
            $('#form-reg-phone').show();
        } else {
            $('#form-reg-email').show();
            $('#form-reg-phone').hide();
        }
    });

    $('.form-type').on('change', function () {
        $('.field-error').empty();
    });


    let appField = $('#modal-sign .form-type');
    appField.on('change', function () {
        if (Number($(this).val()) === 1) {
            $('#form-auth-email').hide();
            $('#form-auth-phone').show();
        } else {
            $('#form-auth-email').show();
            $('#form-auth-phone').hide();
        }
    });

    $(".btn-send").click(function () {
        let name = $('input[name="name"]');
        let phone = $('input[name="phone"]');
        let message = $("textarea#message");
        let isHaveError = false;


        if (name.val().trim().length === 0) {
            name.addClass("error");
            isHaveError = true;
        }

        if (phone.val().trim().length === 0) {
            phone.addClass("error");
            isHaveError = true;
        }

        if (message.val().trim().length === 0) {
            message.addClass("error");
            isHaveError = true;
        }

        if (isHaveError === true) {
            return false;
        }

        $("#formSend")
            .first()
            .submit();
    });

    var loaded = false;

    $('.phone-success').click(function () {
        let passwordMess = $('#modal-register input[name="password"]');
        let fieldError = $('#modal-register .field-error').empty();
        let phoneVal = $('#modal-register input[name="phone"]');
        let phone = $('.get-phone-success').val();
        let inputMessAcept = $('.popup-new__input--accept');
        let isHaveError = false;

        if (phoneVal.val().trim().length === 0) {
            phoneVal.addClass("error");
            fieldError.addClass('have-error');
            let oldError = fieldError.html();
            fieldError.html(oldError + 'Укажите телефон <br>');
            inputMessAcept.removeClass('open');
            inputMessAcept.addClass('close');
            isHaveError = true;

        } else {
            inputMessAcept.addClass('open');
            inputMessAcept.removeClass('close');
            phoneVal.removeClass("error");
        }

        if (loaded) return;

        $('.btn-register').click(function () {
            if (passwordMess.val().trim().length === 0) {
                passwordMess.addClass("error");
                fieldError.addClass('have-error');
                let oldError = fieldError.html();
                isHaveError = true;
                fieldError.html(oldError + 'Укажите пароль из сообщения <br>');
                return false;
            }
        });

        if (isHaveError === true) {
            return false;
        }
        $.ajax({
            type: "GET",
            url: "/sendPassword",
            data: {
                phone: phone
            },
            success: function (response) {
                if(response.error !== false) {
                    fieldError.addClass('have-error');

                    fieldError.html(response.error);
                }
            }
        });

        loaded = true;
    });

    $('.btn-login').click(function () {
        let phone = $('#modal-sign input[name="phone"]');
        let email = $('#modal-sign input[name="email"]');
        let fieldError = $('#modal-sign .field-error').empty();
        let password = $('#modal-sign input[name="password"]');
        let password2 = $('#modal-sign input[name="password2"]');
        let emailPattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z1]{2,4}$/i;
        let isHaveError = false;

        if (Number($('#modal-sign .form-type:checked').val()) === 1) {
            if (phone.val().trim().length === 0) {
                phone.addClass("error");
                fieldError.addClass('have-error');
                isHaveError = true;
                let oldError = fieldError.html();
                fieldError.html(oldError + 'Укажите телефон <br>');
            }
            if (password.val().trim().length === 0) {
                password.addClass("error");
                fieldError.addClass('have-error');
                isHaveError = true;
                let oldError = fieldError.html();
                fieldError.html(oldError + 'Укажите пароль <br>');
            }
        } else {
            if (email.val().trim().length === 0) {
                email.addClass("error");
                fieldError.addClass('have-error');
                isHaveError = true;
                let oldError = fieldError.html();
                fieldError.html(oldError + 'Укажите почту <br>');
            }
            if (!emailPattern.test(email.val())) {
                email.addClass("error");
                fieldError.addClass('have-error');
                isHaveError = true;
            }
            if (password2.val().trim().length === 0) {
                password2.addClass("error");
                fieldError.addClass('have-error');
                isHaveError = true;
                let oldError = fieldError.html();
                fieldError.html(oldError + 'Укажите пароль <br>');
            }
        }

        if (isHaveError === true) {
            return false;
        }

        $.ajax({
            type: "POST",
            url: "/",
            data: {
                phone: phone.val(),
                email: email.val(),
                password: password.val(),
                password2: password2.val(),
                fileType: $('#modal-sign .form-type:checked').val()
            },
            success: function (response) {
                if (response.status == false) {
                    fieldError.addClass('have-error');
                    fieldError.html(response.error);
                } else {
                    fieldError.removeClass('have-error');
                    location.href = response.path;
                }
            }
        });
    })


    $('.reset-password-button').click(function (event) {
        event.preventDefault();
        let fieldError = $('#modal-pass-recovery .field-error').empty();
        let reset_password = $('#reset_password').val();
        let reset_password_confirmation = $('#reset_password_confirmation').val();
        let token = $('#reset_token').val();
        let isHaveError = false;

        if (reset_password.trim().length === 0) {
            fieldError.addClass('have-error');
            isHaveError = true;
            fieldError.html('Укажите пароль <br>');
        }

        if(reset_password_confirmation.trim().length === 0) {
            fieldError.addClass('have-error');
            isHaveError = true;
            let oldError = fieldError.html();
            fieldError.html(oldError + 'Укажите пароль еще раз <br>');
        }

        if(reset_password.trim()!== reset_password_confirmation.trim()) {
            fieldError.addClass('have-error');
            isHaveError = true;
            let oldError = fieldError.html();
            fieldError.html(oldError + 'Пароли должны совпадать <br>');
        }

        if (isHaveError === true) {
            return false;
        } else {
            fieldError.removeClass('have-error');
            $.ajax({
                type: "POST",
                url: "/resetPassword",
                data: {
                    reset_password: reset_password,
                    reset_password_confirmation: reset_password_confirmation,
                    token: token
                },
                success: function (response) {
                    if (response.status === true) {
                        location.href = response.path;
                    } else {
                        fieldError.html(response.error);
                    }
                }
            });
        }
    })

    var feedbackLoaded = false;

    // $('.feedback').click(function (event) {
    //     event.preventDefault();
    //     let fio = $('.fio').val();
    //     let feedbackPhone = $('.feedback-phone').val();
    //     let feedbackText = $('.feedback-text').val();

    //     if(feedbackLoaded) return;

    //     feedbackLoaded = true;

    //     $.ajax({
    //         type: "POST",
    //         url: "/api/home/question",
    //         data: {
    //             name: fio,
    //             phone: feedbackPhone,
    //             message: feedbackText,
    //             gRecaptchaResponse: grecaptcha.getResponse()
    //         },
    //         success: function (response) {
    //             alert('Сообщение отправлено');
    //             document.location.reload(true);
    //         }
    //     });

    //     return false;
    // });




    //
    // function clearVal(val) {
    //     let newVal =val.replace(/[^0-9]/g, '');
    //     if (newVal === '') {
    //         return false;
    //     } else {
    //         return newVal;
    //     }
    //
    // }
    //
    // function getResString(newVal) {
    //     let res = '';
    //     for (let i = 0; i < newVal.length; i++) {
    //         console.log(i)
    //         if ( i === 1 ) {
    //             res += '-';
    //             res += newVal .charAt(i)
    //         } else if ( i === 4 ){
    //             res += '-';
    //             res += newVal .charAt(i)
    //         }
    //         else {
    //             res += newVal .charAt(i)
    //         }
    //     }
    //     return res;
    // }
    //
    // gosNumberField.on('input', function() {
    //     let val = $(this).val();
    //     if (val === '') return;
    //
    //     let newVal = clearVal(val);
    //     if (!newVal) {
    //         $(this).val('');
    //         return;
    //     }
    //     let res = getResString(newVal);
    //     console.log(res)
    // });

    //
    // gosNumberField.on('input', function(e) {
    //
    //     let valFull = $(this).val();
    //     let letters=' +-()1234567890';
    //     let lastSymbol = valFull.substr(-1);
    //     let lengthOfVal = valFull.length;
    //     console.log(lengthOfVal);
    //     console.log(valFull);
    //     console.log(lastSymbol);
    //     if (e.keyCode === 8 || e.keyCode === 46) {}
    //     else if (lengthOfVal === 2 && checkValidNumbers(lastSymbol) === true ) {
    //
    //
    //         return (letters.indexOf(String.fromCharCode(e.which)) !== -1);
    //
    //     }
    //
    // });


    const checkValidSymbols = symbol => {
        for (let validSymbol of ['A', 'А', 'B', 'В', 'E', 'Е', 'K', 'К', 'M', 'М', 'H', 'Н', 'O', 'О', 'P', 'Р', 'C', 'С', 'T', 'Т', 'Y', 'У', 'X', 'Х']) {
            if (validSymbol === symbol.toUpperCase()) {

                return true;
            }

        }

        return false;
    };

    const checkValidNumbers = symbol => {
        for (let validSymbol of ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']) {
            if (validSymbol === symbol) {
                return true;
            }
        }

        return false;
    };

    // gosNumberField.on('input', function(e) {
    //
    //     let valFull = $(this).val();
    //     let letters=' +-()1234567890';
    //     let lastSymbol = valFull.substr(-1);
    //     let lengthOfVal = valFull.length;
    //     console.log(lengthOfVal);
    //     console.log(valFull);
    //     console.log(lastSymbol);
    //     if (e.keyCode === 8 || e.keyCode === 46) {}
    //     else if (lengthOfVal === 2 && checkValidNumbers(lastSymbol) === true ) {
    //
    //
    //         return (letters.indexOf(String.fromCharCode(e.which)) !== -1);
    //
    //             // console.log(valFull);
    //
    //     }
    //
    // });

    const checkValidNumbersRegion = symbol => {
        for (let validSymbol of ['1', '2', '3', '4', '5', '6', '7', '8', '9']) {
            if (validSymbol === symbol) {
                return true;
            }
        }

        return false;
    };

    const checkLatinSymbol = symbol => {
        for (let validSymbol of ['1', '2', '3', '4', '5', '6', '7', '8', '9', 'W', 'E', 'R', 'T', 'Y', 'U', 'P', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Z', 'X', 'C', 'V', 'B', 'N', 'M',]) {
            if (validSymbol === symbol.toUpperCase()) {
                return true;
            }
        }

        return false;
    };

    const checkLatinSymbolAll = symbol => {
        for (let validSymbol of ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'W', 'E', 'R', 'T', 'Y', 'U', 'P', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Z', 'X', 'C', 'V', 'B', 'N', 'M',]) {
            if (validSymbol === symbol.toUpperCase()) {
                return true;
            }
        }

        return false;
    };

    const checkLatinSymbolDefMask = symbol => {
        for (let validSymbol of ['-', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'W', 'E', 'R', 'T', 'Y', 'U', 'P', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Z', 'X', 'C', 'V', 'B', 'N', 'M', 'I', 'Q', 'O']) {
            if (validSymbol === symbol.toUpperCase()) {
                return true;
            }
        }

        return false;
    };

    const checkLatinSymbolAllDefMask = symbol => {
        for (let validSymbol of ['-', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'W', 'E', 'R', 'T', 'Y', 'U', 'P', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Z', 'X', 'C', 'V', 'B', 'N', 'M', 'I', 'Q', 'O']) {
            if (validSymbol === symbol.toUpperCase()) {
                return true;
            }
        }

        return false;
    };

    const checkLatinSymbolDef = symbol => {
        for (let validSymbol of ['-', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'W', 'E', 'R', 'T', 'Y', 'U', 'P', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Z', 'X', 'C', 'V', 'B', 'N', 'M']) {
            if (validSymbol === symbol.toUpperCase()) {
                return true;
            }
        }

        return false;
    };

    const checkLatinSymbolAllDef = symbol => {
        for (let validSymbol of ['-', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'W', 'E', 'R', 'T', 'Y', 'U', 'P', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Z', 'X', 'C', 'V', 'B', 'N', 'M',]) {
            if (validSymbol === symbol.toUpperCase()) {
                return true;
            }
        }

        return false;
    };

    const checkEmailValidate = symbol => {
        for (let validSymbol of ['.', '_', '@', '-', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'Q', 'q', 'W', 'w', 'E', 'e', 'R', 'r', 'T', 't', 'Y', 'y', 'U', 'u', 'I', 'i', 'O', 'o', 'P', 'p', 'A', 'a', 'S', 's', 'D', 'd', 'F', 'f', 'G', 'g', 'H', 'h', 'J', 'j', 'K', 'k', 'L', 'l', 'Z', 'z', 'X', 'x', 'C', 'c', 'V', 'v', 'B', 'b', 'N', 'n', 'M', 'm']) {
            if (validSymbol === symbol) {
                return true;
            }
        }

        return false;
    };

    const checkRusSymbolAll = symbol => {
        for (let validSymbol of ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'Й', 'Ц', 'У', 'К', 'Е', 'Н', 'Г', 'Ш', 'Щ', 'З', 'Х', 'Ъ', 'Ф', 'Ы', 'В', 'А', 'П', 'Р', 'О', 'Л', 'Д', 'Ж', 'Э', 'Я', 'Ч', 'С', 'М', 'И', 'Т', 'Ь', 'Б', 'Ю']) {
            if (validSymbol === symbol.toUpperCase()) {
                return true;
            }
        }

        return false;
    };

    const checkRusSymbol = symbol => {
        for (let validSymbol of ['Й', 'й', 'Ц', 'ц', 'У', 'у', 'К', 'к', 'Е', 'е', 'Н', 'н', 'Г', 'г', 'Ш', 'ш', 'Щ', 'щ', 'З', 'з', 'Х', 'х', 'Ъ', 'ъ', 'Ф', 'ф', 'Ы', 'ы', 'В', 'в', 'А', 'а', 'П', 'п', 'Р', 'р', 'О', 'о', 'Л', 'л', 'Д', 'д', 'Ж', 'ж', 'Э', 'э', 'Я', 'я', 'Ч', 'ч', 'С', 'с', 'М', 'м', 'И', 'и', 'Т', 'т', 'Ь', 'ь', 'Б', 'б', 'Ю', 'ю',]) {
            if (validSymbol === symbol) {
                return true;
            }
        }

        return false;
    };

    const whoTakeSumbol = symbol => {
        for (let validSymbol of [' ', '.', ',', 'Й', 'й', 'Ц', 'ц', 'У', 'у', 'К', 'к', 'Е', 'е', 'у', 'Н', 'н', 'Г', 'г', 'Ш', 'ш', 'Щ', 'щ', 'З', 'з', 'Х', 'х', 'Ъ', 'ъ', 'Ф', 'ф', 'Ы', 'ы', 'В', 'в', 'А', 'а', 'П', 'п', 'Р', 'р', 'О', 'о', 'Л', 'л', 'Д', 'д', 'Ж', 'ж', 'Э', 'э', 'Я', 'я', 'Ч', 'ч', 'С', 'с', 'М', 'м', 'И', 'и', 'Т', 'т', 'Ь', 'ь', 'Б', 'б', 'Ю', 'ю',]) {
            if (validSymbol === symbol.toUpperCase()) {
                return true;
            }
        }

        return false;
    };

    //number//
    let gosNumberField = $('.gosNumberField');

    gosNumberField.on('keyup change', function () {
        let valFull = $(this).val();
        let lastSymbol = valFull.substr(-1);
        let lengthOfVal = valFull.length;
        console.log(lengthOfVal);
        if (lengthOfVal === 1 && checkValidSymbols(lastSymbol) === true) {
            return true;
        } else if (lengthOfVal > 1 && lengthOfVal < 5 && checkValidNumbers(lastSymbol) === true) {

            return true;
        } else if (lengthOfVal > 4 && lengthOfVal < 7 && checkValidSymbols(lastSymbol) === true) {
            return true;
        }

        return $(this).val(valFull.substr(0, valFull.length - 1));

    });

    //region//
    let regionNumberField = $('.regionNumberField');

    regionNumberField.on('keyup change', function () {
        let valFull = $(this).val();
        let lastSymbol = valFull.substr(-1);
        let lengthOfVal = valFull.length;
        if (lengthOfVal === 1 && checkValidNumbersRegion(lastSymbol) === true) {
            return true;
        } else if (lengthOfVal > 1 && lengthOfVal < 4 && checkValidNumbers(lastSymbol) === true) {
            return true;
        }
        return $(this).val(valFull.substr(0, valFull.length - 1));
    });


    //vin number//
    let VinValidField = $('.VinValidField');

    VinValidField.on('keyup change', function () {
        let valFull = $(this).val();
        let lastSymbol = valFull.substr(-1);
        let lengthOfVal = valFull.length;
        if (lengthOfVal === 1 && checkLatinSymbol(lastSymbol) === true) {
            return true;
        } else if (lengthOfVal > 1 && lengthOfVal < 18 && checkLatinSymbolAll(lastSymbol) === true) {
            return true;
        }
        return $(this).val(valFull.substr(0, valFull.length - 1));
    });


    //car number
    let CarValidField = $('.CarValidField');

    CarValidField.on('keyup change', function () {
        let valFull = $(this).val();
        let lastSymbol = valFull.substr(-1);
        let lengthOfVal = valFull.length;
        if (lengthOfVal === 1 && checkLatinSymbolDefMask(lastSymbol) === true) {
            return true;
        } else if (lengthOfVal > 1 && lengthOfVal < 18 && checkLatinSymbolAllDefMask(lastSymbol) === true) {
            return true;
        }
        return $(this).val(valFull.substr(0, valFull.length - 1));
    });

    //chassis number
    let ChassisValidField = $('.ChassisValidField');

    ChassisValidField.on('keyup change', function () {
        let valFull = $(this).val();
        let lastSymbol = valFull.substr(-1);
        let lengthOfVal = valFull.length;
        if (lengthOfVal === 1 && checkLatinSymbolDef(lastSymbol) === true) {
            return true;
        } else if (lengthOfVal > 1 && lengthOfVal < 18 && checkLatinSymbolAllDef(lastSymbol) === true) {
            return true;
        }
        return $(this).val(valFull.substr(0, valFull.length - 1));
    });
    //chassis number


    //sts//
    let vehicleDocumentTypesSerial = $('.vehicleDocumentTypesSerial');

    vehicleDocumentTypesSerial.on('keyup keypress keydown', function () {
        let valFull = $(this).val();
        let lastSymbol = valFull.substr(-1);
        let lengthOfVal = valFull.length;
        console.log(lengthOfVal);
        if (lengthOfVal === 1 && checkValidNumbersRegion(lastSymbol) === true) {
            return true;
            console.log('1 true');
        }
        else if (lengthOfVal > 1 && lengthOfVal <= 2 && checkValidNumbers(lastSymbol) === true) {
            return true;
            console.log('2 true');
        }
        else if (lengthOfVal >= 3 && lengthOfVal <= 4 && checkRusSymbolAll(lastSymbol) === true) {
            return true;
            console.log('3 true');
        }

        return $(this).val(valFull.substr(0, valFull.length - 1));
    });
    //sts//

    //latinDrivePass//
    let latinDrivePass = $('.latinDrivePass');

    latinDrivePass.on('keyup keypress keydown', function () {
        let valFull = $(this).val();
        let lastSymbol = valFull.substr(-1);
        let lengthOfVal = valFull.length;
        console.log(lengthOfVal);
        if (lengthOfVal >= 1 && lengthOfVal <= 2 && checkValidNumbersRegion(lastSymbol) === true) {
            return true;
        }
        else if (lengthOfVal >= 3 && lengthOfVal <= 4 && checkLatinSymbolAll(lastSymbol) === true) {
            return true;
        }

        return $(this).val(valFull.substr(0, valFull.length - 1));
    });
    //latinDrivePass//

    //latinDriveNumber//
    let latinDriveNumber = $('.latinDriveNumber');

    latinDrivePass.on('keyup keypress keydown', function () {
        let valFull = $(this).val();
        let lastSymbol = valFull.substr(-1);
        let lengthOfVal = valFull.length;
        console.log(lengthOfVal);
        if (lengthOfVal >= 1 && checkLatinSymbolAll(lastSymbol) === true) {
            return true;
        }

        return $(this).val(valFull.substr(0, valFull.length - 1));
    });
    //latinDriveNumber//

    //DriversRusName//
    let driversRusName = $('.driversRusName');

    driversRusName.on('keyup keypress keydown', function () {
        let valFull = $(this).val();
        let lastSymbol = valFull.substr(-1);
        let lengthOfVal = valFull.length;
        console.log(lengthOfVal);
        if (lengthOfVal >= 1 && checkRusSymbol(lastSymbol) === true) {
            return true;
        }


        return $(this).val(valFull.substr(0, valFull.length - 1));
    });
    //DriversRusName//

    //whoTake//
    let whoTake = $('.whoTake');

    whoTake.on('keyup keypress keydown', function () {
        let valFull = $(this).val();
        let lastSymbol = valFull.substr(-1);
        let lengthOfVal = valFull.length;
        console.log(lengthOfVal);
        if (lengthOfVal >= 1 && whoTakeSumbol(lastSymbol) === true) {
            return true;
        }
        else if (lengthOfVal > 2 && whoTakeSumbol(lastSymbol) === true) {
            return true;
        }

        return $(this).val(valFull.substr(0, valFull.length - 1));

        function capitalize(s) {
            return s[0].toUpperCase() + s.slice(1);
        }
    });
    //whoTake//

    //validateEmail//
    let validateEmail = $('.validateEmail');
    let emailPattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
    validateEmail.on('keyup keypress keydown', function () {
        let valFull = $(this).val();
        let lastSymbol = valFull.substr(-1);
        let lengthOfVal = valFull.length;
        if (lengthOfVal > 0 && checkEmailValidate(lastSymbol) === true) {
            return true;
        }


        return $(this).val(valFull.substr(0, valFull.length - 1));
    });

    //validateEmail//

// For modal page POLICY, modal remove col //
    jQuery('.js-modal__check').on('click', function () {
        let modal = $(this).data('modal'),
            modalEl = jQuery(`#${modal}`)
        ;
        if ($(this)) {
            $('.modal-js__trans').removeClass('open');
        }
        modalEl.toggleClass('open');
        $('.bg-owerlay2').addClass('openBg');
    });
    $('.bg-owerlay2').on('click', function () {
        $('.modal-js__trans.open').removeClass('open');
        $('.bg-owerlay2').removeClass('openBg');
    });
// For modal page POLICY, modal remove col //

    $(function() {
        $('button').click(function() {
            let inputCopy = $('.input-copy')
            inputCopy.focus().select();
            document.execCommand('copy');
        });
    });
});



