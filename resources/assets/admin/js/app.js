import $ from "jquery";

window._ = require('lodash');
window.Popper = require('popper.js').default;

try {
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
} catch (e) {}

import Vue from 'vue';


import 'select2';
import 'select2/dist/css/select2.css';
import 'vue-directive-tooltip/css/index.css';
// import 'vue2-slot-calendar/lib/calendar.min.css'


import VueResource from 'vue-resource';
import SuiVue from 'semantic-ui-vue';
import vSelect from 'vue-select';
import Tooltip from 'vue-directive-tooltip';
// import calendar from 'vue2-slot-calendar';
import VueMask from 'v-mask'

window.preloader = function () {
    let app = $('#app');
    let preloaderEl = $('#preloader');

    if (app.hasClass('preloader-body')) {
        app.removeClass('preloader-body');
        preloaderEl.addClass('hidden');
    } else {
        app.addClass('preloader-body');
        preloaderEl.removeClass('hidden');
    }
};

window.preloaderOff = () => {
    $('#app').removeClass('preloader-body');
    $('#preloader').addClass('hidden');
};

import HelloTest from './components/hello-test';
import Hello from './components/hello';
import Agents from './components/Agents/agents';
import agentsCreate from './components/Agents/agentsCreate';
import agentsUpdate from './components/Agents/agentsUpdate';
import itemSubagents from './components/Agents/itemSubagents';
import widthraws from './components/Withdraws/withdraws';
import widthrawsUpdate from './components/Withdraws/widthraws-update';
import bets from './components/bets/bets';
import betsCreate from './components/bets/bets-create';
import betsUpdate from './components/bets/bets-update';
import newsCreate from './components/news/news-create';
import news from './components/news/news';
import curatorsCreate from './components/curators/curators-create';
import curatorsEdit from './components/curators/CuratorsEdit';
import curators from './components/curators/curators';
import stockCreate from './components/stock/stock-create';
import stockUpdate from './components/stock/stock-update';
import stock from './components/stock/stock';
import administratorsCreate from './components/administrators/administrators-create';
import administratorsUpdate from './components/administrators/administrators-update';
import administrators from './components/administrators/administrators';
import TechSupport from './../../vue/components/Support/TechSupport.vue';
import ChatSupport from './../../vue/components/Support/ChatSupport.vue';

window.axios = require('axios');

Vue.use(VueResource);
Vue.use(SuiVue);
Vue.use(Tooltip);
Vue.use(VueMask);
Vue.component('pagination', require('laravel-vue-pagination'));

const app = new Vue({
    el: '#app',
    components:{
        vSelect,
        Hello,
        HelloTest,
        Agents,
        VueMask,
        agentsCreate,
        agentsUpdate,
        itemSubagents,
        Tooltip,
        widthraws,
        widthrawsUpdate,
        bets,
        betsCreate,
        betsUpdate,
        newsCreate,
        news,
        curatorsCreate,
        curatorsEdit,
        curators,
        stockCreate,
        stockUpdate,
        stock,
        administratorsCreate,
        administratorsUpdate,
        administrators,
        TechSupport,
        ChatSupport
    },

});



$(document).ready( function () {
   $('.js-sidebar-btn').on('click', function () {
       let $this = $(this);
       $('body').toggleClass('a-sidebar__close')
   })
});

jQuery('.custom-modal-js, .md-o').on('click',function () {
    let modal = $(this).data('modal'),
        modalEl = jQuery(`#${modal}`)
    ;


    if ($(this)) {
        $('.modal-js').removeClass('open');
    }
    modalEl.toggleClass('open');
    $('.bg-owerlay').addClass('openBg');
});
$('.bg-owerlay, .modal-accept__code--close, .js-close__local-modal').on('click',function () {
    $('.modal-js.open').removeClass('open');
    $('.bg-owerlay').removeClass('openBg');
});


if ($('#admin-support').attr('support-message') >= 1) {
    $('#admin-support').addClass('message')
}