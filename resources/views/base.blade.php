<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Юагент - страховой провайдер</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="title" content="Юагент - страховой провайдер">
    <meta name="description" content="Единая платформа для страховых брокеров и агентов.">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">

    <script src="{{ asset('/js/feather.min.js') }}"></script>

    <link rel="stylesheet" href="/node_modules/flatpickr/dist/flatpickr.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css" integrity="sha384-v2Tw72dyUXeU3y4aM2Y0tBJQkGfplr39mxZqlTBDUZAb9BGoC40+rdFCG0m10lXk" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/fontawesome.css" integrity="sha384-q3jl8XQu1OpdLgGFvNRnPdj5VIlCvgsDQTQB6owSOHWlAurxul7f+JpUOVdAiJ5P" crossorigin="anonymous">

    <!-- Favicon -->
    {{--<link rel="apple-touch-icon" href="/apple-touch-icon.png">--}}
    <link rel="shortcut icon" href="{{ asset('/fav48х48.png') }}" type="image/x-icon">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="{{ asset('/css/neat.css') }}">
    <link rel="stylesheet" href="/node_modules/semantic-ui-dropdown/dropdown.min.css">
    <link rel="stylesheet" href="/node_modules/semantic-ui-transition/transition.min.css">
    <link rel="stylesheet" href="/node_modules/semantic-ui-calendar/dist/calendar.min.css">

    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="/css/build.css">

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBbVnID_OxBaP6KdyutgkMokiBSTypnhY8&libraries=places"></script>

    @yield('head')
</head>
<body>
    <div class="bg-owerlay"></div>
    <div class="bg-owerlay2"></div>
    <div class="hidden" id="preloader">
        <div class="sk-wave">
            <div class="sk-rect sk-rect1"></div>
            <div class="sk-rect sk-rect2"></div>
            <div class="sk-rect sk-rect3"></div>
            <div class="sk-rect sk-rect4"></div>
            <div class="sk-rect sk-rect5"></div>
        </div>
    </div>
    <div id="app" class=" @if (\Request::url() == route('home.index')) scroll-home @endif ">
        @yield('body')
    </div>

    <script src="{{ asset('node_modules/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('js/app.js?v='.time()) }}"></script>
    <script src="{{ asset('node_modules/inputmask/dist/jquery.inputmask.bundle.js') }}"></script>
    <script src="{{ asset('/node_modules/semantic-ui-dropdown/dropdown.min.js') }}"></script>
    <script src="{{ asset('/node_modules/semantic-ui-transition/transition.min.js') }}"></script>
    <script src="{{ asset('/node_modules/semantic-ui-calendar/dist/calendar.min.js') }}"></script>
    <script src="{{ asset('/js/modal.js?v='.time()) }}"></script>
    @yield('scripts')
    <script type="text/javascript">
        //modal
        $('body').modalPlugin();

        $(".datepickerVue").inputmask('99-99-9999');
        $("#user-number_cart").inputmask('9999-9999-9999-9999');
        $("#user-cart_date").inputmask('99/99');

        $('.ui.dropdown.twig').dropdown();

        feather.replace();

        
    </script>

    <script src="{{ asset('/js/neat.min.js?v=1.0') }}"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
</body>
</html>
