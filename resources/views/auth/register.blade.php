@extends('layouts.auth')

@section('content')

<div class="c-card c-card--center">
    <h4 class="u-mb-medium">Регистрация</h4>
    {!! BootForm::open(['store' => 'register']) !!}

        @csrf

        <div class="c-field">
            <label class="c-field__label">Имя</label>
            <input class="c-input u-mb-small" type="text" name="name" placeholder="напр. Иван" required>

            @include('forms.error', ['name' => 'name'])
        </div>

        <div class="c-field">
            <label class="c-field__label">Фамилия</label>
            <input class="c-input u-mb-small" type="text" name="last_name" placeholder="напр. Иван Иванов" required>

            @include('forms.error', ['name' => 'last_name'])
        </div>

        <div class="c-field">
            <label class="c-field__label">Город</label>

            <div class="c-select u-mb-xsmall">
                {{ Form::select('city_id', \App\Model\City::query()->orderBy('name')->pluck('name', 'id')->prepend(['Выберите город']), old('city'), ['class' => 'ui dropdown twig search c-select__input']) }}

                @include('forms.error', ['name' => 'city_id'])
            </div>
        </div>

        <div class="c-field">
            <label class="c-field__label">E-mail</label>
            <input class="c-input u-mb-small" type="email" name="email" placeholder="mail@gmail.com" required>

            @include('forms.error', ['name' => 'email'])
        </div>

        <div class="c-field">
            <label class="c-field__label">Номер телефона</label>
            <input class="c-input u-mb-small phone" name="phone" type="text" placeholder="+7(___)_-__-_" required>

            @include('forms.error', ['name' => 'phone'])
        </div>

        <div class="c-field u-mb-small">
            <label class="c-field__label">Пароль</label>
            <input class="c-input" type="password" name="password" required>

            @include('forms.error', ['name' => 'password'])
        </div>

        <div class="c-field u-mb-small">
            <label class="c-field__label">Подтверждение пароля</label>
            <input class="c-input" type="password" name="password_confirmation" placeholder="Повторите свой пароль" required>

            @include('forms.error', ['name' => 'password_confirmation'])
        </div>

        <button class="c-btn c-btn--fullwidth c-btn--info">Зарегистрироваться</button>

    {!! BootForm::close() !!}
</div>

<div class="u-text-center"><a href="{{ route('login') }}">Назад</a></div>

@endsection
