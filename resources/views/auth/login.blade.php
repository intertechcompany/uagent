@extends('layouts.auth')

@section('content')

<div class="c-card c-card--center">

    <h4 class="u-mb-medium">Вход</h4>

    @if ($errors->any())
        <span class="invalid-feedback">
            <strong>{{ $errors->first() }}</strong>
        </span>
    @endif

    {!! BootForm::open(['store' => 'login']) !!}

        <div class="c-field">
            <label class="c-field__label">E-mail</label>
            <input placeholder="mail@gmail.com" class="c-input u-mb-small" type="email" name="email" required>
        </div>

        <div class="c-field">
            <label class="c-field__label">Пароль</label>
            <input class="c-input u-mb-small" type="password" name="password" required>
        </div>

        <button class="c-btn c-btn--fullwidth c-btn--info">Войти</button>

    {!! BootForm::close() !!}

</div>

<div class="u-text-center"><a href="{{ route('register') }}">Зарегистрировать аккаунт</a></div>

@endsection
