<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{ asset('/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css" integrity="sha384-v2Tw72dyUXeU3y4aM2Y0tBJQkGfplr39mxZqlTBDUZAb9BGoC40+rdFCG0m10lXk" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/fontawesome.css" integrity="sha384-q3jl8XQu1OpdLgGFvNRnPdj5VIlCvgsDQTQB6owSOHWlAurxul7f+JpUOVdAiJ5P" crossorigin="anonymous">

    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('/bower_components/Ionicons/css/ionicons.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('/dist/css/AdminLTE.min.css') }}">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect. -->
    <link rel="stylesheet" href="{{ asset('/dist/css/skins/skin-blue.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/admin.css') }}">
    <link href="{{ asset('/bower_components/select2/dist/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/libs/datepicker.min.css') }}" rel="stylesheet">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


    <link rel="stylesheet" href="/node_modules/flatpickr/dist/flatpickr.min.css">

    <!-- Stylesheet -->

    <link rel="stylesheet" href="/node_modules/semantic-ui-dropdown/dropdown.min.css">
    <link rel="stylesheet" href="/node_modules/semantic-ui-transition/transition.min.css">
    <link rel="stylesheet" href="/node_modules/semantic-ui-calendar/dist/calendar.min.css">
    <link rel="stylesheet" href="/node_modules/vue-multiselect/dist/vue-multiselect.min.css">
    <style src=""></style>
    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="{{ asset('/admin/css/build.css') }}">

    @yield('styles')
</head>

<body class="hold-transition skin-blue create-new__theme sidebar-mini">
<div class="wrapper wrapper-flex" id="app">



    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            @include('html.layouts.nav')

        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main Header -->
        <header class="main-header">
            @include('html.layouts.header')
        </header>
        <!-- Main content -->
        <section class="content container-fluid a-container">
            @if(session('alert'))
                <div class="alert alert-{{ array_get(session('alert'),'status') }}">
                    {{ array_get(session('alert'),'message') }}
                </div>
            @endif


            @yield('content')

        </section>
        <!-- /.content -->
    </div>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="{{ asset('/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('/dist/js/adminlte.min.js') }}"></script>


<script src="{{ asset('/bower_components/select2/dist/js/select2.min.js') }}"></script>
<script src="{{ asset('/bower_components/inputmask/dist/min/inputmask/inputmask.min.js') }}"></script>
<script src="{{ asset('/js/libs/datepicker.min.js') }}"></script>
<script src="{{ asset('/js/admin.js') }}"></script>
<script src="{{ asset('/admin/js/app.js') }}"></script>
@yield('scripts')
</body>
</html>
