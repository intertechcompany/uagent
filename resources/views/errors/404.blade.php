@extends('base')

@section('head')

    <title>404 - Не найдено</title>

@endsection

@section('body')

    <div class="c-404">
        <div class="c-404__content">
            <h1 class="c-404__title">404</h1>
            <p class="c-404__des">Простите, но указанная страница не найдена</p>

            <a class="c-404__link" href="{{ url('/') }}"><i class="feather icon-arrow-left"></i> Назад на главную</a>
        </div>

        <span class="c-404__shape1"></span>
        <span class="c-404__shape2"></span>
    </div>

@endsection