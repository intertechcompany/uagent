@extends('base')

@section('head')

    <title>Отказано в доступе</title>

@endsection

@section('body')

    <div class="c-404">
        <div class="c-404__content">
            <h1>Отказано в доступе</h1>

            <a class="c-404__link" href="{{ url('/') }}"><i class="feather icon-arrow-left"></i> Назад на главную</a>
        </div>

        <span class="c-404__shape1"></span>
        <span class="c-404__shape2"></span>
    </div>

@endsection