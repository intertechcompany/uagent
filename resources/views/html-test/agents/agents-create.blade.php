@extends('html.layouts.layout')

@section('content')
    <h1 class="a-content__title">
        Редактирование агента: <span class="a-all__text--pink">Ярик Ильниц</span>
    </h1>
    <agents-create></agents-create>
@endsection