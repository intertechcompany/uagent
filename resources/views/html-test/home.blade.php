@extends('html.layouts.layout')

@section('content')

    <h1 class="a-content__title">
        Главная
    </h1>
    <div class="c-sidebar-row__flex">
        <div class="c-sidebar__item">
            <div class="c-state-card c-state-card--success">
                        <span class="c-state-card__number">
                            30
                        </span>
                <h4 class="c-state-card__title">Продано полисов (Всего)</h4>
            </div>
        </div>
        <div class="c-sidebar__item">
            <div class="c-state-card c-state-card--info">
                        <span class="c-state-card__number">
                            30
                        </span>
                <h4 class="c-state-card__title">Продано полисов (Сегодня)</h4>
            </div>
        </div>
        <div class="c-sidebar__item">
            <div class="c-state-card c-state-card--success">
                <span class="c-state-card__number c-state-card__money">30₽</span>
                <h4 class="c-state-card__title">Ваша прыбыль (Всего)</h4>
            </div>
        </div>
        <div class="c-sidebar__item">
            <div class="c-state-card c-state-card--info">
                <span class="c-state-card__number c-state-card__money">40₽</span>
                <h4 class="c-state-card__title">Последний полис продано на сумму</h4>
            </div>
        </div>
    </div>
    <hello></hello>
@endsection
