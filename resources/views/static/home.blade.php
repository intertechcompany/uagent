@extends('base')

@section('head')
    <link href="https://fonts.googleapis.com/css?family=Exo+2:300,400,500,700" rel="stylesheet">
@endsection

@section('body')

    <header class="position-relative">
        <div class="container">
            <div class="row">
                <div class="col-md-5 text-center">
                    <div class="logo">
                        <div class="image-holder">
                            <img src="{{ asset('/img/logo-head.png') }}">
                        </div>
                        <p>Страховой провайдер</p>
                    </div>
                </div>

                <div class="col-md-7">
                    <p>Единая платформа для страховых брокеров и агентов</p>
                    <div class="home-header-phone" style="font-weight:800; position: absolute;top: 0;right: 10px;"><a href="tel:88006007197" style="color: #000;">8 800 600 71 97</a></div>
                </div>
            </div>
        </div>
    </header>
    <main>
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-7 block-right">
                    <h2>Получите <span>МАКСИМАЛЬНЫЙ ДОХОД</span> на полисах ОСАГО</h2>
                    <ul class="list-desk">
                        <li class="st-border">Самое высокое КВ среди страховых сервисов <a style="color: #ff0d40;margin-left: 10px;" href="{{ asset('/uagent.pdf') }}" target="_blank">узнать подробнее</a></li>
                        <li class="st-border">Онлайн вывод КВ любым удобным способом</li>
                        <li class="st-border">Автозаполнение полей по гос. номеру авто</li>
                        <li class="st-border">Предварительный расчет по всем страховым - 3 минуты и полис готов!</li>
                        <li class="st-border">Регистрация на платформе за 1 минуту</li>
                    </ul>
                    <div class="row">
                        <a href="#modal_reg"  class="btn btn-red js_open_modal" data-modal="#modal_reg">Зарегистрироваться</a>
                        <a href="#modal_in"  class="btn btn-grey js_open_modal" data-modal="#modal_in">Войти</a>
                    </div>
                </div>
            </div>
            <div class="row bg">
                <div class="col-md-7 block-center">
                    <h2>СОВРЕМЕННАЯ ПЛАТФОРМА ЮАГЕНТ ДЛЯ СТРАХОВЫХ <span>брокеров и агентов</span></h2>
                    <div class="text-block">
                        <p>
                            Регистрируйтесь на сервисе, начните оформлять полисы ОСАГО и получите лучшие условия зароботка в страховании
                        </p>
                    </div>
                    <a href="{{ asset('/uagent.pdf') }}" target="_blank" class="btn btn-red" >Скачать презентацию</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 block-left">
                    <h2>КОМУ И КАК ВЫПЛАЧИВАЕМ  <span>ВОЗНАГРАЖДЕНИЕ</span></h2>
                    <div class="text-block">
                        <p>
                            Платформа Юагент работает как с физическими, так и с юридическими лицами.
                            Заявки на вывод комиссионного вознаграждения производятся в режиме онлайн - на банковские карты, электронный кошелек или расчетный счет.
                        </p>
                    </div>
                    <p class="st-border question">Остались вопросы? Напишите нам!</p>
                    <form action="{{ route('home.question') }}" id="questionForm">
                        <div class="row d-flex flex-row flex-wrap justify-content-between">
                            <div class="form-holder col-md-6">
                                <input type="text" name="name" placeholder="Ф.И.О" >
                            </div>
                            <div class="form-holder col-md-6">
                                <input type="text" name="phone" placeholder="Телефон" >
                            </div>
                        </div>
                        <div class="message form-holder">
                            <textarea placeholder="Текст сообщения" name="message" rows="3"></textarea>
                        </div>
                        <div class="submit btn btn-red" id="questionFormButton">Отправить</div>
                    </form>
                </div>
            </div>
        </div>
    </main>
    <footer class="position-relative">
        <div class="container">
            <div class="row ">
                <div class="col-md-6 d-flex flex-row flex-wrap justify-content-end align-items-center">
                    <a href="#modal_reg"  class="btn btn-red js_open_modal" data-modal="#modal_reg">Зарегистрироваться</a>
                    <a href="#modal_in"  class="btn btn-grey js_open_modal" data-modal="#modal_in">Войти</a>
                </div>
                <div class="col-md-6">
                    <img src="{{ asset('/img/logo-f.png') }}">
                    <p>Страховой провайдер</p>
                   <div style="display: flex; justify-content: space-between">
                       <a href="mailto:info@uagent.online">info@uagent.online</a>
                       <span style="color: #fff;font-size: 12px">
                           © {{ date('Y') }} ИП Шильников И.Н. ОГРНИП 315366800006094
                       </span>
                   </div>
                </div>
            </div>
        </div>
    </footer>

   @include('include.modals.auth')

@endsection

@section('scripts')
    <script>
        document.getElementById('questionFormButton').addEventListener('click',function () {
            let form =  $('#questionForm');
            $.post('{{ route('home.question') }}',form.serialize(),function () {
                form.trigger('reset');
            });
        });
    </script>
@endsection
