@extends('layouts.admin')

{{--
@section('title')
    Акции
@endsection
--}}

@section('content')
    <stock></stock>
    
    {{--
    <div class="box">
        <div>
            <a href="{{ route('admin.stocks.create') }}" style="margin: 5px 10px" class="btn btn-info">Создать новую акцию</a>
        </div>
        <div class="box-header with-border">
            <h3 class="box-title">Список акций</h3>
        </div>

        <div class="box-body">
            <table class="table table-bordered">
                <tbody><tr>
                    <th style="width: 10px">Id</th>
                    <th>Заголовок</th>
                    <th>От</th>
                    <th>До</th>
                    <th>Видимость</th>
                    <th>Описание</th>
                    <th></th>
                </tr>
                @foreach($stocks as $stock)
                    <tr>
                        <td>{{ $stock->id }}</td>
                        <td>{{ $stock->title }}</td>
                        <td>{{ $stock->from_time }}</td>
                        <td>{{ $stock->to_time }}</td>
                        <td>{{ $stock->enabled }}</td>
                        <td>{{ $stock->text }}</td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    Действия
                                    <span class="caret"></span>
                                    <span class="sr-only">Действия</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ route('admin.stocks.edit', $stock->id) }}">Редактировать</a></li>
                                    <li><a href="{{ route('admin.stocks.delete', $stock->id) }}">Удалить</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody></table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            {{ $stocks->links() }}
        </div>
    </div>
    --}}
@endsection