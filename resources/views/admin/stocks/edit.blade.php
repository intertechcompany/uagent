@extends('layouts.admin')

{{--
@section('title')
    <i class="c-sidebar__icon feather icon-home"></i>Редактирование акции: {{ $stock->title }}
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('/bower_components/datetimepicker/build/jquery.datetimepicker.min.css') }}">
@endsection
--}}

@section('content')
    <h1 class="a-content__title">
        Редавктирование акции
    </h1>
    <stock-update data-stock-route="{{ route('admin.stocks.edit.ajax', $stock) }}" id="stock-update-route"></stock-update>

    {{--
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Редактирование акции: {{ $stock->title }}</h3>
        </div>
        <div class="box-body">
            <form method="post" action="{{ route('admin.stocks.update', $stock->id) }}">
                {{ csrf_field() }}
                {{ method_field('put') }}

                <div class="form-group col-md-12">
                    <label for="title">Название</label>
                    <input type="text" value="{{ old('title', $stock->title) }}" class="form-control" name="title" id="title"  placeholder="Имя">
                    @if ($errors->first('title'))
                        <small class="has-error">{{ $errors->first('title') }}</small>
                    @endif
                </div>
                <div class="form-group col-md-6">
                    <label for="text">Описание:</label>
                    <textarea class="form-control" rows="5" id="text" name="text">{{ old('text', $stock->text) }}</textarea>
                    @if ($errors->first('text'))
                        <small class="has-error">{{ $errors->first('text') }}</small>
                    @endif
                </div>
                <div class="form-group col-md-6">
                    <label for="information">Информация:</label>
                    <textarea class="form-control" rows="5" id="information" name="information">{{ old('information', $stock->information) }}</textarea>
                    @if ($errors->first('information'))
                        <small class="has-error">{{ $errors->first('information') }}</small>
                    @endif
                </div>
                <div class="form-group col-md-6">
                    <label for="from">Дата начала акции</label>
                    <div class="input-append date" data-date-format="Y-m-d H:i">
                        <input type="text" class="form-control datetimepicker" name="from" id="from" value="{{ old('from', $stock->from_time) }}" placeholder="Дата">
                        <span class="add-on"><i class="icon-th"></i></span>
                    </div>
                    @if ($errors->first('from'))
                        <small class="has-error">{{ $errors->first('from') }}</small>
                    @endif
                </div>
                <div class="form-group col-md-6">
                    <label for="to">Дата конца акции</label>
                    <div class="input-append date" data-date-format="Y-m-d H:i">
                        <input type="text" class="form-control datetimepicker" name="to" id="to" value="{{ old('to', $stock->to_time) }}" placeholder="Дата">
                        <span class="add-on"><i class="icon-th"></i></span>
                    </div>
                    @if ($errors->first('to'))
                        <small class="has-error">{{ $errors->first('to') }}</small>
                    @endif
                </div>
                <div class="form-group col-md-6">
                    <div class="checkbox">
                        <label class="form-check-label" for="is_enabled">
                            <input{{ !empty(old('is_enabled')) ? ' checked' : $stock->is_enabled ? ' checked' : '' }} type="checkbox" value="1" name="is_enabled" class="form-check-input" id="is_enabled"> Видимый
                        </label>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label>Страховые компании</label>
                    @foreach ($insurance as $key => $value)
                        <div>
                            <input class="form-check-input position-static" id="insurance-{{ $key }}" type="checkbox" name="insurance[]" value="{{ $key }}"{{ in_array($key, $stock->getInsurance()) ? ' checked' : '' }}>
                            <label for="insurance-{{ $key }}" class="cursor-pointer">{{ $value }}</label>
                        </div>
                    @endforeach
                    @if ($errors->first('insurance'))
                        <small class="has-error">{{ $errors->first('insurance') }}</small>
                    @endif
                </div>
                <div class="masonry-item col-md-12">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix"></div>
    </div>
    --}}
@endsection

{{--
@section('scripts')
    <script src="{{ asset('/bower_components/datetimepicker/build/jquery.datetimepicker.full.min.js') }}"></script>

    <script src="{{ asset('/bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js') }}"></script>
@endsection
--}}