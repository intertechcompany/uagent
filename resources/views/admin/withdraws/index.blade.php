@extends('layouts.admin')

{{--
@section('title')
    Выплаты
@endsection
--}}

@section('content')
    <h1 class="a-content__title">
        Выплаты
    </h1>
    <widthraws :api-status-url="'{{ route('admin.payments.update') }}'" data-withdraw-route="{{ route('admin.withdraws.load.ajax') }}" id="withdraw-load-route"></widthraws>

    {{--
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Список выплат</h3>
        </div>

        <div class="box-body">
            <table class="table table-bordered">
                <tbody><tr>
                    <th style="width: 10px">Id</th>
                    <th>Выд</th>
                    <th>Сума</th>
                    <th>Номер карты</th>
                    <th>Реквизит</th>
                    <th>Email</th>
                    <th></th>
                </tr>
                @foreach($withdraws as $withdraw)
                    <tr>
                        <td>{{ $withdraw->id }}</td>
                        <td>{{ $withdraw->getTypeName() }}</td>
                        <td>{{ $withdraw->amount }}</td>
                        <td>{{ $withdraw->card_number }}</td>
                        <td>{{ $withdraw->requisites }}</td>
                        <td>{{ $withdraw->email }}</td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    Действия
                                    <span class="caret"></span>
                                    <span class="sr-only">Действия</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ route('admin.withdraws.edit', $withdraw->id) }}">Редактировать</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody></table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            {{ $withdraws->links() }}
        </div>
    </div>
    --}}
@endsection