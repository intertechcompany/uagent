@extends('layouts.admin')

@section('content')
    <widthraws-update
        data-withdraw-edit-route="{{ route('admin.withdraws.edit.ajax', $withdraw->id) }}" id="withdraw-edit-route"
    ></widthraws-update>

    {{--
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Редактирование выплаты: {{ $withdraw->getTypeName() }}</h3>
        </div>
        <div class="box-body">
            <form method="post" action="{{ route('admin.withdraws.update', $withdraw->id) }}">
                {{ csrf_field() }}
                {{ method_field('put') }}

                <div class="form-group col-md-12">
                    <label for="card_number">Номер карты</label>
                    <input type="text" value="{{ old('card_number', $withdraw->card_number) }}" class="form-control" name="card_number" id="card_number"  placeholder="Номер карты">
                    @if ($errors->first('card_number'))
                        <small class="has-error">{{ $errors->first('card_number') }}</small>
                    @endif
                </div>
                <div class="form-group col-md-12">
                    <label for="requisites">Реквизиты</label>
                    <input type="text" value="{{ old('requisites', $withdraw->requisites) }}" class="form-control" name="requisites" id="requisites"  placeholder="Номер карты">
                    @if ($errors->first('requisites'))
                        <small class="has-error">{{ $errors->first('requisites') }}</small>
                    @endif
                </div>
                <div class="form-group col-md-12">
                    <label for="amount">Сума</label>
                    <input type="text" value="{{ old('amount', $withdraw->amount) }}" class="form-control" name="amount" id="amount"  placeholder="Сума">
                    @if ($errors->first('amount'))
                        <small class="has-error">{{ $errors->first('amount') }}</small>
                    @endif
                </div>
                <div class="form-group col-md-12">
                    <label for="email">E-mail</label>
                    <input type="text" value="{{ old('email', $withdraw->email) }}" class="form-control" name="email" id="email"  placeholder="mail@gmail.com">
                    @if ($errors->first('email'))
                        <small class="has-error">{{ $errors->first('email') }}</small>
                    @endif
                </div>
                <div class="masonry-item col-md-12">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix"></div>
    </div>
    --}}
@endsection
