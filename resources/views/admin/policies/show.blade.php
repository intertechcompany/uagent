@extends('layouts.admin')

@section('title')
    <i class="c-sidebar__icon feather icon-home"></i>Полис #{{ $policy->id }} ({{ \App\Model\Policy::insurance($policy->insurance, true) }})
@endsection

@section('content')
    <section class="content container-fluid a-container">
        <div class="info-page">
            @include('include.policies.info',['policy' => $policy,'owner' => $policy->owner])
        </div>
    </section>
@endsection