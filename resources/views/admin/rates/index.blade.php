@extends('layouts.admin')

{{--
@section('title')
    Ставки
@endsection
--}}

@section('content')
    <bets data-rates-route="{{ route('admin.rates_default.load.ajax') }}" id="rates-load-route"></bets>
    
    {{--
    <div class="box">
        <div>
            <a href="{{ route('admin.rates.create') }}" style="margin: 5px 10px" class="btn btn-info">Создать новую ставку</a>
        </div>
        <div class="box-header with-border">
            <h3 class="box-title">Список ставок</h3>
        </div>

        <div class="box-body">
            <table class="table table-bordered">
                <tbody><tr>
                    <th style="width: 10px">Id</th>
                    <th>От</th>
                    <th>До</th>
                    <th>Тип</th>
                    <th>Значение</th>
                    <th>Страховая компания</th>
                    <th></th>
                </tr>
                @foreach($rates as $rate)
                    <tr>
                        <td>{{ $rate->id }}</td>
                        <td>{{ $rate->from }}</td>
                        <td>{{ $rate->to }}</td>
                        <td>{{ $rate->type_name }}</td>
                        <td>{{ $rate->value }}</td>
                        <td>{{ \App\Model\Policy::insurance($rate->insurance) }}</td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    Действия
                                    <span class="caret"></span>
                                    <span class="sr-only">Действия</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ route('admin.rates.edit', $rate->id) }}">Редактировать</a></li>
                                    <li><a href="{{ route('admin.rates.delete', $rate->id) }}">Удалить</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody></table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            {{ $rates->links() }}
        </div>
    </div>
    --}}
@endsection
