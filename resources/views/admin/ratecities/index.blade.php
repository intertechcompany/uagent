@extends('layouts.admin')

@section('title')
    Ставки для городов
@endsection

@section('content')
    <div class="box">
        <div>
            <a href="{{ route('admin.ratecities.create') }}" style="margin: 5px 10px" class="btn btn-info">Создать новую ставку для города</a>
        </div>
        <div class="box-header with-border">
            <h3 class="box-title">Список ставок для городов</h3>
        </div>

        <div class="box-body">
            <table class="table table-bordered">
                <tbody><tr>
                    <th style="width: 10px">Id</th>
                    <th>Страховая компания</th>
                    <th>Город</th>
                    <th>Значение</th>
                    <th></th>
                </tr>
                @foreach($rateCities as $city)
                    <tr>
                        <td>{{ $city->id }}</td>
                        <td>{{ $city->insurance_name }}</td>
                        <td>{{ $city->city->name }}</td>
                        <td>{{ $city->value . ' ' . $city->type_name }}</td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    Действия
                                    <span class="caret"></span>
                                    <span class="sr-only">Действия</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ route('admin.ratecities.edit', $city->id) }}">Редактировать</a></li>
                                    <li><a href="{{ route('admin.ratecities.delete', $city->id) }}">Удалить</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody></table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            {{ $rateCities->links() }}
        </div>
    </div>

@endsection