@extends('layouts.admin')

@section('title')
    <i class="c-sidebar__icon feather icon-home"></i>Создать ставку для города
@endsection

@section('styles')
    <link href="{{ asset('/css/libs/suggestions.css') }}" type="text/css" rel="stylesheet" >
@endsection

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Создать ставку для города</h3>
        </div>
        <div class="box-body">
            <form method="post" action="{{ route('admin.ratecities.store') }}">
                {{ csrf_field() }}

                <div class="form-group col-md-6">
                    <label for="address">Город</label>
                    <input id="address" class="form-control" name="address" type="text"  value="{{ old('address') }}">
                    @if ($errors->first('city_id'))
                        <small class="has-error">{{ $errors->first('city_id') }}</small>
                    @endif
                    <input type="hidden" name="city_id" value="{{ old('city_id') }}">
                </div>
                <div class="form-group col-md-6">
                    <label>Страховые компании</label>
                    <select name="insurance" id="insurance" class="form-control">
                        @foreach ($insurance as $key => $value)
                            @if (isset($insurance[$key]))
                                <option value="{{ $key }}"{{ old('insurance') === $key ? ' selected' : '' }}>
                                    {{ $insurance[$key] }}
                                </option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="box-footer clearfix"></div>
                <div class="form-group col-md-6">
                    <label for="type">Тип</label>
                    <select class="form-control" id="type" name="type">
                        @foreach ($types as $type => $value)
                            <option value="{{ $type }}"{{ $type == old('type') ? ' selected' : '' }}>{{ $value }}</option>
                        @endforeach
                    </select>
                    @if ($errors->first('type'))
                        <small class="has-error">{{ $errors->first('type') }}</small>
                    @endif
                </div>
                <div class="form-group col-md-6">
                    <label for="value">Значение</label>
                    <input type="text" value="{{ old('value') }}" class="form-control" name="value" id="value"  placeholder="Значение">
                    @if ($errors->first('value'))
                        <small class="has-error">{{ $errors->first('value') }}</small>
                    @endif
                </div>

                <div class="masonry-item col-md-12">
                    <button type="submit" class="btn btn-primary">Создать</button>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix"></div>
    </div>
@endsection 

@section('scripts')
    <script type="text/javascript" src="{{ asset('/js/libs/suggestions.js') }}"></script>
@endsection
