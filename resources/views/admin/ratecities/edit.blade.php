@extends('layouts.admin')

@section('content')
    <h1 class="a-content__title">
        Редактировать ставку
    </h1>
    <bets-update
        data-rates-update-route="{{ route('admin.ratecities.edit.ajax', $rateCity) }}"
        id="rates-update-route"
        data-rate-type="rateCity"
    ></bets-update>

    {{--
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Редактирование куратора: {{ $rateCity->full_name }}</h3>
        </div>
        <div class="box-body">
            <form method="post" action="{{ route('admin.ratecities.update', ['rateCity' => $rateCity->id]) }}">
                {{ csrf_field() }}
                {{ method_field('put') }}

                <div class="form-group col-md-6">
                    <label for="address">Город</label>
                    <input id="address" class="form-control" name="address" type="text"  value="{{ old('address', $rateCity->city ? $rateCity->city->name : '') }}">
                    @if ($errors->first('city_id') || $errors->first('address'))
                        <small class="has-error">{{ $errors->first('address') }}</small>
                    @endif
                    <input type="hidden" name="city_id" value="{{ old('city_id', $rateCity->city_id) }}">
                </div>
                <div class="form-group col-md-6">
                    <label>Страховые компании</label>
                    <select name="insurance" id="insurance" class="form-control">
                        @foreach ($insurance as $key => $value)
                            @if (isset($insurance[$key]))
                                <option value="{{ $key }}"{{ old('insurance') == $key ? ' selected' : $rateCity->insurance == $key ? ' selected' : '' }}>
                                    {{ $insurance[$key] }}
                                </option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="box-footer clearfix"></div>
                <div class="form-group col-md-6">
                    <label for="type">Тип</label>
                    <select class="form-control" id="type" name="type">
                        @foreach ($types as $type => $value)
                            <option value="{{ $type }}"{{ $type == old('type') ? ' selected' : $rateCity->type == $type ? ' selected' : '' }}>{{ $value }}</option>
                        @endforeach
                    </select>
                    @if ($errors->first('type'))
                        <small class="has-error">{{ $errors->first('type') }}</small>
                    @endif
                </div>
                <div class="form-group col-md-6">
                    <label for="value">Значение</label>
                    <input type="text" value="{{ old('value', $rateCity->value) }}" class="form-control" name="value" id="value"  placeholder="Значение">
                    @if ($errors->first('value'))
                        <small class="has-error">{{ $errors->first('value') }}</small>
                    @endif
                </div>

                <div class="masonry-item col-md-12">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix"></div>
    </div>
    --}}
@endsection

