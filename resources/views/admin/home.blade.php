@extends('layouts.admin')

@section('content')
    <h1 class="a-content__title">
        Главная
    </h1>
    {{--<hello-test></hello-test>--}}
    <hello :is-curator="{{ empty($isCurator) ? 'false' : 'true' }}" data-route="{{ route('policies.filter') }}"></hello>
@endsection
