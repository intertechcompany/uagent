@extends('layouts.admin')

@section('title')
@endsection

@section('content')
    <tech-support
            :is-admin="true"
            :api-status-url="'{{ route('admin.support.chat.update') }}'"
            :api-url="'{{ route('admin.support.getData') }}'">
    </tech-support>
@endsection
