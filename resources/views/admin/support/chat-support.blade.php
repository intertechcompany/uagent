@extends('layouts.admin')

@section('title')
@endsection

@section('content')
    <chat-support
            :back-url="'{{ route('admin.support') }}'"
            :api-send-url="'{{ route('admin.support.chat.sendData', ['chat'=>$chat]) }}'"
            :api-url="'{{ route('admin.support.chat.getData', ['chat'=>$chat]) }}'">
    </chat-support>
@endsection
