@extends('layouts.admin')

@section('title')
    Верификация
@endsection

@section('content')

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li @if(!request('declined'))class="active"@endif><a href="{{ url('/admin/verifications') }}">В процесе</a></li>
                <li @if(request('declined'))class="active"@endif><a href="{{ url('/admin/verifications?declined=true') }}">Отклоненные</a></li>
            </ul>

            <div class="tab-content">

                <div class="tab-pane active">
                    <table class="table table-bordered">
                        <tbody><tr>
                            <th style="width: 10px">Id</th>
                            <th>Email</th>
                            <th>Имя</th>
                            <th>Фамилия</th>
                            <th>Телефон</th>
                            <th style="width: 40px">Зарегистрирован</th>
                            <th></th>
                        </tr>
                        @foreach($agents as $agent)
                            <tr>
                                <td>{{ $agent->id }}</td>
                                <td>{{ $agent->email }}</td>
                                <td>{{ $agent->name }}</td>
                                <td>{{ $agent->last_name }}</td>
                                <td>{{ $agent->phone }}</td>
                                <td>{{ $agent->created_at }}</td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            Действия
                                            <span class="caret"></span>
                                            <span class="sr-only">Действия</span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="{{ url("admin/verifications/$agent->id/verify") }}">Верифицировать</a></li>

                                            @if(!$agent->declined_at)
                                                <li><a href="{{ url("admin/verifications/$agent->id/decline") }}">Отклонить</a></li>
                                            @endif
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody></table>

                        {{ $agents->links() }}
                </div>

            </div>
            <!-- /.tab-content -->
        </div>

@endsection