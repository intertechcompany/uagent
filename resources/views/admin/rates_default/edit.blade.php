@extends('layouts.admin')

@section('title')
    <i class="c-sidebar__icon feather icon-home"></i>Редактирование ставки по умолчанию: {{ $rate->full_name }}
@endsection

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Редактирование ставки по умолчанию: {{ $rate->full_name }}</h3>
        </div>
        <div class="box-body">
            <form method="post" action="{{ route('admin.rates_default.update', $rate->id) }}">
                {{ csrf_field() }}
                {{ method_field('put') }}

                <div class="form-group col-md-6">
                    <label for="from">От</label>
                    <input type="text" value="{{ old('from', $rate->from) }}" class="form-control"  name="from" id="from" placeholder="От">
                    @if ($errors->first('from'))
                        <small class="has-error">{{ $errors->first('from') }}</small>
                    @endif
                </div>
                <div class="form-group col-md-6">
                    <label for="to">До</label>
                    <input type="text" value="{{ old('to', $rate->to) }}" class="form-control"  name="to" id="to" placeholder="До">
                    @if ($errors->first('to'))
                        <small class="has-error">{{ $errors->first('to') }}</small>
                    @endif
                </div>
                <div class="form-group col-md-6">
                    <label for="type">Тип</label>
                    <select class="form-control" id="type" name="type">
                        @foreach ($types as $type => $value)
                            <option value="{{ $type }}"{{ $type === old('type', $rate->type) ? ' selected' : '' }}>{{ $value }}</option>
                        @endforeach
                    </select>
                    @if ($errors->first('type'))
                        <small class="has-error">{{ $errors->first('type') }}</small>
                    @endif
                </div>
                <div class="form-group col-md-6">
                    <label for="value">Значение</label>
                    <input type="text" value="{{ old('value', $rate->value) }}" class="form-control"  name="value" id="value" placeholder="Значение">
                    @if ($errors->first('value'))
                        <small class="has-error">{{ $errors->first('value') }}</small>
                    @endif
                </div>
                <div class="form-group col-md-6">
                    <label>Страховые компании</label>
                    <select class="form-control" id="insurance" name="insurance">
                        @foreach ($insurance as $key => $value)
                            <option value="{{ $key }}"{{ $key === old('insurance',$rate->insurance) ? ' selected' : '' }}>{{ $value }}</option>
                        @endforeach
                    </select>
                    @if ($errors->first('insurance'))
                        <small class="has-error">{{ $errors->first('insurance') }}</small>
                    @endif
                </div>
                <div class="form-group col-md-12">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix"></div>
    </div>
@endsection
