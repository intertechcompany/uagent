@extends('layouts.admin')

@section('title')
    <i class="c-sidebar__icon feather icon-home"></i>Создать ставку по умолчанию
@endsection

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Создать ставку по умолчанию</h3>
        </div>
        <div class="box-body">
            <form method="post" action="{{ route('admin.rates_default.store') }}">
                {{ csrf_field() }}
                <div class="form-group col-md-6">
                    <label for="to">От</label>
                    <input type="text" value="{{ old('from') }}" class="form-control" name="from" id="from"  placeholder="От">
                    @if ($errors->first('from'))
                        <small class="has-error">{{ $errors->first('from') }}</small>
                    @endif
                </div>
                <div class="form-group col-md-6">
                    <label for="to">До</label>
                    <input type="text" value="{{ old('to') }}" class="form-control" name="to" id="to"  placeholder="До">
                    @if ($errors->first('to'))
                        <small class="has-error">{{ $errors->first('to') }}</small>
                    @endif
                </div>
                <div class="form-group col-md-6">
                    <label for="type">Тип</label>
                    <select class="form-control" id="type" name="type">
                        @foreach ($types as $type => $value)
                            <option value="{{ $type }}"{{ $type === old('type') ? ' selected' : '' }}>{{ $value }}</option>
                        @endforeach
                    </select>
                    @if ($errors->first('type'))
                        <small class="has-error">{{ $errors->first('type') }}</small>
                    @endif
                </div>
                <div class="form-group col-md-6">
                    <label for="value">Значение</label>
                    <input type="text" value="{{ old('value') }}" class="form-control" name="value" id="value"  placeholder="Значение">
                    @if ($errors->first('value'))
                        <small class="has-error">{{ $errors->first('value') }}</small>
                    @endif
                </div>
                <div class="form-group col-md-6">
                    <label>Страховые компании</label>
                    <select class="form-control" id="insurance" name="insurance">
                        @foreach ($insurance as $key => $value)
                            <option value="{{ $key }}"{{ $key === old('insurance') ? ' selected' : '' }}>{{ $value }}</option>
                        @endforeach
                    </select>

                    @if ($errors->first('insurance'))
                        <small class="has-error">{{ $errors->first('insurance') }}</small>
                    @endif
                </div>
                <div class="masonry-item col-md-12">
                    <button type="submit" class="btn btn-primary">Создать</button>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix"></div>
    </div>
@endsection
