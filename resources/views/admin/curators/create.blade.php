@extends('layouts.admin')

@section('title')
    <i class="c-sidebar__icon feather icon-home"></i>Создать куратора
@endsection

@section('content')
    <curators-create></curators-create>
@endsection
