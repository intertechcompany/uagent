@extends('layouts.admin')

@section('title')
    <i class="c-sidebar__icon feather icon-home"></i>Редактирование куратора: {{ $curator->full_name }}
@endsection

@section('content')
    <curators-edit :id="{{$curator->id}}"></curators-edit>
@endsection
