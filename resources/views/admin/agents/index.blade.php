@extends('layouts.admin')

@section('content')
    <h1 class="a-content__title">
        <span>Агенты</span>
    </h1>
    <agents></agents>
    
    {{--
    <div class="box">
        <div>
            <a href="{{ route('admin.agent.create') }}" style="margin: 5px 10px" class="btn btn-info">Создать нового агента</a>
        </div>
        <div class="box-header with-border">
            <h3 class="box-title">Список агентов</h3>
        </div>

        <div class="box-body" style="overflow: auto;">
            <table class="table table-bordered">
                <tbody><tr>
                    <th style="width: 5px"></th>
                    <th style="width: 5px"></th>
                    <th style="width: 10px">Id</th>
                    <th>Агент</th>
                    <th>Email</th>
                    <th>Город</th>
                    <th>Имя</th>
                    <th>Фамилия</th>
                    <th>Телефон</th>
                    <th>Субагенты</th>
                    <th>Полисы</th>
                    <th>Статус</th>
                    <th style="width: 40px">Зарегистрирован</th>
                    <th>Выплачено</th>
                    <th>Начислено</th>
                    <th>Всего</th>
                    <th></th>
                </tr>
                @foreach($agents as $agent)
                    <tr>
                        <td>
                            @if ($agent->is_important)
                                <i class="fa fa-font-awesome" aria-hidden="true" style="color: red"></i>
                            @endif
                        </td>
                        <td>
                            <a href="{{ route('admin.dashboard.index') }}?filter[agents][]={{ $agent->id }}"
                                class="fa fa-info"
                                aria-hidden="true"
                                title="Статистика" 
                            ></a>
                        </td>
                        <td>{{ $agent->id }}</td>
                        <td>
                            @if ($agent->canCreateSubAgents())
                                <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </td>
                        <td>{{ $agent->email }}</td>
                        <td>{{ optional($agent->city)->name }}</td>
                        <td>{{ $agent->name }}</td>
                        <td>{{ $agent->last_name }}</td>
                        <td>{{ $agent->phone }}</td>
                        <td>{{ sizeof($agent->subAgents) }}</td>
                        <td>{{ $agent->polices->count() }}</td>
                        <td>{{ $agent->status_name }}</td>
                        <td>{{ $agent->created_at }}</td>
                        <td>{{ $agent->formated_paidouts }} руб.</td>
                        <td>{{ $agent->formated_accrued }} руб.</td>
                        <td>{{ $agent->formated_balance }} руб.</td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    Действия
                                    <span class="caret"></span>
                                    <span class="sr-only">Действия</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ route('admin.agent.edit',$agent->id) }}">Редактировать</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    @if (sizeof($agent->subAgents) > 0)
                        @include('admin.agents.subaget', ['agents' => $agent->subAgents])
                    @endif
                @endforeach
                </tbody></table>
        </div>
        <div class="box-footer clearfix">
            {{ $agents->links() }}
        </div>
    </div>
    --}}

@endsection
