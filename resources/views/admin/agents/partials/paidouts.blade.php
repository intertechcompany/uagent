<div>
    <div class="btn-group col-md-12" style="margin-top: 10px;margin-bottom: 10px;">
        <button type="button" class="btn" data-toggle="dropdown">
        Действия <span class="caret"></span></button>
        <ul class="dropdown-menu" role="menu">
            <li>
                <a
                    href="javascript:;"
                    class="create-tab"
                    data-url="{{ route('admin.agent.create.tab', 'paidout') }}"
                    data-tab="paidout"
                >Создать</a>
            </li>
            {{--<li>
                <a href="javascript:;">Удалить</a>
            </li>--}}
        </ul>
    </div>

    @if ($agent->paidout->count() > 0)
        <table class="table table-striped">
            <thead>
                <tr>
                    {{--<th style="width: 10px">
                        <input type="checkbox" id="allPaid">
                    </th>--}}
                    <th scope="col">#</th>
                    <th scope="col">Дата</th>
                    <th scope="col">Сума</th>
                    <th scope="col">Документ</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($agent->paidout as $paidout)
                    <tr class="paidout">
                        {{--<td>
                            <input type="checkbox" name="paidout[]" value="{{ 1 }}">
                        </td>--}}
                        <th scope="row">{{ $paidout->id }}</th>
                        <td>{{ number_format($paidout->sum, 2) }}</td>
                        <td>{{ $paidout->date }}</td>
                        <td>
                        @if (!empty($paidout->file))
                            <a href="{{ route('admin.agent.tab.file', ['paidout', $paidout]) }}" target="_blank">Скачать</a>
                        @endif
                        </td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    Действия
                                    <span class="caret"></span>
                                    <span class="sr-only">Действия</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="javascript:;" class="edit-tab" data-url="{{ route('admin.agent.tab.edit', ['paidout', $paidout]) }}" data-tab="paidout">Редактировать</a></li>
                                    <li><a href="{{ route('admin.agent.tab.delete', ['paidout', $paidout]) }}">Удалить</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <table class="table table-striped">
            <tbody>
                <center>
                    <h4>Записей нет</h4>
                </center>
            </tbody>
        </table>
    @endif

    <div class="form-row create-form-tab{{ !$errors->first('tab_date') && !$errors->first('tab_file') && session('tab_type') != $type ? ' hidden' : '' }}" id="formCreateTab{{ $type }}">
        @if (($errors->first('tab_date') || $errors->first('tab_file')) && session('tab_type') == $type)
            @include('admin.agents.partials.form-'.$type, [
                'type' => $type
            ])
        @endif
    </div>
</div>
