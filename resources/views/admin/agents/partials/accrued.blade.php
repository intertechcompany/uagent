<div>
    <div class="btn-group col-md-12" style="margin-top: 10px;margin-bottom: 10px;">
        <button type="button" class="btn" data-toggle="dropdown">
        Действия <span class="caret"></span></button>
        <ul class="dropdown-menu" role="menu">
            <li>
                <a
                    href="javascript:;"
                    class="create-tab"
                    data-url="{{ route('admin.agent.create.tab', 'accrued') }}"
                    data-tab="accrued"
                >Создать</a>
            </li>
        </ul>
    </div>

    @if ($agent->accrued->count() > 0)
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Дата</th>
                    <th scope="col">Сума</th>
                    <th scope="col">Документ</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($agent->accrued as $accrued)
                    <tr class="accrued">
                        <th scope="row">{{ $accrued->id }}</th>
                        <td>{{ number_format($accrued->sum, 2) }}</td>
                        <td>{{ $accrued->date }}</td>
                        <td>
                        @if (!empty($accrued->file))
                            <a href="{{ route('admin.agent.tab.file', ['accrued', $accrued]) }}" target="_blank">Скачать</a>
                        @endif
                        </td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    Действия
                                    <span class="caret"></span>
                                    <span class="sr-only">Действия</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="javascript:;" class="edit-tab" data-url="{{ route('admin.agent.tab.edit', ['accrued', $accrued]) }}" data-tab="accrued">Редактировать</a></li>
                                    <li><a href="{{ route('admin.agent.tab.delete', ['accrued', $accrued]) }}">Удалить</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <table class="table table-striped">
            <tbody>
                <center>
                    <h4>Записей нет</h4>
                </center>
            </tbody>
        </table>
    @endif

    <div class="form-row create-form-tab{{ !$errors->first('tab_date') && !$errors->first('tab_file') ? ' hidden' : '' }}" id="formCreateTabaccrued">
        @if (($errors->first('tab_date') || $errors->first('tab_file')) && session('tab_type') == $type)
            @include('admin.agents.partials.form-paidout', [
                'type' => $type
            ])
        @endif
    </div>
</div>
