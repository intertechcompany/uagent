<div class="form-group row col-md-12">
    <label for="{{ $type }}Date" class="col-sm-2 col-form-label">Дата</label>
    <div class="col-sm-10">
        <div class="input-append date" id="dp3" data-date="12-02-2012" data-date-format="y-m-d">
          <input type="text" class="form-control datepicker-tab" name="tab_date" id="{{ $type }}Date" value="{{ old('tab_date', !empty($tabmodel->date) ? $tabmodel->date : '' ) }}" placeholder="Дата">
          <span class="add-on"><i class="icon-th"></i></span>
        </div>
        @if ($errors->first('tab_date'))
            <small class="has-error">{{$errors->first('tab_date')}}</small>
        @endif
    </div>
</div>
<div class="form-group row col-md-12">
    <label for="{{ $type }}Sum" class="col-sm-2 col-form-label">Сума</label>
    <div class="col-sm-10">
        <input type="number" class="form-control" name="tab_sum" id="{{ $type }}Sum" value="{{ old('tab_sum', !empty($tabmodel->sum) ? number_format($tabmodel->sum, 2) : '') }}" placeholder="Сума">
        @if ($errors->first('tab_sum'))
            <small class="has-error">{{$errors->first('tab_sum')}}</small>
        @endif
    </div>
</div>
<div class="form-group row col-md-12">
    <label for="{{ $type }}File" class="col-sm-2 col-form-label">Документ</label>
    <div class="col-sm-10">
        <input type="file" name="tab_file" class="form-control-file" id="{{ $type }}File" value="{{ old('tab_file', !empty($tabmodel->file) ? $tabmodel->file : '') }}" accept=".xlsx,.xls,.doc, .docx,.ppt, .pptx,.txt,.pdf">
        @if (!empty($tabmodel->file))
            <p>Файл {{ basename($tabmodel->file, '/') }} загружен!</p>
        @endif
        @if ($errors->first('tab_file'))
            <small class="has-error">{{$errors->first('tab_file')}}</small>
        @endif
    </div>
</div>
<input type="hidden" name="tab_id" value="{{ !empty($tabmodel->id) ? $tabmodel->id : 0 }}">
<input type="hidden" name="tab_type" value="{{ $type }}">
