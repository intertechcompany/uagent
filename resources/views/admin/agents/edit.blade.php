@extends('layouts.admin')

{{--
@section('title')
    <i class="c-sidebar__icon feather icon-home"></i>Редактирование агента: {{ $agent->getFullNameAttribute() }}
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('/bower_components/datetimepicker/build/jquery.datetimepicker.min.css') }}">
@endsection
--}}

@section('content')
    <h1 class="a-content__title">
        Редактирование агента: <span class="a-all__text--pink">{{ $agent->getFullNameAttribute() }}</span>
    </h1>
    <agents-update data-agent-route="{{ route('admin.agent.edit.ajax', $agent) }}" id="agent-update-route"></agents-update>

    {{--
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Редактирование агента: {{ $agent->getFullNameAttribute() }}</h3>
        </div>
        <div class="box-body">
            <ul class="nav nav-tabs">
                <li{{ (!$errors->first('tab_date') && !$errors->first('tab_file')) || (session('alert.tab') !== 'paidout' && session('tab_type') !== 'paidout' && session('tab_type') !== 'accrued') ? ' class=active' : '' }}>
                    <a data-toggle="tab" href="#information">Информация</a>
                </li>
                <li{{ ($errors->first('tab_date') || $errors->first('tab_file')) && (session('alert.tab') === 'paidout' || session('tab_type') === 'paidout') ? ' class=active' : '' }}>
                    <a data-toggle="tab" href="#paid">Выплачено</a>
                </li>
                <li{{ ($errors->first('tab_date') || $errors->first('tab_file')) && (session('alert.tab') === 'accrued' || session('tab_type') === 'accrued') ? ' class=active' : '' }}>
                    <a data-toggle="tab" href="#accrued">Начислено</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#paymentInformation">Платежная информация</a>
                </li>
            </ul>
            <form method="post" action="{{ route('admin.agent.update',$agent->id) }}" enctype="multipart/form-data">
                <div class="tab-content">
                    <div id="information" class="tab-pane fade{{ !$errors->first('tab_date') && !$errors->first('tab_file') && (session('alert.tab') !== 'paidout' || session('alert.tab') !== 'accrued') ? ' in active' : '' }}">
                        {{ csrf_field() }}
                        {{ method_field('put') }}
                        <div class="form-group col-md-6">
                            <label for="name">Имя</label>
                            <input type="text" class="form-control" value="{{ old('name', $agent->name) }}" name="name" id="name" placeholder="Имя">
                            @if ($errors->first('name'))<small class="has-error">{{$errors->first('name')}}</small>@endif
                        </div>
                        <div class="form-group col-md-6">
                            <label for="last_name">Фамилия</label>
                            <input type="text" class="form-control" value="{{ old('last_name', $agent->last_name) }}" name="last_name" id="last_name" placeholder="Фамилия">
                            @if ($errors->first('last_name'))<small class="has-error">{{$errors->first('last_name')}}</small>@endif
                        </div>
                        <div class="form-group col-md-6">
                            <label for="phone">Телефон</label>
                            <input type="text" class="form-control" value="{{ old('phone', $agent->phone) }}" name="phone" id="phone" placeholder="Телефон">
                            @if ($errors->first('phone'))<small class="has-error">{{$errors->first('phone')}}</small>@endif
                        </div>
                        <div class="form-group col-md-6">
                            <label for="city">Город</label>
                            <select name="city_id" id="city" class="form-control">
                                <option value="0" selected>Выберите город</option>
                                @foreach($cities as $city)
                                    <option {{ (int) old('city_id',$agent->city_id) === (int) $city->id ? ' selected' : '' }} value="{{ $city->id }}">{{ $city->name }}</option>
                                @endforeach

                            </select>
                            @if ($errors->first('city_id'))
                                <small class="has-error">{{ $errors->first('city_id') }}</small>
                            @endif
                        </div>
                        <div class="form-group col-md-6">
                            <label for="city">Статус</label>
                            <select name="status" id="status" class="form-control">
                                <option value>Выберите статус</option>
                                @foreach($agent->statuses as $statusId => $status)
                                    <option {{ $agent->status === $statusId ? ' selected' : '' }} value="{{ $statusId }}">{{ $status }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="password">Новый пароль</label>
                            <input type="password" class="form-control"  name="password" id="password" placeholder="Пароль">
                            @if ($errors->first('password'))
                                <small class="has-error">{{$errors->first('password')}}</small
                            > @endif
                        </div>

                        <div class="form-group col-md-6">
                            <label for="worktime_from">Время работы от</label>
                            <div class="input-append date" data-date-format="H:i">
                                <input type="text" class="form-control datepicker-worktime" name="worktime_from" id="worktime_from" value="{{ old('worktime_from', $agent->fromTime) }}" placeholder="Дата">
                                <span class="add-on"><i class="icon-th"></i></span>
                            </div>
                            @if ($errors->first('worktime_from'))
                                <small class="has-error">{{ $errors->first('worktime_from') }}</small>
                            @endif
                        </div>
                        <div class="form-group col-md-6">
                            <label for="worktime_to">Время работы до</label>
                            <div class="input-append date" data-date-format="H:i">
                                <input type="text" class="form-control datepicker-worktime" name="worktime_to" id="worktime_to" value="{{ old('worktime_to', $agent->toTime) }}" placeholder="Дата">
                                <span class="add-on"><i class="icon-th"></i></span>
                            </div>
                            @if ($errors->first('worktime_to'))
                                <small class="has-error">{{ $errors->first('worktime_to') }}</small>
                            @endif
                        </div>

                        <div class="form-group col-md-6">
                            <div class="checkbox">
                                <label class="form-check-label" for="is_important">
                                    <input {{ (int) old('is_important', $agent->is_important) === 1 ? ' checked' : '' }} type="checkbox" value="1" name="is_important" class="form-check-input" id="is_important"> Важный
                                </label>
                            </div>
                            <div class="checkbox">
                                <label class="form-check-label" for="policy_subagents">
                                    <input {{ (int) old('is_policy_subagents',$agent->is_policy_subagents) === 1 ? ' checked' : '' }} type="checkbox" value="1" name="is_policy_subagents" class="form-check-input" id="policy_subagents"> Учитывать полисы субагентов
                                </label>
                            </div>
                            <div class="checkbox">
                                <label class="form-check-label" for="is_broker">
                                    <input {{ (int) old('is_broker',$agent->is_broker) === 1 ? ' checked' : '' }} type="checkbox" value="1" name="is_broker" class="form-check-input" id="is_broker"> Брокер
                                </label>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Страховые компании</label>
                            @foreach ($insurance as $key => $value)
                                <div>
                                    <input class="form-check-input position-static" id="insurance-{{ $key }}" type="checkbox" name="insurance[]" value="{{ $key }}"{{ in_array($key, $agent->getInsurance()) ? ' checked' : '' }}>
                                    <label for="insurance-{{ $key }}" class="cursor-pointer">{{ $value }}</label>
                                </div>
                            @endforeach
                        </div>
                        <div class="form-group col-md-6">
                            <label for="agentRates">Ставки</label>
                            <select name="rates[]" id="agentRates" class="form-control js-select2-rates" multiple="multiple">
                                @foreach ($rateInsurance as $key => $rates)
                                    @if (isset($insurance[$key]))
                                        <optgroup label="{{ $insurance[$key] }}">
                                            @foreach ($rates as $rate)
                                                <option title="{{ $insurance[$key] . ' ' . $rate->full_name }}" value="{{ $rate->id }}"{{ !empty(old('rates')) && is_array(old('rates')) && in_array($rate->id, old('rates')) ? ' selected' : sizeof($agentRatesIds) > 0 && in_array($rate->id, $agentRatesIds) ? ' selected' : '' }}>{{ $rate->full_name }}</option>
                                            @endforeach
                                        </optgroup>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        @if (Auth::user()->hasRole(['superadministrator', 'administrator']))
                            <div class="{{ count($curators) > 10 ? ' container' : ''}} form-group col-md-12">
                                <label>Кураторы</label>
                                @foreach ($curators as $curator)
                                    <div>
                                        <input id="curator-{{ $curator->id }}" type="checkbox" name="curators[]" value="{{ $curator->id }}"{{ sizeof(old('curators',[])) > 0 && in_array($curator->id, old('curators')) ? ' checked' : in_array($curator->id, $agentCuratorListIds) ? ' checked' : '' }}>
                                        <label class="cursor-pointer" for="curator-{{ $curator->id }}">{{ $curator->full_name }}</label>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <div id="paid" class="tab-pane fade{{ ($errors->first('tab_date') || $errors->first('tab_file')) && (session('alert.tab') === 'paidout' || session('tab_type') === 'paidout') ? ' in active' : '' }}">
                        @include('admin.agents.partials.paidouts', [
                            'type' => 'paidout',
                        ])
                    </div>
                    <div id="accrued" class="tab-pane fade{{ ($errors->first('tab_date') || $errors->first('tab_file')) && (session('alert.tab') === 'accrued' || session('tab_type') === 'accrued') ? ' in active' : '' }}">
                        @include('admin.agents.partials.accrued', [
                            'type' => 'accrued',
                        ])
                    </div>

                    <div id="paymentInformation" class="tab-pane fade">
                        <div class="form-group col-md-8">
                            <label for="number_cart">Номер карты</label>
                            <p>{{ $agent->paymentInformation ? $agent->paymentInformation->number_cart : '' }}</p>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="cart_date">Срок действия</label>
                            <p>{{ $agent->paymentInformation ? $agent->paymentInformation->cart_date : '' }}</p>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="number_qiwi">Киви кошелек</label>
                            <p>{{ $agent->paymentInformation ? $agent->paymentInformation->number_qiwi : '' }}</p>
                        </div>
                    </div>

                </div>
                <div class="form-group col-md-12">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            <ul>
                @foreach($logs as $log)
                    <li>
                        {{ $log->description }} - {{ $log->created_at->format('d-m-Y') }}
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
    --}}
@endsection

{{--
@section('scripts')
    <script src="{{ asset('/bower_components/datetimepicker/build/jquery.datetimepicker.full.min.js') }}"></script>
    <script src="{{ asset('/bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js') }}"></script>
    <script src="{{ asset('/bower_components/inputmask/dist/jquery.inputmask.bundle.js') }}"></script>
@endsection
--}}