@foreach($agents as $agent)
    <tr>
        <td>
            @if ($agent->is_important)
                <i class="fa fa-font-awesome" aria-hidden="true" style="color: red"></i>
            @endif
        </td>
        <td>
            <a href="{{ route('admin.dashboard.index') }}?filter[agents][]={{ $agent->id }}"
                class="fa fa-info"
                aria-hidden="true"
                title="Статистика" 
            ></a>
        </td>
        <td>{{ $agent->id }}</td>
        <td>-</td>
        <td>{{ $agent->email }}</td>
        <td>{{ optional($agent->city)->name }}</td>
        <td>{{ $agent->name }}</td>
        <td>{{ $agent->last_name }}</td>
        <td>{{ $agent->phone }}</td>
        <td>{{ sizeof($agent->subAgents) }}</td>
        <td>{{ $agent->polices->count() }}</td>
        <td>{{ $agent->status_name }}</td>
        <td>{{ $agent->created_at }}</td>
        <td>{{ $agent->formated_paidouts }} руб.</td>
        <td>{{ $agent->formated_accrued }} руб.</td>
        <td>{{ $agent->formated_balance }} руб.</td>
        <td>
            <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    Действия
                    <span class="caret"></span>
                    <span class="sr-only">Действия</span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="{{ route('admin.agent.edit',$agent->id) }}">Редактировать</a></li>
                </ul>
            </div>
        </td>
    </tr>
    @if (sizeof($agent->subAgents) > 0)
        @include('admin.agents.subaget', ['agents' => $agent->subAgents])
    @endif
@endforeach
