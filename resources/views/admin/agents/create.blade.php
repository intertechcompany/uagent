@extends('layouts.admin')

{{--
@section('title')
    <i class="c-sidebar__icon feather icon-home"></i>Создание агента
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('/bower_components/datetimepicker/build/jquery.datetimepicker.min.css') }}">
@endsection
--}}
@section('content')
    <h1 class="a-content__title">
        Создание агента
    </h1>
    <agents-create></agents-create>
    {{--
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Создание агента</h3>
        </div>
        <div class="box-body">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a data-toggle="tab" href="#information">Информация</a>
                </li>
            </ul>
            <form method="post" action="{{ route('admin.agent.store') }}" enctype="multipart/form-data">
                <div class="tab-content">
                    <div id="information" class="tab-pane fade active in">
                        {{ csrf_field() }}
                        <div class="form-group col-md-12">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" id="email"  placeholder="mail@gmail.com">
                            @if ($errors->first('email'))<small class="has-error">{{$errors->first('email')}}</small>@endif
                        </div>
                        <div class="form-group col-md-6">
                            <label for="name">Имя</label>
                            <input type="text" class="form-control" value="{{ old('name') }}" name="name" id="name" placeholder="Имя">
                            @if ($errors->first('name'))<small class="has-error">{{$errors->first('name')}}</small>@endif
                        </div>
                        <div class="form-group col-md-6">
                            <label for="last_name">Фамилия</label>
                            <input type="text" class="form-control" value="{{ old('last_name') }}" name="last_name" id="last_name" placeholder="Фамилия">
                            @if ($errors->first('last_name'))<small class="has-error">{{$errors->first('last_name')}}</small>@endif
                        </div>
                        <div class="form-group col-md-6">
                            <label for="phone">Телефон</label>
                            <input type="text" class="form-control" value="{{ old('phone') }}" name="phone" id="phone" placeholder="Телефон">
                            @if ($errors->first('phone'))<small class="has-error">{{$errors->first('phone')}}</small>@endif
                        </div>
                        <div class="form-group col-md-6">
                            <label for="city">Город</label>
                            <select name="city_id" id="city" class="form-control">
                                <option value="0" selected>Выберите город</option>
                                @foreach($cities as $city)
                                    <option {{ (int) old('city_id') === (int) $city->id ? ' selected' : '' }} value="{{ $city->id }}">{{ $city->name }}</option>
                                @endforeach

                            </select>
                            @if ($errors->first('city_id'))
                                <small class="has-error">{{ $errors->first('city_id') }}</small>
                            @endif
                        </div>
                        <div class="form-group col-md-6">
                            <label for="city">Статус</label>
                            <select name="status" id="status" class="form-control">
                                <option value>Выберите статус</option>
                                @foreach(\App\User::$statuses as $statusId => $status)
                                    <option value="{{ $statusId }}">{{ $status }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="password">Новый пароль</label>
                            <input type="password" class="form-control"  name="password" id="password" placeholder="Пароль">
                            @if ($errors->first('password'))
                                <small class="has-error">{{$errors->first('password')}}</small>
                            @endif
                        </div>

                        <div class="form-group col-md-6">
                            <label for="worktime_from">Время работы от</label>
                            <div class="input-append date" data-date-format="H:i">
                                <input type="text" class="form-control datepicker-worktime" name="worktime_from" id="worktime_from" value="{{ old('worktime_from') }}" placeholder="Дата">
                                <span class="add-on"><i class="icon-th"></i></span>
                            </div>
                            @if ($errors->first('worktime_from'))
                                <small class="has-error">{{ $errors->first('worktime_from') }}</small>
                            @endif
                        </div>
                        <div class="form-group col-md-6">
                            <label for="worktime_to">Время работы до</label>
                            <div class="input-append date" data-date-format="H:i">
                                <input type="text" class="form-control datepicker-worktime" name="worktime_to" id="worktime_to" value="{{ old('worktime_to') }}" placeholder="Дата">
                                <span class="add-on"><i class="icon-th"></i></span>
                            </div>
                            @if ($errors->first('worktime_to'))
                                <small class="has-error">{{ $errors->first('worktime_to') }}</small>
                            @endif
                        </div>

                        <div class="form-group col-md-6">
                            <div class="checkbox">
                                <label class="form-check-label" for="is_important">
                                    <input {{ (int) old('is_important') === 1 ? ' checked' : '' }} type="checkbox" value="1" name="is_important" class="form-check-input" id="is_important"> Важный
                                </label>
                            </div>
                            <div class="checkbox">
                                <label class="form-check-label" for="policy_subagents">
                                    <input {{ (int) old('is_policy_subagents') === 1 ? ' checked' : '' }} type="checkbox" value="1" name="is_policy_subagents" class="form-check-input" id="policy_subagents"> Учитывать полисы субагентов
                                </label>
                            </div>
                            <div class="checkbox">
                                <label class="form-check-label" for="is_broker">
                                    <input {{ (int) old('is_broker') === 1 ? ' checked' : '' }} type="checkbox" value="1" name="is_broker" class="form-check-input" id="is_broker"> Брокер
                                </label>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Страховые компании</label>
                            @foreach ($insurance as $key => $value)
                                <div>
                                    <input class="form-check-input position-static" id="insurance-{{ $key }}" type="checkbox" name="insurance[]" value="{{ $key }}">
                                    <label for="insurance-{{ $key }}" class="cursor-pointer">{{ $value }}</label>
                                </div>
                            @endforeach
                        </div>
                        <div class="form-group col-md-6">
                            <label for="agentRates">Ставки</label>
                            <select name="rates[]" id="agentRates" class="form-control js-select2-rates" multiple="multiple">
                                @foreach ($rateInsurance as $key => $rates)
                                    @if (isset($insurance[$key]))
                                        <optgroup label="{{ $insurance[$key] }}">
                                            @foreach ($rates as $rate)
                                                <option title="{{ $insurance[$key] . ' ' . $rate->full_name }}" value="{{ $rate->id }}"{{ \in_array($rate->id, old('rates', []),true) ? ' selected' : '' }}>{{ $rate->full_name }}</option>
                                            @endforeach
                                        </optgroup>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        @if (Auth::user()->hasRole(['superadministrator', 'administrator']))
                            <div class="{{ $curators->count() > 10 ? ' container' : ''}} form-group col-md-12">
                                <label>Кураторы</label>
                                @foreach ($curators as $curator)
                                    <div>
                                        <input id="curator-{{ $curator->id }}" type="checkbox" name="curators[]" value="{{ $curator->id }}"{{ \in_array($curator->id, old('curators', []),true) ? ' checked' : '' }}>
                                        <label class="cursor-pointer" for="curator-{{ $curator->id }}">{{ $curator->full_name }}</label>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>

                </div>
                <div class="form-group col-md-12">
                    <button type="submit" class="btn btn-primary">Создать</button>
                </div>
            </form>
        </div>
    </div>
    --}}
@endsection
{{--
@section('scripts')
    <script src="{{ asset('/bower_components/datetimepicker/build/jquery.datetimepicker.full.min.js') }}"></script>
    <script src="{{ asset('/bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js') }}"></script>
    <script src="{{ asset('/bower_components/inputmask/dist/jquery.inputmask.bundle.js') }}"></script>
@endsection
--}}