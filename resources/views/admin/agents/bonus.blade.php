@extends('layouts.admin')

@section('title')
    <i class="c-sidebar__icon feather icon-home"></i>Бонус агента: {{ $agent->getFullNameAttribute() }}
@endsection

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Бонус агента: {{ $agent->getFullNameAttribute() }}</h3>
        </div>
        <div class="box-body">
            <form method="post" action="{{ route('admin.agent.bonus.store',$agent->id) }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="type">Тип бонуса</label>
                    <select class="form-control" name="type" id="type">
                        <option value="rub">Руб.</option>
                        <option value="percent">Процент от полиса</option>
                    </select>
                </div>
                <div class="form-group">
                    <label  id="label-bonus" for="bonus">Добавить бонус (руб.)</label>
                    <input type="text" class="form-control" name="bonus" id="bonus" placeholder="Бонус">
                    @if ($errors->first('bonus'))<small class="has-error">{{$errors->first('bonus')}}</small>@endif
                </div>
                <button type="submit" class="btn btn-primary">Изменить</button>
            </form>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            <ul>
                @foreach($logs as $log)
                    <li>
                        {{ $log->description }} - {{ $log->created_at->format('d-m-Y') }}
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('#type').on('change',function () {
            var type = $('#type').val();
            if (type == 'percent') {
                $('#label-bonus').text('Введите процент от полиса');
                $('#bonus').attr('placeholder','%');
            } else {
                $('#label-bonus').text('Добавить бонус (руб.)');
                $('#bonus').attr('placeholder','Бонус');
            }
        });
    </script>
@endsection