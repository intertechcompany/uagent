@extends('layouts.admin')

@section('content')
    <h1 class="a-content__title">
        Редавктирование администратора
    </h1>
    <administrators-update :change-pass-url="'{{ route('admin.admins.changePass', ['user'=>$user]) }}'" data-admins-edit-route="{{ route('admin.admins.edit.ajax', $user) }}" id="admins-edit-route"></administrators-update>
    {{--
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Редактирование администратора: {{ $user->getFullNameAttribute() }}</h3>
        </div>
        <div class="box-body">
            <form method="post" action="{{ route('admin.admins.update',$user->id) }}">
                {{ csrf_field() }}
                {{ method_field('put') }}
                <div class="form-group">
                    <label for="password">Новый пароль</label>
                    <input type="password" class="form-control"  name="password" id="password" placeholder="Пароль">
                    @if ($errors->first('password'))<small class="has-error">{{$errors->first('password')}}</small>@endif
                </div>
                <div class="form-group">
                    <label for="last_name">Фамилия</label>
                    <input type="text" class="form-control" value="{{ $user->last_name }}" name="last_name" id="last_name" placeholder="Фамилия">
                    @if ($errors->first('last_name'))<small class="has-error">{{$errors->first('last_name')}}</small>@endif
                </div>
                <div class="form-group">
                    <label for="name">Имя</label>
                    <input type="text" class="form-control" value="{{ $user->name }}" name="name" id="name" placeholder="Имя">
                    @if ($errors->first('name'))<small class="has-error">{{$errors->first('name')}}</small>@endif
                </div>
                <div class="form-group">
                    <label for="phone">Телефон</label>
                    <input type="text" class="form-control" value="{{ $user->phone }}" name="phone" id="phone" placeholder="Телефон">
                    @if ($errors->first('phone'))<small class="has-error">{{$errors->first('phone')}}</small>@endif
                </div>
                <div class="form-group col-md-6">
                    <label for="city">Город</label>
                    <select name="city_id" id="city" class="form-control">
                        <option value="0" selected>Выберите город</option>
                        @foreach($cities as $city)
                            <option @if($city->id == $user->city_id) selected @endif value="{{ $city->id }}">{{ $city->name }}</option>
                        @endforeach

                    </select>
                    @if ($errors->first('city_id'))<small class="has-error">{{$errors->first('city_id')}}</small>@endif
                </div>
                <div class="form-group col-md-6">
                    @foreach($roles as $role)
                        <div class="form-group col-md-12">
                            <input id="role{{ $role->id }}" type="checkbox" name="roles[]" {{ $user->hasRole($role->name) ? ' checked' : '' }} value="{{ $role->id }}">
                            <label for="role{{ $role->id }}">{{ trans('general.roles.' . $role->name) }}</label>
                        </div>
                    @endforeach
                    @if ($errors->first('roles'))
                        <small class="has-error">{{$errors->first('roles')}}</small>
                    @endif
                </div>
                <div class="masonry-item col-md-12">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">

        </div>
    </div>
    --}}
@endsection