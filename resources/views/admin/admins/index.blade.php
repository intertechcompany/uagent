@extends('layouts.admin')

@section('content')
    <administrators data-admins-route="{{ route('admin.admins.load.ajax') }}" id="admins-load-route"></administrators>
@endsection
