<div id="overlay" class="hide"></div>
<div class="modal_div hide"  id="modal_in">
    <div class="modal_close" ></div>
    <div class="content-holder">
        <div class="image-holder">
            <img src="{{ asset('img/bg-in.png') }}" alt="">
        </div>
        <h3>Вход</h3>
        <form action="{{ route('login') }}" method="post">
            {{ csrf_field() }}
            @if ($errors->any())
                <span class="invalid-feedback">
                    <strong>{{ $errors->first() }}</strong>
                </span>
            @endif
            <div class="row">
                <div class="form-holder col-md-12">
                    <input type="text" name="email" value="{{ old('email') }}" placeholder="mail@gmail.com">
                </div>
                <div class="form-holder col-md-12">
                    <input type="password" name="password" placeholder="Пароль" >
                </div>
            </div>
            <button type="submit" class="submit btn btn-red">Отправить</button>
            <div>
                <a href="#" onclick="$('#modal_in').addClass('hide');$('#modal_reg').removeClass('hide');" class="open-reg">Регистрация</a>
            </div>
        </form>
    </div>
</div>

<div class="modal_div hide"  id="modal_reg">
    <div class="modal_close"></div>
    <div class="content-holder">
        <div class="image-holder">
            <img src="{{ asset('img/bg-reg.png') }}" alt="">
        </div>
        <h3>Регистрация</h3>
        @if ($errors->any())
            <span class="invalid-feedback">
                <strong>{{ $errors->first() }}</strong>
            </span>
        @endif
        <form action="{{ route('register') }}" method="post">
            {{ csrf_field() }}
            <div class="row">
                <div class="form-holder col-md-12">
                    <input type="text" name="name" value="{{ old('name') }}" placeholder="Имя" >
                </div>
                <div class="form-holder col-md-12">
                    <input type="text" name="last_name" value="{{ old('last_name') }}" placeholder="Фамилия" >
                </div>
                <div class="form-holder col-md-12">
                    @php($cities = \App\Model\City::query()->orderBy('name')->pluck('name', 'id'))
                    <select name="city_id">
                        <option value="">Выберите город</option>
                        @foreach($cities as $key => $city)
                            <option value="{{ $key }}">{{ $city }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-holder col-md-12">
                    <input type="email" name="email" value="{{ old('email') }}" placeholder="mail@gmail.com">
                </div>
                <div class="form-holder col-md-12">
                    <input type="text" name="phone" value="{{ old('phone') }}" placeholder="Номер телефона" >
                </div>
                <div class="form-holder col-md-12">
                    <input type="password" name="password" placeholder="Пароль" >
                </div>
                <div class="form-holder col-md-12">
                    <input type="password" name="password_confirmation" placeholder="Подтверждение пароля" >
                </div>
            </div>
            <button type="submit" class="submit btn btn-red">Отправить</button>
            <div>
                <a class="open-in" href="#" onclick="$('#modal_reg').addClass('hide');$('#modal_in').removeClass('hide');">Войти</a>
            </div>
        </form>
    </div>
</div>



