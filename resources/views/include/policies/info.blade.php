<div class="row block_text_info">
    <div class="col-12">
        <div class="c-card c-card__info">
            <h3 class="c-card__info--head">Персональная информация о собственнике</h3>
            <div class="row"></div>
            <div class="row">
                <div class="col-md-4">Фамилия:</div>
                <div class="col-md-8">{{ $owner->last_name }}</div>
            </div>
            <div class="row">
                <div class="col-md-4">Имя:</div>
                <div class="col-md-8">{{ $owner->first_name }}</div>
            </div>
            <div class="row">
                <div class="col-md-4">Отчество:</div>
                <div class="col-md-8">{{ $owner->middle_name }}</div>
            </div>
            <div class="row">
                <div class="col-md-4">Дата рождения:</div>
                <div class="col-md-8">{{ $owner->dob }}</div>
            </div>
            <div class="row">
                <div class="col-md-4">Серия паспорта:</div>
                <div class="col-md-8">{{ $owner->personal_document_serial }}</div>
            </div>
            <div class="row">
                <div class="col-md-4">Номер паспорта:</div>
                <div class="col-md-8">{{ $owner->personal_document_number }}</div>
            </div>
            <div class="row">
                <div class="col-md-4">Дата выдачи паспорта:</div>
                <div class="col-md-8">{{ $owner->personal_document_date }}</div>
            </div>
            <div class="row">
                <div class="col-md-4">Кем выдан:</div>
                <div class="col-md-8">{{ $owner->personal_document_issued_by }}</div>
            </div>
            <div class="row">
                <div class="col-md-4">Место рождения:</div>
                <div class="col-md-8">{{ $owner->place_of_birth }}</div>
            </div>
            <div class="row">
                <div class="col-md-4">Адрес:</div>
                <div class="col-md-8">{{ $owner->address }}</div>
            </div>
            <div class="row">
                <div class="col-md-4">Дом:</div>
                <div class="col-md-8">{{ $owner->house }}</div>
            </div>
            <div class="row">
                <div class="col-md-4">Корпус:</div>
                <div class="col-md-8">{{ $owner->address2 }}</div>
            </div>
            <div class="row">
                <div class="col-md-4">Квартира:</div>
                <div class="col-md-8">{{ $owner->address3 }}</div>
            </div>

            <div class="row">
                <div class="col-md-4">Телефон:</div>
                <div class="col-md-8">{{ $policy->phone }}</div>
            </div>

            <div class="row">
                <div class="col-md-4">E-Mail:</div>
                <div class="col-md-8">{{ $policy->email }}</div>
            </div>
        </div>

        <div class="c-card c-card__info">
            <h3 class="c-card__info--head">Информация о полисе</h3>
            <div class="row"></div>

            <div class="row">
                <div class="col-md-4">Электронный полис:</div>
                <div class="col-md-4">@if($policy->is_electronic === 1) Да @else Нет @endif</div>
            </div>
            <div class="row">
                <div class="col-md-4">Дата начала полиса:</div>
                <div class="col-md-4">{{ $policy->policy_start_date }}</div>
            </div>
            @if ($policy->insurance_id !== null)
                <div class="row">
                    <div class="col-md-4">Премия:</div>
                    <div class="col-md-4">{{ $policy->premium }}</div>
                </div>
                <div class="row">
                    <div class="col-md-4">{{ ucfirst($policy->insurance) }} ID:</div>
                    <div class="col-md-4">{{ $policy->insurance_id }}</div>
                </div>
                <div class="row">
                    <div class="col-md-4">{{ ucfirst($policy->insurance) }} Number:</div>
                    <div class="col-md-4">{{ $policy->insurance_number }}</div>
                </div>
            @endif
        </div>

        <div class="c-card c-card__info">
            <h3 class="c-card__info--head">Информация о ТС </h3>
            <div class="row"></div>
            <div class="row">
                <div class="col-md-4">Номер карты:</div>
                <div class="col-md-8">{{ $policy->diagnostic_number }}</div>
            </div>

            <div class="row">
                <div class="col-md-4">Срок действия с:</div>
                <div class="col-md-8">{{ $policy->diagnostic_until_date }}</div>
            </div>
            <div class="row">
                <div class="col-md-4">Водители:</div>
                <div class="col-md-8">@if($policy->driver_count === 'specified')1 и более @else Неограниченное количество @endif</div>
            </div>
            <div class="row">
                <div class="col-md-4">Марка:</div>
                <div class="col-md-8">{{ $policy->make->name }}</div>
            </div>
            <div class="row">
                <div class="col-md-4">Модель:</div>
                <div class="col-md-8">{{ $policy->model->name }}</div>
            </div>
            <div class="row">
                <div class="col-md-4">Мощность, л.с:</div>
                <div class="col-md-8">{{ $policy->power }}</div>
            </div>
            <div class="row">
                <div class="col-md-4">Дата выдачи документа:</div>
                <div class="col-md-8">{{ $policy->vehicle_document_date }}</div>
            </div>
            <div class="row">
                <div class="col-md-4">Документ на автомобиль:</div>
                <div class="col-md-8">Номер: {{ $policy->vehicle_document_number }}, серия: {{ $policy->vehicle_document_serial }}</div>
            </div>
        </div>

        <div class="c-card c-card__info">
        
            <h3 class="c-card__info--head">@if($policy->vehicle_document_type === 'sts') СТС @else ПТС @endif </h3>
            <div class="row"></div>
            <div class="row">
                <div class="col-md-4">Тип идентификаторп ТС:</div>
                <div class="col-md-8">{{ $policy->getVehicleType() }}</div>
            </div>
            <div class="row">
                <div class="col-md-4">Идентификатор ТС:</div>
                <div class="col-md-8">{{ $policy->vehicle_id }}</div>
            </div>
            <div class="row">
                <div class="col-md-4">Гос. номер авто:</div>
                <div class="col-md-8">{{ $policy->vehicle_plate_number }}</div>
            </div>
            <div class="row">
                <div class="col-md-4">Регион:</div>
                <div class="col-md-8">{{ $policy->vehicle_plate_region }}</div>
            </div>
            <div class="row">
                <div class="col-md-4">Год выпуска:</div>
                <div class="col-md-8">{{ $policy->vehicle_registration_year }}</div>
            </div>
            <div class="row">
                <div class="col-md-4">Используется с прицепом:</div>
                <div class="col-md-8">@if($policy->is_vehicle_use_trailer === 1) Да @else Нет @endif</div>
            </div>
        </div>
        
        @if($policy->driver_count === 'specified')
            <div class="c-card c-card__info">
                <h3 class="c-card__info--head">Информация о водителях</h3>
                <div class="row"></div>
                @foreach($policy->drivers as $key => $value)
                    <div class="row">
                        <div class="col-md-4">Фамилия</div>
                        <div class="col-md-4">{{ $value->last_name }}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">Имя</div>
                        <div class="col-md-4">{{ $value->first_name }}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">Отчество</div>
                        <div class="col-md-4">{{ $value->middle_name }}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">Дата рождения</div>
                        <div class="col-md-4">{{ $value->dob }}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">Водительское удостоверение</div>
                        <div class="col-md-4">Серия: {{ $value->document_serial }} номер: {{ $value->document_number }}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">Стаж с</div>
                        <div class="col-md-4">{{ $value->start_date }}</div>
                    </div>
                @endforeach
            </div>
        @endif
    </div>

</div>