<aside class="c-sidebar">
    <div class="c-sidebar__brand">
        <a class="c-sidebar__logo" href="{{ route('home') }}"><img src="{{ asset('/img/top-logo.png') }}"></a>
        <a
            href="javascript:;"
            class="c-sidebar-btn js-sidebar-btn"
            id="leftMSlideMenu"
            data-url="{{ route('agent.menu.fixed', 'agent-left-menu') }}"
        >
            <div class="wrap-img__mnu">
                <img src="{{ '/img/mnu.png' }}" alt="nav">
            </div>
        </a>
    </div>

    <div class="c-sidebar__account">
        <div class="account-img">
            <img src="{{ auth()->user()->avatar ? asset('storage/app/avatar/'.auth()->user()->avatar) : asset('/img/default_avatar.png') }}" alt="{{ auth()->user()->full_name }}">
        </div>
        <div class="account-info">
            <div class="account-name custom-modal-js" data-modal="js-settings__account">
                <a href="#">{{ auth()->user()->full_name }}</a>
            </div>
            <div class="account-email">
                {{ auth()->user()->email }}
            </div>
        </div>

    </div>

    <div class="c-sidebar__body">
        <ul class="c-sidebar__list">
            <li class="{{ Nav::isRoute('home') }}">
                <a class="c-sidebar__link " href="{{ route('home') }}">
                    <img src="{{ asset('/img/icons/icons-nav/home' . Nav::hasSegment(['home'], 1, '_active') . '.svg') }}" alt="{{ trans('general.home') }}">{{ trans('general.home') }}
                </a>
            </li>
            @if (auth()->user()->isBroker() === false)
                <li class="{{ Nav::hasSegment('policies')}}">
                    <a class="c-sidebar__link" href="{{ route('policies') }}">
                        <img src="{{ asset('/img/icons/icons-nav/polices' . (Nav::hasSegment(['policies'], 1) == 'active' ? '_active.svg' : '.png')) }}" alt="home">{{ trans('general.policies') }}
                    </a>
                </li>
            @endif
            <li class="{{ Nav::isRoute('statistic') }}">
                <a class="c-sidebar__link" href="{{ route('statistic') }}">
                    <img src="{{ asset('/img/icons/icons-nav/' . (Nav::hasSegment(['statistic'], 1) == 'active' ? 'statistic_active.svg' : 'statistc.png')) }}" alt="{{ trans('general.statistic') }}">{{ trans('general.statistic') }}
                </a>
            </li>
            <li class="{{ Nav::isRoute('withdraws')}}">
                <a class="c-sidebar__link" href="{{ route('withdraws') }}">
                    <img src="{{ asset('/img/icons/icons-nav/payments' . (Nav::hasSegment(['withdraws'], 1) == 'active' ? '_active.svg' : '.png')) }}" alt="home">{{ trans('general.withdraws') }}
                </a>
            </li>
        </ul>
    </div>

    <div class="c-sidebar__footer c-sidebar__description">
        <p>(c){{ date('Y') }} Uagent | Единая платформа для страховых брокеров и агентов</p>
    </div>
</aside>
