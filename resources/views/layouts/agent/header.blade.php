<header class="c-navbar">
    <button class="c-sidebar-toggle js-sidebar-toggle">
        <i class="feather icon-align-left"></i>
    </button>
    <div class="modal-curators">

    </div>
    <div class="head-navbar">
        <ul class="head-navbar__flex">
            <li>
                <a data-info="{{ '+' . auth()->user()->calcNewAgents() }}" class="" id="counter" href="{{ route('agents') }}">
                    <img  class="navbar-icons my-agents__inf" src="{{ asset('/img/icons/agents.png') }}" alt="agents">
                    Мои Агенты
                </a>
            </li>
            <li>
                <a href="{{ route('kv') }}">
                    <img class="navbar-icons" src="{{ asset('/img/icons/kv.png') }}" alt="kv">
                    Моё КВ
                </a>
            </li>
            <li>
               <a  class="open-modal__curators custom-modal-js" data-modal="modal-curators" href="#">
                   <img style="max-width: 24px;" class="navbar-icons" src="{{ asset('/img/icons/kurators.png') }}" alt="curators">
                   Кураторы
               </a>
            </li>
            <li>
               <a href="{{ route('settings') }}"><i class="c-sidebar__icon feather icon-settings" ></i>Настройки</a>
            </li>
            <li>
                <form action="/search" method="get">
                    <input class="head-navbar__search" type="search" placeholder="Найти" name="term" value="{{ $term ?? null }}">
                    <input class="head-navbar__submit" type="submit" value="">
                </form>
            </li>
        </ul>
        <ul class="head-items__last">
            <li>
                <a data-support="{{ auth()->user()->countUnreadMessages() }}" class="tech-support__link" href="{{ route('support') }}">Тех. Поддержка</a>
            </li>
            <li>
                <span class="balance">Баланс: </span><a class="open-modal__balance custom-modal-js" href="#" data-modal="modal-balance">{{ auth()->user()->balance }}р.</a>
            </li>
            <li>
                @php($stocks = auth()->user()->stocks())
                <a class="btn-notifications custom-modal-js" data-modal="notification-modal" href="javascript:;" data-number="{{ $stocks->count() }}">
                    <img class="btn-notifications__img" src="{{ asset('/img/bell.png') }}" alt="bell" >
                </a>
                <div class="modal-notifications" id="notification-modal">
                    @if ($stocks->count() === 0)
                    <div class="modal-notifications__none">
                        no notifications
                        <a href="#" class="c-sidebar__none-close custom-modal-js" data-modal="notification-modal"></a>
                    </div>
                    @else
                    @foreach ($stocks as $stock)
                        <div class="modal-notifications__generate">
                            <h4 class="modal-notificationsl__h4">{{ $stock->title }}</h4>
                            <p class="modal-notifications__text">{{ $stock->text }}</p>
                            <a href="#" class="modal-notifications__close js-close__notifications-modal"></a>
                            @if ($stock->information)
                                <div class="modal-notificationsl__info-hover">
                                    <i class="modal-notifications__info feather icon-info"></i>
                                    <div class="modal-notifications__info-text">{{ $stock->information }}</div>
                                </div>
                            @endif
                        </div>
                    @endforeach
                    @endif
                </div>  
            </li>
        </ul>
    </div>
</header>
