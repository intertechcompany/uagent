@extends('base')

@section('head')
    <link rel="stylesheet" href="/node_modules/semantic-ui-step/step.css">
    <link rel="stylesheet" href="{{ asset('css/build.css') }}">

    <!-- add for vue -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
    {{--Пiдключено FontAwesome через CDN /--}}
@endsection

@section('body')

    <div class="modal-wrap">

    
    <div class="o-page">
        <div class="o-page__sidebar js-page-sidebar {{ request()->session()->get('agent-left-menu') ? 'open' : 'close' }}">
            @include('layouts.agent.nav')
        </div>

        <main class="o-page__content create-mw {{ request()->session()->get('agent-left-menu') ? 'open' : 'close' }}">

            @include('layouts.agent.header')

            <div class="container">

                @if(session()->has('success'))
                <div class="c-alert c-alert--success u-mb-medium">
                    <span class="c-alert__icon">
                      <i class="feather icon-check"></i>
                    </span>

                    <div class="c-alert__content">
                        <h4 class="c-alert__title">{{ session()->get('success') }}</h4>
                    </div>
                </div>
                @endif

                @if(session()->has('error'))
                    <div class="c-alert c-alert--warning u-mb-medium">
                        <span class="c-alert__icon">
                          <i class="feather icon-alert-triangle"></i>
                        </span>

                        <div class="c-alert__content">
                            <h4 class="c-alert__title">{{ session()->get('error') }}</h4>
                        </div>
                    </div>
                @endif

                @yield('content')

            </div>
            <footer class="c-footer">
                <p></p>
                <span class="c-footer__divider"></span>
            </footer>
        </main>
    </div>

    <div id="js-settings__account" class="little-modal modal-js">
        <div class="wrap-border__bottom">
            <a href="{{ route('settings') }}" class="settings-account">Настройки аккаунта</a>
        </div>
        <div class="ttu">
            <a href="{{ route('logout') }}" class="account-exit">выйти <i class="feather icon-settings"></i></a>
        </div>
    </div>

    {{--
    <div class="dark-modal dark-modal__kw little-modal modal-js" id="modal-kv">
        @foreach(\App\Model\Policy::insurance() as $insurance => $insuranceName)
            @php($insuranceKv = auth()->user()->getKv($insurance))
            @php($insuranceCities = auth()->user()->getCityRate($insurance))

            @if ($insuranceKv !== null || $insuranceCities !== null)
            <div class="wrap-border__bottom">
                <h6>{{ $insuranceName }}:</h6>

                @foreach($insuranceCities as $rateCity)
                    <div class="line-2 mw-260">
                        <p class="c-grey">{{ $rateCity->city->name }}:</p>
                        <p>{{ number_format($rateCity->value,2) }}<span class="c-pink"> {{ $rateCity->type === 'percent' ? '%' : '₽' }}</span></p>
                    </div>
                @endforeach

                @if ($insuranceKv !== null)
                    <div class="line-2 mw-260">
                        <p class="c-grey ">Другие регионы:</p>
                        <p>{{ number_format($insuranceKv->getValue(),2) }}<span class="c-pink"> {{ $insuranceKv->getType() === 'percent' ? '%' : '₽' }}</span></p>
                    </div>
                @endif
            </div>
            @endif
        @endforeach
    </div>
    --}}
    <div class="dark-modal dark-modal__kurator little-modal modal-js" id="modal-curators">

        @forelse(auth()->user()->getCurators() as $curator)
            <div class="item-1">
                <div class="wrap-border__bottom">
                    <div class="line-1">
                        <h6>Ваш куратор: {{ $curator->full_name }}</h6>
                        <p class="c-pink"></p>
                    </div>
                </div>
                <div class="wrap-border__bottom">
                    <div class="line-1">
                        <h6>Телефон: {{ $curator->phone }}</h6>
                        <p class="c-pink"> <span class="wrap-i"><a href="#" class="c-pink"><i class="fab fa-viber"></i></a><a class="c-pink" href="#"><i class="fab fa-whatsapp"></i></a><a class="c-pink" href="#"><i class="fab fa-telegram-plane"></i></a>  </span> <span class="c-grey"></span></p>
                    </div>
                </div>
                <div class="wrap-border__bottom">
                    <div class="line-1">
                        <h6>E-mail: {{ $curator->email }}</h6>
                        <p class="c-grey"></p>
                    </div>
                </div>
            </div>
        @empty
            <div class="curator-not-found">
                <span>Куратор не назначен. По всем вопросам обращайтесь по телефону:</span>
                <a class="phone-curator-default" href="tel:88006007197">8 800 600 71 97</a>
            </div>
        @endforelse
    </div>

    {{--MODAL-3--}}
    <div class="dark-modal dark-modal__balance little-modal modal-js" id="modal-balance">
        <div class="wrap-border__bottom">
            <div class="line-1">
                <h6>Баланс:</h6>
                <p>{{ auth()->user()->balance }} ₽</p>
            </div>
        </div>
        <div class="wrap-border__bottom">
            <div class="line-1">
                <h6>Доход за месяц:</h6>
                <p> {{ auth()->user()->getIncomeForMonth() }}</p>
            </div>
        </div>
        <div class="wrap-border__bottom">
            <div class="line-1">
                <a href="{{ route('withdraws-funds') }}">Вывести средства</a>
            </div>
        </div>
    </div>
    
    </div>   
@endsection
