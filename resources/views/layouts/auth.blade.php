@extends('base')

@section('head')

@endsection

@section('body')

    <div class="o-page o-page--center">
        <div class="o-page__card">

            <img style="padding: 20px" src="/img/logo.png">

            @yield('content')
        </div>
    </div>
@endsection