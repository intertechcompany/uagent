<!-- Sidebar Menu -->
<div class="a-sidebar">
    <div class="a-sidebar__brand">
        <a href="{{ url('admin/home') }}" class="a-sidebar__logo">
            <img src="{{ asset('img/top-logo.png') }}">
        </a>
        <a
            href="javascript:;"
            class="a-sidebar-btn js-sidebar-btn"
            id="leftMSlideMenu"
            data-url="{{ route('admin.menu.fixed', 'admin-left-menu') }}"

        >
            <div class="wrap-img__mnu">
                <img src="{{ asset('img/mnu.png') }}" alt="nav">
            </div>
        </a>
    </div>
    <div class="a-sidebar__account">
        <div class="account-info">
            <div data-modal="js-settings__account" class="account-name custom-modal-js">
                <a data-modal="js-settings__account" href="#">{{ auth()->user()->full_name }}</a>
            </div>
            <div class="account-email">
                {{ auth()->user()->email }}
            </div>
        </div>
    </div>
    <div class="a-sidebar__body">
        <ul class="a-sidebar__list ">
            <li{{ Nav::hasSegment(['home'], 2, ' class=active') }}>
                <a class="a-sidebar__link " href="{{ url('admin/home') }}">
                    <img class="icons-nav" src={{ asset ('img/icons/icons-nav/home' . (Nav::hasSegment(['home'], 2, '_active')) . '.svg') }} alt="{{ trans('general.home') }}">
                    <span>{{ trans('general.home') }}</span>
                </a>
            </li>
            <li{{ Nav::hasSegment(['agents'], 2, ' class=active') }}>
                <a class="a-sidebar__link" href="{{ url('admin/agents') }}">
                    <img class="icons-nav" src={{ asset ('img/icons/icons-nav/agents' . (Nav::hasSegment(['agents'], 2, '_active')) . '.svg') }} alt="{{ trans('general.agents') }}">
                    <span>{{ trans('general.agents') }}</span>
                </a>
            </li>
            @if (Auth::user()->hasRole(['superadministrator', 'administrator']))
                <li{{ Nav::hasSegment(['curators'], 2, ' class=active') }}>
                    <a class="a-sidebar__link" href="{{ url('admin/curators') }}">
                        <img class="icons-nav" src={{ asset ('img/icons/icons-nav/curators' . (Nav::hasSegment(['curators'], 2, '_active')) . '.svg') }} alt="{{ trans('general.curators') }}">
                        <span>{{ trans('general.curators') }}</span>
                    </a>
                </li>
                <li{{ Nav::hasSegment(['rates'], 2, ' class=active') }}>
                    <a class="a-sidebar__link" href="{{ url('admin/rates') }}">
                        <img class="icons-nav" src={{ asset ('img/icons/icons-nav/bets' . (Nav::hasSegment(['rates'], 2, '_active')) . '.svg') }} alt="{{ trans('general.rates') }}">
                        <span>{{ trans('general.rates') }}</span>
                    </a>
                </li>
                {{--<li{{ Nav::hasSegment(['news'], 2, ' class=active') }}>
                    <a class="a-sidebar__link" href="{{ route('html',['dir' => 'news','page' => 'news']) }}">
                        <img class="icons-nav" src={{ asset ('img/icons/icons-nav/news' . (Nav::hasSegment(['news'], 2, '_active')) . '.svg') }} alt="{{ trans('general.news') }}">
                        <span>{{ trans('general.news') }}</span>
                    </a>
                </li>
                <li{{ Nav::hasSegment(['stocks'], 2, ' class=active') }}>
                    <a class="a-sidebar__link" href="{{ route('admin.stocks.index') }}">
                        <img class="icons-nav" src={{ asset ('img/icons/icons-nav/stock' . (Nav::hasSegment(['stocks'], 2, '_active')) . '.svg') }} alt="{{ trans('general.stocks') }}">
                        <span>{{ trans('general.stocks') }}</span>
                    </a>
                </li>--}}
                <li{{ Nav::hasSegment(['withdraws'], 2, ' class=active') }}>
                    <a class="a-sidebar__link" href="{{ route('admin.withdraws.index') }}">
                        <img class="icons-nav" src={{ asset ('img/icons/icons-nav/withdraws' . (Nav::hasSegment(['withdraws'], 2, '_active')) . '.svg') }} alt="{{ trans('general.withdraws') }}">
                        <span>{{ trans('general.withdraws') }}</span>
                    </a>
                </li>
            @endif
            @if (Auth::user()->hasRole(['superadministrator']))
                <li{{ Nav::hasSegment(['admins'], 2, ' class=active') }}>
                    <a class="a-sidebar__link" href="{{ route('admin.admins.index') }}">
                        <img class="icons-nav" src={{ asset ('img/icons/icons-nav/admins' . (Nav::hasSegment(['admins'], 2, '_active')) . '.svg') }} alt="{{ trans('general.administrators') }}">
                        <span>{{ trans('general.administrators') }}</span>
                    </a>
                </li>
                <li{{ Nav::hasSegment(['support'], 2, ' class=active') }}>
                    <a class="a-sidebar__link a-sidebar__link--support" href="{{ url('admin/support') }}">
                        <img class="icons-nav" src="{{ asset ('img/icons/icons-nav/circulation' . (Nav::hasSegment(['support'], 2, '_active')) . '.svg') }}" alt="Обращения">
                        <span class="" id="admin-support" support-message="{{ auth()->user()->countUnreadMessages() }}">Обращения</span>
                    </a>
                </li>
            @endif

        </ul>
    </div>
</div>
<!-- /.sidebar-menu -->
