<div class="col-md-3">
    <div class="c-state-card c-state-card--success">
        <h4 class="c-state-card__title" style="min-height: 40px;">{{ $dashboardblock->name }}</h4>
        <span class="c-state-card__number">
            <i class="c-sidebar__icon feather icon-file"></i>{{ $dashboardblock->getResult() }}
        </span>
    </div>
</div>