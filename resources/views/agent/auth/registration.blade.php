<div class="home-new__modal register-modal__new modal-js" id="modal-register">
    <a href="#" class="close-new__modal js-close__local-modal"></a>
    <form class="register-form" id="register-form" action="{{ route('register') }}" method="post">
        {{ csrf_field() }}
        <div class="title-popup__new">
            Регистрация
        </div>
        <div class="field-error"></div>
        <div class="row-choice">
            <label class="container-b">Телефон
                <input class="rfield form-type" type="radio" id="contact-choice12" name="fileType" value="1"
                       checked="checked">
                <span class="checkmark"></span>
            </label>
            <span class="c-pink__new">или</span>
            <label class="container-b">E-mail
                <input class="rfield form-type" type="radio" id="contact-choice22" name="fileType" value="2">
                <span class="checkmark"></span>
            </label>
        </div>


        <div class="wrap-input__new contact-choice2 open" id="form-reg-email" style="display: none">
            <input type="email" class="form-new__input popup-new__input" name="email" id="reg_email"
                   placeholder="E-mail">
            <input type="password" class="form-new__input popup-new__input" name="reg_password" id="reg_password"
                   placeholder="Пароль">
            <input type="password" class="form-new__input popup-new__input" name="reg_password_confirmation"
                   id="reg_password_confirmation" placeholder="Повторите пароль">
        </div>

        <div class="wrap-input__new contact-choice1 open" id="form-reg-phone" >
            <input type="text" class="form-new__input popup-new__input tell-new get-phone-success" name="phone"
                   placeholder="Телефон">
            <div class="row-btn row-btn__flex-j">
                <button type="button" class="register h-btn__link phone-success">Подтвердить телефон</button>
                <input type="text" class="form-new__input popup-new__input popup-new__input--accept close" name="password"
                       placeholder="Пароль из смс">
            </div>
        </div>


        <div class="row-btn">
            <div class="c-choice c-choice--checkbox">
                <input checked type="checkbox" id="i-true" name="i-true" value="1" class="c-choice__input">
                <label for="i-true" class="c-choice__label">Согласен с &nbsp; <a href="#" class="h-btn__link custom-modal-js" data-modal="terms-of__use">политика
                        конфиденциальности</a></label>
            </div>
        </div>
        <div class="row-btn">
            <button type="button" class="h-btn h-btn__pink btn-registe g-recaptcha" data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}" data-callback='onSubmit3'>
                Регистрация
            </button>
            <button type="button" class="register h-btn__link custom-modal-js" data-modal="modal-sign">Войти</button>
        </div>
    </form>
</div>


