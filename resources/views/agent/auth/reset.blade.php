<div class="home-new__modal modal-js pass-recovery" id="modal-pass-recovery" >
    <a href="#" class="close-new__modal js-close__local-modal"></a>
    <div class="title-popup__new">
        Восстановление
        пароля
    </div>
    <div class="field-error"></div>
    <form action="{{route('resetSend')}}" method="post" id="form-recovery-pass">
        {{csrf_field()}}
        <div class="row-choice">
            <label class="container-b">Телефон
                <input class="rfield form-type" type="radio" id="contact-choice13" name="fileType" value="1" checked="checked">
                <span class="checkmark"></span>
            </label>
            <span class="c-pink__new">или</span>
            <label class="container-b">E-mail
                <input class="rfield form-type" type="radio" id="contact-choice23" name="fileType" value="2">
                <span class="checkmark"></span>
            </label>
        </div>
        <div class="wrap-input__new">
            <input type="text" class="form-new__input popup-new__input tell-new contact-choice1 open" name="phone" id="reset-pass__phone" placeholder="Телефон">
            <input type="email" class="form-new__input popup-new__input contact-choice2 open" name="email" id="reset-pass__email" style="display: none" placeholder="E-mail">
        </div>

        <div class="row-btn">
            <button type="button" class="h-btn h-btn__pink btn-recovery__pass g-recaptcha" data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}" data-callback='onSubmit'>
                Восстановить пароль
            </button>
            <button type="button" class="register h-btn__link custom-modal-js" data-modal="modal-sign">Войти</button>
        </div>
    </form>
</div>