<div class="home-new__modal modal-js" id="modal-sign" >
    <form action="{{route('login')}}" method="POST" id="formLogin" onsubmit="event.preventDefault()">
        {{ csrf_field() }}
        <a href="#" class="close-new__modal js-close__local-modal"></a>
        <div class="title-popup__new">
            Войти
        </div>
        <div class="field-error"></div>

        <div class="row-choice">
            <label class="container-b">Телефон
                <input class="rfield form-type" type="radio" id="contact-choice15" name="fileType" value="1" checked>
                <span class="checkmark"></span>
            </label>

            <span class="c-pink__new">или</span>
            <label class="container-b">E-mail
                <input class="rfield form-type"  type="radio" id="contact-choice25" name="fileType" value="2">
                <span class="checkmark"></span>
            </label>
        </div>
        <div class="w-all" id="login-form">
            <div class="wrap-input__new" id="form-auth-phone">
                <input type="text" class="form-new__input popup-new__input tell-new contact-choice1 open" name="phone" placeholder="Телефон">
                <input type="password" class="form-new__input popup-new__input contact-choice1 open" name="password" id="password" placeholder="Пароль">
            </div>
            <div class="wrap-inut_new2" id="form-auth-email" style="display: none">
                <input type="email" class="form-new__input popup-new__input contact-choice2 open" name="email" id="email" placeholder="E-mail">
                <input type="password" class="form-new__input popup-new__input contact-choice2 open" name="password2" id="password2" placeholder="Пароль">
            </div>
        </div>
        <div class="row-btn">
            <div class="c-choice c-choice--checkbox">
                <input type="checkbox" id="save-me" name="save-me" value="1" class="c-choice__input">
                <label for="save-me" class="c-choice__label">Запомнить меня</label>
            </div>
            <button type="button" class="register h-btn__link custom-modal-js" data-modal="modal-register">Регистрация</button>
        </div>
        <div class="row-btn">
            <button tabindex="-1" type="button" class="h-btn h-btn__pink btn-login g-recaptcha" data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}" data-callback='onSubmit'>Войти</button>
            <button type="button" data-modal="modal-pass-recovery" class="register h-btn__link custom-modal-js">Забыли пароль?</button>
        </div>
    </form>
</div>
