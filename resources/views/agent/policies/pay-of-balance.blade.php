@extends('layouts.agent')

@section('title')
    <i class="c-sidebar__icon feather icon-file"></i>Оплатить с баланса
@endsection

@section('content')
        <div class="container">
            <div class="home-container pay-of__balance--body">
                <h3 class="c-content__h3">Оплатить с баланса</h3>
                <div class="container-withdrawal">
                    <div class="withdrawal-header">
                        <span>Реквизиты для вывода</span>
                    </div>
                    <div class="withdrawal-body">
                        <form action="{{ route('policies.payment.submit', ['policy'=>$policy]) }}" method="post">
                            {!! csrf_field() !!}
                            <div class="row-withdrawal">
                                <span class="bold">Передайте ссылку на оплату клиенту</span>
                                <i class="fas fa-edit"></i>
                            </div>
                            <div class="row-withdrawal__input">
                                <div class="input-title">Url адрес</div>
                                <div class="wrap-input">
                                    <input class="c-input input-copy" type="text" value="{{ $policy->payment->formUrl }}" placeholder="https://uagent.online/" readonly>
                                    <a class="copy-to"><i class="far fa-copy"></i></a>
                                </div>
                            </div>
                            <div class="row-withdrawal__input">
                                <div class="input-title">К оплате</div>
                                <div class="wrap-card-input">
                                    <input class="c-input input-with__pay" name="amount" type="number" placeholder="Сумма к оплате" value="{{ $policy->premium }}" readonly>
                                    <img src="/img/icons/p-money.png" alt="P" class="p-money" style="width: 13px; height: 16px;">
                                    <div class="text-grey">Комиссия согласно
                                        платежной системы</div>
                                </div>
                            </div>
                            <button class="c-btn c-btn--success">
                                Оплатить
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
@endsection