@extends('layouts.agent')

@section('title')
    <i class="c-sidebar__icon feather icon-file"></i>Создать полис
@endsection

@section('content')
    <re-calc :policyid="{{ $policy->id}}"
             :makeid="{{ $policy->make_id}}"
    ></re-calc>
@endsection
