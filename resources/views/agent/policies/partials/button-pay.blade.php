@if ($policy->insurance == 'rgs' || $policy->is_verify_phone_rgs == false)
    @if ($policy->status === 'paid' || $policy->status === 'inPayment')
        <button type="button" data-status="success" disabled="disabled" class="c-btn c-btn--black u-mb-xsmall">Оплатить</button>
    @else
        <a class="c-btn md-o c-btn--success u-mb-xsmall" data-modal="modal-accept">Оплатить</a>
    @endif
@else
    <button type="button" data-status="paid" @if($policy->status === 'paid' || $policy->status === 'inPayment') disabled @endif class="c-btn c-btn--success u-mb-xsmall">Оплатить</button>
@endif
@if ($policy->status === 'inPayment')
    <button type="button" data-status="inPayment" class="c-btn c-btn--danger u-mb-xsmall">Проверить оплату</button>
@endif


{{--MODAL-4--}}
<div
    class="modal-accept__code modal-js"
    id="modal-accept"
    data-url="{{ route('policies.verify.phone', $policy) }}"
>
    <a href="#" class="modal-accept__code--close"></a>

    <div class="accept-code">
        <h3 class="accept-code__title">
            Подтверждение номер телефона <br>
            <span class="a-all__text--weight">{{ $policy->phone }}</span>
        </h3>

        <div class="wrap-btn">
            <div
                class="c-btn c-btn--success ajax-send-code"
            >Отправить код</div>
        </div>
    </div>

    <div class="policy-pay" style="display: none;">
        <div class="wrap-btn">
            <div
                class="c-btn c-btn--success ajax-pay-code"
                data-pay="true"
            >Оплатить</div>
        </div>
    </div>

    <div class="beforeSend" style="display: none;">
        <h3 class="accept-code__title">
            Ожидание...
        </h3>
    </div>

    {{--display: none--}}
    <div class="paste-code" style="display: none;">
        <h3 class="accept-code__title">
            Введите код для подтвержения номера телефона
        </h3>
        <input type="number" class="c-input accept-code__input">

        <span id="success_message"></span>

        <div class="wrap-btn">
            <div
                class="c-btn c-btn--success ajax-apply-code"
            >Подтвердить</div>
        </div>
    </div>
    {{--display: none--}}


</div>