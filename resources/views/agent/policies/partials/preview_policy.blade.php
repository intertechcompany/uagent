<div class="row block_text_info">
    <div class="col-12">
        <h3><i class="fa fa-user"></i>Персональная информация о собственнике</h3>
        <div class="row">
            <div class="col-md-5">Фамилия:</div>
            <div class="col-md-5">{{ array_get($data,'owner.last_name') }}</div>
        </div>
        <div class="row">
            <div class="col-md-5">Имя:</div>
            <div class="col-md-5">{{ array_get($data,'owner.first_name') }}</div>
        </div>
        <div class="row">
            <div class="col-md-5">Отчество:</div>
            <div class="col-md-5">{{ array_get($data,'owner.middle_name') }}</div>
        </div>
        <div class="row">
            <div class="col-md-5">Дата рождения:</div>
            <div class="col-md-5">{{ array_get($data,'owner.dob') }}</div>
        </div>
        <div class="row">
            <div class="col-md-5">Серия паспорта:</div>
            <div class="col-md-5">{{ array_get($data,'owner.personal_document_serial') }}</div>
        </div>
        <div class="row">
            <div class="col-md-5">Номер паспорта:</div>
            <div class="col-md-5">{{ array_get($data,'owner.personal_document_number') }}</div>
        </div>
        <div class="row">
            <div class="col-md-5">Дата выдачи паспорта:</div>
            <div class="col-md-5">{{ array_get($data,'owner.personal_document_date') }}</div>
        </div>
        <div class="row">
            <div class="col-md-5">Кем выдан:</div>
            <div class="col-md-5">{{ array_get($data,'owner.personal_document_issued_by') }}</div>
        </div>
        <div class="row">
            <div class="col-md-5">Место рождения:</div>
            <div class="col-md-5">{{ array_get($data,'owner.place_of_birth') }}</div>
        </div>
        <div class="row">
            <div class="col-md-4">Адрес:</div>
            <div class="col-md-4">{{ array_get($data,'owner.address.data.source') }}</div>
        </div>
        <div class="row">
            <div class="col-md-5">Дом:</div>
            <div class="col-md-5">{{ array_get($data,'owner.house') }}</div>
        </div>
        <div class="row">
            <div class="col-md-5">Корпус:</div>
            <div class="col-md-5">{{ array_get($data,'owner.address2') }}</div>
        </div>
        <div class="row">
            <div class="col-md-5">Квартира:</div>
            <div class="col-md-5">{{ array_get($data,'owner.address3') }}</div>
        </div>

        <div class="row">
            <div class="col-md-5">Телефон:</div>
            <div class="col-md-5">{{ array_get($data,'phone') }}</div>
        </div>

        <div class="row">
            <div class="col-md-5">E-Mail:</div>
            <div class="col-md-5">{{ array_get($data,'email') }}</div>
        </div>

        <hr>

        <h3><i class="fa fa-car"></i>Информация о ТС </h3>

        <div class="row">
            <div class="col-md-5">Номер карты:</div>
            <div class="col-md-5">{{ array_get($data,'diagnostic_number') }}</div>
        </div>

        <div class="row">
            <div class="col-md-5">Срок действия с:</div>
            <div class="col-md-5">{{ array_get($data,'diagnostic_until_date') }}</div>
        </div>
        <div class="row">
            <div class="col-md-5">Водители:</div>
            <div class="col-md-5">@if($data['driver_count'] == 'specified')1 и более @else Неограниченное количество @endif</div>
        </div>
        <div class="row">
            <div class="col-md-5">Марка:</div>
            <div class="col-md-5">{{ array_get($data,'make.name') }}</div>
        </div>
        <div class="row">
            <div class="col-md-5">Модель:</div>
            <div class="col-md-5">{{ array_get($data,'model.name') }}</div>
        </div>
        <div class="row">
            <div class="col-md-5">Мощность, л.с:</div>
            <div class="col-md-5">{{ array_get($data,'power') }}</div>
        </div>
        <div class="row">
            <div class="col-md-5">Дата выдачи документа:</div>
            <div class="col-md-5">{{ array_get($data,'vehicle_document_date') }}</div>
        </div>
        <div class="row">
            <div class="col-md-5">Документ на автомобиль:</div>
            <div class="col-md-5">Номер: {{ array_get($data,'vehicle_document_number') }}, серия: {{ array_get($data,'vehicle_document_serial') }}</div>
        </div>

        <hr>

        <h3><i class="fas fa-address-card"></i> @if( $data['vehicle_document_type'] == 'sts') СТС @else ПТС @endif </h3>

        <div class="row">
            <div class="col-md-5">Тип идентификаторп ТС:</div>
            <div class="col-md-5">@if( $data['vehicle_document_type'] == 'vin') VIN @endif</div>
        </div>
        <div class="row">
            <div class="col-md-5">Идентификатор ТС:</div>
            <div class="col-md-5">{{ array_get($data,'vehicle_id') }}</div>
        </div>
        <div class="row">
            <div class="col-md-5">Гос. номер авто:</div>
            <div class="col-md-5">{{ array_get($data,'vehicle_plate_number') }}</div>
        </div>
        <div class="row">
            <div class="col-md-5">Регион:</div>
            <div class="col-md-5">{{ array_get($data,'vehicle_plate_region') }}</div>
        </div>
        <div class="row">
            <div class="col-md-5">Год выпуска:</div>
            <div class="col-md-5">{{ array_get($data,'vehicle_registration_year') }}</div>
        </div>
        <div class="row">
            <div class="col-md-5">Используется с прицепом:</div>
            <div class="col-md-5">@if( $data['is_vehicle_use_trailer'] == 1) Да @else Нет @endif</div>
        </div>

        @if( $data['driver_count']  == 'specified')
            <hr>
            <h3><i class="fas fa-users"></i> Информация о водителях</h3>
            @foreach( $data['drivers'] as $key => $value)
                <div class="row">
                    <div class="col-md-5">Фамилия</div>
                    <div class="col-md-5">{{ array_get($value,'last_name') }}</div>
                </div>
                <div class="row">
                    <div class="col-md-5">Имя</div>
                    <div class="col-md-5">{{ array_get($value,'first_name') }}</div>
                </div>
                <div class="row">
                    <div class="col-md-5">Отчество</div>
                    <div class="col-md-5">{{ array_get($value,'middle_name') }}</div>
                </div>
                <div class="row">
                    <div class="col-md-5">Дата рождения</div>
                    <div class="col-md-5">{{ array_get($value,'dob') }}</div>
                </div>
                <div class="row">
                    <div class="col-md-5">Водительское удостоверение</div>
                    <div class="col-md-5">Серия: {{ array_get($value,'document_serial') }} номер: {{ array_get($value,'document_number') }}</div>
                </div>
                <div class="row">
                    <div class="col-md-5">Стаж с</div>
                    <div class="col-md-5">{{  array_get($value,'start_date') }}</div>
                </div>
            @endforeach
        @endif
    </div>
</div>