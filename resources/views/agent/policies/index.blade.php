@extends('layouts.agent')

@section('title')
    <i class="c-sidebar__icon feather icon-file"></i>Полисы
@endsection

@section('content')
    @include('agent.include.tablePolicies')
@endsection