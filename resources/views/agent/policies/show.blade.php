@extends('layouts.agent')

@section('title')
    <i class="c-sidebar__icon feather icon-file"></i>Полис #{{ $policy->id }}
@endsection

@section('content')
    <div class="col-xl-12 col-lg-12 col-md-12">
        <div class="c-card status-policy status-policy__change">
            @if ($policy->insurance_id !== null)
                <div class="status-policy__row">
                    <h3 class="status-policy__number">Полис <span class="status-policy__number--pink c-pink">№{{ $policy->insurance_id }}</span></h3>
                </div>
            @endif
            <div class="status-policy__row">
                <h3 class="status-policy__status">Статус: <span class="status-{{ $policy->status }}">{{ $policy->getStatusRus() }}</span></h3>
            </div>
            <div class="status-policy__row">
                @if ($policy->insurance_id !== null)
                    <form class="statusPolicy" method="post" action="{{ route('policies.set.status',$policy->id) }}" onsubmit="window.preloader();$(this).find('button').attr('disabled','disabled')">
                        {{ csrf_field() }}
                        <input type="hidden" name="status" class="status" value="{{ $policy->status }}">
                        @if (in_array($policy->status,['decorated','success','forbidden'],true))
                            <button disabled class="c-btn  c-btn__void c-btn--secondary u-mb-xsmall"><span>Аннулировать</span></button>
                            <button disabled class="c-btn c-btn__subscribe u-mb-xsmall"><span>Подписать</span></button>
                            <button disabled class="c-btn c-btn--success u-mb-xsmall">Оплатить</button>
                        @else
                            <button type="button" data-status="decorated" @if($policy->status === 'paid') disabled @endif class="c-btn c-btn__void c-btn--secondary u-mb-xsmall"><span>Аннулировать</span></button>
                            <button type="button" data-status="success" @if($policy->status !== 'paid') disabled @endif class="c-btn c-btn__subscribe u-mb-xsmall"><span>Подписать</span></button>
                            {{--
                            @include('agent.policies.partials.button-pay')
                            --}}
                            <button type="button" data-status="paid" @if($policy->status === 'paid' || $policy->status === 'inPayment') disabled @endif class="c-btn c-btn--success u-mb-xsmall">Оплатить</button>
                            @if(!in_array($policy->status, ['paid', 'inPayment']) && auth()->user()->checkBalance($policy->premium))
                            <a href="{{ route('policies.payment', ['policy'=>$policy]) }}" class="c-btn c-btn__pink u-mb-xsmall">Оплатить с баланса</a>
                            @endif
                            @if($policy->status === 'inPayment')<button type="button" data-status="inPayment" class="c-btn c-btn--danger u-mb-xsmall">Проверить оплату</button> @endif
                        @endif
                    </form>
                @else
                    <button onclick="window.location = '{{ route('policies.reCalc.form',['policy' => $policy->id]) }}'" class="c-btn c-btn--success u-mb-xsmall">Перерасчет</button>
                @endif
            </div>
        </div>
        <div >
            @include('include.policies.info',['policy' => $policy,'owner' => $policy->owner])
        </div>
    </div>


@endsection

@section('scripts')
    <script src="{{ asset('/js/custom/policy.js') }}"></script>
@endsection
