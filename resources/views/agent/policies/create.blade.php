@extends('layouts.agent')

@section('title')
    <i class="c-sidebar__icon feather icon-file"></i>Создать полис
@endsection

@section('content')
    <policy-create-form :user="{{ $userId }}" :min-date-start-police="'{{ date('d.m.Y', strtotime('+1days')) }}'"></policy-create-form>
@endsection
