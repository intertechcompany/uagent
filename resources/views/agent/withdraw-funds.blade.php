@extends('layouts.agent')

@section('title')
    <i class="c-sidebar__icon" data-feather="dollar-sign"></i>Выплаты
@endsection

@section('content')
    <div class="home-container">
        <div class="row">
            <div class="col-xl-6">
                <div class="c-content">
                    <h3 class="c-content__h3">Выплаты</h3>
                </div>
                <div class="statistics-table__filter">
                    <h4 class="c-card__statistics--thead">Способ вывода</h4>
                    <div class="c-card__statistics--tbody c-card__card--tbody c-card-withdraw">
                        <div class="container">

                            <div class="row align-items-center">
                                <div class="col-12">

                                    <div class="bank-card__head js-check"{!! old('type') === 'yandex' || old('type') === 'requisites' ? ' style="background-color: #ffedf3"' : '' !!}>
                                        <label for="radio-card" class="contain">Банковская карта
                                            <input id="radio-card" type="radio" name="radio" checked>
                                            <span class="checkmark"></span>
                                        </label>
                                        <img src="{{ asset('/img/icons/card.png') }}" alt="card" class="card">
                                    </div>
                                    <div class="bank-card__body js-toggle__card">
                                        <p>
                                            Выплаты на карты банков осуществляются только в случае, если комиссионное вознаграждение в месяц не превышает 10000 рублей. В случае, если сумма КВ выше - оформите кошелек Яндекс денег или укажите реквизиты ИП/Юр лица.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row align-items-center">
                                <div class="col-12">
                                    <div class=" bank-yd__head js-check"{!! old('type') === 'yandex' ? ' style="background-color: #f16192"' : '' !!}>

                                        <label for="radio-yd" class="contain">Яндекс деньги
                                            <input id="radio-yd" type="radio" name="radio">
                                            <span class="checkmark"></span>
                                        </label>
                                        <img src="{{ asset('/img/icons/ym.png') }}" alt="YM" class="card">
                                    </div>
                                    <div class="bank-card__body bank-yd__body js-toggle__card">
                                        <p>
                                            Если у вас нет счета ИП или юридического лица, вы можете указать кошелек Яндекс денег. Если у вас нет кошелька, вы можете открыть его
                                            <a href="#" class="underline c-pink">здесь</a> . Верифицировать кошелек можно с помощью Сбербанка онлайн. <a href="#" class="underline c-pink">Подробнее</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row align-items-center">
                                <div class="col-12">
                                    <div class=" bank-pp__head js-check"{!! old('type') === 'requisites' ? ' style="background-color: #f16192"' : '' !!}>
                                        <label for="radio-pp" class="contain">Реквизиты ИП / Юр. лица
                                            <input id="radio-pp" type="radio" name="radio">
                                            <span class="checkmark"></span>
                                        </label>
                                        <img src="{{ asset('/img/icons/baks.png') }}" alt="pp" class="card">
                                    </div>
                                    <div class="bank-card__body bank-pp__body js-toggle__card">
                                        <p>
                                            Если вы являетесь индивидуальным предпринимателем или у вас есть юридическое лицо - укажите реквизиты для выплаты комиссионного вознаграждения.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 form-card"{!! old('type') === 'yandex' || old('type') === 'requisites' ? ' style="display: none"' : '' !!}>
                {!! Form::model($user, ['route' => 'withdraws-funds.edit']) !!}
                <input type="hidden" name="type" value="bank_card">
                <div class="c-content">
                    <h3 class="c-content__h3 opacity-0">Реквизиты для вывода1</h3>
                </div>
                <div class="statistics-table__statistics">
                    <div class="c-card__statistics--thead">
                        Реквизиты для вывода
                    </div>
                    <div class="c-card__statistics--tbody c-card__card--tbody ">
                        <div class="col-12">
                            <div class="card-input">
                                <label for="card-inpt">Номер карты</label>
                                <input
                                        id="card-inpt"
                                        name="card_number"
                                        class="settings-input card_number"
                                        type="text"
                                        placeholder="XXXX XXXX XXXX XXXX"
                                        value="{{ $user->getSavedWithraw('bank_card', 'card_number') }}"
                                        readonly
                                >
                                <a href="javascript:;" class="edit"><i class="fas fa-edit"></i></a>
                                @include('forms.error', ['name' => 'card_number'])
                            </div>
                            <label for="card-inpt2" class="title-card__input">Сумма вывода</label>
                            <div class="card-input card-input__flex">
                                <div class="wrap-card-input">
                                    <input id="card-inpt2" name="amount" type="number" placeholder="Сумма вывода" value="{{ old('amount') }}">
                                    <img class="p-money" src="/img/icons/p-money.png" alt="P" style="width: 13px; height: 16px;">
                                    @if ($errors->has('amount') && !empty($errors->get('amount')[0]))
                                        <div class="text-grey">{{ $errors->get('amount')[0] }}</div>
                                    @endif
                                    <p class="text-grey">Комиссия согласно
                                        платежной системы</p>
                                </div>
                                <div class="wrap-card-input">
                                    <p class="text-grey">Доступно к выводу:</p>
                                    <input class="card-inpt2" type="number" placeholder="Сумма вывода" value="{{ $user->balance }}" readonly>
                                    <img class="p-money" src="/img/icons/p-money.png" alt="P" style="width: 13px; height: 16px;">
                                </div>

                            </div>
                            <div class="wrap-btn">
                                <button class="c-btn c-btn--success btn-export__exel btn-export__money">
                                    <i class="fas fa-check-circle"></i>Вывести средства</a>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
                {!! Form::close() !!}
            </div>
            <div class="col-xl-6 form-yd"{!! old('type') === 'yandex' ? ' style="display: block"' : '' !!}>
                {!! Form::model($user, ['route' => 'withdraws-funds.edit']) !!}
                <input type="hidden" name="type" value="yandex">
                <div class="c-content">
                    <h3 class="c-content__h3 opacity-0">Реквизиты для вывода</h3>
                </div>
                <div class="statistics-table__statistics">
                    <div class="c-card__statistics--thead">
                        Реквизиты для вывода
                    </div>
                    <div class="c-card__statistics--tbody c-card__card--tbody c-card-withdraw">
                        <div class="col-12">
                            <div class="card-input">
                                <label for="card-inpt">Номер кошелька Яндекс Денег</label>
                                <input
                                        id="card-inpt"
                                        name="card_number"
                                        class="settings-input yandex_number"
                                        type="text"
                                        placeholder="XXXX XXXX XXXX XXXX"
                                        value="{{ $user->getSavedWithraw('yandex', 'card_number') }}"
                                        readonly
                                >
                                <a href="javascript:;" class="edit"><i class="fas fa-edit"></i></a>
                            </div>
                            <label for="card-inpt2" class="title-card__input">Сумма вывода</label>
                            <div class="card-input card-input__flex">
                                <div class="wrap-card-input">
                                    <input id="card-inpt2" name="amount" type="number" placeholder="Сумма вывода" value="{{ old('amount') }}">
                                    <img class="p-money" src="/img/icons/p-money.png" alt="P" style="width: 13px; height: 16px;">
                                    @if ($errors->has('amount') && !empty($errors->get('amount')[0]))
                                        <div class="text-grey">{{ $errors->get('amount')[0] }}</div>
                                    @endif
                                    <p class="text-grey">Комиссия согласно
                                        платежной системы</p>
                                </div>

                                <div class="wrap-card-input">
                                    <p class="text-grey">Доступно к выводу:</p>
                                    <input class="card-inpt2" type="number" placeholder="Сумма вывода" value="{{ $user->balance }}" readonly>
                                    <img class="p-money" src="/img/icons/p-money.png" alt="P" style="width: 13px; height: 16px;">
                                </div>

                            </div>
                            <div class="wrap-btn">
                                <button class="c-btn c-btn--success btn-export__exel btn-export__money">
                                    <i class="fas fa-check-circle"></i>Вывести средства</a>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
                {!! Form::close() !!}
            </div>

            <div class="col-xl-6 form-pp"{!! old('type') === 'requisites' ? ' style="display: block"' : '' !!}>
                {!! Form::model($user, ['route' => 'withdraws-funds.edit']) !!}
                <input type="hidden" name="type" value="requisites">
                <div class="c-content">
                    <h3 class="c-content__h3 opacity-0">Реквизиты для вывода</h3>
                </div>
                <div class="statistics-table__statistics">
                    <div class="c-card__statistics--thead">
                        Введите реквизиты ИП / Юр. лица
                    </div>
                    <div class="c-card__statistics--tbody c-card__card--tbody c-card-withdraw">
                        <div class="col-12">
                            <div class="card-input card-input__flex">
                                <div class="collum">
                                    <input
                                            class="settings-input card-inpt2"
                                            type="text"
                                            name="full_name"
                                            placeholder="Полное наименование юр. лица"
                                            value="{{ old('full_name', $user->getSavedWithraw('requisites', 'full_name')) }}"
                                            readonly
                                    >
                                    <input
                                            class="settings-input card-inpt2"
                                            type="text"
                                            name="leader_organization"
                                            placeholder="Представитель/руководитель организации"
                                            value="{{ old('leader_organization',  $user->getSavedWithraw('requisites', 'leader_organization')) }}"
                                            readonly
                                    >
                                    <input
                                            class="settings-input card-inpt2"
                                            type="text"
                                            name="based"
                                            placeholder="На основании"
                                            value="{{ old('based', $user->getSavedWithraw('requisites', 'based')) }}"
                                            readonly
                                    >
                                    <input
                                            class="settings-input card-inpt2"
                                            type="text"
                                            name="legal_address"
                                            placeholder="Юридический адрес"
                                            value="{{ old('legal_address', $user->getSavedWithraw('requisites', 'legal_address')) }}"
                                            readonly
                                    >
                                    <input
                                            class="settings-input card-inpt2"
                                            type="text"
                                            name="mailing_address"
                                            placeholder="Почтовый адрес"
                                            value="{{ old('mailing_address', $user->getSavedWithraw('requisites', 'mailing_address')) }}"
                                            readonly
                                    >
                                    <input
                                            class="settings-input card-inpt2"
                                            type="tel"
                                            name="phone"
                                            placeholder="Телефон"
                                            value="{{ old('phone', $user->getSavedWithraw('requisites', 'phone')) }}"
                                            readonly
                                    >
                                    <input
                                            class="settings-input card-inpt2"
                                            type="text"
                                            name="inn"
                                            placeholder="ИНН" value="ИНН"
                                            value="{{ old('inn', $user->getSavedWithraw('requisites', 'inn')) }}"
                                            readonly
                                    >
                                </div>
                                <div class="collum">
                                    <input
                                            class="settings-input card-inpt2"
                                            type="text"
                                            name="kpp"
                                            placeholder="КПП " value="КПП"
                                            value="{{ old('kpp', $user->getSavedWithraw('requisites', 'kpp')) }}"
                                            readonly
                                    >
                                    <input
                                            class="settings-input card-inpt2"
                                            type="text"
                                            name="ogrn"
                                            placeholder="ОГРН"
                                            value="{{ old('ogrn', $user->getSavedWithraw('requisites', 'ogrn')) }}"
                                            readonly
                                    >
                                    <input
                                            class="settings-input card-inpt2"
                                            type="text"
                                            name="bank"
                                            placeholder="Банк"
                                            value="{{ old('bank', $user->getSavedWithraw('requisites', 'bank')) }}"
                                            readonly
                                    >
                                    <input
                                            class="settings-input card-inpt2"
                                            type="text"
                                            name="bik"
                                            placeholder="БИК"
                                            value="{{ old('bik', $user->getSavedWithraw('requisites', 'bik')) }}"
                                            readonly
                                    >
                                    <input
                                            class="settings-input card-inpt2"
                                            type="text"
                                            name="settlement_account"
                                            placeholder="Р/с"
                                            value="{{ old('settlement_account', $user->getSavedWithraw('requisites', 'settlement_account')) }}"
                                            readonly
                                    >
                                    <input
                                            class="settings-input card-inpt2"
                                            type="text"
                                            name="ks"
                                            placeholder="К/с"
                                            value="{{ old('ks', $user->getSavedWithraw('requisites', 'ks')) }}"
                                            readonly
                                    >
                                    <input
                                            class="settings-input card-inpt2"
                                            type="text"
                                            name="payer_description"
                                            placeholder="Описание плательщик"
                                            value="{{ old('payer_description', $user->getSavedWithraw('requisites', 'payer_description')) }}"
                                            readonly
                                    >
                                </div>

                            </div>
                            <a href="javascript:;" class="edit-settings"><i class="fas fa-edit"></i></a>
                            <label class="title-card__input" for="card-inpt2">Сумма вывода</label>
                            <div class="card-input card-input__flex">
                                <div class="wrap-card-input">

                                    <input class="card-inpt2" id="card-inpt2" type="number" name="amount" placeholder="Сумма вывода" value="{{ old('amount') }}">
                                    <img class="p-money" src="{{ asset('/img/icons/p-money.png') }}" alt="P" style="width: 13px; height: 16px;">
                                    @if ($errors->has('amount') && !empty($errors->get('amount')[0]))
                                        <div class="text-grey">{{ $errors->get('amount')[0] }}</div>
                                    @endif
                                    <p class="text-grey">Комиссия согласно
                                        платежной системы</p>

                                </div>
                                <div class="wrap-card-input">
                                    <p class="text-grey">Доступно к выводу:</p>
                                    <input class="card-inpt2" type="number" placeholder="Сумма вывода" value="{{ $user->balance }}" readonly>
                                    <img class="p-money" src="{{ asset('/img/icons/p-money.png') }}" alt="P" style="width: 13px; height: 16px;">
                                </div>
                            </div>
                            <div class="wrap-btn">
                                <button class="c-btn c-btn--success btn-export__exel btn-export__money">
                                    <i class="fas fa-check-circle"></i>Вывести средства
                                </button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
    </div>

@endsection

