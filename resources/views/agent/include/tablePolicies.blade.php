@section('content')
    <div class="vue-component">
        <policy-filter :api-url-delete="'{{ route('policies.delete') }}'" :api-url="'{{ route('policies.filter') }}'"></policy-filter>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/js/policy.js') }}"></script>
@endsection
