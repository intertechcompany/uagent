@extends('layouts.agent')

@section('title')
    <i class="c-sidebar__icon feather icon-settings"></i>Настройки
@endsection

@section('content')

    {!! Form::model($user, [
        'route' => 'settings.update', 'files' => true,
        'class' => 'ajax-setting-form',
        'data-url' => route('settings.update.field')
    ]) !!}

    <div class="home-container settings-body">
        <div class="c-content__flex">
            <h3 class="u-inline-flex pl-20 title-settings">Настройки профиля</h3>
        </div>

        <div class="row settings-row">
            <div class="col min-w">

                <div class="statistics-table__statistics settings-table">
                    <div class="c-card__statistics--thead">
                        <p class="settings-inform__title">Основная Информация</p>
                    </div>
                    <div class="c-card__statistics--tbody settings-table--body">
                        <div class="container">
                            <div class="row align-items-center">
                                <div class="col">
                                    <div class="wrap-img">
                                        <a href="#">
                                            <img src="{{ asset('storage/app/avatar/'.$user->avatar) }}" style="width: 198px; height: 198px;" src="" alt="account-img">
                                        </a>
                                    </div>
                                    <label for="file" class="label-file "><img src="/img/icons/image.png" alt=""></label>
                                    <input style="display: none;" class="file-input" id="file" type="file" name="avatar">
                                </div>
                            </div>
                            <div class="row row-settings">
                                <div class="div-1">ФИО:</div>
                                <div class="div-2 settings-col">
                                    {{ Form::text('full_name', null, ['class' => 'settings-input', 'readonly' => true]) }}
                                    @include('forms.error', ['name' => 'full_name'])
                                    <a class="edit-settings fio-settings-field" href="javascript:;"><i class="fas fa-edit"></i></a>
                                </div>
                            </div>
                            <div class="row row-settings">
                                <div class="div-1">Пол:</div>
                                <div class="div-2">
                                    {{ Form::select('gender', [
                                        '0' => 'Женский',
                                        '1' => 'Мужской',
                                    ], $user->information ? $user->information->gender : null, [
                                        'class' => 'ui dropdown twig search c-select__input settings-input ajax-select-field',
                                        'placeholder' => 'Укажите пол'
                                    ]) }}
                                    @include('forms.error', ['name' => 'gender'])
                                </div>
                            </div>
                            <div class="row row-settings">
                                <div class="div-1">Дата рождения:</div>
                                <div class="div-2 settings-col">
                                    {{ Form::text('birthday', $user->information ? $user->information->birthdayDate : null, [
                                        'class' => 'settings-input',
                                        'readonly' => true
                                    ]) }}
                                    @include('forms.error', ['name' => 'birthday'])
                                    <a class="edit-settings" href="javascript:;"><i class="fas fa-edit"></i></a>
                                </div>
                            </div>
                            <div class="row row-settings">
                                <div class="div-1">Город:</div>
                                <div class="div-2 settings-col col-city">
                                    {{ Form::select('city_id', \App\Model\City::query()->orderBy('name')->pluck('name', 'id'), $user->city_id, [
                                        'class' => 'settings-input ui dropdown twig search c-select__input ajax-select-field',
                                        'readonly' => true,
                                        'placeholder' => 'Укажите город'
                                    ]) }}
                                    @include('forms.error', ['name' => 'city_id'])
                                </div>
                            </div>
                            <div class="row row-settings">
                                <div class="div-1">Адрес:</div>
                                <div class="div-2 settings-col">
                                    {{ Form::text('address', $user->information ? $user->information->address : null, [
                                        'id' => 'address',
                                        'class' => 'settings-input',
                                        'readonly' => true
                                    ]) }}
                                    @include('forms.error', ['name' => 'address'])
                                    <a class="edit-settings" href="javascript:;"><i class="fas fa-edit"></i></a>
                                </div>
                            </div>
                            <div class="row row-settings">
                                <div class="div-1">Телефон:</div>
                                <div class="div-2 settings-col">
                                    {{ Form::text('phone', $user->phone, ['class' => 'settings-input tell-new__settings', 'readonly' => true]) }}
                                    @include('forms.error', ['name' => 'phone'])
                                    <a class="edit-settings" href="javascript:;"><i class="fas fa-edit"></i></a>
                                </div>
                            </div>
                            <div class="row row-settings">
                                <div class="div-1">E-mail:</div>
                                <div class="div-2 settings-col">
                                    {{ Form::email('email', $user->email, ['class' => 'settings-input', 'readonly' => true]) }}
                                    @include('forms.error', ['name' => 'phone'])
                                    <a class="edit-settings" href="javascript:;">
                                        <i class="fas fa-edit"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col min-w">
                <div class="statistics-table__statistics settings-table">
                    <div class="c-card__statistics--thead">
                        <p class="settings-inform__title">Безопасность</p>
                    </div>
                    <div class="c-card__statistics--tbody settings-table--body">
                        <div class="container">
                            <div class="row row-settings">
                                <div class="div-1">Логин:</div>
                                <div class="div-2">
                                    <input class="settings-input" type="name" value="{{ $user->email }}" readonly>
                                    <a class="edit" href="javascript:;"><i class="fas fa-edit"></i></a>
                                    @include('forms.error', ['name' => 'email'])
                                </div>
                            </div>
                            <div class="row row-settings">
                                <div class="div-1">Пароль:</div>
                                <div class="div-2">
                                    <input class="settings-input" type="password" name="password" value="" placeholder="************" readonly>
                                    <a class="edit" href="javascript:;"><i class="fas fa-edit"></i></a>
                                    @include('forms.error', ['name' => 'password'])
                                </div>
                            </div>
                            <div class="row row-settings">
                                <div class="div-1">Новый пароль:</div>
                                <div class="div-2">
                                    <input class="settings-input" type="password" name="password_confirmation" placeholder="************" readonly>
                                    <a class="edit" href="javascript:;"><i class="fas fa-edit"></i></a>
                                    @include('forms.error', ['name' => 'password_confirmation'])
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-9 offset-3">
                                    {{--<a class="btn c-btn__pink c-btn__pink--reset">Сбросить пароль</a>--}}
                                    <button type="submit" class="btn c-btn__pink c-btn__pink--reset">Сбросить пароль</button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="c-field">
                                        <div class="c-choice c-choice--checkbox">
                                            <input type="checkbox" id="is_two_step_auth" name="is_two_step_auth" class="c-choice__input ajax-edit-field" value="1"{{ $user->information && $user->information->is_two_step_auth ? ' checked' : '' }}>
                                            <label for="is_two_step_auth" class="c-choice__label">Включить двухэтапную аутентификацию</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="c-field">
                                        <div class="c-choice c-choice--checkbox">
                                            <input type="checkbox" id="is_remind_change_pass" name="is_remind_change_pass" class="c-choice__input ajax-edit-field" value="1"{{ $user->information && $user->information->is_remind_change_pass ? ' checked' : '' }}>
                                            <label for="is_remind_change_pass" class="c-choice__label">Напоминать о необходимости смены пароля</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="c-field">
                                        <div class="c-choice c-choice--checkbox">
                                            <input type="checkbox" id="is_send_email_suspiciou_login" name="is_send_email_suspiciou_login" class="c-choice__input ajax-edit-field" value="1"{{ $user->information && $user->information->is_send_email_suspiciou_login ? ' checked' : '' }}>
                                            <label for="is_send_email_suspiciou_login" class="c-choice__label">Присылать уведомление на почту о подозрительных входах</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col min-w">
                <div class="statistics-table__statistics settings-table">
                    <div class="c-card__statistics--thead">
                        <p class="settings-inform__title">Допольнительно</p>
                    </div>
                    <div class="c-card__statistics--tbody settings-table--body">
                        <div class="container">
                            <div class="ajax-blocks">
                                @foreach ($blocks as $key => $block)
                                    <div class="row more-row">
                                        <div class="c-field">
                                            <div class="c-choice c-choice--checkbox">
                                                <input type="checkbox" id="chek-{{ $key }}" name="blocks[]" value="{{ $key }}" class="c-choice__input ajax-edit-blocks" {{ isset($dashboardBlocks[$key]) && $dashboardBlocks[$key]->is_enabled  ? 'checked' : ''  }}>
                                                <label for="chek-{{ $key }}" class="c-choice__label"><span class="settings-label__3">Отображать на главной странице</span><span class="bold ttu"> «{{ $block }}»</span></label>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col min-w opacity-0 n-t">
                <div class="statistics-table__statistics settings-table">
                    <div class="c-card__statistics--thead">
                        <p class="settings-inform__title">Допольнительно</p>
                    </div>
                    <div class="c-card__statistics--tbody settings-table--body">
                    </div>
                </div>
            </div>

        </div>
    </div>
    {!! Form::close() !!}
@endsection
