@extends('layouts.agent')

@section('title')
    <i class="c-sidebar__icon feather icon-home"></i>Главная

@endsection

@section('content')
    <div class="home-container home-body">
        <Progress-Circle></Progress-Circle>
        @if ($user->canShowStocks())
            <div class="c-sidebar-row__modal">
                <div class="c-sidebar__local-modal">
                    @foreach ($user->stocks() as $stock)
                        <div class="local-modal__generate">
                            <h4 class="local-modal__h4">{{ $stock->title }}</h4>
                            <p class="local-modal__text">{{ $stock->text }}</p>
                            <a href="#" class="c-sidebar__modal-close js-close__local-modal"></a>
                            @if (!empty($stock->information))
                                <div class="local-modal__info-hover">
                                    <i class="local-modal__info feather icon-info"></i>
                                    <div class="local-modal__info-text">{{ $stock->information }}</div>
                                </div>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
        <div class="c-sidebar-row__flex">
            @if ($user->canCreatePolicy())
                <a href="{{ url('policies/create') }}" class="c-sidebar__item ">
                    <div class="c-btn__create c-state-card">
                        <p class="c-btn--success__create c-btn--large">Создать полис</p>
                    </div>
                </a>
            @endif
            @foreach ($dashboards as $dashboardBlock)
                <div class="c-sidebar__item">
                    <div class="c-state-card c-state-card--success">
                    <span class="c-state-card__number{{ $dashboardBlock->getClass() }}">
                        {{ $dashboardBlock->getFormattedValue() }}
                    </span>
                        <h4 class="c-state-card__title"> {{ $dashboardBlock->getName() }}</h4>
                    </div>
                </div>
            @endforeach
        </div>

        <h3 class="u-mb-small pl-3-desk">Последние полисы</h3>

        <div class="row">
            <div class="col-12 c-table__scroll">
                <div class="c-table__hide c-table-responsive@wide">
                    <table class="c-table">
                        <thead class="c-table__head">
                        <tr class="c-table__row c-table-bg">
                            <th class="c-table__cell c-table__cell--head">№</th>
                            <th class="c-table__cell c-table__cell--head">Стахователь</th>
                            <th class="c-table__cell c-table__cell--head">Номер полиса</th>
                            <th class="c-table__cell c-table__cell--head">Контакты</th>
                            <th class="c-table__cell c-table__cell--head">Дата и время</th>
                            <th class="c-table__cell c-table__cell--head">Премия</th>
                            <th class="c-table__cell c-table__cell--head">Статус</th>
                            <th class="c-table__cell c-table__cell--head">Печать</th>
                            <th class="c-table__cell c-table__cell--head">Пересчет</th>
                        </tr>
                        </thead>
                        <tbody class="">
                        @foreach($polices as $police)
                            <tr class="c-table__row ">
                                <td class="c-table__cell"><span class="polices-number">{{ $police->id  }}</span> </td>
                                <td class="c-table__cell">
                                    <div class="o-media">
                                        <div class="o-media__img u-mr-xsmall">
                                            <div class="c-avatar c-avatar--small">
                                                @if ($police->insurance !== null) <img class="c-avatar__img" src="/img/{{ $police->insurance }}_avatar.jpg" alt="{{ $police->insurance }}"> @endif
                                            </div>
                                        </div>
                                        <div class="o-media__body" style="padding-top: 16px;"><span class="polices-account__name">
                                            <a href="{{ route('policies.show',$police->id) }}"><h6>{{ $police->owner->getFullName() }}</h6></a></span>
                                        </div>
                                    </div>
                                </td>
                                <td class="c-table__cell"><span class="inner-cont">{{ $police->insurance_id ?? '-' }}</span> </td>
                                <td class="c-table__cell">{{ $police->email }}<br>{{ $police->phone }}</td>
                                <td class="c-table__cell">{{ $police->getDateTimeStartPolicy()->format('d.m.Y') }}<br>{{ $police->getDateTimeStartPolicy()->format('H:m:i') }}</td>
                                <td class="c-table__cell"><span class="prize">{{ $police->premium }}p.</span> </td>
                                <td class="c-table__cell">
                                    <span class="status-{{ $police->status }}">{{ $police->getStatusRus() }}</span>
                                </td>
                                <td class="c-table__cell">
                                    <div class="c-select printTypeDiv drop-icon">
                                        <select class="c-select__input c-select__input--create" onchange="if (this.value !== '') {window.open('/policies/print/{{ $police->id }}/' + this.value,'_blank');}">
                                            <option value="">Выберите форму</option>
                                            <option value="policy">Полис</option>
                                        </select>
                                        <i class="fas fa-sort-down"></i>
                                    </div>
                                </td>
                                <td class="c-table__cell">
                                    <div class="c-select printTypeDiv">
                                        <a class="c-btn c-btn--success btn-recalc" href="{{ route('policies.reCalc.form',['policy' => $police->id]) }}">Перерасчет</a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('scripts')
    <script src="{{ asset('/js/policy.js') }}"></script>
@endsection
