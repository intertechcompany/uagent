@extends('layouts.agent')

@section('title')
    <i class="c-sidebar__icon" data-feather="dollar-sign"></i>Выплаты
@endsection

@section('content')

    <div class="row">
        <div class="col-12 c-table__scroll">
            <div class="c-table-responsive@wide ">
                <table class="c-table">
                    <thead class="c-table__head">
                    <tr class="c-table__row c-table-bg">
                        <th class="c-table__cell c-table__cell--head">№</th>
                        <th class="c-table__cell c-table__cell--head">Стахователь</th>
                        <th class="c-table__cell c-table__cell--head">Номер полиса</th>
                        <th class="c-table__cell c-table__cell--head">Контакты</th>
                        <th class="c-table__cell c-table__cell--head">Дата и время</th>
                        <th class="c-table__cell c-table__cell--head">Премия</th>
                        <th class="c-table__cell c-table__cell--head">Статус</th>
                        <th class="c-table__cell c-table__cell--head">Печать</th>
                        <th class="c-table__cell c-table__cell--head">Пересчет</th>
                    </tr>
                    </thead>

                    <tbody class="">
                        @foreach($polices as $police)
                            <tr class="c-table__row ">
                                <td class="c-table__cell"><span class="polices-number">{{ $police->id  }}</span> </td>
                                <td class="c-table__cell">
                                    <div class="o-media">
                                        <div class="o-media__img u-mr-xsmall">
                                            <div class="c-avatar c-avatar--small">
                                                <img class="c-avatar__img" src="/img/{{ $police->insurance }}_avatar.jpg" alt="Jessica Alba">
                                            </div>
                                        </div>
                                        <div class="o-media__body" style="padding-top: 16px;"><span class="polices-account__name">
                                            <a href="{{ route('policies.show',$police->id) }}"><h6>{{ $police->owner->getFullName() }}</h6></a></span>
                                        </div>
                                    </div>
                                </td>
                                <td class="c-table__cell"><span class="inner-cont">{{ $police->insurance_id }}</span> </td>
                                <td class="c-table__cell">{{ $police->email }}<br>{{ $police->phone }}</td>
                                <td class="c-table__cell">{{ $police->getDateTimeStartPolicy()->format('d.m.Y') }}<br>{{ $police->getDateTimeStartPolicy()->format('H:m:i') }}</td>
                                <td class="c-table__cell"><span class="prize">{{ $police->premium }}p.</span> </td>
                                <td class="c-table__cell">
                                    <span class="status-{{ $police->status }}">{{ $police->getStatusRus() }}</span>
                                </td>
                                <td class="c-table__cell">
                                    <div class="c-select printTypeDiv">
                                        <select class="c-select__input c-select__input--create" name="print" data-policyId="{{ $police->id }}" id="printType">
                                            <option value="">Выберите форму</option>
                                            <option value="">Полис</option>
                                            <option value="">Квитанция</option>
                                            <option value="2210">Заявление</option>
                                            <option value="2611">Акт осмотра</option>
                                        </select>
                                    </div>
                                </td>
                                <td class="c-table__cell">
                                    <div class="c-select printTypeDiv selectRecount">
                                        <select class="c-select__input c-select__input--create" name="recount" data-policyId="" id="selectRecount">
                                            <option value="">Альфа</option>
                                            <option value="">Бетта</option>
                                            <option value="">Гамма</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection

