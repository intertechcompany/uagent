@extends('layouts.agent')

@section('title')
    <i class="c-sidebar__icon feather icon-bar-chart-2"></i>Статистика

@endsection

@section('content')
<div class="vue-component" data-url="{{ route('statistic.filter') }}" id="app">
    <statistics></statistics>
</div>
@endsection
