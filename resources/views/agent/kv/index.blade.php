@extends('layouts.agent')

@section('content')
    <div class="home-container kv-body">
        <h3 class="c-content__h3 myagents-h3">Моё кв</h3>
        <div class="row-block__kv">
            @foreach($insurances as $insurance => $insuranceName)
                @php($insuranceKv = auth()->user()->getKv($insurance))
                @php($insuranceCities = auth()->user()->getCityRate($insurance))

                @if ($insuranceKv !== null || $insuranceCities !== null)
                <div class="box">
                    <div class="row-title">
                        <div class="img-company">
                            <img src="{{ asset('/img/'.$insurance.'_avatar.jpg') }}" alt="insurance-img">
                        </div>
                        <div class="name-company">
                            {{ $insuranceName }}
                        </div>
                    </div>
                    @foreach($insuranceCities as $rateCity)
                    <div class="row-content">
                        <span class="text-grey">{{ $rateCity->city->name }}:</span>
                        <span class="bold">{{ number_format($rateCity->value,2) }}{{ $rateCity->type === 'percent' ? '%' : '₽' }}</span>
                    </div>
                    @endforeach
                    @if ($insuranceKv !== null)
                        <div class="row-content">
                            <span class="text-grey">Другие регионы:</span>
                            <span class="bold">{{ number_format($insuranceKv->getValue(),2) }}{{ $insuranceKv->getType() === 'percent' ? '%' : '₽' }}</span>
                        </div>
                    @endif
                    <!--<div class="info-kv">
                        <i class="fas fa-info-circle"></i>
                        Оформи еще 20 полисов и получи больше КВ!
                    </div>-->
                </div>
                @endif
            @endforeach
        </div>
    </div>
@endsection