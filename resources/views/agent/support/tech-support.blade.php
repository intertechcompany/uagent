@extends('layouts.agent')

@section('title')
@endsection

@section('content')
    <tech-support :api-create-url="'{{ route('support.create') }}'" :api-url="'{{ route('support.getData') }}'"></tech-support>
@endsection
