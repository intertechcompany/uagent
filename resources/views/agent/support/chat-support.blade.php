@extends('layouts.agent')

@section('title')
@endsection

@section('content')
    <chat-support
            :back-url="'{{ route('support') }}'"
            :api-send-url="'{{ route('support.chat.sendData', ['chat'=>$chat]) }}'"
            :api-url="'{{ route('support.chat.getData', ['chat'=>$chat]) }}'">
    </chat-support>
@endsection
