@php($rateType = \App\Model\Commission::TYPE_OF_RATE_RATE_CITY)
@foreach ($cityRates as $city)
    <div class="children-body"
         data-rate_type="{{ $rateType }}"
         data-id="{{ $city->id }}"
         data-is_commission="{{ $city->isCommission() ? 1 : 0  }}"
    >
        <div class="toggle-children__title c-blue pl-20">
            {{ $city->isCommission() ? $city->rateCity->city->name : $city->city->name }}
        </div>
        <div class="row">
            <div class="col-4">
                <label class="pl-20" for="inpt-1">Всего, {{ \App\Model\Rate::$types[$city->getType()] }}</label>
                <input
                    class="settings-input ajax-reate-target rate-all"
                    type="number"
                    value="{{ $city->getValue() }}"
                    readOnly
                    step="0.01"
                >
            </div>
            <div class="col-4">
                <label class="pl-20" for="inpt-2">Вам, {{ \App\Model\Rate::$types[$city->getType()] }}</label>
                <input
                    class="settings-input ajax-reate-target rate-you"
                    type="number"
                    placeholder="0"
                    name="rates[{{ $city->insurance }}][{{ $rateType }}][{{ $city->id }}][you]"
                    data-commission-name="you"

                    @if (($commission = $agentCommissions->getCommission($city->insurance,$rateType,$city->getRateId())) !== null)
                        value="{{ $commission->you }}"
                        data-commission_id="{{ $commission->id }}"
                    @else
                        value="{{ old('rates.'.$city->insurance.'.'.$rateType.'.'.$city->id.'.you') }}"
                    @endif

                    step="0.01"
                >
            </div>
            <div class="col-4">
                <label class="pl-20" for="inpt-3">Агенту, {{ \App\Model\Rate::$types[$city->getType()] }}</label>
                <input
                    class="settings-input ajax-reate-target rate-agent"
                    type="number"
                    name="rates[{{ $city->insurance }}][{{ $rateType }}][{{ $city->id }}][agent]"
                    placeholder="0"
                    data-commission-name="agent"

                    @if (($commission = $agentCommissions->getCommission($city->insurance,$rateType,$city->getRateId())) !== null)
                        value="{{ $commission->agent }}"
                        data-commission_id="{{ $commission->id }}"
                    @else
                        value="{{ old('rates.'.$city->insurance.'.'.$rateType.'.'.$city->id.'.agent') }}"
                    @endif

                    step="0.01"
                >
            </div>     
        </div>
        <input type="hidden" name="rates[{{ $city->insurance }}][{{ $rateType }}][{{ $city->id }}][type]" value="{{ $rateType }}">
        <input type="hidden" name="rates[{{ $city->insurance }}][{{ $rateType }}][{{ $city->id }}][rate_id]" value="{{ $city->id }}">
        <input type="hidden" name="rates[{{ $city->insurance }}][{{ $rateType }}][{{ $city->id }}][is_commission]" value="{{ $city->isCommission() ? 1 : 0 }}">
    </div>
@endforeach
