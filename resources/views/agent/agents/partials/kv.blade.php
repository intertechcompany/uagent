@if ($ratesForSubAgent->isHaveRates() === true)
    <div class="c-card__statistics--thead">
        <p class="settings-inform__title">КВ</p>
    </div>
    <div class="c-card__statistics--tbody settings-table--body toggle-row__body">
        <div class="">
            <div class="row">
                @foreach (\App\Model\Policy::$activeInsurances as $insurance)
                    @if ($ratesForSubAgent->isHaveOneOfRatesByInsurance($insurance))
                        <div class="cont-p">
                            <div class="toggle-row__1 toggle-row">
                                <div class="toggle-children__head pl-20 js-toggle__nsub">
                                    <p>{{ \App\Model\Policy::insurance($insurance) }}</p>
                                    <i class="fas fa-caret-down"></i>
                                </div>

                                <div class="toggle-children__body">

                                    @if ($ratesForSubAgent->isHaveCityRatesByInsurance($insurance))
                                        @include('agent.agents.partials.city',[
                                            'cityRates' => $ratesForSubAgent->getCityRateByInsurance($insurance),
                                        ])
                                    @endif

                                    @if ($ratesForSubAgent->isHaveRatesByInsurance($insurance))
                                        @include('agent.agents.partials.other', [
                                            'rates' => $ratesForSubAgent->getRateByInsurance($insurance),
                                            'isAll' => $ratesForSubAgent->isHaveCityRatesByInsurance($insurance) === false,
                                        ])
                                    @endif

                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
@endif
