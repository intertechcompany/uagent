@php($rateType = \App\Model\Commission::TYPE_OF_RATE_RATE)
@foreach($rates as $rate)
    <div class="children-body"
         data-rate_type="{{ $rateType }}"
         data-id="{{ $rate->id }}"
         data-is_commission="{{ $rate->isCommission() ? 1 : 0  }}"
    >
        <div class="toggle-children__title c-blue pl-20">
            {{ $isAll ? 'Все' : 'Остальные' }} регионы
        </div>
        <div class="row">

            <div class="col-4">
                <label class="pl-20" for="inpt-1">Всего, {{ \App\Model\Rate::$types[$rate->getType()] }}</label>
                <input
                    class="settings-input ajax-reate-target rate-all"
                    type="number"
                    value="{{ $rate->getValue() }}"
                    readOnly
                    step="0.01"
                >
            </div>

            <div class="col-4">
                <label class="pl-20" for="inpt-2">Вам, {{ \App\Model\Rate::$types[$rate->getType()] }}</label>
                <input
                    class="settings-input ajax-reate-target rate-you"
                    type="number"
                    name="rates[{{ $rate->insurance }}][{{ $rateType }}][{{ $rate->id }}][you]"
                    placeholder="0"
                    data-commission-name="you"

                    @if (($commission = $agentCommissions->getCommission($rate->insurance,$rateType,$rate->getRateId())) !== null)
                        value="{{ $commission->you }}"
                        data-commission_id="{{ $commission->id }}"
                    @else
                        value="{{ old('rates.'.$rate->insurance.'.'.$rateType.'.'.$rate->id .'.you') }}"
                    @endif

                    step="0.01"
                >
            </div>
            <div class="col-4">
                <label class="pl-20" for="inpt-3">Агенту, {{ \App\Model\Rate::$types[$rate->getType()] }}</label>
                <input
                    class="settings-input ajax-reate-target rate-agent"
                    type="number"
                    name="rates[{{ $rate->insurance }}][{{ $rateType }}][{{ $rate->id }}][agent]"
                    placeholder="0"
                    data-commission-name="agent"

                    @if (($commission = $agentCommissions->getCommission($rate->insurance,$rateType,$rate->getRateId())) !== null)
                        value="{{ $commission->agent }}"
                        data-commission_id="{{ $commission->id }}"
                    @else
                        value="{{ old('rates.'.$rate->insurance.'.'.$rateType.'.'.$rate->id.'.agent') }}"
                    @endif

                    step="0.01"
                >
            </div>
            <input type="hidden" name="rates[{{ $rate->insurance }}][{{ $rateType }}][{{ $rate->id }}][type]" value="{{ $rateType }}">
            <input type="hidden" name="rates[{{ $rate->insurance }}][{{ $rateType }}][{{ $rate->id }}][rate_id]" value="{{ $rate->id }}">
            <input type="hidden" name="rates[{{ $rate->insurance }}][{{ $rateType }}][{{ $rate->id }}][is_commission]" value="{{ $rate->isCommission() ? 1 : 0  }}">
        </div>
    </div>

@endforeach
