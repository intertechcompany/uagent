@extends('layouts.agent')

@section('content')

    <div class="c-card c-card--center">
        {!! BootForm::open(['store' => 'subAgents.store']) !!}
        @csrf

        <div class="c-field" @if($errors->has('password')) has-error @endif>
            <label class="c-field__label">Пароль</label>
            {!! BootForm::text(null, null, null, ['name'=>'password','class' => 'c-input u-mb-small','placeholder'=>'Введите Пароль']) !!}
            @if ($errors->has('password'))
                <div class="text-danger">{!! $errors->first('password') !!}</div>
            @endif
        </div>

        <div class="c-field" @if($errors->has('password_confirmation')) has-error @endif>
            <label class="c-field__label">Подтверждение Пароля</label>
            {!! BootForm::text(null, null, null, ['name'=>'password_confirmation','class' => 'c-input u-mb-small','placeholder'=>'Подтвердите Пароль']) !!}
            @if ($errors->has('password_confirmation'))
                <div class="text-danger">{!! $errors->first('password_confirmation') !!}</div>
            @endif
        </div>

        <button type="submit" class="c-btn c-btn--fullwidth c-btn--info">Зарегистрировать</button>

        {!! BootForm::close() !!}

    </div>


@endsection
