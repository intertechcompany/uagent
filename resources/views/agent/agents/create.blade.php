@extends('layouts.agent')

@section('content')

    {!! BootForm::open(['store' => 'agents.store','role' => 'form', 'enctype' => 'multipart/form-data']) !!}
    <div class="row settings-row">
        <div class="col-4 min-w">
            <h3 class="title-new__agent ">Новый субагент</h3>
            <div class="statistics-table__statistics settings-table add-new__agent">
                <div class="c-card__statistics--thead">
                    <p class="settings-inform__title">Общая информация</p>
                </div>
                <div class="c-card__statistics--tbody settings-table--body">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <label class="necessarily" for="last_name ">Фамилия</label>
                                {!! Form::input('last_name', old('last_name'), null, [
                                    'name'=>'last_name',
                                    'id' => 'last_name',
                                    'class' => 'settings-input add-new__agent',
                                    'placeholder'=>'Введите фамилию'
                                ]) !!}
                                @if ($errors->has('last_name'))
                                    <div class="text-danger">{!! $errors->first('last_name') !!}</div>
                                @endif
                            </div>
                        </div>  
                        <div class="row">
                            <div class="col-12">
                                <label class="necessarily" for="name">Имя</label>
                                {!! Form::text('name', old('name'), null, [
                                    'name'=>'name',
                                    'id' => 'name',
                                    'class' => 'settings-input add-new__agent',
                                    'placeholder'=>'Введите имя'
                                ]) !!}
                                @if ($errors->has('name'))
                                    <div class="text-danger">{!! $errors->first('name') !!}</div>
                                @endif
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col-12">
                                <label for="patronymic">Отчество</label>
                                {!! Form::text('patronymic', old('patronymic'), null, [
                                    'name'=>'patronymic',
                                    'id' => 'patronymic',
                                    'class' => 'settings-input add-new__agent',
                                    'placeholder'=>'Введите отчество'
                                ]) !!}
                                @if ($errors->has('patronymic'))
                                    <div class="text-danger">{!! $errors->first('patronymic') !!}</div>
                                @endif
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col-12">
                                <label for="city">Населенный пункт</label>
                                {{ Form::select('city_id', \App\Model\City::query()->orderBy('name')->pluck('name', 'id'), null, ['class' => 'settings-input add-new__agent ui dropdown twig search ', 'id' => 'city', 'readonly' => true]) }}
                                @include('forms.error', ['name' => 'city_id'])
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <label class="necessarily" for="phone">Телефон</label>
                                {!! Form::text('phone', old('phone'), null, [
                                    'name'=>'phone',
                                    'id' => 'phone',
                                    'class' => 'settings-input add-new__agent input-phone',
                                    'placeholder'=>'Введите телефон'
                                ]) !!}
                                @if ($errors->has('phone'))
                                    <div class="text-danger">{!! $errors->first('phone') !!}</div>
                                @endif
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col-12">
                                <label class="necessarily" for="email">E-mail</label>
                                {!! Form::text('email', old('email'), null, [
                                    'name'=>'email',
                                    'id' => 'email',
                                    'class' => 'settings-input add-new__agent',
                                    'placeholder'=>'Введите E-mail'
                                ]) !!}
                                @if ($errors->has('email'))
                                    <div class="text-danger">{!! $errors->first('email') !!}</div>
                                @endif
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col-12">
                                <label class="necessarily" for="password">Пароль</label>
                                {!! Form::text('password', null, null, [
                                    'name'=>'password',
                                    'class' => 'settings-input add-new__agent',
                                    'id' =>'password',
                                    'placeholder'=>'Введите пароль'
                                ]) !!}
                                @if ($errors->has('password'))
                                    <div class="text-danger">{!! $errors->first('password') !!}</div>
                                @endif
                            </div>
                        </div>    
                        <div class="row">
                            <div class="col-12">
                                <label for="text">Комментарий</label>
                                <textarea id="text" class="settings-input" type="text" name="comment" placeholder="Введите текст" >{{ old('comment') }}</textarea>
                                @if ($errors->has('comment'))
                                    <div class="text-danger">{!! $errors->first('comment') !!}</div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="c-field">
                                    <div class="c-choice c-choice--checkbox">
                                        <input type="checkbox" id="is_opportunity_sub_agent" name="is_opportunity_sub_agent" value="1" checked="checked" class="c-choice__input">
                                        <label for="is_opportunity_sub_agent" class="c-choice__label">Может создавать субагента</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-4 min-w">
            <h3 class="u-inline-flex pl-20 opacity-0 title-new__agent ">Настройки профиля</h3>
            <div class="statistics-table__statistics settings-table new-agg">
                @include('agent.agents.partials.kv')
            </div>
            <button type="submit" class="c-btn c-btn--success addn-subagent"><i class="fas fa-check-circle"></i>Добавить субагента</button>
        </div>
    </div>
    {!! BootForm::close() !!}

@endsection
