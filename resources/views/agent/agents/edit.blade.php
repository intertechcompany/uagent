@extends('layouts.agent')

@section('content')

    {!! Form::model($agent, ['route' => [
        'agents.update', 
        $agent->id],
        'role' => 'form', 
        'enctype' => 'multipart/form-data', 
        'method' => 'PUT',
        'class' => 'agent-update-form',
        'data-url' => route('agents.update.field', $agent)
    ]) !!}
    <div class="row edit-sub" data-route_rate="{{ route('agents.update.rates',['agent' => $agent->id]) }}">
        <div class="col-md-12 col-xl-4 col-lg-6 min-w">
            <div class="c-content">
                <h3 class="c-content__h3">Субагент <span class="c-pink">{{ $agent->full_name }}</span></h3>
            </div>
            <div class="statistics-table__filter">
                <h4 class="c-card__statistics--thead">Общая информация</h4>
                <div class="c-card__statistics--tbody c-card__card--tbody">
                    <div class="container">
                        <form action="">
                            <div class="row align-items-center">
                                <div class="col-12">
                                    <div class="bank-card__body create-table__sub">
                                        <div class="row align-items-center">
                                            <div class="col-5">Фамилия</div>
                                            <div class="col ajax-edit-field">
                                                <input class="settings-input" readonly value="{{ $agent->last_name }}" name="last_name">
                                                <a class="edit" href="">
                                                    <i class="fas fa-edit"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="row align-items-center">
                                            <div class="col-5">Имя</div>
                                            <div class="col ajax-edit-field">
                                                <input class="settings-input" readonly value="{{ $agent->name }}" name="name">
                                                <a class="edit" href=""><i class="fas fa-edit"></i></a>
                                            </div>
                                        </div>
                                        <div class="row align-items-center">
                                            <div class="col-5">Отчество</div>
                                            <div class="col ajax-edit-field">
                                                <input class="settings-input" readonly value="{{ $agent->patronymic }}" name="patronymic">
                                                <a class="edit" href="#"><i class="fas fa-edit"></i></a>
                                            </div>
                                        </div>
                                        <div class="row align-items-center">
                                            <div class="col-5">Населенный пункт</div>
                                            <div class="col ajax-edit-field">
                                                {{ Form::select('city_id', \App\Model\City::query()->orderBy('name')->pluck('name', 'id'), $agent->city_id, ['class' => 'settings-input ui dropdown twig search min-wv', 'readonly' => true]) }}
                                                @include('forms.error', ['name' => 'city_id'])
                                                <a class="edit" href="#">
                                                    <i class="fas fa-edit"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="row align-items-center">
                                            <div class="col-5">Телефон</div>
                                            <div class="col ajax-edit-field">
                                                <input class="settings-input input-phone" readonly type="number" name="phone" value="{{ $agent->phone }}" placeholder="XXXXXXX" ><a class="edit" href="#"><i class="fas fa-edit"></i></a>
                                            </div>
                                        </div>
                                        <div class="row align-items-center">
                                            <div class="col-5">E-mail</div>
                                            <div class="col ajax-edit-field">
                                                <input class="settings-input" readonly type="email" name="email" value="{{ $agent->email }}" ><a class="edit" href="#"><i class="fas fa-edit"></i></a>
                                            </div>
                                        </div>
                                        <div class="row align-items-center">
                                            <div class="col-5">Пароль</div>
                                            <div class="col ajax-edit-field">
                                                <input class="settings-input" readonly type="password" name="password" value="{{ $agent->password }}" >
                                                <a class="edit" href="#"><i class="fas fa-edit"></i></a>
                                            </div>
                                        </div>
                                        <div class="row align-items-center">
                                            <div class="col-5">Комментарий</div>
                                            <div class="col ajax-edit-field">
                                                <input class="settings-input" readonly type="text" name="comment" value="{{ $agent->comment }}" >
                                                <a class="edit" href="#">
                                                    <i class="fas fa-edit"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="c-field">
                                                    <div class="c-choice c-choice--checkbox">
                                                        <input
                                                            type="checkbox"
                                                            id="is_opportunity_sub_agent"
                                                            name="is_opportunity_sub_agent"
                                                            value="1"
                                                            @if((bool) $agent->is_opportunity_sub_agent === true) checked="checked" @endif
                                                            class="c-choice__input"
                                                            data-route="{{ route('agents.update.canCreateAgent',['agent' => $agent->id]) }}"
                                                        >
                                                        <label for="is_opportunity_sub_agent" class="c-choice__label">Может создавать субагента</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-xl-4 col-lg-6 min-w">
            <h3 class="c-content__h3 opacity-0">Субагент</h3>
            <div class="statistics-table__statistics settings-table edit-table">
                @include('agent.agents.partials.kv')
            </div>
            <button type="submit" class="c-btn c-btn--success addn-subagent"><i class="fas fa-check-circle"></i>Сохранить субагента</button>
        </div>
    </div>
    {!! BootForm::close() !!}

@endsection
