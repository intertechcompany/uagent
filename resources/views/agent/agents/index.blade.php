@extends('layouts.agent')

@section('title')
    <i class="c-sidebar__icon feather icon-users"></i>Мои агенты
@endsection

@section('content')
<div class="vue-component" data-url="{{ route('agents.filter') }}" id="app">
    <Agents></Agents>
</div>
@endsection