@extends('base')

@section('head')
    <link href="https://fonts.googleapis.com/css?family=Exo+2:300,400,500,700" rel="stylesheet">
@endsection

@section('body')
    <div class="main-new">
        <section class="section-head">
            <div class="container-new">
                <header>
                    <div class="wrap-header">
                        <div class="image-holder image-holder__logo--mobile image-holder__logo">
                            <a href="/">
                                <img src="/img/home-new/uagent-logo.png" alt="Uagent" class="logo-header__new">
                                <img src="/img/home-new/uagent-logo__mobile.png" alt="Uagent" class="logo-header__new--mobile">
                            </a>
                        </div>
                        <div class="central-text">
                            <a class="central-text__number" href="tel:8 800 600 71 97">8 800 600 71 97</a>
                            <p class="text-mw__233">
                                Единая платформа для
                                страховых брокеров и агентов
                            </p>
                        </div>
                        <div class="h-btn__box">
                            <button class="h-btn h-btn__dark custom-modal-js" data-modal="modal-sign">Войти</button>
                            <button class="register h-btn__link custom-modal-js" data-modal="modal-register">Регистрация</button>
                        </div>
                    </div>
                </header>
                <div class="section-head__content">
                    <h2 class="title-dark__section title-section__head">
                        ПОЛУЧАЙТЕ СРАЗУ
                        <span class="c-pink__new">МАКСИМАЛЬНЫЙ ДОХОД</span>
                        НА ПОЛИСАХ ОСАГО
                    </h2>
                    <p class="text-dark__section">
                        Регистрируйтесь на платформе и начните оформлять полисы ОСАГО на лучших условиях заработка в
                        страховании
                    </p>
                    <button data-modal="modal-register" class="h-btn h-btn__pink custom-modal-js">
                        Получить легкий доступ
                    </button>
                    <div class="img-holder__absolute img-holder__absolute--1">
                        <div class="image-holder">
                            <img class="desk-img" src="/img/home-new/section-1-bg.svg" alt="">
                            <img class="mobile-img" src="/img/home-new/mobile-bg-1.png" alt="">
                        </div>
                    </div>
                </div>

            </div>
            <div class="wrap-items__section">
                <div class="container-new">
                    <div class="section-head__bottom--items">
                        <div class="bottom-item">
                            <div class="number-item">
                                625
                                <span class="c-pink__fz">₽/полис</span>
                            </div>
                            <p class="bottom-sub__text">
                                Средний доход с полиса
                            </p>
                        </div>
                        <div class="bottom-item">
                            <div class="number-item">
                                32 500
                                <span class="c-pink__fz">₽/мес</span>
                            </div>
                            <p class="bottom-sub__text">
                                Средний заработок агента
                            </p>
                        </div>
                        <div class="bottom-item">
                            <div class="number-item">
                                1252
                                <span class="c-pink__fz">человек/мес</span>
                            </div>
                            <p class="bottom-sub__text">
                                Регистраций новых агентов
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section-presentation">
            <div class="container-new container-presentation">
                <h2 class="title-light__section">
                    ЮАГЕНТ <span class="span-ml">- СОВРЕМЕННАЯ ПЛАТФОРМА
                ДЛЯ СТРАХОВЫХ </span><span class="c-pink__new">БРОКЕРОВ И АГЕНТОВ</span>
                </h2>
                <div class="section-presentation__content">

                    <ul class="list-light__section">
                        <li>Автозаполнение полей по номеру авто</li>
                        <li>Интуитивно-понятный интерфейс</li>
                        <li>Редактируемые черновики</li>
                        <li>Автоподгрузка КБМ по всем водителям</li>
                        <li>Предварительный расчет по всем страховым -
                            3 минуты и полис готов!
                        </li>
                    </ul>
                    <a href="uagent.pdf" target="_blank" class="h-btn h-btn__transparent">Скачать презентацию</a>
                    <div class="img-holder__absolute img-holder__absolute--2">
                        <div class="image-holder">
                            <img class="desk-img" src="/img/home-new/section-2-bg.svg" alt="">
                            <img class="mobile-img" src="/img/home-new/mobile-bg-2.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section-insured__company">
            <div class="container-new">
                <h2 class="title-white__section">
                    Доступ к <span class="c-pink__new">самым востребованным</span>
                    страховым компаниям
                </h2>
                <ul class="section-insured__content">
                    <li class="item-insured">
                        <img src="img/home-new/alfa-insured.jpg" alt="alfa">
                        <p class="item-insured__sub--title">
                            АльфаСтрахование
                        </p>
                    </li>
                    <li class="item-insured">
                        <img src="img/home-new/soglasie-insured.png" alt="soglasie">
                        <p class="item-insured__sub--title">
                            Согласие
                        </p>
                    </li>
                    <li class="item-insured">
                        <img src="img/home-new/ingoss-insured.jpg" alt="ingoss">
                        <p class="item-insured__sub--title">
                            Ингосстрах
                        </p>
                    </li>
                    <li class="item-insured">
                        <img src="img/home-new/russ-insured.jpg" alt="russ">
                        <p class="item-insured__sub--title">
                            Росгосстрах
                        </p>
                    </li>
                </ul>
            </div>
        </section>
        <section class="section-indepented__agent">
            <div class="container-new container-new__indepented">
                <h2 class="title-dark__section title-section__indepented">
                    БУДЬТЕ <span class="c-pink__new">НЕЗАВИСИМЫМ АГЕНТОМ</span> <br>
                    ИЛИ РАЗВИВАЙТЕ СВОЮ СЕТЬ АГЕНТОВ
                </h2>
                <div class="section-indepented__content">
                    <p class="section-indepented__text">
                        Платформой Юагент может пользоваться любой желающий.
                        Регистрируйте внутри платформы неограниченное количество аккаунтов, выставляя каждому агенту свое КВ
                        по каждой страховой компании.
                        Максимально прозрачная система начисления КВ и вывода средств.
                        <br>А так же подробная статистика по вашим и агентским продажам
                    </p>
                    <button data-modal="modal-register" class="h-btn h-btn__pink custom-modal-js">
                        Получить легкий доступ
                    </button>
                    <p class="section-indepented__text--sub">
                        Регистрация займет менее 1 минуты.
                    </p>
                    <div class="img-holder__absolute img-holder__absolute--3">
                        <div class="image-holder">
                            <img class="desk-img" src="/img/home-new/section-3-bg.svg" alt="bg-3">
                            <img class="mobile-img" src="/img/home-new/mobile-bg-3.png" alt="bg-3">
                        </div>
                    </div>
                </div>


            </div>
        </section>
        <section class="section-earn__more">
            <div class="container-new container-earn__more">
                <div class="section-earn__content">
                    <h2 class="title-white__section title-earn__section">
                        ЗАРАБАТЫВАЙТЕ<span class="c-pink__new"> БОЛЬШЕ,</span>
                        ПОЛУЧАЙТЕ <span class="c-pink__new">БЫСТРЕЕ</span>
                    </h2>
                    <p class="section-earn__text">
                        Заявки на вывод комиссионного вознаграждения производятся в режиме «онлайн» - на банковские карты,
                        электронный кошелек или расчетный счет.
                        Средний время выплаты «от заявки до получения КВ» - 7 дней!
                    </p>
                    <div class="img-holder__absolute img-holder__absolute--4">
                        <div class="image-holder">
                            <img class="desk-img" src="/img/home-new/section-4-bg.svg" alt="bg-3">
                            <img class="mobile-img" src="/img/home-new/mobile-bg-4.png" alt="bg-3">
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <section class="section-care__service">
            <div class="container-new container-care__service">
                <div class="section-care__content">
                    <h2 class="title-dark__section title-section__care">
                        <span class="c-pink__new">СЛУЖБА ЗАБОТЫ</span>
                        ОНЛАЙН НА СВЯЗИ
                    </h2>
                    <p class="section-care__text">
                        Все сотрудники поддержки Юагент - это «боевые» страховые агенты и они полностью понимают
                    </p>
                    <button data-modal="modal-register" class="h-btn h-btn__pink custom-modal-js">
                        Получить легкий доступ
                    </button>
                    <p class="section-care_text--sub">
                        Получите моментальный доступ к платформе и начните зарабатывать больше, чем другие на каждом проданном полисе.
                    </p>
                    <div class="img-holder__absolute img-holder__absolute--5">
                        <div class="image-holder">
                            <img class="desk-img" src="/img/home-new/section-5-bg.svg" alt="bg-3">
                            <img class="mobile-img" src="/img/home-new/mobile-bg-5.png" alt="bg-3">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section-form__quetions">
            <div class="container-new container-form__quetions">
                <div class="section-form__content">
                    <h2 class="title-white__section title-form__section">
                        <span class="c-pink__new">Остались вопросы?</span>
                        Напишите нам!
                    </h2>
                    <p class="section-earn__text">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                    </p>
                    <div class="img-holder__absolute form-holder__absolute--5">
                        <div class="form-holder__bottom">
                            <form id="formSend questionForm" class="form-bottom" method="post">
                                <input placeholder="Ф.И.О" name="name" class="form-new__input fio" type="text" required>
                                <input placeholder="Телефон" name="phone" class="form-new__input tell-new feedback-phone" type="text" required>
                                <textarea id="message" placeholder="Текст" name="message" class="form-new__mesege feedback-text"></textarea>
                                <button type="button"  class="h-btn h-btn__pink btn-send feedback g-recaptcha" data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}" data-callback='feedback' id="questionFormButton">
                                    Отправить сообщение
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <footer class="new-footer">
            <div class="container-new container-new__footer">
                <div class="footer-content">
                    <div class="image-holder image-holder__logo">
                        <a href="/"> <img src="/img/home-new/uagent-logo.png" alt="Uagent" class="logo-header__new">
                        </a>
                    </div>
                    <div class="footer-content__central">
                        <a class="central-text__number" href="tel:88006007197">8 800 600 71 97</a>
                        <p class="sub-text">Единая платформа для страховых брокеров и агентов </p>
                        <p class="sub-text__2">© 2019 ИП Шильников И.Н. ОГРНИП 315366800006094</p>
                    </div>
                    <div class="footer-content__last">
                        <div class="h-btn__box">
                            <button class="h-btn h-btn__dark custom-modal-js" data-modal="modal-sign">Войти</button>
                            <button class="register h-btn__link  custom-modal-js" data-modal="modal-register">Регистрация</button>
                        </div>
                        <a href="#" class="privacy-policy custom-modal-js " data-modal="terms-of__use">Политика конфиденциальности</a>
                    </div>
                </div>
                <div class="footer-content__mobile">
                    <div class="image-holder image-holder__logo">
                        <a href="/"><img src="/img/home-new/uagent-logo__mobile.png" alt="Uagent" class="logo-header__new">
                        </a>
                    </div>
                    <div class="h-btn__box">
                        <button class="h-btn h-btn__dark custom-modal-js" data-modal="modal-sign">Войти</button>
                        <button class="register h-btn__link  custom-modal-js" data-modal="modal-register">Регистрация</button>
                    </div>
                    <a class="central-text__number" href="tel:88006007197">8 800 600 71 97</a>
                    <p class="sub-text">Единая платформа для страховых брокеров и агентов </p>
                    <a href="#" class="privacy-policy custom-modal-js" data-modal="terms-of__use">Политика конфиденциальности</a>
                    <p class="sub-text__2">© 2019 ИП Шильников И.Н. ОГРНИП 315366800006094</p>
                </div>
            </div>
        </footer>
        @if(isset($token))
            <div class="home-new__modal modal-js pass-recovery open" id="modal-pass-recovery" >
                <button class="close-new__modal js-close__local-modal"></button>
                <div class="title-popup__new">
                    создать новый
                    пароль
                </div>
                <div class="field-error"></div>
                <form action="{{route('reset')}}" method="post">
                    {{csrf_field()}}
                    <input type="hidden" value="{{$token}}" id="reset_token" name="token">
                    <div class="wrap-input__new">
                        <input type="password" class="form-new__input popup-new__input" name="reset_password" id="reset_password" placeholder="Введите пароль">
                        <input type="password" class="form-new__input popup-new__input" name="reset_password_confirmation" id="reset_password_confirmation" placeholder="Подтверждение пароля">
                    </div>
                    <div class="row-btn">
                        <button type="button" class="h-btn h-btn__pink reset-password-button">
                            Восстановить пароль
                        </button>
                    </div>
                </form>
            </div>
        @endif
        @include('agent.auth.login')
        @include('agent.auth.registration')
        @include('agent.auth.reset')
        @include('agent.auth.terms-of-use')
    </div>









    {{--@include('include.modals.auth')--}}

@endsection

@section('scripts')
@endsection