@extends('layouts.agent')

@section('title')
    <i class="c-sidebar__icon" data-feather="dollar-sign"></i>Выплаты
@endsection

@section('content')

<div class="vue-component" data-url="{{ route('withdraws.filter') }}" id="app">
	<payment></payment>
</div>

@endsection

