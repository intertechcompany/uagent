<?php

use Illuminate\Database\Seeder;

class UserDashboardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('dashboard_blocks')->delete();

    	$blocks = [
            [
                'name' => 'КОЛИЧЕСВО ПРОДАНЫХ ПОЛИСОВ (ВСЕГО)',
                'key' => 'count_sales_policies_all',
                'sort_order' => 1,
            ],
            [
                'name' => 'КОЛИЧЕСВО ПРОДАНЫХ ПОЛИСОВ (СЕГОДНЯ)',
                'key' => 'count_sales_policies_today',
                'sort_order' => 2,
            ],
            [
                'name' => 'ВАША ПРИБЫЛЬ (ВСЕГО)',
                'key' => 'your_money_all',
                'sort_order' => 3,
            ],
            [
                'name' => 'ПОСЛЕДНИЙ ПОЛИС ПРОДАНО НА СУМУ',
                'key' => 'last_policy_sale_sum',
                'sort_order' => 4,
            ],
        ];

		DB::table('dashboard_blocks')->insert($blocks);
    }
}
