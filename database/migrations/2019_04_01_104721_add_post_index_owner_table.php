<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPostIndexOwnerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('owners', 'post_code')) {
            return;
        }

        Schema::table('owners', function (Blueprint $table) {
            $table->string('post_code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn('owners', 'post_code')) {
            return;
        }

        Schema::table('owners', function(Blueprint $table) {
            $table->dropColumn('post_code');
        });
    }
}
