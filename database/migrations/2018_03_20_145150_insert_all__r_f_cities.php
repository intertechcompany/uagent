<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertAllRFCities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::statement('INSERT INTO `cities` (`name`)
VALUES
(\'Гвардейск\'),
	(\'Гурьевск\'),
	(\'Гусев\'),
	(\'Зеленоградск\'),
	(\'Краснознаменск\'),
	(\'Неман\'),
	(\'Нестеров\'),
	(\'Озерск\'),
	(\'Полесск\'),
	(\'Правдинск\'),
	(\'Славск\'),
	(\'Черняховск\'),
	(\'Балтийск\'),
	(\'Приморск\'),
	(\'Светлогорск\'),
	(\'Калуга\'),
	(\'Обнинск\'),
	(\'Ермолино\'),
	(\'Боровск\'),
	(\'Балабаново\'),
	(\'Ермолино\'),
	(\'Боровск-1\'),
	(\'Спас-Деменск\'),
	(\'Кондрово\'),
	(\'Жиздра\'),
	(\'Жуков\'),
	(\'Белоусово\'),
	(\'Кременки\'),
	(\'Киров\'),
	(\'Козельск\'),
	(\'Сосенский\'),
	(\'Людиново\'),
	(\'Малоярославец\'),
	(\'Медынь\'),
	(\'Мещовск\'),
	(\'Мосальск\'),
	(\'Сухиничи\'),
	(\'Таруса\'),
	(\'Юхнов\'),
	(\'Юхнов-1\'),
	(\'Юхнов-2\'),
	(\'Петропавловск-Камчатский\'),
	(\'Вилючинск\'),
	(\'Елизово\'),
	(\'Анжеро-Судженск\'),
	(\'Березовский\'),
	(\'Калтан\'),
	(\'Киселевск\'),
	(\'Мыски\'),
	(\'Осинники\'),
	(\'Тайга\'),
	(\'Кемерово\'),
	(\'Ленинск-Кузнецкий\'),
	(\'Полысаево\'),
	(\'Новокузнецк\'),
	(\'Прокопьевск\'),
	(\'Юрга\'),
	(\'Белово\'),
	(\'Междуреченск\'),
	(\'Кемерово\'),
	(\'Белово\'),
	(\'Гурьевск\'),
	(\'Салаир\'),
	(\'Ленинск-Кузнецкий\'),
	(\'Полысаево\'),
	(\'Мариинск\'),
	(\'Новокузнецк\'),
	(\'Прокопьевск\'),
	(\'Таштагол\'),
	(\'Топки\'),
	(\'Юрга\'),
	(\'Киров\'),
	(\'Кирово-Чепецк\'),
	(\'Кирово-Чепецк\'),
	(\'Белая Холуница\'),
	(\'Кирс\'),
	(\'Вятские Поляны\'),
	(\'Сосновка\'),
	(\'Зуевка\'),
	(\'Кирово-Чепецк\'),
	(\'Кирово-Чепецк\'),
	(\'Котельнич\'),
	(\'Луза\'),
	(\'Малмыж\'),
	(\'Мураши\'),
	(\'Нолинск\'),
	(\'Омутнинск\'),
	(\'Орлов\'),
	(\'Халтурин\'),
	(\'Слободской\'),
	(\'Советск\'),
	(\'Уржум\'),
	(\'Яранск\'),
	(\'Волгореченск\'),
	(\'Майкоп\'),
	(\'Адыгейск\'),
	(\'Уфа\'),
	(\'Агидель\'),
	(\'Нефтекамск\'),
	(\'Октябрьский\'),
	(\'Салават\'),
	(\'Сибай\'),
	(\'Кумертау\'),
	(\'Межгорье\'),
	(\'Стерлитамак\'),
	(\'Уфа\'),
	(\'Баймак\'),
	(\'Белебей\'),
	(\'Белорецк\'),
	(\'Межгорье\'),
	(\'Бирск\'),
	(\'Благовещенск\'),
	(\'Ишимбай\'),
	(\'Мелеуз\'),
	(\'Стерлитамак\'),
	(\'Туймазы\'),
	(\'Учалы\'),
	(\'Янаул\'),
	(\'Давлеканово\'),
	(\'Дюртюли\'),
	(\'Улан-Удэ\'),
	(\'Северобайкальск\'),
	(\'Закаменск\'),
	(\'Бабушкин\'),
	(\'Кяхта\'),
	(\'Северобайкальск\'),
	(\'Гусиноозерск\'),
	(\'Горно-Алтайск\'),
	(\'Махачкала\'),
	(\'Дагестанские Огни\'),
	(\'Избербаш\'),
	(\'Каспийск\'),
	(\'Южно-Сухокумск\'),
	(\'Дербент\'),
	(\'Кизилюрт\'),
	(\'Кизляр\'),
	(\'Хасавюрт\'),
	(\'Буйнакск\'),
	(\'Магас\'),
	(\'Карабулак\'),
	(\'Назрань\'),
	(\'Малгобек\'),
	(\'Сунжа\'),
	(\'Назрань\'),
	(\'Малгобек\'),
	(\'Карабулак\'),
	(\'Орджоникидзевское\'),
	(\'Нальчик\'),
	(\'Прохладный\'),
	(\'Баксан\'),
	(\'Баксан\'),
	(\'Майский\'),
	(\'Прохладный\'),
	(\'Терек\'),
	(\'Нарткала\'),
	(\'Чегем\'),
	(\'Тырныауз\'),
	(\'Элиста\'),
	(\'Городовиковск\'),
	(\'Лагань\'),
	(\'Черкесск\'),
	(\'Карачаевск\'),
	(\'Теберда\'),
	(\'Теберда\'),
	(\'Усть-Джегута\'),
	(\'Петрозаводск\'),
	(\'Костомукша\'),
	(\'Сортавала\'),
	(\'Беломорск\'),
	(\'Кемь\'),
	(\'Кондопога\'),
	(\'Лахденпохья\'),
	(\'Медвежьегорск\'),
	(\'Олонец\'),
	(\'Питкяранта\'),
	(\'Пудож\'),
	(\'Сегежа\'),
	(\'Суоярви\'),
	(\'Сыктывкар\'),
	(\'Воркута\'),
	(\'Вуктыл\'),
	(\'Инта\'),
	(\'Печора\'),
	(\'Сосногорск\'),
	(\'Усинск\'),
	(\'Ухта\'),
	(\'Воркута\'),
	(\'Вуктыл\'),
	(\'Инта\'),
	(\'Емва\'),
	(\'Печора\'),
	(\'Сосногорск\'),
	(\'Усинск\'),
	(\'Микунь\'),
	(\'Ухта\'),
	(\'Йошкар-Ола\'),
	(\'Волжск\'),
	(\'Козьмодемьянск\'),
	(\'Волжск\'),
	(\'Козьмодемьянск\'),
	(\'Звенигово\'),
	(\'Саранск\'),
	(\'Ковылкино\'),
	(\'Рузаевка\'),
	(\'Ардатов\'),
	(\'Инсар\'),
	(\'Ковылкино\'),
	(\'Краснослободск\'),
	(\'Рузаевка\'),
	(\'Темников\'),
	(\'Олекминск\'),
	(\'Якутск\'),
	(\'Нерюнгри\'),
	(\'Алдан\'),
	(\'Томмот\'),
	(\'Верхоянск\'),
	(\'Вилюйск\'),
	(\'Ленск\'),
	(\'Мирный\'),
	(\'Удачный\'),
	(\'Нюрба\'),
	(\'Олекминск\'),
	(\'Среднеколымск\'),
	(\'Покровск\'),
	(\'Владикавказ\'),
	(\'Алагир\'),
	(\'Ардон\'),
	(\'Дигора\'),
	(\'Моздок\'),
	(\'Беслан\'),
	(\'Казань\'),
	(\'Набережные Челны\'),
	(\'Агрыз\'),
	(\'Азнакаево\'),
	(\'Альметьевск\'),
	(\'Арск\'),
	(\'Бавлы\'),
	(\'Бугульма\'),
	(\'Буинск\'),
	(\'Иннополис\'),
	(\'Елабуга\'),
	(\'Заинск\'),
	(\'Зеленодольск\'),
	(\'Лаишево\'),
	(\'Лениногорск\'),
	(\'Мамадыш\'),
	(\'Менделеевск\'),
	(\'Мензелинск\'),
	(\'Нижнекамск\'),
	(\'Нурлат\'),
	(\'Болгар\'),
	(\'Тетюши\'),
	(\'Набережные Челны\'),
	(\'Чистополь\'),
	(\'Кызыл\'),
	(\'Ак-Довурак\'),
	(\'Чадан\'),
	(\'Туран\'),
	(\'Шагонар\'),
	(\'Ижевск\'),
	(\'Сарапул\'),
	(\'Воткинск\'),
	(\'Глазов\'),
	(\'Можга\'),
	(\'Воткинск\'),
	(\'Глазов\'),
	(\'Камбарка\'),
	(\'Можга\'),
	(\'Абакан\'),
	(\'Саяногорск\'),
	(\'Черногорск\'),
	(\'Абаза\'),
	(\'Сорск\'),
	(\'Абаза\'),
	(\'Сорск\'),
	(\'Грозный\'),
	(\'Аргун\'),
	(\'Гудермес\'),
	(\'Урус-Мартан\'),
	(\'Шали\'),
	(\'Чебоксары\'),
	(\'Алатырь\'),
	(\'Канаш\'),
	(\'Новочебоксарск\'),
	(\'Шумерля\'),
	(\'Козловка\'),
	(\'Мариинский Посад\'),
	(\'Цивильск\'),
	(\'Ядрин\'),
	(\'Барнаул\'),
	(\'Алейск\'),
	(\'Белокуриха\'),
	(\'Бийск\'),
	(\'Камень-на-Оби\'),
	(\'Новоалтайск\'),
	(\'Рубцовск\'),
	(\'Славгород\'),
	(\'Заринск\'),
	(\'Яровое\'),
	(\'Змеиногорск\'),
	(\'Змеиногорск\'),
	(\'Камень-на-Оби\'),
	(\'Горняк\'),
	(\'Краснодар\'),
	(\'Армавир\'),
	(\'Геленджик\'),
	(\'Горячий Ключ\'),
	(\'Кропоткин\'),
	(\'Новороссийск\'),
	(\'Сочи\'),
	(\'Абинск\'),
	(\'Анапа\'),
	(\'Апшеронск\'),
	(\'Хадыженск\'),
	(\'Белореченск\'),
	(\'Гулькевичи\'),
	(\'Ейск\'),
	(\'Кропоткин\'),
	(\'Кореновск\'),
	(\'Крымск\'),
	(\'Курганинск\'),
	(\'Лабинск\'),
	(\'Новокубанск\'),
	(\'Приморско-Ахтарск\'),
	(\'Славянск-на-Кубани\'),
	(\'Темрюк\'),
	(\'Тимашевск\'),
	(\'Тихорецк\'),
	(\'Туапсе\'),
	(\'Усть-Лабинск\'),
	(\'Красноярск\'),
	(\'Бородино\'),
	(\'Дивногорск\'),
	(\'Железногорск\'),
	(\'Красноярск-26\'),
	(\'Зеленогорск\'),
	(\'Красноярск-45\'),
	(\'Игарка\'),
	(\'Кайеркан\'),
	(\'Лесосибирск\'),
	(\'Норильск\'),
	(\'Сосновоборск\'),
	(\'Талнах\'),
	(\'Ачинск\'),
	(\'Боготол\'),
	(\'Енисейск\'),
	(\'Заозерный\'),
	(\'Канск\'),
	(\'Минусинск\'),
	(\'Назарово\'),
	(\'Шарыпово\'),
	(\'Ачинск\'),
	(\'Боготол\'),
	(\'Енисейск\'),
	(\'Иланский\'),
	(\'Канск\'),
	(\'Кодинск\'),
	(\'Восточный\'),
	(\'Артемовск\'),
	(\'Минусинск\'),
	(\'Назарово\'),
	(\'Заозерный\'),
	(\'Заозерный\'),
	(\'Игарка\'),
	(\'Ужур\'),
	(\'Уяр\'),
	(\'Уяр\'),
	(\'Шарыпово\'),
	(\'Дудинка\'),
	(\'Владивосток\'),
	(\'Арсеньев\'),
	(\'Артем\'),
	(\'Находка\'),
	(\'Партизанск\'),
	(\'Фокино\'),
	(\'Большой Камень\'),
	(\'Дальнегорск\'),
	(\'Дальнереченск\'),
	(\'Спасск-Дальний\'),
	(\'Уссурийск\'),
	(\'Город Уссурийск\'),
	(\'Лесозаводск\'),
	(\'Ставрополь\'),
	(\'Ессентуки\'),
	(\'Железноводск\'),
	(\'Кисловодск\'),
	(\'Лермонтов\'),
	(\'Невинномысск\'),
	(\'Пятигорск\'),
	(\'Георгиевск\'),
	(\'Минеральные Воды\'),
	(\'Благодарный\'),
	(\'Буденновск\'),
	(\'Георгиевск\'),
	(\'Изобильный\'),
	(\'Ипатово\'),
	(\'Новопавловск\'),
	(\'Минеральные Воды\'),
	(\'Минеральные Воды\'),
	(\'Нефтекумск\'),
	(\'Новоалександровск\'),
	(\'Светлоград\'),
	(\'Зеленокумск\'),
	(\'Михайловск\'),
	(\'Хабаровск\'),
	(\'Амурск\'),
	(\'Бикин\'),
	(\'Комсомольск-на-Амуре\'),
	(\'Николаевск-на-Амуре\'),
	(\'Советская Гавань\'),
	(\'Амурск\'),
	(\'Бикин\'),
	(\'Вяземский\'),
	(\'Николаевск-на-Амуре\'),
	(\'Советская Гавань\'),
	(\'Углегорск\'),
	(\'Благовещенск\'),
	(\'Райчихинск\'),
	(\'Белогорск\'),
	(\'Зея\'),
	(\'Свободный\'),
	(\'Тында\'),
	(\'Шимановск\'),
	(\'Циолковский\'),
	(\'Циолковский\'),
	(\'Завитинск\'),
	(\'Сковородино\'),
	(\'Архангельск\'),
	(\'Мирный\'),
	(\'Новодвинск\'),
	(\'Северодвинск\'),
	(\'Коряжма\'),
	(\'Вельск\'),
	(\'Каргополь\'),
	(\'Котлас\'),
	(\'Сольвычегодск\'),
	(\'Мезень\'),
	(\'Няндома\'),
	(\'Онега\'),
	(\'Шенкурск\'),
	(\'Астрахань\'),
	(\'Знаменск\'),
	(\'Ахтубинск\'),
	(\'Ахтубинск-7\'),
	(\'Ахтубинск 7\'),
	(\'Камызяк\'),
	(\'Нариманов\'),
	(\'Харабали\'),
	(\'Белгород\'),
	(\'Старый Оскол\'),
	(\'Шебекино\'),
	(\'Губкин\'),
	(\'Алексеевка\'),
	(\'Валуйки\'),
	(\'Грайворон\'),
	(\'Губкин\'),
	(\'Короча\'),
	(\'Бирюч\'),
	(\'Новый Оскол\'),
	(\'Старый Оскол\'),
	(\'Шебекино\'),
	(\'Строитель\'),
	(\'Брянск\'),
	(\'Сельцо\'),
	(\'Клинцы\'),
	(\'Новозыбков\'),
	(\'Фокино\'),
	(\'Стародуб\'),
	(\'Дятьково\'),
	(\'Фокино\'),
	(\'Жуковка\'),
	(\'Злынка\'),
	(\'Карачев\'),
	(\'Мглин\'),
	(\'Почеп\'),
	(\'Севск\'),
	(\'Стародуб\'),
	(\'Сураж\'),
	(\'Трубчевск\'),
	(\'Унеча\'),
	(\'Владимир\'),
	(\'Радужный\'),
	(\'Гусь-Хрустальный\'),
	(\'Ковров\'),
	(\'Муром\'),
	(\'Александров\'),
	(\'Карабаново\'),
	(\'Струнино\'),
	(\'Вязники\'),
	(\'Гороховец\'),
	(\'Гусь-Хрустальный\'),
	(\'Курлово\'),
	(\'Камешково\'),
	(\'Киржач\'),
	(\'Ковров\'),
	(\'Кольчугино\'),
	(\'Меленки\'),
	(\'Муром\'),
	(\'Петушки\'),
	(\'Костерево\'),
	(\'Покров\'),
	(\'Собинка\'),
	(\'Лакинск\'),
	(\'Судогда\'),
	(\'Суздаль\'),
	(\'Юрьев-Польский\'),
	(\'Волгоград\'),
	(\'Волжский\'),
	(\'Камышин\'),
	(\'Михайловка\'),
	(\'Урюпинск\'),
	(\'Фролово\'),
	(\'Дубовка\'),
	(\'Жирновск\'),
	(\'Калач-на-Дону\'),
	(\'Камышин\'),
	(\'Петров Вал\'),
	(\'Котельниково\'),
	(\'Котово\'),
	(\'Ленинск\'),
	(\'Михайловка\'),
	(\'Николаевск\'),
	(\'Новоаннинский\'),
	(\'Палласовка\'),
	(\'Серафимович\'),
	(\'Краснослободск\'),
	(\'Суровикино\'),
	(\'Урюпинск\'),
	(\'Фролово\'),
	(\'Вологда\'),
	(\'Череповец\'),
	(\'Бабаево\'),
	(\'Белозерск\'),
	(\'Великий Устюг\'),
	(\'Красавино\'),
	(\'Красавино\'),
	(\'Вытегра\'),
	(\'Грязовец\'),
	(\'Кириллов\'),
	(\'Никольск\'),
	(\'Сокол\'),
	(\'Кадников\'),
	(\'Тотьма\'),
	(\'Устюжна\'),
	(\'Харовск\'),
	(\'Воронеж\'),
	(\'Лиски\'),
	(\'Нововоронеж\'),
	(\'Воронеж-45\'),
	(\'Бобров\'),
	(\'Богучар\'),
	(\'Борисоглебск\'),
	(\'Бутурлиновка\'),
	(\'Калач\'),
	(\'Лиски\'),
	(\'Новохоперск\'),
	(\'Острогожск\'),
	(\'Павловск\'),
	(\'Поворино\'),
	(\'Россошь\'),
	(\'Семилуки\'),
	(\'Эртиль\'),
	(\'Иваново\'),
	(\'Кинешма\'),
	(\'Кохма\'),
	(\'Вичуга\'),
	(\'Гаврилов Посад\'),
	(\'Заволжск\'),
	(\'Кинешма\'),
	(\'Наволоки\'),
	(\'Комсомольск\'),
	(\'Приволжск\'),
	(\'Плес\'),
	(\'Пучеж\'),
	(\'Родники\'),
	(\'Тейково\'),
	(\'Фурманов\'),
	(\'Шуя\'),
	(\'Южа\'),
	(\'Юрьевец\'),
	(\'Иркутск-45\'),
	(\'Саянск\'),
	(\'Иркутск\'),
	(\'Ангарск\'),
	(\'Братск\'),
	(\'Бодайбо\'),
	(\'Зима\'),
	(\'Нижнеудинск\'),
	(\'Тайшет\'),
	(\'Тулун\'),
	(\'Усолье-Сибирское\'),
	(\'Усть-Илимск\'),
	(\'Усть-Кут\'),
	(\'Черемхово\'),
	(\'Шелехов\'),
	(\'Свирск\'),
	(\'Иркутск\'),
	(\'Ангарск\'),
	(\'Бодайбо\'),
	(\'Братск\'),
	(\'Вихоревка\'),
	(\'Зима\'),
	(\'Киренск\'),
	(\'Железногорск-Илимский\'),
	(\'Нижнеудинск\'),
	(\'Алзамай\'),
	(\'Слюдянка\'),
	(\'Байкальск\'),
	(\'Тайшет\'),
	(\'Бирюсинск\'),
	(\'Тулун\'),
	(\'Усолье-Сибирское\'),
	(\'Усть-Илимск\'),
	(\'Усть-Кут\'),
	(\'Черемхово\'),
	(\'Свирск\'),
	(\'Шелехов\'),
	(\'Калининград\'),
	(\'Балтийск\'),
	(\'Пионерский\'),
	(\'Светлогорск\'),
	(\'Светлый\'),
	(\'Советск\'),
	(\'Ладушкин\'),
	(\'Мамоново\'),
	(\'Багратионовск\'),
	(\'Ладушкин\'),
	(\'Мамоново\'),
	(\'Кострома\'),
	(\'Кострома\'),
	(\'Буй\'),
	(\'Галич\'),
	(\'Кологрив\'),
	(\'Макарьев\'),
	(\'Мантурово\'),
	(\'Нея\'),
	(\'Нерехта\'),
	(\'Солигалич\'),
	(\'Чухлома\'),
	(\'Шарья\'),
	(\'Курган\'),
	(\'Шадринск\'),
	(\'Далматово\'),
	(\'Катайск\'),
	(\'Куртамыш\'),
	(\'Макушино\'),
	(\'Петухово\'),
	(\'Шумиха\'),
	(\'Щучье\'),
	(\'Курск\'),
	(\'Курчатов\'),
	(\'Железногорск\'),
	(\'Льгов\'),
	(\'Щигры\'),
	(\'Дмитриев\'),
	(\'Дмитриев-Льговский\'),
	(\'Железногорск\'),
	(\'Курчатов\'),
	(\'Льгов\'),
	(\'Обоянь\'),
	(\'Рыльск\'),
	(\'Суджа\'),
	(\'Фатеж\'),
	(\'Щигры\'),
	(\'Сертолово\'),
	(\'Ивангород\'),
	(\'Пикалево\'),
	(\'Сосновый Бор\'),
	(\'Бокситогорск\'),
	(\'Пикалево\'),
	(\'Волосово\'),
	(\'Волосово\'),
	(\'Волхов\'),
	(\'Новая Ладога\'),
	(\'Сясьстрой\'),
	(\'Всеволожск\'),
	(\'Сертолово\'),
	(\'Выборг\'),
	(\'Высоцк\'),
	(\'Каменногорск\'),
	(\'Приморск\'),
	(\'Светогорск\'),
	(\'Гатчина\'),
	(\'Коммунар\'),
	(\'Кингисепп\'),
	(\'Ивангород\'),
	(\'Кириши\'),
	(\'Кировск\'),
	(\'Отрадное\'),
	(\'Шлиссельбург\'),
	(\'Лодейное Поле\'),
	(\'Луга\'),
	(\'Подпорожье\'),
	(\'Приозерск\'),
	(\'Сланцы\'),
	(\'Тихвин\'),
	(\'Тосно\'),
	(\'Никольское\'),
	(\'Любань\'),
	(\'Липецк\'),
	(\'Елец\'),
	(\'Грязи\'),
	(\'Данков\'),
	(\'Елец\'),
	(\'Задонск\'),
	(\'Лебедянь\'),
	(\'Усмань\'),
	(\'Чаплыгин\'),
	(\'Магадан\'),
	(\'Сусуман\'),
	(\'Домодедово\'),
	(\'Бронницы\'),
	(\'Дубна\'),
	(\'Железнодорожный\'),
	(\'Жуковский\'),
	(\'Звенигород\'),
	(\'Ивантеевка\'),
	(\'Климовск\'),
	(\'Королев\'),
	(\'Калининград\'),
	(\'Красноармейск\'),
	(\'Краснознаменск\'),
	(\'Лобня\'),
	(\'Лыткарино\'),
	(\'Протвино\'),
	(\'Пущино\'),
	(\'Реутов\'),
	(\'Рошаль\'),
	(\'Троицк\'),
	(\'Фрязино\'),
	(\'Щербинка\'),
	(\'Электросталь\'),
	(\'Юбилейный\'),
	(\'Дзержинский\'),
	(\'Подольск\'),
	(\'Орехово-Зуево\'),
	(\'Коломна\'),
	(\'Серпухов\'),
	(\'Долгопрудный\'),
	(\'Химки\'),
	(\'Лосино-Петровский\'),
	(\'Котельники\'),
	(\'Электрогорск\'),
	(\'Домодедово\'),
	(\'Черноголовка\'),
	(\'Городской округ Черноголовка\'),
	(\'Балашиха\'),
	(\'Кашира\'),
	(\'Кашира\'),
	(\'Егорьевск\'),
	(\'Озеры\'),
	(\'Мытищи\'),
	(\'Балашиха\'),
	(\'Волоколамск\'),
	(\'Воскресенск\'),
	(\'Дмитров\'),
	(\'Яхрома\'),
	(\'Домодедово\'),
	(\'Домодедово\'),
	(\'Егорьевск\'),
	(\'Зарайск\'),
	(\'Истра\'),
	(\'Дедовск\'),
	(\'Истра-1\'),
	(\'Снегири\'),
	(\'Кашира\'),
	(\'Ожерелье\'),
	(\'Кашира-8\'),
	(\'Кашира\'),
	(\'Клин\'),
	(\'Высоковск\'),
	(\'Красногорск\'),
	(\'Московский\'),
	(\'Видное\'),
	(\'Луховицы\'),
	(\'Люберцы\'),
	(\'Котельники\'),
	(\'Можайск\'),
	(\'Мытищи\'),
	(\'Долгопрудный\'),
	(\'Наро-Фоминск\'),
	(\'Апрелевка\'),
	(\'Верея\'),
	(\'Городское поселение Наро-Фоминск\'),
	(\'Городское поселение Апрелевка\'),
	(\'Городское поселение Верея\'),
	(\'Городское поселение Киевский\'),
	(\'Городское поселение Кокошкино\'),
	(\'Городское поселение Селятино\'),
	(\'Городское поселение Кокошкино\'),
	(\'Старая Купавна\'),
	(\'Ногинск\'),
	(\'Электроугли\'),
	(\'Черноголовка\'),
	(\'Черноголовка\'),
	(\'Старая Купавна\'),
	(\'Голицыно\'),
	(\'Одинцово\'),
	(\'Голицыно\'),
	(\'Кубинка\'),
	(\'Озеры\'),
	(\'Дрезна\'),
	(\'Куровское\'),
	(\'Ликино-Дулево\'),
	(\'Павловский Посад\'),
	(\'Электрогорск\'),
	(\'Пушкино\'),
	(\'Раменское\'),
	(\'Руза\'),
	(\'Краснозаводск\'),
	(\'Хотьково\'),
	(\'Пересвет\'),
	(\'Сергиев Посад\'),
	(\'Сергиев Посад-7\'),
	(\'Серебряные Пруды\'),
	(\'Солнечногорск-2\'),
	(\'Солнечногорск-7\'),
	(\'Солнечногорск-25\'),
	(\'Солнечногорск-30\'),
	(\'Солнечногорск\'),
	(\'Солнечногорск-2\'),
	(\'Солнечногорск-7\'),
	(\'Солнечногорск-25\'),
	(\'Солнечногорск-30\'),
	(\'Ступино\'),
	(\'Талдом\'),
	(\'Химки\'),
	(\'Сходня\'),
	(\'Чехов\'),
	(\'Чехов-2\'),
	(\'Чехов-3\'),
	(\'Чехов-7\'),
	(\'Чехов-8\'),
	(\'Чехов-1\'),
	(\'Чехов-4\'),
	(\'Чехов-5\'),
	(\'Чехов-6\'),
	(\'Шатура\'),
	(\'Щелково\'),
	(\'Лосино-Петровский\'),
	(\'Мурманск\'),
	(\'Апатиты\'),
	(\'Заозерск\'),
	(\'Кандалакша\'),
	(\'Кировск\'),
	(\'Мончегорск\'),
	(\'Оленегорск\'),
	(\'Островной\'),
	(\'Полярные Зори\'),
	(\'Полярный\'),
	(\'Североморск\'),
	(\'Гаджиево\'),
	(\'Снежногорск\'),
	(\'Оленегорск-1\'),
	(\'Оленегорск-2\'),
	(\'Оленегорск-4\'),
	(\'Кандалакша\'),
	(\'Ковдор\'),
	(\'Кола\'),
	(\'Заполярный\'),
	(\'Нижний Новгород\'),
	(\'Горький\'),
	(\'Дзержинск\'),
	(\'Саров\'),
	(\'Арзамас\'),
	(\'Бор\'),
	(\'Семенов\'),
	(\'Выкса\'),
	(\'Первомайск\'),
	(\'Шахунья\'),
	(\'Кулебаки\'),
	(\'Арзамас\'),
	(\'Балахна\'),
	(\'Богородск\'),
	(\'Бор\'),
	(\'Ветлуга\'),
	(\'Володарск\'),
	(\'Выкса\'),
	(\'Городец\'),
	(\'Заволжье\'),
	(\'Княгинино\'),
	(\'Кстово\'),
	(\'Лукоянов\'),
	(\'Лысково\'),
	(\'Навашино\'),
	(\'Павлово\'),
	(\'Ворсма\'),
	(\'Горбатов\'),
	(\'Первомайск\'),
	(\'Перевоз\'),
	(\'Семенов\'),
	(\'Сергач\'),
	(\'Урень\'),
	(\'Чкаловск\'),
	(\'Шахунья\'),
	(\'Великий Новгород\'),
	(\'Великий Новгород\'),
	(\'Боровичи\'),
	(\'Валдай\'),
	(\'Малая Вишера\'),
	(\'Окуловка\'),
	(\'Пестово\'),
	(\'Сольцы\'),
	(\'Сольцы 2\'),
	(\'Старая Русса\'),
	(\'Холм\'),
	(\'Чудово\'),
	(\'Новосибирск\'),
	(\'Бердск\'),
	(\'Обь\'),
	(\'Барабинск\'),
	(\'Искитим\'),
	(\'Куйбышев\'),
	(\'Татарск\'),
	(\'Болотное\'),
	(\'Карасук\'),
	(\'Каргат\'),
	(\'Купино\'),
	(\'Тогучин\'),
	(\'Черепаново\'),
	(\'Чулым-3\'),
	(\'Чулым\'),
	(\'Омск\'),
	(\'Исилькуль\'),
	(\'Калачинск\'),
	(\'Называевск\'),
	(\'Тара\'),
	(\'Тюкалинск\'),
	(\'Оренбург\'),
	(\'Медногорск\'),
	(\'Новотроицк\'),
	(\'Орск\'),
	(\'Бугуруслан\'),
	(\'Бузулук\'),
	(\'Гай\'),
	(\'Кувандык\'),
	(\'Сорочинск\'),
	(\'Ясный\'),
	(\'Бугуруслан\'),
	(\'Гай\'),
	(\'Сорочинск\'),
	(\'Ясный\'),
	(\'Бузулук\'),
	(\'Кувандык\'),
	(\'Соль-Илецк\'),
	(\'Абдулино\'),
	(\'Орёл\'),
	(\'Орел\'),
	(\'Ливны\'),
	(\'Мценск\'),
	(\'Орел\'),
	(\'Болхов\'),
	(\'Дмитровск\'),
	(\'Ливны\'),
	(\'Малоархангельск\'),
	(\'Мценск\'),
	(\'Новосиль\'),
	(\'Пенза\'),
	(\'Заречный\'),
	(\'Кузнецк\'),
	(\'Кузнецк-8\'),
	(\'Кузнецк-12\'),
	(\'Спасск\'),
	(\'Беднодемьяновск\'),
	(\'Белинский\'),
	(\'Городище\'),
	(\'Сурск\'),
	(\'Каменка\'),
	(\'Кузнецк\'),
	(\'Кузнецк-12\'),
	(\'Кузнецк-8\'),
	(\'Нижний Ломов\'),
	(\'Никольск\'),
	(\'Сердобск\'),
	(\'Пермь\'),
	(\'Березники\'),
	(\'Александровск\'),
	(\'Гремячинск\'),
	(\'Губаха\'),
	(\'Добрянка\'),
	(\'Кизел\'),
	(\'Краснокамск\'),
	(\'Кунгур\'),
	(\'Лысьва\'),
	(\'Соликамск\'),
	(\'Чайковский\'),
	(\'Чусовой\'),
	(\'Кудымкар\'),
	(\'Верещагино\'),
	(\'Горнозаводск\'),
	(\'Чермоз\'),
	(\'Красновишерск\'),
	(\'Нытва\'),
	(\'Оса\'),
	(\'Оханск\'),
	(\'Очер\'),
	(\'Усолье\'),
	(\'Чердынь\'),
	(\'Чернушка\'),
	(\'Краснокамск\'),
	(\'Псков\'),
	(\'Великие Луки\'),
	(\'Великие Луки-1\'),
	(\'Гдов\'),
	(\'Дно\'),
	(\'Невель\'),
	(\'Новоржев\'),
	(\'Новосокольники\'),
	(\'Опочка\'),
	(\'Остров\'),
	(\'Печоры\'),
	(\'Порхов\'),
	(\'Пустошка\'),
	(\'Пыталово\'),
	(\'Себеж\'),
	(\'Ростов-на-Дону\'),
	(\'Батайск\'),
	(\'Волгодонск\'),
	(\'Гуково\'),
	(\'Донецк\'),
	(\'Зверево\'),
	(\'Каменск-Шахтинский\'),
	(\'Новочеркасск\'),
	(\'Новошахтинск\'),
	(\'Таганрог\'),
	(\'Шахты\'),
	(\'Азов\'),
	(\'Азов\'),
	(\'Аксай\'),
	(\'Белая Калитва\'),
	(\'Зерноград\'),
	(\'Константиновск\'),
	(\'Красный Сулин\'),
	(\'Миллерово\'),
	(\'Морозовск\'),
	(\'Пролетарск\'),
	(\'Сальск\'),
	(\'Семикаракорск\'),
	(\'Цимлянск\'),
	(\'Рязань\'),
	(\'Сасово\'),
	(\'Скопин\'),
	(\'Касимов\'),
	(\'Рязань\'),
	(\'Касимов\'),
	(\'Спас-Клепики\'),
	(\'Кораблино\'),
	(\'Михайлов\'),
	(\'Новомичуринск\'),
	(\'Рыбное\'),
	(\'Ряжск\'),
	(\'Сасово\'),
	(\'Скопин\'),
	(\'Спасск-Рязанский\'),
	(\'Шацк\'),
	(\'Самара\'),
	(\'Жигулевск\'),
	(\'Новокуйбышевск\'),
	(\'Октябрьск\'),
	(\'Отрадный\'),
	(\'Чапаевск\'),
	(\'Тольятти\'),
	(\'Сызрань\'),
	(\'Похвистнево\'),
	(\'Кинель\'),
	(\'Нефтегорск\'),
	(\'Саратов\'),
	(\'Шиханы\'),
	(\'Аткарск\'),
	(\'Балаково\'),
	(\'Балашов\'),
	(\'Вольск\'),
	(\'Вольск-18\'),
	(\'Красноармейск\'),
	(\'Маркс\'),
	(\'Петровск\'),
	(\'Пугачев\'),
	(\'Ртищево\'),
	(\'Хвалынск\'),
	(\'Энгельс\'),
	(\'Энгельс-1\'),
	(\'Энгельс-19\'),
	(\'Энгельс-2\'),
	(\'Аркадак\'),
	(\'Балаково\'),
	(\'Балашов\'),
	(\'Вольск\'),
	(\'Ершов\'),
	(\'Калининск\'),
	(\'Красноармейск\'),
	(\'Красный Кут\'),
	(\'Маркс\'),
	(\'Новоузенск\'),
	(\'Петровск\'),
	(\'Пугачев\'),
	(\'Ртищево\'),
	(\'Хвалынск\'),
	(\'Энгельс\'),
	(\'Южно-Сахалинск\'),
	(\'Анива\'),
	(\'Долинск\'),
	(\'Корсаков\'),
	(\'Курильск\'),
	(\'Макаров\'),
	(\'Невельск\'),
	(\'Горнозаводск\'),
	(\'Оха\'),
	(\'Поронайск\'),
	(\'Северо-Курильск\'),
	(\'Красногорск\'),
	(\'Томари\'),
	(\'Красногорск\'),
	(\'Углегорск\'),
	(\'Шахтерск\'),
	(\'Чехов\'),
	(\'Холмск\'),
	(\'Чехов\'),
	(\'Александровск-Сахалинский\'),
	(\'Екатеринбург\'),
	(\'Свердловск\'),
	(\'Асбест\'),
	(\'Березовский\'),
	(\'Верхняя Пышма\'),
	(\'Заречный\'),
	(\'Ивдель\'),
	(\'Карпинск\'),
	(\'Качканар\'),
	(\'Кировград\'),
	(\'Краснотурьинск\'),
	(\'Красноуральск\'),
	(\'Кушва\'),
	(\'Лесной\'),
	(\'Свердловск-45\'),
	(\'Нижняя Тура\'),
	(\'Новоуральск\'),
	(\'Свердловск-44\'),
	(\'Первоуральск\'),
	(\'Полевской\'),
	(\'Ревда\'),
	(\'Североуральск\'),
	(\'Каменск-Уральский\'),
	(\'Нижний Тагил\'),
	(\'Алапаевск\'),
	(\'Артемовский\'),
	(\'Богданович\'),
	(\'Нижняя Салда\'),
	(\'Верхняя Салда\'),
	(\'Ирбит\'),
	(\'Камышлов\'),
	(\'Красноуфимск\'),
	(\'Невьянск\'),
	(\'Реж\'),
	(\'Серов\'),
	(\'Сухой Лог\'),
	(\'Тавда\'),
	(\'Верхний Тагил\'),
	(\'Среднеуральск\'),
	(\'Волчанск\'),
	(\'Верхняя Тура\'),
	(\'Дегтярск\'),
	(\'Тавда\'),
	(\'Невьянск\'),
	(\'Верхняя Салда\'),
	(\'Артемовский\'),
	(\'Богданович\'),
	(\'Верхняя Салда\'),
	(\'Верхотурье\'),
	(\'Невьянск\'),
	(\'Нижние Серги-3\'),
	(\'Нижние Серги\'),
	(\'Михайловск\'),
	(\'Новая Ляля\'),
	(\'Реж\'),
	(\'Сухой Лог\'),
	(\'Сысерть\'),
	(\'Арамиль\'),
	(\'Тавда\'),
	(\'Талица\'),
	(\'Туринск\'),
	(\'Десногорск\'),
	(\'Смоленск\'),
	(\'Велиж\'),
	(\'Вязьма\'),
	(\'Гагарин\'),
	(\'Демидов\'),
	(\'Дорогобуж\'),
	(\'Духовщина\'),
	(\'Ельня\'),
	(\'Починок\'),
	(\'Рославль\'),
	(\'Рудня\'),
	(\'Сафоново\'),
	(\'Сычевка\'),
	(\'Ярцево\'),
	(\'Котовск\'),
	(\'Моршанск\'),
	(\'Тамбов\'),
	(\'Кирсанов\'),
	(\'Мичуринск\'),
	(\'Рассказово\'),
	(\'Уварово\'),
	(\'Жердевка\'),
	(\'Тверь\'),
	(\'Торжок\'),
	(\'Ржев\'),
	(\'Нелидово\'),
	(\'Кимры\'),
	(\'Вышний Волочек\'),
	(\'Калинин\'),
	(\'Тверь\'),
	(\'Андреаполь\'),
	(\'Бежецк\'),
	(\'Белый\'),
	(\'Бологое\'),
	(\'Весьегонск\'),
	(\'Вышний Волочек\'),
	(\'Западная Двина\'),
	(\'Зубцов\'),
	(\'Калязин\'),
	(\'Кашин\'),
	(\'Кимры\'),
	(\'Конаково\'),
	(\'Красный Холм\'),
	(\'Кувшиново\'),
	(\'Лихославль\'),
	(\'Нелидово\'),
	(\'Осташков\'),
	(\'Ржев\'),
	(\'Старица\'),
	(\'Торжок\'),
	(\'Торопец\'),
	(\'Удомля\'),
	(\'Томск\'),
	(\'Кедровый\'),
	(\'Северск\'),
	(\'Стрежевой\'),
	(\'Асино\'),
	(\'Колпашево\'),
	(\'Тула\'),
	(\'Донской\'),
	(\'Алексин\'),
	(\'Белев\'),
	(\'Богородицк\'),
	(\'Венев\'),
	(\'Ефремов\'),
	(\'Кимовск\'),
	(\'Киреевск\'),
	(\'Болохово\'),
	(\'Липки\'),
	(\'Новомосковск\'),
	(\'Сокольники\'),
	(\'Плавск\'),
	(\'Суворов\'),
	(\'Чекалин\'),
	(\'Узловая\'),
	(\'Щекино\'),
	(\'Советск\'),
	(\'Ясногорск\'),
	(\'Тюмень\'),
	(\'Тобольск\'),
	(\'Ишим\'),
	(\'Заводоуковск\'),
	(\'Ялуторовск\'),
	(\'Заводоуковск\'),
	(\'Ишим\'),
	(\'Ялуторовск\'),
	(\'Ульяновск\'),
	(\'Димитровград\'),
	(\'Барыш\'),
	(\'Новоульяновск\'),
	(\'Новоульяновск\'),
	(\'Инза\'),
	(\'Сенгилей\'),
	(\'Челябинск\'),
	(\'Верхний Уфалей\'),
	(\'Златоуст\'),
	(\'Карабаш\'),
	(\'Копейск\'),
	(\'Кыштым\'),
	(\'Магнитогорск\'),
	(\'Миасс\'),
	(\'Озерск\'),
	(\'Снежинск\'),
	(\'Трехгорный\'),
	(\'Усть-Катав\'),
	(\'Южноуральск\'),
	(\'Нязепетровск\'),
	(\'Куса\'),
	(\'Трехгорный-1\'),
	(\'Чебаркуль\'),
	(\'Троицк\'),
	(\'Миньяр\'),
	(\'Сим\'),
	(\'Аша\'),
	(\'Карталы\'),
	(\'Касли\'),
	(\'Юрюзань\'),
	(\'Катав-Ивановск\'),
	(\'Бакал\'),
	(\'Сатка\'),
	(\'Верхнеуральск\'),
	(\'Куса\'),
	(\'Нязепетровск\'),
	(\'Троицк\'),
	(\'Чебаркуль\'),
	(\'Еманжелинск\'),
	(\'Коркино\'),
	(\'Пласт\'),
	(\'Чита\'),
	(\'Чита-46\'),
	(\'Балей\'),
	(\'Борзя\'),
	(\'Краснокаменск\'),
	(\'Могоча\'),
	(\'Нерчинск\'),
	(\'Петровск-Забайкальский\'),
	(\'Сретенск\'),
	(\'Хилок\'),
	(\'Шилка\'),
	(\'Ярославль\'),
	(\'Переславль-Залесский\'),
	(\'Гаврилов-Ям\'),
	(\'Данилов\'),
	(\'Любим\'),
	(\'Мышкин\'),
	(\'Пошехонье\'),
	(\'Ростов\'),
	(\'Рыбинск\'),
	(\'Тутаев\'),
	(\'Углич\'),
	(\'Москва\'),
	(\'Московский\'),
	(\'Зеленоград\'),
	(\'Щербинка\'),
	(\'Троицк\'),
	(\'Московский\'),
	(\'Московский\'),
	(\'Московский\'),
	(\'Санкт-Петербург\'),
	(\'Ленинград\'),
	(\'Зеленогорск\'),
	(\'Колпино\'),
	(\'Красное Село\'),
	(\'Кронштадт\'),
	(\'Ломоносов\'),
	(\'Павловск\'),
	(\'Петергоф\'),
	(\'Петродворец\'),
	(\'Пушкин\'),
	(\'Сестрорецк\'),
	(\'Биробиджан\'),
	(\'Биробиджан\'),
	(\'Облучье\'),
	(\'Нарьян-Мар\'),
	(\'Дудинка\'),
	(\'Ханты-Мансийск\'),
	(\'Когалым\'),
	(\'Лангепас\'),
	(\'Мегион\'),
	(\'Нягань\'),
	(\'Покачи\'),
	(\'Пыть-Ях\'),
	(\'Урай\'),
	(\'Сургут\'),
	(\'Нижневартовск\'),
	(\'Белоярский\'),
	(\'Нефтеюганск\'),
	(\'Радужный\'),
	(\'Югорск\'),
	(\'Советский\'),
	(\'Лянтор\'),
	(\'Анадырь\'),
	(\'Билибино\'),
	(\'Певек\'),
	(\'Салехард\'),
	(\'Губкинский\'),
	(\'Лабытнанги\'),
	(\'Муравленко\'),
	(\'Надым\'),
	(\'Новый Уренгой\'),
	(\'Ноябрьск\'),
	(\'Тарко-Сале\'),
	(\'Керчь\'),
	(\'Армянск\'),
	(\'Саки\'),
	(\'Красноперекопск\'),
	(\'Судак\'),
	(\'Джанкой\'),
	(\'Симферополь\'),
	(\'Ялта\'),
	(\'Алупка\'),
	(\'Евпатория\'),
	(\'Феодосия\'),
	(\'Подгорное\'),
	(\'Алушта\'),
	(\'Бахчисарай\'),
	(\'Белогорск\'),
	(\'Старый Крым\'),
	(\'Армянськ\'),
	(\'Красноперекопск\'),
	(\'Красноперекопск\'),
	(\'Щелкино\'),
	(\'Саки\'),
	(\'Севастополь\'),
	(\'Инкерман\'),
	(\'Байконур\');');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
