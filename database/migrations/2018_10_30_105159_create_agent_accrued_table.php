<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentAccruedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_accrued', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('sum', 10, 4)->default(0);
            $table->date('date')->nullable();
            $table->string('file')->nullable();
            $table->integer('agent_id')->unsigned();
            $table->foreign('agent_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agent_accrued');
    }
}
