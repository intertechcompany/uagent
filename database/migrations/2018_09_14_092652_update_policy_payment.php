<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePolicyPayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('policy_payments', function (Blueprint $table) {
			$table->string('invoice_id')->nullable();
			$table->decimal('price',12,2)->nullable();
		});

		Schema::table('policies', function (Blueprint $table) {
			$table->boolean('is_save_to_osago')->default(false);
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
