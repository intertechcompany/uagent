<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentFrontendRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_frontend_rates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type')->nullable();
            $table->integer('value')->default(0);
            $table->string('insurance')->nullable();
            $table->integer('agent_id')->unsigned();
            $table->foreign('agent_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agent_frontend_rates');
    }
}
