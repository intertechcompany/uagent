<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoliciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('policies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('diagnostic_number');
            $table->timestamp('diagnostic_until_date')->nullable();
            $table->string('driver_count');
            $table->string('email');
            $table->boolean('insurer_and_owner_same')->default(false);
            $table->boolean('is_electronic')->default(true);
            $table->integer('make_id')->unsigned();
            $table->integer('model_id')->unsigned();
            $table->integer('owner_id')->unsigned();
            $table->string('phone');
            $table->timestamp('policy_start_date')->nullable();
            $table->integer('power');
            $table->timestamp('vehicle_document_date')->nullable();
            $table->string('vehicle_document_number');
            $table->string('vehicle_document_serial');
            $table->string('vehicle_document_type');
            $table->string('vehicle_id');
            $table->string('vehicle_id_type');
            $table->string('vehicle_plate_number');
            $table->integer('vehicle_plate_region')->unsigned();
            $table->integer('vehicle_registration_year')->unsigned();
            $table->boolean('is_vehicle_plate_absent')->default(false);
            $table->boolean('is_vehicle_use_trailer')->default(false);
            $table->boolean('send_to_company')->default(false);
            $table->integer('user_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('policies');
    }
}
