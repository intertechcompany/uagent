<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_informations', function (Blueprint $table) {
            $table->boolean('is_two_step_auth')->default(false);
            $table->boolean('is_remind_change_pass')->default(false);
            $table->boolean('is_send_email_suspiciou_login')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_informations', function(Blueprint $table) {
            $table->dropColumn([
                'is_two_step_auth',
                'is_remind_change_pass',
                'is_send_email_suspiciou_login',
            ]);
        });
    }
}
