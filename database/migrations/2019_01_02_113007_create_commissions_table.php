<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commissions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('all')->default(0);
            $table->integer('you')->default(0);
            $table->integer('agent')->default(0);
            $table->string('type')->nullable();
            $table->string('insurance')->nullable();
            $table->boolean('is_city')->default(false);
            $table->integer('agent_id')->unsigned()->nullable();
            $table->foreign('agent_id')
                ->references('id')
                ->on('users')
                ->onDelete('set null');
            $table->integer('rate_id')->unsigned()->nullable();
            $table->foreign('rate_id')
                ->references('id')
                ->on('rates')
                ->onDelete('set null');
            $table->integer('rate_city_id')->unsigned()->nullable();
            $table->foreign('rate_city_id')
                ->references('id')
                ->on('rate_cities')
                ->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commissions');
    }
}
