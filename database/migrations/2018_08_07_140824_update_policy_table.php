<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePolicyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('policies', function (Blueprint $table) {
			$table->integer('type_id')->unsigned()->nullable();
			$table->integer('purpose_id')->unsigned()->nullable();
			$table->dropColumn('kias_id');
			$table->string('insurance_id');
			$table->string('insurance');
			$table->string('status');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
