<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePolicyParamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('policy_params', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('policy_id')->unsigned();
            $table->string('key');
            $table->string('value',1000)->nullable();
            $table->timestamps();
        });

        Schema::table('policy_payments', function (Blueprint $table) {
           $table->dropColumn('transaction_xml');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('policy_params');
    }
}
