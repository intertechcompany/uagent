<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePaymentTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('policy_payments', function (Blueprint $table) {
			$table->unsignedInteger('policy_id')->nullable()->index()->change();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('policy_payments', function (Blueprint $table) {
            $table->dropIndex(['policy_id']);
        });
    }
}
