<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAgentPaidoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('agent_paidout', function (Blueprint $table) {
            $table->renameColumn('name', 'sum');
        });

        Schema::table('agent_paidout', function (Blueprint $table) {
            $table->decimal('sum', 10, 4)->default(0)->change();
            $table->mediumText('file')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('curators', function(Blueprint $table) {
            $table->renameColumn('sum', 'name');
        });

        Schema::table('curators', function(Blueprint $table) {
            $table->string('name')->nullable()->change();
            $table->string('file')->nullable()->change();
        });
    }
}
