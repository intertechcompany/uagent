<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDriverAndOwner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('owners', function (Blueprint $table) {
			$table->dropColumn([
				'document_serial',
				'document_number',
				'start_date',
			]);
			$table->text('address')->nullable();
		});

		Schema::table('drivers', function (Blueprint $table) {
			$table->dropColumn([
				'personal_document_serial',
				'personal_document_number',
			]);
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('owners', function (Blueprint $table) {
			$table->string('personal_document_serial');
			$table->string('personal_document_number');
			$table->timestamp('personal_document_date')->nullable();
			$table->timestamp('start_date')->nullable();
		});

		Schema::table('drivers', function (Blueprint $table) {
			$table->string('personal_document_serial');
			$table->string('personal_document_number');
		});
    }
}
