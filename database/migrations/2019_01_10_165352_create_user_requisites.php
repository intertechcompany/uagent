<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRequisites extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_requisites', function (Blueprint $table) {
            $table->increments('id');
            $table->string('full_name')->nullable();
            $table->string('kpp')->nullable();
            $table->string('leader_organization')->nullable();
            $table->string('ogrn')->nullable();
            $table->string('based')->nullable();
            $table->string('bank')->nullable();
            $table->string('legal_address')->nullable();
            $table->string('bik')->nullable();
            $table->string('mailing_address')->nullable();
            $table->string('settlement_account')->nullable();
            $table->string('phone')->nullable();
            $table->string('ks')->nullable();
            $table->string('inn')->nullable();
            $table->string('payer_description')->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_requisites');
    }
}
