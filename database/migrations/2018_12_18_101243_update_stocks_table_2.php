<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateStocksTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumns('stocks', ['link', 'text', 'information'])) {
            return;
        }

        Schema::table('stocks', function (Blueprint $table) {
            $table->text('link')->nullable();
            $table->text('text')->nullable();
            $table->text('information')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('stocks')) {
            Schema::table('stocks', function (Blueprint $table) {
                $table->renameColumn('title', 'name');
                $table->dropColumn([
                    'link',
                    'text',
                    'information',
                ]);
            });
        }
    }
}
