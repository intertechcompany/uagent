<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportCarMakesAndModels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('vehicle_makes', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
		});

		Schema::create('vehicle_models', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('vehicle_make_id')->unsigned();
			$table->string('name');
			$table->string('vehicle_type');

			$table->foreign('vehicle_make_id')->references('id')->on('vehicle_makes')->onDelete('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
