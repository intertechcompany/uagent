<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOwnerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('owners', function (Blueprint $table) {
			$table->string('personal_document_serial');
			$table->string('personal_document_number');
			$table->timestamp('personal_document_date')->nullable();
			$table->timestamp('start_date')->nullable();
			$table->renameColumn('document_issued_by','personal_document_issued_by');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
