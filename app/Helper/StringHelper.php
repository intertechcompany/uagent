<?php

namespace App\Helper;


use Illuminate\Http\Request;

class StringHelper
{
    public static function pages(Request $request, $total): string
    {
        if(!$total) {
            $current = '0-0';
        } else {
            $next = $request->has('page') ? $request->get('page') * $request->get('perPage') : $request->get('perPage');
            $next = $total < $next ? $total : $next;
            $current = ($request->get('page') > 1 ? (($request->get('page') - 1) * $request->get('perPage')) : 1) . '-' . $next;;
        }

        return $current;
    }

	public static function multiexplode ($delimiters, $string)
	{
		$ready = str_replace($delimiters, $delimiters[0], trim($string));
		$launch = explode($delimiters[0], $ready);

		foreach ($launch as $key => $value) {
			if ($value !== '') {
				return $value;
			}
		}

		return null;
	}
}