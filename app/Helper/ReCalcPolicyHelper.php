<?php

namespace App\Helper;

use App\Model\Policy;
use App\Service\PolicyService\Provider\InsuranceStaticFactory;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class ReCalcPolicyHelper
{
    /**
     * @var Policy
     */
    private $policy;

    /**
     * @var string
     */
    private $insurance;

    /**
     * ReCalcPolicyHelper constructor.
     * @param Policy $policy
     * @param string $insurance
     */
    public function __construct(Policy $policy, string $insurance)
    {
        $this->policy = $policy;
        $this->insurance = $insurance;
    }

    /**
     * Calc Policy in insurance
     * @return Policy
     */
    public function reCalc(): Policy
    {
        $serializePolicy = new SerializeDataForPolicy($this->policy);
        $data = $serializePolicy->getData();

        $data['insurance'] = $this->insurance;
        $data['user_id'] = $this->policy->user_id;

        try {
            $policy = InsuranceStaticFactory::factory($this->insurance)->create($data);

        } catch (\Exception $exception) {
            PolicyLogHelper::getInstance('policy')
                ->setStringLog($exception->getMessage())
                ->setStringLog($exception->getFile())
                ->setStringLog($exception->getTraceAsString())
                ->saveLog(storage_path('policies/fails/' . Carbon::now()->format('d-m-Y') . '/'.$this->insurance.'-'.$this->policy->id.'.xml'));

            Log::debug($exception);

            $policy = null;
        }

        if ($policy === null) {
            $data['premium'] = 0;
            $data['insuranceId'] = '';
            $data['contractNumber'] = null;

            $policyCreator = new PolicyCreateHelper($data,$this->insurance);

            $policy = $policyCreator->create();
        }

        $policy->user_id = $this->policy->user_id;
        $policy->save();

        return $policy;
    }
}
