<?php

namespace App\Helper;

use App\Model\Driver;
use App\Model\Owner;
use App\Model\Policy;
use App\Service\DadataService;
use Carbon\Carbon;

class SerializeDataForPolicy
{
    /**
     * @var Policy
     */
    private $policy;

    /**
     * SerializeDataForPolicy constructor.
     * @param Policy $policy
     */
    public function __construct(Policy $policy)
    {
        $this->policy = $policy;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        $insurer = $this->policy->insurer ?? $this->policy->owner;

        return [
            'make_id' => $this->policy->make_id,
            'model_id' => $this->policy->model_id,
            'vehicle_registration_year' => (string)$this->policy->vehicle_registration_year,
            'vehicle_id' => $this->policy->vehicle_id,
            'vehicle_id_type' => $this->policy->vehicle_id_type,
            'vehicle_document_type' => $this->policy->vehicle_document_type,
            'driver_count' => $this->policy->driver_count,
            'drivers' => $this->getDrivers(),
            'owner' => $this->getPerson($this->policy->owner),
            'insurer' => $this->getPerson($insurer),
            'insurer_and_owner_same' => $this->policy->owner->id === $insurer->id,
            'is_electronic' => $this->policy->is_electronic,
            'email' => $this->policy->email,
            'policy_number_series' => NULL,
            'is_vehicle_use_trailer' => (bool)$this->policy->is_vehicle_use_trailer,
            'type' => $this->policy->type_id,
            'purpose' => $this->policy->purpose_id,
            'power' => $this->policy->power,
            'vehicle_plate_region' => $this->policy->vehicle_plate_region,
            'vehicle_plate_number' => $this->policy->vehicle_plate_number,
            'vehicle_document_serial' => $this->policy->vehicle_document_serial,
            'vehicle_document_number' => $this->policy->vehicle_document_number,
            'vehicle_document_date' => $this->formatDate($this->policy->vehicle_document_date),
            'policy_start_date' => $this->formatDateStartDate($this->policy->policy_start_date),
            'diagnostic_number' => $this->policy->diagnostic_number,
            'diagnostic_until_date' => $this->formatDate($this->policy->diagnostic_until_date),
            'phone' => $this->policy->phone,
            'status' => $this->policy->status,
        ];
    }

    /**
     * @param string $date
     * @return string
     */
    public function formatDate(string $date): string
    {
        return (new Carbon($date))->format('d-m-Y');
    }

    /**
     * @param string $date
     * @return string
     */
    private function formatDateStartDate(string $date): string
    {
        $now = now()->setTime(23,59,59);
        $oldDate = new Carbon($date);

        return ($oldDate->timestamp < $now->timestamp ? now()->addDay() : $oldDate)->format('d-m-Y');
    }

    /**
     * @return array
     */
    private function getDrivers(): array
    {
        return $this->policy->drivers()->select(
            'last_name',
            'first_name',
            'middle_name',
            'dob',
            'document_serial',
            'document_number',
            'start_date',
            'id'
        )
            ->get()
            ->map(function (Driver $driver){
                $driver->dob = $this->formatDate($driver->dob);
                $driver->start_date = $this->formatDate($driver->start_date);
                return $driver;
            })
            ->toArray();
    }

    /**
     * @param Owner $owner
     * @return array
     */
    private function getPerson(Owner $owner): array
    {
        $address = [
            'address' => array_shift(DadataService::suggest([
                'query' => $owner->address,
                'count' => 1
            ])['suggestions']),
        ];

        $data = array_only($owner->toArray(),[
            'last_name',
            'first_name',
            'middle_name',
            'dob',
            'personal_document_serial',
            'personal_document_number',
            'personal_document_issued_by',
            'personal_document_date',
            'address2',
            'address3',
            'place_of_birth',
            'house',
            'postal_code',
            'id',
        ]);

        $data['dob'] = $this->formatDate($data['dob']);
        $data['personal_document_date'] = $this->formatDate($data['personal_document_date']);

        $data['postal_code'] = array_get($address, 'address.data.postal_code');

        return array_merge($data, $address);
    }
}
