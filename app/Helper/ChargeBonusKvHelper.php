<?php

namespace App\Helper;

use App\Entity\Kv;
use App\Model\Commission;
use App\Model\KvBonus;
use App\Model\Policy;
use App\User;

class ChargeBonusKvHelper
{
    /**
     * @var Policy
     */
    private $policy;

    /**
     * ChargeBonusKvHelper constructor.
     * @param Policy $policy
     */
    public function __construct(Policy $policy)
    {
        $this->policy = $policy;
    }

    /**
     * Charrge Bonus
     */
    public function chargeBonus(): void
    {
        $user = $this->policy->user;

        $userKv = (new KvHelper($user,$this->policy->insurance,$this->policy->getKladers()))->getKv();
        $parentKv = null;

        if ($userKv !== null) {

            $parentBonus = $this->saveTransaction($userKv,$user);
            $user->balance += $parentBonus->sum;
            $user->save();
            $parentKv = new Kv($userKv->getModel(),$userKv->getType(),$userKv->getParentValue());
        }
        /**
         * @var User $parentAgent
         */
        $parentAgent = $user->agent()->first();

        if ($parentKv !== null && $parentAgent !== null) {
            $parentBonus = $this->saveTransaction($parentKv,$parentAgent);
            $parentAgent->balance += $parentBonus->sum;
            $parentAgent->save();
        }

    }

    /**
     * @param Kv $kv
     * @param User $user
     * @return KvBonus
     */
    private function saveTransaction(Kv $kv,User $user): KvBonus
    {
        $sum = self::calcSum($kv,$this->policy->premium);

        $kvBonus = new KvBonus();
        $kvBonus->policy_id = $this->policy->id;
        $kvBonus->user_id = $user->id;
        $kvBonus->kv = $kv->getValue();
        $kvBonus->sum = $sum;
        $kvBonus->type = $kv->getType();
        $kvBonus->is_sub_agent = $kv->getParentValue() !== null;

        $kvBonus->save();

        return $kvBonus;
    }

    /**
     * @param Kv|null $kv
     * @param float $premium
     * @return float
     */
    public static function calcSum(?Kv $kv,float $premium): float
    {
        if ($kv === null) {
            return (float)0;
        }

        if ($kv->getType() === Commission::RUB) {
            return $kv->getValue();
        }

        $percent = $kv->getValue();

        return $premium / 100 * $percent;
    }
}
