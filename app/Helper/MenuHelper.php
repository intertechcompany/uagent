<?php

namespace App\Helper;

use Illuminate\Http\Request;

class MenuHelper
{
    /**
     * @var string
     */
    public $type;

    /**
     * @return self
     */
	public static function factory(): self
	{
		return new self;
	}

    /**
     * Set tyoe session
     *
     * @param $type
     * @return App\Helper\MenuHelper
     */
    public function setType(string $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Make menu position
     *
     * @return ?string
     */
    public function makePosition(): ?string
    {
        if (!request()->session()->has($this->type)) {
            return request()->session()->put($this->type, true);
        }

        return request()->session()->forget($this->type);
    }
}
