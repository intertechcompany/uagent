<?php

namespace App\Helper;

use App\Model\Log;
use Illuminate\Database\Eloquent\Collection;

class LogHelper
{
	private $object;

	private $action;

	private $objectId;

	private $description;

	/**
	 * Factory static pattern
	 * @return LogHelper
	 */
	public static function factory(): self
	{
		return new self();
	}

	/**
	 * Set class
	 * @param object $object
	 * @return LogHelper
	 */
	public function setObject($object): self
	{
		$this->object = \get_class($object);
		$this->objectId = $object->id;

		return $this;
	}

	/**
	 * Set action
	 * @param string $action
	 * @return LogHelper
	 */
	public function setAction(string $action): self
	{
		$this->action = $action;

		return $this;
	}

	/**
	 * Set description for log
	 * @param string $description
	 * @return LogHelper
	 */
	public function setDescription(string $description): self
	{
		$this->description = $description;

		return $this;
	}

	/**
	 * Save Log to DB
	 * @return Log
	 */
	public function save(): Log
	{
		$log = new Log();
		$log->action = $this->action;
		$log->log_id = $this->objectId;
		$log->log_type = $this->object;
		$log->description = $this->description;
		$log->save();

		return $log;
	}

    /**
     * @return Collection
     */
	public function getLog(): Collection
	{
		return Log::query()
            ->where(['log_type' => $this->object,'log_id' => $this->objectId, 'action' => $this->action])
            ->orderBy('created_at','desc')
            ->get()
        ;
	}
}
