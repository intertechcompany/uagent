<?php

namespace App\Helper;

class UserHelper
{
	public static function format_phone($phone = ''){
		return preg_replace('~[^0-9]+~', '', $phone);
	}
}