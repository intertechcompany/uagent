<?php

namespace App\Helper;

use App\User;
use Illuminate\Support\Collection;

class UserSubAgents
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var iterable
     */
    private $columns;

    /**
     * @var iterable
     */
    private $filter;

    /**
     * UserSubAgents constructor.
     * @param User $user
     * @param iterable $filter
     * @param iterable $columns
     */
    public function __construct(User $user, iterable $filter = [], iterable $columns = ['*'])
    {
        $this->user = $user;
        $this->columns = $columns;
        $this->filter = $filter;
    }

    /**
     * @return Collection
     */
    public function getSubAgents(): Collection
    {
        return $this->user->isBroker() ?
            $this->getSubAgentsForBroker() :
            $this->findChildrenByUser($this->user,$this->columns,$this->filter)
        ;
    }

    /**
     * @return Collection
     */
    private function getSubAgentsForBroker(): Collection
    {
        if (\count(array_filter($this->filter)) === 0) {
            return $this->getAllChildrenAgents(collect(),$this->user,$this->columns,$this->filter);
        }

        $agents = $this->getAllChildrenAgents(collect(),$this->user,['id','parent_user_id']);

        return User::filter($this->filter)
            ->whereIn('id',$agents->pluck('id'))
            ->get($this->columns)
        ;
    }

    /**
     * @param Collection $collection
     * @param User $user
     * @param iterable $columns
     * @param iterable $filters
     * @return Collection
     */
    private function getAllChildrenAgents(
        Collection $collection,
        User $user,
        iterable $columns = ['*'],
        iterable $filters = []
    ): Collection
    {
        $subAgents = $this->findChildrenByUser($user,$columns,$filters);
        $collection = $collection->merge($subAgents);

        foreach ($subAgents as $subAgent) {
            $collection = $this->getAllChildrenAgents($collection,$subAgent,$columns,$filters);
        }

        return $collection;
    }

    /**
     * @param User $user
     * @param iterable $columns
     * @param iterable $filters
     * @return Collection
     */
    private function findChildrenByUser(User $user,iterable $columns, iterable $filters): Collection
    {
        return $user
            ->subAgents()
            ->filter($filters)
            ->get($columns)
        ;
    }
}
