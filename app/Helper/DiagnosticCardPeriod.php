<?php

namespace App\Helper;

/**
 * 0-3 года - ТО не нужно. присваивается при покупке
 * возраст 3-7 лет - 1 раз в 2 года
 * свыше 7 - ежегодно
 */

class DiagnosticCardPeriod
{
    /**
     * @param int $year
     * @return int|null
     */
    public static function getPeriod(int $year): ?int
    {
        $now = (int) date('Y');

        $diff = $now - $year;

        if ($diff < 7) {
            return 2;
        }

        return 1;
    }
}
