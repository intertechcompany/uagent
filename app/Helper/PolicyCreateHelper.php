<?php

namespace App\Helper;

use App\Model\Driver;
use App\Model\Owner;
use App\Model\Policy;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class PolicyCreateHelper
{
    /**
     * @var array
     */
    private $data;
    
    /**
     * @var Owner
     */
    private $insurer;

    /**
     * @var Owner
     */
    private $owner;

    /**
     * @var Policy
     */
    private $policy;

    /**
     * @var string|null
     */
    private $insurance;

    /**
     * PolicyCreateHelper constructor.
     * @param array $data
     * @param string|null $insurance
     */
    public function __construct(array $data,?string $insurance = null)
    {
        $this->data = $data;
        $this->insurance = $insurance;
    }

    /**
     * @return Policy
     */
    public function create(): Policy
    {
        $ownerRow = array_get($this->data,'owner');
        $this->owner = $this->insurer = $this->createPerson($ownerRow,Owner::OWNER);
        
        if ((int)array_get($this->data,'insurer_and_owner_same') === 0) {
            $insurerRow = array_get($this->data,'insurer');
            $this->insurer = $this->createPerson($insurerRow,Owner::INSURER);
        }

        $this->policy = $this->createPolicy();
        $this->createDrivers();
        $this->saveLogs();
        return $this->policy;
    }

    /**
     * @param Policy|null $policy
     * @return Policy
     */
    public function createUpdate(Policy $policy = null): Policy
    {
        $this->policy = $policy;

        if ($this->policy === null) {
            $this->policy = new Policy();
        }

        $this->checkInsurer();
        $this->saveUpdatePolicy();
        $this->createDriversCreteUpdate();
        $this->saveLogs();

        return $this->policy;
    }


    private function checkInsurer(): void
    {
        $ownerRow = array_get($this->data,'owner');
        $this->owner = $this->insurer = $this->createPerson($ownerRow,Owner::OWNER);

        if ((int)array_get($this->data,'insurer_and_owner_same') === 0) {
            $insurerRow = array_get($this->data,'insurer');
            $this->insurer = $this->createPerson($insurerRow,Owner::INSURER);
        }
    }

    private function saveUpdatePolicy(): void
    {
        $this->policy->owner_id = $this->owner->id;
        $this->policy->insurer_id = $this->insurer->id;
        $this->policy->diagnostic_number = array_get($this->data,'diagnostic_number');
        $this->policy->diagnostic_until_date = new Carbon(array_get($this->data,'diagnostic_until_date'));
        $this->policy->driver_count = array_get($this->data,'driver_count');
        $this->policy->email = array_get($this->data,'email');
        $this->policy->insurer_and_owner_same = array_get($this->data,'insurer_and_owner_same');
        $this->policy->is_electronic = array_get($this->data,'is_electronic');
        $this->policy->make_id = array_get($this->data,'make_id');
        $this->policy->model_id = array_get($this->data,'model_id');
        $this->policy->phone = array_get($this->data,'phone');
        $this->policy->policy_start_date = new Carbon(array_get($this->data,'policy_start_date'));
        $this->policy->power = array_get($this->data,'power');
        $this->policy->vehicle_document_date = new Carbon(array_get($this->data,'vehicle_document_date'));
        $this->policy->vehicle_document_number = array_get($this->data,'vehicle_document_number');
        $this->policy->vehicle_document_serial = array_get($this->data,'vehicle_document_serial');
        $this->policy->vehicle_document_type = array_get($this->data,'vehicle_document_type');
        $this->policy->vehicle_id = array_get($this->data,'vehicle_id');
        $this->policy->vehicle_id_type = array_get($this->data,'vehicle_id_type');
        $this->policy->vehicle_plate_number = array_get($this->data,'vehicle_plate_number');
        $this->policy->vehicle_plate_region = array_get($this->data,'vehicle_plate_region');
        $this->policy->vehicle_registration_year = array_get($this->data,'vehicle_registration_year');
        $this->policy->is_vehicle_plate_absent = array_get($this->data,'is_vehicle_plate_absent') !== null ?? array_get($this->data,'is_vehicle_plate_absent');
        $this->policy->is_vehicle_use_trailer = (boolean)array_get($this->data,'is_vehicle_use_trailer');
        $this->policy->premium = $this->data['premium'];
        $this->policy->type_id = array_get($this->data,'type');
        $this->policy->purpose_id = array_get($this->data,'purpose');
        $this->policy->insurance = $this->insurance;
        $this->policy->insurance_id = array_get($this->data,'insuranceId');
        $this->policy->insurance_number = array_get($this->data,'contractNumber');
        $this->policy->status = 'draft';
        $this->policy->kladr_id = array_get($this->data,'owner.address.data.city_kladr_id');
        $this->policy->region_kladr_id = array_get($this->data,'owner.address.data.region_kladr_id');
        $this->policy->version = Policy::NEW_VERSION;
        $this->policy->kt = array_get($this->data,'kt');
        $this->policy->kbm = array_get($this->data,'kbm');
        $this->policy->kvs = array_get($this->data,'kvs');
        $this->policy->tb = array_get($this->data,'tb');

        $this->policy->save();
    }

    /**
     * @param array $person
     * @param int $type
     * @return Owner
     */
    private function createPerson(array $person,int $type): Owner
    {
        $owner = new Owner();
        $owner->last_name = array_get($person,'last_name');
        $owner->first_name = array_get($person,'first_name');
        $owner->middle_name = array_get($person,'middle_name');
        $owner->dob = new Carbon(array_get($person,'dob'));
        $owner->personal_document_serial = array_get($person,'personal_document_serial');
        $owner->personal_document_number = array_get($person,'personal_document_number');
        $owner->personal_document_issued_by = array_get($person,'personal_document_issued_by');
        $owner->personal_document_date = new Carbon(array_get($person,'personal_document_date'));
        $owner->place_of_birth = array_get($person,'place_of_birth');
        $owner->house = array_get($person,'house');
        $owner->address = array_get($person,'address.value');
        $owner->address2 = array_get($person,'address2');
        $owner->address3 = array_get($person,'address3');
        $owner->type = $type;
        $owner->save();
        return $owner;
    }

    /**
     * @return Policy
     */
    private function createPolicy(): Policy
    {
        $policy = new Policy();
        $policy->owner_id = $this->owner->id;
        $policy->insurer_id = $this->insurer->id;
        $policy->diagnostic_number = array_get($this->data,'diagnostic_number');
        $policy->diagnostic_until_date = new Carbon(array_get($this->data,'diagnostic_until_date'));
        $policy->driver_count = array_get($this->data,'driver_count');
        $policy->email = array_get($this->data,'email');
        $policy->insurer_and_owner_same = array_get($this->data,'insurer_and_owner_same');
        $policy->is_electronic = array_get($this->data,'is_electronic');
        $policy->make_id = array_get($this->data,'make_id');
        $policy->model_id = array_get($this->data,'model_id');
        $policy->phone = array_get($this->data,'phone');
        $policy->policy_start_date = new Carbon(array_get($this->data,'policy_start_date'));
        
        /**
         * Change power type form integer to float
         */
        $policy->power = array_get($this->data, 'power');

        $policy->vehicle_document_date = new Carbon(array_get($this->data,'vehicle_document_date'));
        $policy->vehicle_document_number = array_get($this->data,'vehicle_document_number');
        $policy->vehicle_document_serial = array_get($this->data,'vehicle_document_serial');
        $policy->vehicle_document_type = array_get($this->data,'vehicle_document_type');
        $policy->vehicle_id = array_get($this->data,'vehicle_id');
        $policy->vehicle_id_type = array_get($this->data,'vehicle_id_type');
        $policy->vehicle_plate_number = array_get($this->data,'vehicle_plate_number');
        $policy->vehicle_plate_region = array_get($this->data,'vehicle_plate_region');
        $policy->vehicle_registration_year = array_get($this->data,'vehicle_registration_year');
        $policy->is_vehicle_plate_absent = array_get($this->data,'is_vehicle_plate_absent') !== null ?? array_get($this->data,'is_vehicle_plate_absent');
        $policy->is_vehicle_use_trailer = (boolean)array_get($this->data,'is_vehicle_use_trailer');
        $policy->premium = $this->data['premium'];
        $policy->type_id = array_get($this->data,'type');
        $policy->purpose_id = array_get($this->data,'purpose');
        $policy->insurance = $this->insurance;
        $policy->insurance_id = array_get($this->data,'insuranceId');
        $policy->insurance_number = array_get($this->data,'contractNumber');
        $policy->status = 'draft';
        $policy->kladr_id = array_get($this->data,'owner.address.data.city_kladr_id');
        $policy->region_kladr_id = array_get($this->data,'owner.address.data.region_kladr_id');
        $policy->version = Policy::NEW_VERSION;
        $policy->kt = array_get($this->data,'kt');
        $policy->kbm = array_get($this->data,'kbm');
        $policy->kvs = array_get($this->data,'kvs');
        $policy->tb = array_get($this->data,'tb');
        $policy->save();
        return $policy;
    }

    /**
     * @return Collection
     */
    private function createDrivers(): Collection
    {
        $drivers = [];

        if (array_get($this->data,'driver_count') === 'specified') {
            $drivers = array_get($this->data,'drivers');
            foreach ($drivers as $driver){
                $newDriver = new Driver();
                $newDriver->first_name = array_get($driver,'first_name');
                $newDriver->last_name = array_get($driver,'last_name');
                $newDriver->middle_name = array_get($driver,'middle_name');
                $newDriver->dob = new Carbon(array_get($driver,'dob'));
                $newDriver->document_serial = array_get($driver,'document_serial');
                $newDriver->document_number = array_get($driver,'document_number');
                $newDriver->start_date = new Carbon(array_get($driver,'start_date'));
                $newDriver->kbm = array_get($driver,'kbm');
                $newDriver->save();
                $drivers[] = $newDriver;
                $this->policy->drivers()->attach($newDriver->id);
            }
        }
        return collect($drivers);
    }

    /**
     * @return Collection
     */
    private function createDriversCreteUpdate(): Collection
    {
        $driversCollection = collect();

        if (array_get($this->data,'driver_count') === 'specified') {

            $drivers = array_get($this->data,'drivers');

            foreach ($drivers as $driver) {

                $driverId = array_get($driver,'id');

                $newDriver = $this->policy->id === null || $driverId === null ? new Driver() : Driver::query()->find($driverId);

                $newDriver->first_name = array_get($driver,'first_name');
                $newDriver->last_name = array_get($driver,'last_name');
                $newDriver->middle_name = array_get($driver,'middle_name');
                $newDriver->dob = new Carbon(array_get($driver,'dob'));
                $newDriver->document_serial = array_get($driver,'document_serial');
                $newDriver->document_number = array_get($driver,'document_number');
                $newDriver->start_date = new Carbon(array_get($driver,'start_date'));
                $newDriver->kbm = array_get($driver,'kbm');
                $newDriver->save();

                $driversCollection->push($newDriver);
            }

            $this->policy->drivers()->sync($driversCollection->pluck('id'));
        }

        return $driversCollection;
    }

    /**
     * Save policy log
     */
    private function saveLogs(): void
    {
        PolicyLogHelper::getInstance('policy')
            ->setPolicyID($this->policy->id)
            ->saveLog(storage_path('policies/provider/'  . Carbon::now()->format('d-m-Y') . '/'. $this->policy->insurance . '-' . $this->policy->id . '.xml'));
    }
}
