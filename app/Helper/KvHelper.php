<?php

namespace App\Helper;

use App\Entity\Kv;
use App\Model\Rate;
use App\Model\RateCity;
use App\User;
use Illuminate\Database\Eloquent\Builder;

class KvHelper
{
    /**
     * @var User $user
     */
    private $user;

    /**
     * @var array
     */
    private $kladrIds = [];

    /**
     * @var string
     */
    private $insurance;

    /**
     * KvHelper constructor.
     * @param User $user
     * @param string $insurance
     * @param array|null $address
     */
    public function __construct(User $user,string $insurance,array $address = null)
    {
        $this->user = $user;
        $this->insurance = $insurance;
        $this->parseKladers($address);
    }

    /**
     * @param array|null $address
     */
    private function parseKladers(?array $address): void
    {
        if ($address === null) {
            return;
        }

        foreach (['region','city'] as $type) {
            $index = $type . '_kladr_id';

            if (array_get($address,$index) !== null) {
                $this->kladrIds[] = $address[$index];
            }
        }
    }

    /**
     * @return Kv|null
     */
    public function getKv(): ?Kv
    {
        return $this->user->parent_user_id === null ? $this->getKvForAgent() : $this->getKvForSubAgent();
    }

    /**
     * @return Kv|null
     */
    private function getKvForAgent(): ?Kv
    {
        $rateCity = $this->getRateCity();

        return $rateCity ?? $this->getRate() ?? $this->getRateDefault();
    }

    /**
     * @return Kv|null
     */
    public function getKvForSubAgent(): ?Kv
    {
        $rateCity = $this->getRateCityForSubAgent();

        return $rateCity ?? $this->getRateForSubAgent();

    }

    /**
     * @return Kv|null
     */
    private function getRate(): ?Kv
    {
        $rate = $this
            ->user
            ->rates()
            ->where('insurance', $this->insurance)
            ->where('is_default',false)
            ->where('from', '<=', $this->user->getSuccessPolicies())
            ->first()
        ;

        return $rate ? new Kv($rate,$rate->type,$rate->value) : null;
    }

    private function getRateDefault(): ?Kv
    {
        $rate = Rate::query()
            ->where('insurance', $this->insurance)
            ->where('is_default',true)
            ->where('from', '<=', $this->user->getSuccessPolicies())
            ->first()
        ;

        return $rate ? new Kv($rate,$rate->type,$rate->value) : null;
    }

    /**
     * @return Kv|null
     */
    public function getRateForSubAgent(): ?Kv
    {
        $rate = $this
            ->user
            ->commissions()
            ->where('insurance',$this->insurance)
            ->where('is_city',false)
            ->first()
        ;

        return $rate ? new Kv($rate->rate,$rate->type,$rate->agent,$rate->you) : null;
    }

    /**
     * @return Kv|null
     */
    private function getRateCity(): ?Kv
    {
        $rate = null;

        foreach ($this->kladrIds as $kladrId) {
            $rate = RateCity::query()
                ->where('insurance',$this->insurance)
                ->where('city_id',$kladrId)
                ->first()
            ;

            if ($rate !== null) {
                break;
            }
        }

        return $rate ? new Kv($rate,$rate->type,$rate->value) : null;
    }

    /**
     * @return Kv|null
     */
    public function getRateCityForSubAgent(): ?Kv
    {
        $rate = null;

        foreach ($this->kladrIds as $kladrId) {

            $rate = $this
                ->user
                ->commissions()
                ->where('insurance',$this->insurance)
                ->where('is_city',true)
                ->whereHas('rateCity',function (Builder $q) use ($kladrId) {
                    $q->where('city_id',$kladrId);
                })
                ->first()
            ;

            if ($rate !== null) {
                break;
            }
        }

        return $rate ? new Kv($rate->rate,$rate->type,$rate->agent,$rate->you) : null;
    }
}
