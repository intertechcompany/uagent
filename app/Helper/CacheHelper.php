<?php

namespace App\Helper;

use App\Model\Cache;

class CacheHelper
{
	/**
	 * Get Value
	 * @param string $key
	 * @return string|null
	 */
	public static function get(string $key):? string
	{
		$value = Cache::query()->where('key',$key)->first();

		return $value !== null ? $value->value : null;
	}

    /**
     * @param string $key
     * @param string $value
     */
	public static function set(string $key,string $value): void
	{
		$cache = Cache::query()->where('key',$key)->first();

		if ($cache === null) {
			$cache = new Cache();
			$cache->key = $key;
		}

		$cache->value = $value;
		$cache->save();
	}

    /**
     * @param string $key
     */
	public static function remove(string $key): void
	{
		Cache::query()->where('key',$key)->delete();
	}
}
