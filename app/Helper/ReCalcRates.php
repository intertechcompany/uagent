<?php

namespace App\Helper;

use App\Model\Commission;
use App\Model\Rate;
use App\Model\RateCity;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class ReCalcRates
{
    /**
     * @var RateCity|Rate
     */
    private $rate;

    /**
     * @var bool
     */
    private $isCity;

    /**
     * @var Commission[]|Collection
     */
    private $commissions;

    /**
     * ReCalcRates constructor.
     * @param RateCity|Rate $rate
     */
    public function __construct($rate)
    {
        $this->rate = $rate;
        $this->isCity = $this->rate instanceof RateCity;
    }

    /**
     * Clear commissions when we remove Rate
     */
    public function clearCommissions(): void
    {
        $column = $this->isCity ? 'rate_city_id' : 'rate_id';

        Commission::query()
            ->where($column,$this->rate->id)
            ->where('is_city',$this->isCity)
            ->delete()
        ;
    }

    /**
     *
     */
    public function calc(): void
    {
        DB::beginTransaction();

        $column = $this->isCity ? 'rate_city_id' : 'rate_id';

        $this->commissions = Commission::query()
            ->where($column,$this->rate->id)
            ->where('is_city',$this->isCity)
            ->get()
        ;

        foreach ($this->commissions as $commission) {
            $this->resolveCommission($commission);
        }

        DB::commit();
    }

    /**
     * @param Commission $commission
     */
    private function resolveCommission(Commission $commission): void
    {
        $all = $this->rate->value;

        if ($commission->agent > $all){
            $commission->agent = $all;
            $commission->you = 0;
        }elseif ($commission->you > $all) {
            $commission->you = $all;
            $commission->agent = 0;
        } else {
            $commission->you = $all - $commission->agent;
        }

        $commission->all = $all;
        $commission->save();
    }
}
