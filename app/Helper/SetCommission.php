<?php

namespace App\Helper;

use App\Model\Commission;
use App\Model\Rate;
use App\Model\RateCity;
use App\User;

class SetCommission
{
    /**
     * @var User
     */
    private $agent;

    /**
     * @var bool
     */
    private $isCity;

    /**
     * @var float
     */
    private $agentCommission;

    /**
     * @var float
     */
    private $youCommission;

    /**
     * @var int
     */
    private $commissionId;

    /**
     * @var Rate|RateCity
     */
    private $rate;

    /**
     * SetCommission constructor.
     * @param User $agent
     * @param bool $isCity
     * @param int $rateId
     * @param float $agentCommission
     * @param float $youCommission
     * @param int|null $commissionId
     */
    public function __construct(User $agent, bool $isCity, int $rateId,float $agentCommission, float $youCommission,int $commissionId = null)
    {
        $this->agent = $agent;
        $this->isCity = $isCity;
        $this->agentCommission = $agentCommission;
        $this->youCommission = $youCommission;
        $this->commissionId = $commissionId;
        $this->rate = $this->isCity ? RateCity::query()->find($rateId) : Rate::query()->find($rateId);
    }

    /**
     * @return Commission
     */
    public function calcCommission(): Commission
    {
        $this->clearCommission();
        $commission = $this->findCommission();

        if (($this->agentCommission + $this->youCommission) > $commission->all) {
            throw new \LogicException('The amount of the agent and subagent commission must not be greater than the value of the rate.');
        }

        $commission->agent = $this->agentCommission;
        $commission->you = $this->youCommission;
        $commission->save();

        return $commission;
    }

    /**
     * @return Commission
     */
    private function findCommission(): Commission
    {
        $column = $this->isCity ? 'rate_city_id' : 'rate_id';
        $commission = $this->commissionId !== 0 ? Commission::query()->find($this->commissionId) : new Commission();

        $commission->agent_id = $this->agent->id;
        $commission->is_city = $this->isCity;
        $commission->$column = $this->rate->id;
        $commission->all = $this->rate->value;
        $commission->type = $this->rate->type;
        $commission->insurance = $this->rate->insurance;

        return $commission;
    }

    private function clearCommission(): void
    {
        if ($this->commissionId === 0) {
            $column = $this->isCity ? 'rate_city_id' : 'rate_id';

            Commission::query()
                ->where($column,$this->rate->id)
                ->where('agent_id',$this->agent->id)
                ->where('is_city',$this->isCity)
                ->where('insurance', $this->rate->insurance)
                ->delete()
            ;
        }
    }
}
