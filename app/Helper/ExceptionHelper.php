<?php

namespace App\Helper;

use Mail;

class ExceptionHelper
{
	private $exception;

	public static function error(\Exception $exception): void
	{
		$handle = new self();
		$handle->exception = $exception;
//		$handle->sendError();
         \Log::debug($exception);
	}

	private function sendError(): void
	{
		Mail::raw($this->exception->getMessage(), function ($message) {
			$message->to('eyarij@gmail.com')
    		->subject($this->exception->getCode());
		});
	}
}
