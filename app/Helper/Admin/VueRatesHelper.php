<?php

namespace App\Helper\Admin;

use stdClass;
use App\User;
use App\Model\City;
use App\Model\Rate;
use App\Model\Cache;
use App\Model\Policy;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use App\Components\Admin\Statistics\Statistics;

class VueRatesHelper
{
    public $model = [
        'agents' => '\App\User',
        'polices' => '\App\Model\Policy',
    ];

	public static function factory(): self
	{
		return new self;
	}

    public function getData(Request $request)
    {
        return [
            'insurances' => $this->getInsurances(),
            'cities' => $this->getCities(),
            'types' => $this->getTypes(),
            'agents' => $this->getAgentsAndSubAgents(),
            'storeUrl' => route('admin.rates.store.ajax'),
        ];
    }

    private function getTypes(array $types = []): array
    {
        foreach (Rate::$types as $key => $type) {
            $types[] = [
                'key' => $key,
                'value' => $key,
                'text' => $type,
            ];
        }

        return $types;
    }

	public function getCities(array $cities = []): array
	{
        foreach (City::withAgents()->get() as $city) {
            $cities[] = [
                'key' => $city->id,
                'value' => $city->id,
                'text' => $city->name,
            ];
        }

        return $cities;
	}

	public function getStatuses($type = null, $statuses = [])
	{
        if (class_exists($this->model[$type])) {
            $model = $this->model[$type];
        }

        foreach ($model::$statuses as $key => $status) {
            $statuses[] = [
                'code' => $key,
                'name' => $status,
            ];
        }

        return $statuses;
	}

	public function getAgentsAndSubAgents(array $agents = []): array
	{
        $users = User::where('parent_user_id', null)->get();

        foreach ($users as $agent) {
            $agents[] = $this->getdataAgent($agent);

            if ($agent->subAgents->count() > 0) {
                foreach ($agent->subAgents as $subAgent) {
                    $agents[] = $this->getdataAgent($subAgent);
                }
            }
        }

        return $agents;
	}

    private function getdataAgent(User $agent): array
    {
        return [
            'code' => $agent->id,
            'is_agent' => null === $agent->parent_user_id,
            'name' => $agent->full_name,
        ];
    }

	public function getInsurances(array $insurances = []): array
	{
        foreach (Policy::insurance() as $key => $insurance) {
            $insurances[] = [
                'key' => $key,
                'value' => $key,
                'text' => $insurance,
            ];
        }

        return $insurances;
	}
}
