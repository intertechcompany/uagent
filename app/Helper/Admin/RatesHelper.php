<?php

namespace App\Helper\Admin;

use App\User;
use Exception;
use App\Model\Rate;
use App\Model\RateCity;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class RatesHelper
{
    /**
     * @var array
     */
    public $data;

    /**
     * @var \Illuminate\Http\Request
     */
    public $request;

    /**
     * @var int
     */
    public $id;

    /**
     * @return self
     */
    public static function factory(): self
    {
        return new self;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return self
     */
    public function setData(Request $request)
    {
        $this->data = array_filter(
            $request->all()
        );

        $this->request = $request;

        return $this;
    }

    public function setRateId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    public function getRate()
    {
        if (!$this->request->has('rate_type')) {
            return null;
        }

        /**
         * @var string
         */
        $type = $this->request->get('rate_type');

        if ($type == 'rate') {
            return Rate::find($this->id);
        }

        if ($type == 'rateCity') {
            return RateCity::find($this->id);
        }

        return null;
    }

    public function storeRate(): array
    {
        /**
         * Process individual rate for selected agents
         */

        if ($this->isIndividualRate()) {
            return $this->storeStock();
        }

        if ($this->isCityRate()) {
            return $this->storeRateCity();
        }

        /**
         * Process not individual rate for selected agents
         */
        if (!$this->isIndividualRate()) {
            return $this->storeStock(true);
        }

        return [];
    }

    /**
     * Store stock model
     *
     * @param bool $isDefault
     */
    public function storeStock(bool $isDefault = false): array
    {
        /**
         * @var Rate $rate
         */
        $rate = Rate::query()
            ->create([
                'from' => $this->request->get('from'),
                'to' => $this->request->get('to'),
                'type' => $this->request->get('type'),
                'value' => $this->request->get('value'),
                'is_default' => $isDefault,
                'insurance' => $this->request->get('insurance')
            ])
        ;

        /**
         * If not individual rate, return model
         */
        if (false === $isDefault) {

            /**
             * Sync agents to rate
             */
            $rate->agents()->sync(
                $this->getAgents()
            );
        }

        return $this->getDataResponse([
            'rate' => $rate,
            'is_city' => false,
            'is_default' => $isDefault,
        ]);
    }

    public function storeRateCity(): array
    {
        /**
         * @var RateCity $rateCity
         */
        $rateCity = RateCity::query()->create([
            'city_id' => $this->request->get('city_id'),
            'type' => $this->request->get('type'),
            'value' => $this->request->get('value'),
            'insurance' => $this->request->get('insurance'),
        ]);

        /**
         * Process create DadataCity model
         */
        $rateCity->processCity($this->request);

        return $this->getDataResponse([
            'rate' => $rateCity,
            'is_city' => true,
            'is_default' => true,
        ]);
    }

    private function getDataResponse(array $data): array
    {
        $rate = array_get($data, 'rate');

        if (array_get($data, 'is_city') == true) {
            return [
                'redirectUrl' => route('admin.ratecities.edit', $rate)
            ];
        }

        return [
            'redirectUrl' => route('admin.rates.edit', $rate)
        ];
    }

    /**
     * Check request for is indivudual stock store model
     *
     * @return bool
     */
    public function isIndividualRate(): bool
    {
        return $this->isHasAgents() && !$this->isRateCity();
    }

    public function isCityRate(): bool
    {
        return !$this->isHasAgents() && $this->isRateCity();
    }

    /**
     * @return bool
     */
    public function isHasAgents(): bool
    {
        return $this->request->has('agents') && is_array($this->request->get('agents')) && count($this->request->get('agents')) > 0;
    }

    /**
     * @return bool
     */
    public function isRateCity(): bool
    {
        return $this->request->has('city_id') && !empty($this->request->get('city_id')) ? true : false;
    }

    /**
     * @return array
     */
    public function getAgents(): array
    {
        return $this->layoutVueMultiselect('agents');
    }

    /**
     * @param string $key
     * @return array
     */
    public function layoutVueMultiselect(string $key): array
    {
        try {
            if ($this->request->has($key)) {
                $data = $this->request->get($key, []);

                return array_column($data, 'code');
            }
        } catch (Exception $ex) {
            
        }

        return [];
    }
}
