<?php

namespace App\Helper\Admin;

use stdClass;
use App\User;
use App\Model\City;
use App\Model\Cache;
use App\Model\Policy;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use App\Components\Admin\Statistics\Statistics;

class VueAgentHelper
{
    public $model = [
        'agents' => '\App\User',
        'polices' => '\App\Model\Policy',
    ];

	public static function factory(): self
	{
		return new self;
	}

	public function dashboard(Request $request, string $type = null): array
	{
       	$data = [
       		'statuses' => $this->getStatuses($type),
	       	'agents' => $this->getAgentsAndSubAgents(),
	        'cities' => $this->getCities(),
       	];

        if ($type == 'agents') {
            return Arr::except($data, ['agents']);
        }

        return $data;
	}

	public function getCities(array $cities = []): array
	{
        foreach (City::withAgents()->get() as $city) {
            $cities[] = [
                'code' => $city->id,
                'name' => $city->name,
            ];
        }

        return $cities;
	}

	public function getStatuses($type = null, $statuses = [])
	{
        if (class_exists($this->model[$type])) {
            $model = $this->model[$type];
        }

        foreach ($model::$statuses as $key => $status) {
            $statuses[] = [
                'code' => $key, 
                'name' => $status,
            ];
        }

        return $statuses;
	}

	public function getAgentsAndSubAgents(array $agents = []): array
	{
        $users = User::with('subAgents')->where('parent_user_id', null)
            ->dashboard()->get();

        foreach ($users as $agent) {
            $agents[] = $this->getdataAgent($agent);

            if ($agent->subAgents->count() > 0) {
                foreach ($agent->subAgents as $subAgent) {
                    $agents[] = $this->getdataAgent($subAgent);
                }
            }
        }

        return $agents;
	}

    private function getdataAgent(User $agent): array
    {
        return [
            'code' => $agent->id,
            'is_agent' => null === $agent->parent_user_id,
            'name' => $agent->full_name,
        ];
    }

	public function getInsurances(array $insurances = []): array
	{
        foreach (Policy::insurance() as $key => $insurance) {
            $insurances[] = [
                'code' => $key,
                'name' => $insurance,
            ];
        }

        return $insurances;
	}

    public function formedFilters(Request $request): array
    {
        $filters = array_filter($request->all());
        $filters['date_register'] = $request->get('date');

        if ($cities = array_get($filters, 'cities')) {
            $filters['cities'] = array_column($cities, 'code');
        }

        return array_filter($filters);
    }
}
