<?php

namespace App\Helper\Admin;

use stdClass;
use App\User;
use App\Model\City;
use App\Model\Cache;
use App\Model\Policy;
use App\Model\Curator;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use App\Components\Admin\Statistics\Statistics;

class VuePolicyHelper
{
    public $model = [
        'agents' => '\App\User',
        'polices' => '\App\Model\Policy',
    ];

	public static function factory(): self
	{
		return new self;
	}

	public function dashboard(Request $request, string $type = null, $policesQuery): array
	{
       	$data = [
       		'insurances' => $this->getInsurances(),
       		'statuses' => $this->getStatuses($type),
	       	'agents' => $this->getAgentsAndSubAgents(),
	        'cities' => $this->getCities(),
            'statistic' => $this->getDashboardStatistic($request, $policesQuery),
            'curators' => $this->getCurators(),
            'selectedAgents' => $this->getSelectedAgents($request),
       	];

        if ($type == 'agents') {
            return Arr::except($data, ['agents']);
        }

        return $data;
	}

	public function getCities(array $cities = []): array
	{
        $addresses = array_unique(array_filter(
            \App\Model\Owner::orderBy('id', 'desc')
            ->get()
            ->each(function($owner) {
                $address = explode(',', $owner->address);
                $owner->address = array_get($address, '0');
            })
            ->pluck('address', 'id')
            ->toArray()
        ));

        // foreach (City::withAgents()->get() as $city) {
        foreach ($addresses as $ownerId => $cityName) {
            $cities[] = [
                'code' => $ownerId,
                'name' => $cityName,
            ];
        }

        return $cities;
	}

	public function getStatuses($type = null, $statuses = [])
	{
        if (class_exists($this->model[$type])) {
            $model = $this->model[$type];
        }

        foreach ($model::$statuses as $key => $status) {
            $statuses[] = [
                'code' => $key,
                'name' => $status,
            ];
        }

        return $statuses;
	}

	public function getAgentsAndSubAgents(array $agents = []): array
	{
        $users = User::with(User::getSubAgentsRelationsLevel())->where('parent_user_id', null)
            ->dashboard();

        $authUser = auth()->user();

        if (User::ROLE_CURATOR === $authUser->role) {
            if($authUser->curator) {
                $ids = $authUser->curator->agents->pluck('id');
                $users->whereIn('id', $ids);
            } else {
                $users->with('curator.user')
                    ->whereHas('curator.user', function($q) use($authUser) {
                        $q->where('id', $authUser->id);
                    });
            }
        }

        $users = $users->get();

        foreach ($users as $agent) {
            $agents[] = $this->getdataAgent($agent);

            $this->getSubAgentsTree($agent, $agents);
        }

        return $agents;
	}

    private function getSubAgentsTree($agent, array &$agents): array
    {
        if ($agent->subAgents->count() == 0) {
            return [];
        }

        foreach ($agent->subAgents as $subAgent) {
            $agents[] = $this->getdataAgent($subAgent);

            if ($subAgent->subAgents && $subAgent->subAgents->count() > 0) {
                $this->getSubAgentsTree($subAgent, $agents);
            }
        }

        return $agents;
    }

    private function getdataAgent(User $agent): array
    {
        return [
            'code' => $agent->id,
            'is_agent' => null === $agent->parent_user_id,
            'name' => $agent->name . ' ' . $agent->last_name,
            'level' => $agent->level,
        ];
    }

	public function getInsurances(array $insurances = []): array
	{
        foreach (Policy::insurance() as $key => $insurance) {
            $insurances[] = [
                'code' => $key,
                'name' => $insurance,
            ];
        }

        return $insurances;
	}

    public function formedFilters(Request $request): array
    {
        $filters = array_filter($request->all());
        $filters['date'] = $request->only('from', 'to');

        $filters['agents'] = $this->formedFilterAgents($filters);

        if (!empty($filters['insurances'])) {
            $filters['insurances'] = array_column($filters['insurances'], 'code');
        }

        if (!empty($filters['statuses'])) {
            $filters['statuses'] = array_column($filters['statuses'], 'code');
        }

        if (!empty($filters['cities'])) {
            $filters['cities'] = array_column($filters['cities'], 'code');
        }

        if (!empty($filters['curators'])) {
            $filters['curators'] = array_column($filters['curators'], 'code');
        }

        if (!empty($filters['ownercities'])) {
            $filters['ownercities'] = array_column($filters['ownercities'], 'name');
        }

        return array_filter($filters);
    }

    private function formedFilterAgents(array $filters): array
    {
        if (is_array(array_get($filters, 'agents')) && $agents = array_get($filters, 'agents')) {

            $firstElement = array_get($agents, '0');

            if (array_get($firstElement, 'code')) {
                return array_column($agents, 'code');                
            }

            return $agents;
        }

        return [];
    }

    public function getDashboardStatistic($request, $policesQuery): stdClass
    {
        $statistics = new Statistics($request, $policesQuery);

        return $statistics->render();
    }   

    public function getCurators(array $curators = [])
    {
        foreach (Curator::get() as $curator) {
            $curators[] = [
                'code' => $curator->id,
                'name' => $curator->full_name,
            ];
        }

        return $curators;
    }

    public function getSelectedAgents(Request $request)
    {
        if (!$request->has('agents')) {
            return [];
        }

        try {
            $agent = User::find(array_first($request->get('agents')));

            return [
                'code' => $agent->id,
                'name' => "({$agent->id}) $agent->full_name",
            ];
        } catch (\Exception $e) {}

        return [];
    }
}
