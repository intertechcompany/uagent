<?php

namespace App\Helper;


class PolicyLogHelper
{
    public static $_instance = [];

    private $data = [];

    private $event;

    private $policyID;

    private $stringLogs;

    /**
     * @param string $event
     * @return PolicyLogHelper
     */
    public static function getInstance(string $event): self
    {
        if (!isset(self::$_instance[$event])) {
            self::$_instance[$event] = new self();
        }
        return self::$_instance[$event];
    }

    /**
     * @param string $log
     * @return PolicyLogHelper
     */
    public function setStringLog(string $log): self
    {
        $this->data[] = $log;
        return $this;
    }

    /**
     * @param string $event
     * @return PolicyLogHelper
     */
    public function setEvent(string $event): self
    {
        $this->event = $event;
        return $this;
    }

    /**
     * @param int $policy
     * @return PolicyLogHelper
     */
    public function setPolicyID(int $policy): self
    {
        $this->policyID = $policy;
        return $this;
    }

    /**
     * @return PolicyLogHelper
     */
    public function renderString(): self
    {
        $this->stringLogs = implode("\n",$this->data);
        return $this;
    }

    /**
     * @return string
     */
    public function getString(): string
    {
        if (null === $this->stringLogs) {
            $this->renderString();
        }
        return $this->stringLogs;
    }

    /**
     * @param string $path
     */
    public function saveLog(string $path): void
    {
        if (null === $this->stringLogs) {
            $this->renderString();
        }

        if(!is_dir(rtrim(strtr($path, [basename($path) => '']), '/'))) {
            @mkdir(rtrim(strtr($path, [basename($path) => '']), '/'));
        }

        file_put_contents($path,$this->stringLogs,FILE_APPEND);
    }
}
