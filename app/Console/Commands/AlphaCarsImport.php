<?php

namespace App\Console\Commands;

use App\Model\AlphaMake;
use App\Model\AlphaModel;
use App\Service\PolicyService\Client\Alpha\OsagoListsClient;
use Illuminate\Console\Command;

class AlphaCarsImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'alpha:cars:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Cars From Alpha';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$client = new OsagoListsClient();

        $marks = AlphaMake::importFromAlpha($client->listMark());

		foreach ($marks as $key => $mark) {
			AlphaModel::importModelFromAlpha($client->listModel($key),$mark->id);
		}

		$this->info('All saved');
    }
}
