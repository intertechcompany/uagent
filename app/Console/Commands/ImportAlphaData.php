<?php

namespace App\Console\Commands;

use App\Model\Category;
use App\Model\Purpose;
use App\Model\Type;
use App\Service\PolicyService\Client\Alpha\OsagoListsClient;
use Illuminate\Console\Command;

class ImportAlphaData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:alpha';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import data from alpha(category,type,Purpose)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new OsagoListsClient();
		$alphaCategories = $this->groupBy($client->listTransportCategory());
		Category::importCategory($alphaCategories);
		Type::importType($client->listTransportType(),$alphaCategories);
		Purpose::importPurpose($client->listTransportPurpose());

		$this->info('All saved');
    }

	/**
	 * Group by categoryID for listTransportCategory
	 * @param array $data
	 * @return array
	 */
    private function groupBy(array $data): array
	{
		$result = [];

		foreach ($data as $item) {
			$result[$item->categoryId] = $item;
		}

		unset($data);

		return $result;
	}
}
