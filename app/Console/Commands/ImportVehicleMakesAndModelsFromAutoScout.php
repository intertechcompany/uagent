<?php

namespace App\Console\Commands;

use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ImportVehicleMakesAndModelsFromAutoScout extends Command
{
	protected $signature = 'import:vehicle_makes_and_models';

	protected $description = 'Imports vehicle makes and models from scout24 API';

	protected $http;

	public function __construct()
	{
		$this->http = new Client([
			'base_uri' => 'https://api.autoscout24.com/',
			'headers' => [
				'X-AS24-Version' => '1.1',
				'Accept-Language' => 'en-GB',
				'Accept' => 'application/json',
			]
		]);

		parent::__construct();
	}

	public function handle(): void
	{
		$makes = $this->getMakes();

		foreach ($makes as $make) {

            $checkDB = DB::table('vehicle_makes')->where('name',array_get($make, 'name'))->first();

            echo "\t" . array_get($make, 'name') . "\n";

            if (empty($checkDB)) {
                $makeId = DB::table('vehicle_makes')->insertGetId([
                    'name' => array_get($make, 'name'),
                ]);
            } else {
                $makeId = $checkDB->id;
            }

			$models = $this->getModels(array_get($make, 'id'));

			foreach ($models as $model) {
                $checkModeDB = DB::table('vehicle_models')->where('name',array_get($model, 'name'))->first();
                if (empty($checkModeDB)) {
                    DB::table('vehicle_models')->insert([
                        'vehicle_make_id' => $makeId,
                        'name' => array_get($model, 'name'),
                        'vehicle_type' => array_get($model, 'vehicleType')
                    ]);
                    echo array_get($model, 'name') . "\n";
                }


			}
		}
	}

	private function getMakes()
	{
		$response = json_decode($this->http->get('makes')->getBody()->getContents(), true);

		return array_get($response, '_data.makes');
	}

	private function getModels($externalMakeId)
	{
		$response = json_decode($this->http->get("makes/$externalMakeId/models")->getBody()->getContents(), true);

		return array_get($response, '_data.models');
	}
}
