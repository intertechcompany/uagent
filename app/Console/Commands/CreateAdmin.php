<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class CreateAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates admin';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$user = new User();
		$user->is_email_confirmed = true;
		$user->verified_at = now();
		$user->role = User::ROLE_ADMIN;
		$user->name = $this->ask('name');
		$user->email = $this->ask('email');
		$user->password = Hash::make($this->ask('password'));
		$user->save();
    }
}
