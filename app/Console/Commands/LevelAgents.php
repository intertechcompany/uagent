<?php

namespace App\Console\Commands;

use App\User;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;

class LevelAgents extends Command
{
    /**
     * @var int
     */
    public const START_LEVEL = 1;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'agents:level';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for create levels agents, subagents etc.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Start set level for agents');

        foreach ($this->getAgents() as $agent) {
            $this->setLevelAgent($agent, self::START_LEVEL);

            $this->processSubAgents($agent, 2);
        }

        $this->info('End');
    }

    public function processSubAgents(User $agent, int $level)
    {
        try {
            foreach ($agent->subAgents as $subAgent) {
                if (!$subAgent) {
                    continue;
                }

                $this->setLevelAgent($subAgent, $level);

                if ($subAgent->subAgents && $subAgent->subAgents->count() > 0) {
                    $this->processSubAgents($subAgent, $level+1);
                }
            }
        } catch (Exception $ex) {
            $this->info("Error: " . $ex->getMessage());
        }

        return null;
    }

    public function getAgents(): ?Collection
    {
        return User::where('role', User::ROLE_AGENT)
            ->where('parent_user_id', null)
            ->get();
    }

    public function setLevelAgent(User $agent, int $level): ?User
    {
        if (null === $agent->parent_user_id) {
            $level = self::START_LEVEL;
        }

        $agent->level = $level;
        $agent->save();

        $this->info("Agent {$agent->full_name} set level: {$level}");

        return $agent;
    }
}
