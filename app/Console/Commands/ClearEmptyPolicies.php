<?php

namespace App\Console\Commands;

use App\Model\Policy;
use Illuminate\Console\Command;

class ClearEmptyPolicies extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'policy:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear empty policies';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $yesterday = now()->addDay(-1);

        Policy::query()
            ->whereNull('user_id')
            ->where('updated_at','<=',$yesterday)
            ->delete()
        ;

		$this->info('Cleared!');
    }
}
