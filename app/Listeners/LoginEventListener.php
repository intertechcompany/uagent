<?php

namespace App\Listeners;

use App\Events\LoginEvent;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;

class LoginEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(LoginEvent $event)
    {
        $r = Request::createFromGlobals();

        DB::table('login_history')->insert([
            'user_id' => $event->user->id,
            'ip_address' => $r->ip(),
            'user_agent' => $r->header('user_agent'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
