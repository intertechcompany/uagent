<?php

namespace App\Components\Kv;

use App\Model\Commission;
use App\Model\Rate;
use App\Model\RateCity;
use App\Model\RateInterface;
use App\User;
use Illuminate\Database\Eloquent\Model;

class SetCommission
{
    /**
     * @var User
     */
    private $agent;

    /**
     * @var string
     */
    private $rateType;

    /**
     * @var float
     */
    private $agentCommission;

    /**
     * @var float
     */
    private $youCommission;

    /**
     * @var int
     */
    private $commissionId;

    /**
     * @var Commission|Rate|RateCity
     */
    private $rate;

    /**
     * @var bool
     */
    private $rateIsCommission;

    /**
     * SetCommission constructor.
     * @param User $agent
     * @param string $rateType
     * @param int $rateId
     * @param bool $isCommission
     * @param float $agentCommission
     * @param float $youCommission
     * @param int|null $commissionId
     */
    public function __construct(
        User $agent,
        int $rateId,
        string $rateType,
        bool $isCommission,
        float $agentCommission,
        float $youCommission,
        int $commissionId = null
    )
    {
        $this->agent = $agent;
        $this->rateType = $rateType;
        $this->agentCommission = $agentCommission;
        $this->youCommission = $youCommission;
        $this->commissionId = $commissionId;
        $this->rateIsCommission = $isCommission;

        $this->rate = $this->findRate($rateId,$isCommission);
    }

    /**
     * @return Commission
     */
    public function calcCommission(): Commission
    {
        $this->clearCommission();
        $commission = $this->findCommission();

        if (bccomp($this->agentCommission + $this->youCommission,$commission->all) !== 0) {
            throw new \LogicException('The amount of the agent and subagent commission must not be greater than the value of the rate.');
        }

        $commission->agent = $this->agentCommission;
        $commission->you = $this->youCommission;
        $commission->save();

        ReCalcRates::factory()->calcCommission($commission);

        return $commission;
    }

    /**
     * @param int $rateID
     * @param bool $isCommission
     * @return Model|RateInterface
     */
    private function findRate(int $rateID, bool $isCommission): RateInterface
    {
        if ($isCommission === true) {
            return Commission::query()->find($rateID);
        }

        return $this->rateType === Commission::TYPE_OF_RATE_RATE ?
            Rate::query()->find($rateID) :
            RateCity::query()->find($rateID)
        ;
    }

    /**
     * @return Commission
     */
    private function findCommission(): Commission
    {
        $commission = $this->commissionId !== 0 ? Commission::query()->find($this->commissionId) : new Commission();

        $commission->agent_id = $this->agent->id;
        $commission->rate_type = $this->rateType;
        $commission->rate_id =  $this->rateIsCommission === false ? $this->rate->id : $this->rate->rate_id;
        $commission->all = $this->rate->getValue();
        $commission->type = $this->rate->type;
        $commission->insurance = $this->rate->insurance;
        $commission->parent_commission_id = $this->rateIsCommission === true ? $this->rate->id : null;

        return $commission;
    }

    private function clearCommission(): void
    {
        if ($this->commissionId === 0) {

            Commission::query()
                ->where('rate_id',$this->rate->getRateId())
                ->where('agent_id',$this->agent->id)
                ->where('rate_type',$this->rateType)
                ->where('insurance', $this->rate->insurance)
                ->delete()
            ;
        }
    }
}
