<?php

namespace App\Components\Kv;

use App\Model\Commission;
use App\Model\KvBonus;
use App\Model\Policy;
use App\Model\RateInterface;
use App\User;
use Illuminate\Support\Facades\DB;

class ChargeBonusKvHelper
{
    /**
     * @var Policy
     */
    private $policy;

    /**
     * ChargeBonusKvHelper constructor.
     * @param Policy $policy
     */
    public function __construct(Policy $policy)
    {
        $this->policy = $policy;
    }

    /**
     * Charge Bonus
     */
    public function chargeBonus(): void
    {
        $user = $this->policy->user;

        $userKv = (new KvHelper($user,$this->policy->insurance,$this->policy->getKladers()))->getKv();

        if ($userKv !== null) {

            DB::beginTransaction();

            $this->charge($user,$userKv->getModel());

            DB::commit();
        }
    }

    /**
     * @param string $type
     * @param float $value
     * @param float $premium
     * @return float
     */
    public static function calcSum(string $type,float $value,float $premium): float
    {
        if(empty($premium)) {
            return 0;
        }

        if ($type === Commission::RUB) {
            return $value;
        }

        $percent = $value;

        return $premium / 100 * $percent;
    }

    /**
     * @param User $user
     * @param RateInterface|Commission $rate
     * @return bool
     */
    private function charge(User $user, RateInterface $rate): bool
    {
        if ($rate->isCommission() === false) {
            $this->storeKvBonus($rate->getType(),$rate->getValue(),$user,true);

            return true;
        }

        $this->storeKvBonus($rate->getType(), $rate->getValue(), $user,true);
        $this->saveTransaction($user->agent, $rate);

        return true;
    }

    /**
     * @param User $user
     * @param Commission $commission
     */
    private function saveTransaction(User $user, Commission $commission): void
    {
        $this->storeKvBonus($commission->getType(), $commission->you, $user);

        if ($commission->isHaveParent() === true) {
            $parentCommission = $commission->parentCommission;
            $this->saveTransaction($user->agent,$parentCommission);
        }
    }

    /**
     * @param string $type
     * @param float $value
     * @param User $user
     * @param bool $isOwner
     * @return KvBonus
     */
    private function storeKvBonus(string $type,float $value,User $user, bool $isOwner = false): KvBonus
    {
        $sum = self::calcSum($type,$value,$this->policy->premium);

        $kvBonus = new KvBonus();
        $kvBonus->policy_id = $this->policy->id;
        $kvBonus->user_id = $user->id;
        $kvBonus->kv = $value;
        $kvBonus->sum = $sum;
        $kvBonus->type = $type;
        $kvBonus->is_owner_policy = $isOwner;
        $kvBonus->save();

        $user->balance += $sum;
        $user->save();

        return $kvBonus;
    }
}
