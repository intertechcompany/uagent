<?php

namespace App\Components\Kv;

use App\Entity\Kv;
use App\Model\Commission;
use App\Model\Policy;
use App\Model\Rate;
use App\User;

class UpdateRateForAgent
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var iterable
     */
    private $newRates;

    /**
     * UpdateRateForAgent constructor.
     * @param User $user
     * @param iterable $newRates
     */
    public function __construct(User $user,iterable $newRates)
    {
        $this->user = $user;
        $this->newRates = \count($newRates) > 0 ?
            Rate::query()->whereIn('id',$newRates)->get()
                // ->keyBy('insurance')
                :
            collect()
        ;
    }

    public function update(): void
    {
        $oldKv = [];

        foreach (Policy::$activeInsurances as $insurance) {
            $oldKv[$insurance] = (new KvHelper($this->user, $insurance))->getKv();
        }

        $this->user->rates()->sync($this->newRates);

        foreach (Policy::$activeInsurances as $insurance) {
            $newKv = (new KvHelper($this->user, $insurance))->getKv();
            $this->resolveKv($oldKv[$insurance],$newKv);
        }
    }

    /**
     * @param Kv|null $oldKv
     * @param Kv|null $newKv
     */
    private function resolveKv(?Kv $oldKv, ?Kv $newKv): void
    {
        if (
            $oldKv !== null &&
            $newKv !== null &&
            $oldKv->getModel()->getRateId() !== $newKv->getModel()->getRateId()
        ) {
            $changeRate = new ChangeRateForAgent($oldKv->getModel()->getRateId(), $newKv->getModel(),$this->user);
            $changeRate->change();
        } elseif ($oldKv !== null && $newKv === null) {
            $rate = $oldKv->getModel();

            $subAgentsIds = $this->user->subAgents()->pluck('id');

                Commission::query()
                ->whereIn('agent_id',$subAgentsIds)
                ->where('rate_id', $rate->getRateId())
                ->where('rate_type', $rate->getTypeOfRate())
                ->delete()
            ;
        }
    }
}
