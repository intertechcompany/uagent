<?php

namespace App\Components\Kv;

use App\Model\Commission;
use App\Model\RateInterface;
use App\User;

class ChangeRateForAgent
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var int
     */
    private $oldRateId;

    /**
     * @var RateInterface
     */
    private $rate;

    /**
     * ChangeRateForAgent constructor.
     * @param int $oldRateID
     * @param RateInterface $rate
     * @param User $user
     */
    public function __construct(int $oldRateID, RateInterface $rate, User $user)
    {
        $this->rate = $rate;
        $this->user = $user;
        $this->oldRateId = $oldRateID;
    }

    public function change(): void
    {
        $subAgentsIds = $this
            ->user
            ->subAgents()
            ->pluck('id')
        ;

        Commission::query()
            ->whereIn('agent_id', $subAgentsIds)
            ->where('rate_id', $this->oldRateId)
            ->where('rate_type', $this->rate->getTypeOfRate())
            ->update([
                'rate_id' => $this->rate->getRateId(),
                'type' => $this->rate->getType(),
            ])
        ;

        $commissions = Commission::query()
            ->whereIn('agent_id', $subAgentsIds)
            ->where('rate_id', $this->rate->getRateId())
            ->where('rate_type', $this->rate->getTypeOfRate())
            ->whereNull('parent_commission_id')
            ->get()
        ;

        foreach ($commissions as $commission) {
            ReCalcRates::factory()->calcCommissionByAll($commission, $this->rate->getValue());
        }
    }
}
