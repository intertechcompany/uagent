<?php

namespace App\Components\Kv;

use App\Model\Commission;
use App\Model\RateInterface;
use Illuminate\Support\Facades\DB;

class ReCalcRates
{
    /**
     * @return ReCalcRates
     */
    public static function factory(): self
    {
        return new self;
    }

    /**
     * @param RateInterface $rate
     */
    public function calc(RateInterface $rate): void
    {
        DB::beginTransaction();

        Commission::query()
            ->where('rate_id',$rate->getRateId())
            ->where('rate_type',$rate->getTypeOfRate())
            ->whereNull('parent_commission_id')
            ->update([
                'type' => $rate->getType(),
            ])
        ;

        $commissions = Commission::query()
            ->where('rate_id',$rate->getRateId())
            ->where('rate_type',$rate->getTypeOfRate())
            ->whereNull('parent_commission_id')
            ->get()
        ;

        foreach ($commissions as $commission) {
            $this->resolveCommission($commission,$rate->getValue());
        }

        DB::commit();
    }

    /**
     * @param RateInterface $rate
     */
    public function clearCommissions(RateInterface $rate): void
    {
        Commission::query()
            ->where('rate_id', $rate->getRateId())
            ->where('rate_type', $rate->getTypeOfRate())
            ->delete()
        ;
    }

    /**
     * @param Commission $commission
     */
    public function calcCommission(Commission $commission): void
    {
        DB::beginTransaction();

        foreach ($commission->childrenCommission()->get() as $child) {
            $this->resolveCommission($child, $commission->getValue());
        }

        DB::commit();
    }

    /**
     * @param Commission $commission
     * @param float $all
     */
    public function calcCommissionByAll(Commission $commission, float $all): void
    {
        DB::beginTransaction();

        $this->resolveCommission($commission, $all);

        DB::commit();
    }

    /**
     * @param Commission $commission
     * @param float $all
     */
    private function resolveCommission(Commission $commission, float $all): void
    {
        $youPercent = round($commission->you / $commission->all,2);

        $commission->all = $all;
        $commission->you = $youPercent * $commission->all;
        $commission->agent = $commission->all - $commission->you;
        $commission->save();

        $commission->childrenCommission()->update([
            'rate_id' => $commission->getRateId(),
            'type' => $commission->getType(),
        ]);

        foreach ($commission->childrenCommission()->get() as $child) {
            $this->resolveCommission($child,$commission->getValue());
        }
    }
}
