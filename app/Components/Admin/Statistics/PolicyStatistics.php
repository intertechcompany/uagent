<?php

namespace App\Components\Admin\Statistics;

use stdClass;
use App\User;
use App\Model\Policy;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

class PolicyStatistics
{
    public $request;
    public $query;

    public function __construct(Statistics $statistics)
    {
        $this->request = $statistics->request;
        $this->query = $statistics->query;
    }

    /**
     * Method get all agents or subagents by vue filtred
     *
     * @param $request
     * @return int
     */
    public function getStatistics(): Builder
    {
        return $this->filterUsers(
            array_filter($this->request->all())
        );
    }

    private function filterUsers(array $filters = []): Builder
    {
        /**
         * Get query policy with statuses - success, paid
         */
        return $this->query;

        foreach ($filters as $relation => $filter) {
            if (!is_array($filter)) {
                if ($relation == 'number') {
                    $query->where('insurance_id', $filter);
                }

                continue;
            }

            $this->setFilter($query, $relation, $filter);
        }

        return $query;
    }

    private function setFilter(Builder $query, string $relation, array $filter = []): Builder
    {
        $ids = array_column($filter, 'code');

        switch ($relation) {

            /**
             * Find polices by insurances
             */
            case 'insurances':
                $query->whereIn('insurance', $ids);

                break;

            /**
             * Find polices by city id
             */
            case 'cities':
                $query->whereHas('user.city', function($queryCity) use ($ids) {
                    $queryCity->whereIn('cities.id', $ids);
                });

                break;

            /**
             * Find polices by status
             */
            case 'statuses':
                $query->whereIn('status', $ids);

                break;

            /**
             * Find user by parent user id (subagents)
             */
            case 'agents':
                $query->whereHas('user', function($queryCity) use ($ids) {
                    $queryCity->whereIn('id', $ids);
                });

                break;

            default:
                # code...
                break;
        }

        return $query;
    }
}
