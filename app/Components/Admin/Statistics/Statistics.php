<?php

namespace App\Components\Admin\Statistics;

use stdClass;
use App\User;
use App\Model\City;
use App\Model\Cache;
use App\Model\Policy;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

class Statistics
{
    public $request;
    public $query;

    public function __construct(Request $request, $policesQuery)
    {
        $this->request = $request;
        $this->query = $policesQuery;
    }

    public function render()
    {
        $statistic = new stdClass();

        /**
         * Get all agents, subagents and subagents of one subagent
         */
        $agentStatistics = new AgentStatistics($this);
        $statistic->allAgents = $agentStatistics->getStatistics();

        /**
         * Get all policies
         */
        $policyStatistics = new PolicyStatistics($this);
        $getStatistics = $policyStatistics->getStatistics();

        $statistic->allPolices = $getStatistics->count();

        $statistic->allPolicesSumPremium = number_format($getStatistics->sum('premium'), 2);
        $statistic->avgPolicesPremium = number_format($getStatistics->avg('premium'), 2);
        $statistic->is_filtred = $this->request->has('agents') && is_array($this->request->get('agents'));

        return $statistic;
    }

    /**
     * Method get all polices or subagents by vue filtred
     *
     * @param $request
     * @return int
     */
    private function getStatisticPolices(): int
    {
        return Policy::dashboard()->count();
    }
}
