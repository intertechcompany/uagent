<?php

namespace App\Components\Admin\Statistics;

use stdClass;
use App\User;
use App\Model\City;
use App\Model\Cache;
use App\Model\Policy;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

class AgentStatistics
{
    public $request;
    public $query;

    public function __construct(Statistics $statistics)
    {
        $this->request = $statistics->request;
        $this->query = $statistics->query;
    }

    /**
     * Method get all agents or subagents by vue filtred
     *
     * @param $request
     * @return int
     */
    public function getStatistics(): int
    {
        return $this->filterUsers(
            array_filter($this->request->all())
        )->count();
    }

    private function filterUsers(array $filters = []): Builder
    {
        $formedFilters = $filters;
        if (isset($formedFilters['page'])) {
            unset($formedFilters['page']);
        }

        if (empty($formedFilters)) {
            return User::query();
        }

        if(!empty($filters['agents'])) {
            $query = User::query()
                ->whereIn('parent_user_id', array_pluck($filters['agents'], 'code'))
            ;
        } elseif(count($filters) > 2) {
            $query = User::query()
                ->whereHas('polices', function ($query) {
                    return $query->whereIn('id', $this->query->pluck('id'));
                });
        } else {
            $query = User::query()->where('role', User::ROLE_AGENT);
        }

        if (User::ROLE_CURATOR === auth()->user()->role && auth()->user()->curator !== null) {
            //$listIds = $authUser->curator->agents->pluck('id')->toArray();
            $query->whereIn('id', auth()->user()->curator->agents->pluck('id'));
        }

        return $query;

        foreach ($filters as $relation => $filter) {
            if (!is_array($filter)) {
                if ($relation == 'number') {
                    $query->whereHas('polices', function($queryPolicy) use ($filter) {
                        $queryPolicy->where('insurance_id', $filter);
                    });
                }

                continue;
            }

            $this->setFilter($query, $relation, $filter);
        }

        return $query;
    }

    private function setFilter(Builder $query, string $relation, array $filter = []): Builder
    {
        $ids = array_column($filter, 'code');

        switch ($relation) {

            /**
             * Find user by policy insurance
             */
            case 'insurances':
                $query->whereHas('polices', function($queryPolicy) use ($ids) {
                    $queryPolicy->whereIn('insurance', $ids);
                });

                break;

            /**
             * Find user by parent user id (subagents)
             */
            case 'agents':
                $query->whereIn('parent_user_id', $ids);

                break;

            /**
             * Find user by city id
             */
            case 'cities':
                $query->whereIn('city_id', $ids);

                break;

            /**
             * Find user by policy status
             */
            case 'statuses':
                $query->whereHas('polices', function($queryPolicy) use ($ids) {
                    $queryPolicy->whereIn('status', $ids);
                });

                break;
            
            default:
                # code...
                break;
        }

        return $query;
    }
}
