<?php

namespace App\Components\Dashboard\Types;

use App\Components\Dashboard\DashboardInterface;
use App\Model\Policy;
use DateTime;

class CountSalesPoliciesTodayType extends AbstractDashboard
{
    /**
     * Продано полисов за все время
     */
    public const KEY = 'count_sales_policies_today';

    /**
     * @return int|mixed
     */
    public function getData(): DashboardInterface
    {
        $today = new DateTime();
        $todayFormatted = $today->format('Y-m-d') . ' ';

        $this->value = $this
            ->user
            ->polices()
            ->whereBetween('created_at',[$todayFormatted . '00:00:00',$todayFormatted . '23:59:59'])
            ->where('status',Policy::SUCCESS_STATUS)
            ->count()
        ;

        return $this;
    }
}
