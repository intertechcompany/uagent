<?php

namespace App\Components\Dashboard\Types;

use App\Components\Dashboard\DashboardInterface;
use App\Model\Policy;

class IncomeTodayType extends AbstractDashboard
{
    /**
     * Доход за день
     */
    public const KEY = 'income_today';

    /**
     * @return mixed
     */
    public function getData(): DashboardInterface
    {
        $today = new \DateTime();
        $todayFormatted = $today->format('Y-m-d') . ' ';

        $this->value = $this
            ->user
            ->kvBonus()
            ->whereBetween('created_at',[$todayFormatted . '00:00:00',$todayFormatted . '23:59:59'])
            ->sum('sum')
        ;

        return $this;
    }

    /**
     * @return string
     */
    public function getFormattedValue(): string
    {
        return number_format($this->value,2) . ' ₽';
    }

    /**
     * @return string
     */
    public function getClass(): string
    {
        return ' c-state-card__money';
    }
}
