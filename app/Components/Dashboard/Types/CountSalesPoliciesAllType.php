<?php

namespace App\Components\Dashboard\Types;

use App\Components\Dashboard\DashboardInterface;
use App\Model\Policy;

class CountSalesPoliciesAllType extends AbstractDashboard
{
    /**
     * КОЛИЧЕСВО ПРОДАНЫХ ПОЛИСОВ (ВСЕГО)
     */
    public const KEY = 'count_sales_policies_all';

    /**
     * @inheritdoc
     */
    public function getData(): DashboardInterface
    {
        $this->value = $this
            ->user
            ->polices()
            ->where('status',Policy::SUCCESS_STATUS)
            ->count()
        ;

        return $this;
    }
}
