<?php

namespace App\Components\Dashboard\Types;

use App\Components\Dashboard\DashboardInterface;
use App\Model\KvBonus;
use App\Model\Policy;

class IncomeAllType extends AbstractDashboard
{
    /**
     * Доход за все время
     */
    public const KEY = 'income_all';

    /**
     * @return mixed
     */
    public function getData(): DashboardInterface
    {
        $this->value = $this
            ->user
            ->kvBonus()
            ->sum('sum')
        ;

        return $this;
    }

    /**
     * @return string
     */
    public function getFormattedValue(): string
    {
        return number_format($this->value,2) . ' ₽';
    }

    /**
     * @return string
     */
    public function getClass(): string
    {
        return ' c-state-card__money';
    }
}
