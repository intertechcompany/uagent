<?php

namespace App\Components\Dashboard\Types;

use App\Components\Dashboard\DashboardInterface;
use App\Model\Policy;
use Carbon\Carbon;

class CountSalesPoliciesMonthType extends AbstractDashboard
{
    /**
     * Продано полисов (за этот месяц)
     */
    public const KEY = 'count_sales_policies_month';

    /**
     * @return int|mixed
     */
    public function getData(): DashboardInterface
    {
        $firstDayOfMonth = Carbon::now()->startOfMonth();
        $lastDayOfMonth = Carbon::now()->endOfMonth();

        $this->value =  $this
            ->user
            ->polices()
            ->whereBetween('created_at',[$firstDayOfMonth,$lastDayOfMonth])
            ->where('status',Policy::SUCCESS_STATUS)
            ->count()
        ;

        return $this;
    }
}
