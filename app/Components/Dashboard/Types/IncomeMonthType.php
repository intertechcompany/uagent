<?php

namespace App\Components\Dashboard\Types;

use App\Components\Dashboard\DashboardInterface;
use App\Model\Policy;
use Carbon\Carbon;

class IncomeMonthType extends AbstractDashboard
{
    /**
     * Доход за месяц
     */
    public const KEY = 'income_month';

    /**
     * @return mixed
     */
    public function getData(): DashboardInterface
    {
        $firstDayOfMonth = Carbon::now()->startOfMonth();
        $lastDayOfMonth = Carbon::now()->endOfMonth();

        $this->value = $this
            ->user
            ->kvBonus()
            ->whereBetween('created_at',[$firstDayOfMonth,$lastDayOfMonth])
            ->sum('sum')
        ;

        return $this;
    }

    /**
     * @return string
     */
    public function getFormattedValue(): string
    {
        return number_format($this->value,2) . ' ₽';
    }

    /**
     * @return string
     */
    public function getClass(): string
    {
        return ' c-state-card__money';
    }
}
