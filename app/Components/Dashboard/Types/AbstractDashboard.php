<?php

namespace App\Components\Dashboard\Types;

use App\Components\Dashboard\DashboardInterface;
use App\Model\UserDashboardBlock;
use App\User;
use Illuminate\Contracts\Auth\Guard;

abstract class AbstractDashboard implements DashboardInterface
{
    public const KEY = null;

    /**
     * @var Guard
     */
    protected $guard;

    /**
     * @var User $user
     */
    protected $user;

    /**
     * @var mixed
     */
    protected $value;

    /**
     * AbstractDashboard constructor.
     * @param Guard $guard
     */
    public function __construct(Guard $guard)
    {
        $this->guard = $guard;
        $this->user = $this->guard->user();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return UserDashboardBlock::BLOCKS[static::KEY];
    }

    /**
     * @return string
     */
    public function getFormattedValue(): string
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getClass(): string
    {
        return '';
    }
}
