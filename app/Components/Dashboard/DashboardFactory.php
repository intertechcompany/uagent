<?php

namespace App\Components\Dashboard;

use App\Components\Dashboard\Types\CountSalesPoliciesAllType;
use App\Components\Dashboard\Types\CountSalesPoliciesMonthType;
use App\Components\Dashboard\Types\CountSalesPoliciesTodayType;
use App\Components\Dashboard\Types\IncomeAllType;
use App\Components\Dashboard\Types\IncomeMonthType;
use App\Components\Dashboard\Types\IncomeTodayType;

class DashboardFactory
{
    public static function factory(string $key): DashboardInterface
    {
        switch ($key) {
            case CountSalesPoliciesAllType::KEY:
                return app()->make(CountSalesPoliciesAllType::class);
            case CountSalesPoliciesTodayType::KEY:
                return app()->make(CountSalesPoliciesTodayType::class);
            case CountSalesPoliciesMonthType::KEY:
                return app()->make(CountSalesPoliciesMonthType::class);
            case IncomeAllType::KEY:
                return app()->make(IncomeAllType::class);
            case IncomeTodayType::KEY:
                return app()->make(IncomeTodayType::class);
            case IncomeMonthType::KEY:
                return app()->make(IncomeMonthType::class);
        }

    }
}
