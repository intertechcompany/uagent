<?php

namespace App\Components\Dashboard;

use Illuminate\Contracts\Auth\Guard;

interface DashboardInterface
{
    /**
     * DashboardInterface constructor.
     * @param Guard $guard
     */
    public function __construct(Guard $guard);

    /**
     * @return mixed
     */
    public function getData(): DashboardInterface;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return string
     */
    public function getFormattedValue(): string;

    /**
     * @return string
     */
    public function getClass(): string;
}
