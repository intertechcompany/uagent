<?php

namespace App\Components\RatesForSubAgent;

use App\Entity\SubAgentRates;
use App\Model\Commission;
use App\Model\Policy;
use App\Model\RateCity;
use App\User;

class RatesForSubAgentsHelper
{
    /**
     * @var User
     */
    private $user;

    /**
     * RatesForSubAgentsHelper constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return SubAgentRates
     */
    public function getRates(): SubAgentRates
    {
        if ($this->user->parent_user_id === null) {
            return $this->getSubAgentRatesByAgentFirstLvl();
        }

        return $this->getSubAgentRatesBySubAgent();
    }

    /**
     * @return SubAgentRates
     */
    private function getSubAgentRatesBySubAgent(): SubAgentRates
    {
        $subAgentRates = new SubAgentRates();

        /**
         * @var Commission[] $commissions
         */
        $commissions = $this->user->commissions()->get();

        foreach ($commissions as $commission) {
            if ($commission->getTypeOfRate() === Commission::TYPE_OF_RATE_RATE_CITY) {
                $subAgentRates->addCityRate($commission->insurance, $commission);
                continue;
            }
            $subAgentRates->addRate($commission->insurance, $commission);
        }

        return $subAgentRates;
    }

    /**
     * @return SubAgentRates
     */
    private function getSubAgentRatesByAgentFirstLvl(): SubAgentRates
    {
        $subAgentRates = new SubAgentRates();

        foreach (Policy::$activeInsurances as $insurance) {

            $rate = $this->user->getKv($insurance);
            if ($rate !== null) {
                $subAgentRates->addRate($insurance, $rate->getModel());
            }

            $rateCities = RateCity::byInsurance($insurance)->with('city')->get();

            foreach ($rateCities as $rateCity) {
                $subAgentRates->addCityRate($insurance, $rateCity);
            }
        }

        return $subAgentRates;
    }
}
