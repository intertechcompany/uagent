<?php

namespace App\Components\RatesForSubAgent;

use App\Model\Commission;
use App\User;

class AgentCommissions
{
    /**
     * @var User
     */
    private $agent;

    /**
     * @var Commission[]
     */
    private $commissions;

    /**
     * AgentCommissions constructor.
     * @param User $agent
     */
    public function __construct(User $agent)
    {
        $this->agent = $agent;
    }

    /**
     * @return AgentCommissions
     */
    public function renderCommissions(): self
    {
        /**
         * @var Commission[] $commissions
         */
        $commissions = $this->agent->commissions()->get();

        foreach ($commissions as $commission) {
            $this->commissions[$commission->insurance][$commission->rate_type][$commission->getRateId()] = $commission;
        }

        return $this;
    }

    /**
     * @param string $insurance
     * @param string $type
     * @param int $rateID
     * @return Commission|null
     */
    public function getCommission(string $insurance, string $type, int $rateID): ?Commission
    {
        return $this->commissions[$insurance][$type][$rateID] ?? null;
    }
}
