<?php

namespace App\Components\Settings;

use App\Model\UserInformation;
use App\User;
use Illuminate\Support\Facades\Validator;

abstract class AbstractSettings implements SettingsInterface
{
    /**
     * @var User $user
     */
    protected $user;

    /**
     * @var mixed
     */
    protected $value;

    /**
     * @var array
     */
    protected $options;

    /**
     * @var array
     */
    protected $rules = [];

    /**
     * @var string
     */
    protected $name;

    /**
     * @inheritdoc
     */
    public function setUser(User $user): SettingsInterface
    {
        $this->user = $user;

        if ($this->user->information === null) {
            $this->user->information()->create();
        }

        return $this;
    }

    /**
     * @param $value
     * @return SettingsInterface
     */
    public function setValue($value): SettingsInterface
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param array $options
     * @return SettingsInterface
     */
    public function setOptions(array $options): SettingsInterface
    {
        $this->options = $options;

        return $this;
    }

    /**
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validate(): \Illuminate\Contracts\Validation\Validator
    {
        return Validator::make([$this->name => $this->value],$this->rules);

    }
}
