<?php

namespace App\Components\Statistics;

use App\Helper\UserSubAgents;
use App\Model\Policy;
use App\Repository\KvBonusRepository;
use App\User;
use Illuminate\Support\Collection;

class Statistics
{
    /**
     * @var User $user
     */
    private $user;

    /**
     * @var iterable
     */
    private $filter;

    /**
     * @var iterable
     */
    private $userStatistic;

    /**
     * @var iterable
     */
    private $totalStatistic;

    /**
     * @var iterable
     */
    private $subAgentStatistic;

    /**
     * @var KvBonusRepository|\App\Repository\Repository
     */
    private $kvBonusRepository;

    private $chartStatistic;

    /**
     * AgentStatisticExport constructor.
     * @param User $user
     * @param iterable $filter
     */
    public function __construct(User $user, iterable $filter)
    {
        $this->user = $user;
        $this->filter = array_filter($filter);
        $this->filter['date'] = [array_get($this->filter,'from'), array_get($this->filter,'to')];
        $this->kvBonusRepository = KvBonusRepository::factory();
    }

    /**
     * @param User $user
     * @return iterable
     */
    private function mapUserForStatistic(User $user): iterable
    {
        $successPremiumsArray = $user->getSuccessPoliciesPremiums();
        $countSuccessPolicies = \count($successPremiumsArray);
        $sumPremiumSum = array_sum($successPremiumsArray);

        return [
            'id' => $user->id,
            'name' => $user->name,
            'last_name' => $user->last_name,
            'my_income' =>  $this->kvBonusRepository->getMyIncome($user,$this->user),
            'income' => $this->kvBonusRepository->getIncomeForAgent($user),
            'countSubAgents' => $this->calcSubAgents($user),
            'avg_policies_premium' => $countSuccessPolicies > 0 ? $sumPremiumSum / $countSuccessPolicies : 0,
            'count_success_policies' => $countSuccessPolicies,
            'sum_success_policies_premium' => array_sum($successPremiumsArray),
        ];
    }

    /**
     * @param User $user
     * @return int
     */
    private function calcSubAgents(User $user): int
    {
        $count = $user->subAgents->count();

        foreach ($user->subAgents as $subAgent) {
            $count += $this->calcSubAgents($subAgent);
        }

        return $count;
    }

    /**
     * @return Statistics
     */
    public function generate(): self
    {
        $subAgents = new UserSubAgents($this->user,$this->filter,['name','last_name','id','parent_user_id']);

        $subAgents = $subAgents->getSubAgents()
            ->map(function(User $agent) {
                return $this->mapUserForStatistic($agent);
            })
        ;

        if(!$subAgents->isEmpty()) {
            $policiesGroups = Policy::with('user.city')
                ->whereIn('user_id', $subAgents->pluck('id'))
                ->whereHas('user', function ($q) {
                    $q->filter($this->filter);
                })
                ->where('status', Policy::SUCCESS_STATUS)
                ->get()
                ->groupBy('user.city.id');
        } else {
            $policiesGroups = new Collection([]);
        }

        $successPremiumsArray = $this->user->getSuccessPoliciesPremiums();
        $countSuccessPolicies = \count($successPremiumsArray);
        $sumPremiumSum = array_sum($successPremiumsArray);
        $countSubAgents = $subAgents->count();
        $myIncome = $this->kvBonusRepository->getIncomeForAgent($this->user);

        $agent = [
            'id' => $this->user->id,
            'name' => $this->user->name,
            'last_name' => $this->user->last_name,
            'income' => $myIncome,
            'my_income' => $myIncome,
            'countSubAgents' => $countSubAgents,
            'avg_policies_premium' => $countSuccessPolicies > 0 ? $sumPremiumSum / $countSuccessPolicies : 0,
            'count_success_policies' => $countSuccessPolicies,
            'sum_success_policies_premium' => array_sum($successPremiumsArray),
            'is_opportunity_sub_agent' => $this->user->is_opportunity_sub_agent,
        ];

        $successTotalPremium = $agent['sum_success_policies_premium'] + $subAgents->sum('sum_success_policies_premium');
        $successTotalCount = $countSuccessPolicies + $subAgents->sum('count_success_policies');

        $totals = [
            'income' => $agent['income'] + $subAgents->sum('income'),
            'countSubAgents' => $countSubAgents,
            'avg_policies_premium' => $successTotalCount > 0 ? $successTotalPremium / $successTotalCount : 0,
            'count_success_policies' => $successTotalCount,
            'sum_success_policies_premium' => $successTotalPremium,
            'my_income' => $subAgents->sum('my_income') + $myIncome,
        ];

        $columnsForFormat = ['income','avg_policies_premium','sum_success_policies_premium','my_income'];

        $this->userStatistic = $this->numberFormat($agent,$columnsForFormat);
        $this->totalStatistic = $this->numberFormat($totals,$columnsForFormat);
        $this->subAgentStatistic = $subAgents->map(function (iterable $subAgent) use ($columnsForFormat) {
            return $this->numberFormat($subAgent,$columnsForFormat);
        });
        $this->chartStatistic = $this->generateDataForChart($policiesGroups, count($successPremiumsArray));

        return $this;
    }

    private function generateDataForChart($policiesGroups, $userPoliciesCount)
    {
        $data = [];
        $data['regions'][$this->user->city_id] = $this->user->city->name;
        $data['policiesCount'][$this->user->city_id] = $userPoliciesCount;

        if(!$policiesGroups->isEmpty()) {
            foreach ($policiesGroups as $cityId => $policies) {
                $data['regions'][$cityId] = @$policies->first()->user->city->name;

                if(!isset($data['policiesCount'][$cityId])) {
                    $data['policiesCount'][$cityId] = 0;
                }

                $data['policiesCount'][$cityId] += $policies->count();
            }
        }

        return $data;
    }

    /**
     * @param iterable $arr
     * @param iterable $columns
     * @return iterable
     */
    private function numberFormat(iterable $arr, iterable $columns): iterable
    {
        foreach ($columns as $column) {
            $arr[$column] = number_format($arr[$column],2);
        }

        return $arr;
    }

    /**
     * @return iterable
     */
    public function getUserStatistic(): iterable
    {
        return $this->userStatistic;
    }

    /**
     * @return iterable
     */
    public function getSubAgentStatistic(): iterable
    {
        return $this->subAgentStatistic;
    }

    /**
     * @return iterable
     */
    public function getTotalStatistic(): iterable
    {
        return $this->totalStatistic;
    }

    public function getChartStatistic(): iterable
    {
        return $this->chartStatistic;
    }
}
