<?php

namespace App\Components\PolicyStatus;

class SessionMessage
{
    public const ERROR = 'error';
    public const SUCCESS = 'success';

    /**
     * @var string
     */
    private $status;

    /**
     * @var string
     */
    private $message;

    /**
     * SessionMessage constructor.
     * @param string $status
     * @param string $message
     */
    public function __construct(string $status,string $message)
    {
        $this->status = $status;
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getStatus():string
    {
        return $this->status;
    }

    public function getMessage(): string
    {
        return $this->message;
    }
}
