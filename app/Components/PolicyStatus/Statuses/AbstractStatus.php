<?php

namespace App\Components\PolicyStatus\Statuses;

use App\Components\PolicyStatus\SessionMessage;
use App\Model\Policy;

abstract class AbstractStatus implements PolicyStatusInterface
{
    /**
     * @var Policy
     */
    protected $policy;

    /**
     * @var SessionMessage
     */
    protected $sessionMessage;

    /**
     * @var bool
     */
    protected $isHasReturn = false;

    /**
     * @var array
     */
    protected $options;

    /**
     * @inheritdoc
     */
    public function setOptions(array $options): PolicyStatusInterface
    {
        $this->options = $options;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setPolicy(Policy $policy): PolicyStatusInterface
    {
       $this->policy = $policy;

       return $this;
    }

    /**
     * @inheritdoc
     */
    public function getSessionMessage(): ?SessionMessage
    {
        return $this->sessionMessage;
    }

    /**
     * @inheritdoc
     */
    public function return()
    {
        return null;
    }
}
