<?php

namespace App\Components\PolicyStatus\Statuses;

use App\Components\Kv\ChangeRateForAgent;
use App\Components\Kv\ChargeBonusKvHelper;
use App\Components\Kv\KvHelper;
use App\Components\PolicyStatus\SessionMessage;
use App\Model\Policy;
use App\Service\PolicyService\Sign\SignStaticFactory;

class SuccessStatus extends AbstractStatus
{
    /**
     * @inheritdoc
     */
    public function execute(): PolicyStatusInterface
    {
        if ($this->policy->status === 'paid') {

            $oldKv = [];

            if ($this->policy->user->parent_user_id === null) {
                foreach (Policy::$activeInsurances as $insurance) {
                    $oldKv[$insurance] = $this->policy->user->getKv($insurance);
                }
            }

            $result = SignStaticFactory::factory($this->policy->insurance)->sign($this->policy);

            if ($result !== true) {

                $this->sessionMessage = new SessionMessage(SessionMessage::ERROR,$result->getMessage());

                return $this;
            }

            $chargeBonus = new ChargeBonusKvHelper($this->policy);
            $chargeBonus->chargeBonus();

            if ($this->policy->user->parent_user_id === null) {
                foreach (Policy::$activeInsurances as $insurance) {
                    if ($oldKv[$insurance] !== null) {
                        $this->reCalcCommissionForAgentFirstLvl($oldKv[$insurance]->getModel()->getRateId(),$insurance);
                    }
                }
            }
            
            $this->sessionMessage = new SessionMessage(SessionMessage::SUCCESS,'Полис успешно подписан!');

            return $this;
        }

        $this->sessionMessage = new SessionMessage(SessionMessage::ERROR,'Полис не оплачен!');

        return $this;
    }

    /**
     * @param int $oldRateId
     * @param string $insurance
     */
    private function reCalcCommissionForAgentFirstLvl(int $oldRateId, string $insurance): void
    {
        $newKv = (new KvHelper($this->policy->user, $insurance))->getKv();

        if ($newKv !== null && $oldRateId !== $newKv->getModel()->getRateId()) {

            $changeRate = new ChangeRateForAgent($oldRateId, $newKv->getModel(),$this->policy->user);
            $changeRate->change();

        }
    }
}
