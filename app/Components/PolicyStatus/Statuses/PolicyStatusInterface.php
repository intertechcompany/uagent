<?php

namespace App\Components\PolicyStatus\Statuses;

use App\Components\PolicyStatus\SessionMessage;
use App\Model\Policy;

interface PolicyStatusInterface
{
    /**
     * @param Policy $policy
     * @return mixed
     */
    public function setPolicy(Policy $policy): PolicyStatusInterface;

    /**
     * @return PolicyStatusInterface
     */
    public function execute(): PolicyStatusInterface;

    /**
     * @return SessionMessage|null
     */
    public function getSessionMessage(): ?SessionMessage;

    /**
     * @return mixed
     */
    public function return();

    /**
     * @param array $options
     * @return PolicyStatusInterface
     */
    public function setOptions(array $options): PolicyStatusInterface;
}
