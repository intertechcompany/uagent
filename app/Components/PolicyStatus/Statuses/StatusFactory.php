<?php

namespace App\Components\PolicyStatus\Statuses;

class StatusFactory
{
    /**
     * @param string $status
     * @return PolicyStatusInterface
     */
    public static function factory(string $status): PolicyStatusInterface
    {
        if ($status === 'paid') {
            return new PaidStatus();
        }

        if ($status === 'success') {
            return new SuccessStatus();
        }

        if ($status === 'inPayment') {
            return new InPaymentStatus();
        }

        return (new DefaultStatus())->setOptions([
            'status' => $status
        ]);
    }
}
