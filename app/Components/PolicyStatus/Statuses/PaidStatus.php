<?php

namespace App\Components\PolicyStatus\Statuses;

use App\Components\PolicyStatus\SessionMessage;
use App\Helper\PolicyLogHelper;
use App\Service\PolicyService\Payment\PaymentFactory;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;

class PaidStatus extends AbstractStatus
{
    /**
     * @var string
     */
    private $paymentUrl;

    /**
     * @inheritdoc
     */
    public function execute($changeStatus = true): PolicyStatusInterface
    {
        try {
            $this->paymentUrl = PaymentFactory::factory($this->policy->insurance)
                ->create($this->policy)
                ->formUrl
            ;

            $logger = PolicyLogHelper::getInstance('policy');

            $logger->saveLog(storage_path('policies/payment/'. $this->policy->insurance .'-'. $this->policy->id . '.xml'));

            if ($this->paymentUrl !== '' && $this->paymentUrl !== null) {
                if($changeStatus) {
                    $this->policy->status = 'inPayment';
                    $this->policy->save();
                }

                return $this;
            }

            $this->sessionMessage = new SessionMessage(SessionMessage::ERROR,'Не удалось получить ссылку для оплаты!');

        } catch (\Exception $exception) {

            $logger = PolicyLogHelper::getInstance('policy');

            $logger->saveLog(storage_path('policies/fails/' . Carbon::now()->format('d-m-Y') . '/'. $this->policy->insurance .'-'. $this->policy->id . '.xml'));

            $this->sessionMessage = new SessionMessage(SessionMessage::ERROR,'Не удалось получить ссылку для оплаты!');
        }

        return $this;
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|mixed|null
     */
    public function return()
    {
        return Redirect::to($this->paymentUrl);
    }
}
