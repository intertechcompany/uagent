<?php

namespace App\Components\PolicyStatus\Statuses;

class DefaultStatus extends AbstractStatus
{
    /**
     * @inheritdoc
     */
    public function execute(): PolicyStatusInterface
    {
        $this->policy->status = $this->options['status'];
        $this->policy->save();

        return $this;
    }
}
