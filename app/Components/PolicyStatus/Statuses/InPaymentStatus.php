<?php

namespace App\Components\PolicyStatus\Statuses;

use App\Components\PolicyStatus\SessionMessage;
use App\Helper\PolicyLogHelper;
use App\Service\PolicyService\Payment\PaymentException;
use App\Service\PolicyService\Payment\PaymentFactory;

class InPaymentStatus extends AbstractStatus
{
    /**
     * @inheritdoc
     */
    public function execute(): PolicyStatusInterface
    {
        try {
            if (PaymentFactory::factory($this->policy->insurance)->checkPayment($this->policy) === true) {
                $this->policy->status = 'paid';

                $this->policy->save();

                $this->sessionMessage = new SessionMessage(SessionMessage::SUCCESS,'Оплата прошла успешно!');

            } else {
                $this->sessionMessage = new SessionMessage(SessionMessage::ERROR,'Оплата не прошла успешно!');
            }

        } catch (PaymentException $exception) {
            $this->sessionMessage = new SessionMessage(SessionMessage::ERROR,$exception->getMessage());
        }

        $logger = PolicyLogHelper::getInstance('policy');

        $logger->saveLog(storage_path('policies/payment/'. $this->policy->insurance .'-' . $this->policy->id . '.xml'));


        return $this;
    }
}
