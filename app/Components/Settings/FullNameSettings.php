<?php

namespace App\Components\Settings;

use App\User;

class FullNameSettings extends AbstractSettings
{
    /**
     * Save Full Name Attr
     */
    public function saveSetting(): SettingsInterface
    {
        $fullName = explode(' ', trim($this->value));
        $lastName = $patronymic = null;

        if (\count($fullName) > 2) {
            [$firstName, $lastName,$patronymic] = $fullName;
        } elseif(\count($fullName) > 1) {
            [$firstName, $lastName] = $fullName;
        } else {
            $firstName = $this->value;
        }

        $this->user->name = $firstName;
        $this->user->last_name = $lastName;
        $this->user->patronymic = $patronymic;

        if ($this->user->role === User::ROLE_GUEST) {
            $this->user->role = User::ROLE_AGENT;
        }

        return $this;
    }
}