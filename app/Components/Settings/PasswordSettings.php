<?php

namespace App\Components\Settings;

use Illuminate\Support\Facades\Hash;

class PasswordSettings extends AbstractSettings
{
    public function saveSetting(): SettingsInterface
    {
        $this->user->password = Hash::make($this->value);

        return $this;
    }
}
