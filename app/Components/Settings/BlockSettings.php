<?php

namespace App\Components\Settings;

use App\Model\UserDashboardBlock;

class BlockSettings extends AbstractSettings
{
    /**
     * @return SettingsInterface
     */
    public function saveSetting(): SettingsInterface
    {
        $userBlocks = $this->user->dashboardBlocks()->get()->keyBy('key');

        foreach ($this->value as $key => $block) {
            $blockUpdate = $userBlocks[$key] ?? new UserDashboardBlock();
            $blockUpdate->user_id = $this->user->id;
            $blockUpdate->key = $key;
            $blockUpdate->is_enabled = (int)$block;
            $blockUpdate->save();
        }

        return $this;
    }
}
