<?php

namespace App\Components;

use App\Model\UserDashboardBlock;
use App\User;

class SettingsComponent
{
    /**
     * @var User
     */
    private $user;

    /**
     * SettingsComponent constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @param array $blocks
     */
    public function saveBlocks(array $blocks): void
    {
        $userBlocks = $this->user->dashboardBlocks()->get()->keyBy('key');

        foreach ($blocks as $key => $block) {
            $blockUpdate = $userBlocks[$key] ?? new UserDashboardBlock();
            $blockUpdate->user_id = $this->user->id;
            $blockUpdate->key = $key;
            $blockUpdate->is_enabled = (int)$block;
            $blockUpdate->save();
        }
    }
}
