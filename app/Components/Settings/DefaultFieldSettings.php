<?php

namespace App\Components\Settings;

use App\Model\UserInformation;

class DefaultFieldSettings extends AbstractSettings
{
    /**
     * @return SettingsInterface
     */
    public function saveSetting(): SettingsInterface
    {
        $settingName = $this->options['name'];

        if (\in_array($settingName,UserInformation::$fields,true)) {
            $this->user->information->$settingName = $this->value;
            return $this;
        }

        $this->user->$settingName = $this->value;

        return $this;
    }
}
