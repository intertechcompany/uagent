<?php

namespace App\Components\Settings;

final class SettingsFactory
{
    /**
     * @param string $name
     * @return SettingsInterface
     */
    public static function factory(string $name): SettingsInterface
    {
        switch ($name) {
            case 'birthday':
                return new BirthdaySettings();
            case 'blocks':
                return new BlockSettings();
            case 'full_name':
                return new FullNameSettings();
            case 'avatar':
                return new AvatarSettings();
            case 'password':
                return new PasswordSettings();
            default:
                return (new DefaultFieldSettings())->setOptions(['name' => $name]);
        }
    }
}
