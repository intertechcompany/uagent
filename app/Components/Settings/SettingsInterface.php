<?php

namespace App\Components\Settings;

use App\User;
use Illuminate\Contracts\Validation\Validator;

interface SettingsInterface
{
    /**
     * @param User $user
     * @return SettingsInterface
     */
    public function setUser(User $user): SettingsInterface;

    /**
     * @param $value
     * @return SettingsInterface
     */
    public function setValue($value): SettingsInterface;

    /**
     * @return SettingsInterface
     */
    public function saveSetting(): SettingsInterface;

    /**
     * @return User
     */
    public function getUser(): User;

    /**
     * @param array $options
     * @return SettingsInterface
     */
    public function setOptions(array $options): SettingsInterface;

    /**
     * @return Validator
     */
    public function validate(): Validator;
}
