<?php
/**
 * Created by PhpStorm.
 * User: mr_evrey
 * Date: 1/17/19
 * Time: 4:40 PM
 */

namespace App\Components\Settings;


class AvatarSettings extends AbstractSettings
{
    public function saveSetting(): SettingsInterface
    {
        $avatarName = $this->user->id . '_avatar' . time() . '.' . $this->value->getClientOriginalExtension();

        if ($this->value->storeAs('avatar', $avatarName)) {
            $this->user->information->avatar = $avatarName;
        }

        return $this;
    }
}
