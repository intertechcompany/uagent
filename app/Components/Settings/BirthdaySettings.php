<?php

namespace App\Components\Settings;

use Carbon\Carbon;

class BirthdaySettings extends AbstractSettings
{
    public function saveSetting(): SettingsInterface
    {
        $this->user->information->birthday = Carbon::createFromFormat('d-m-Y',$this->value)->format('Y-m-d');

        return $this;
    }
}
