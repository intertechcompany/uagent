<?php

namespace App\Repositories\Chat;

use App\Model\Chat;
use App\Model\Message;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DbChatRepository implements ChatRepositoryInterface {

    private $request;

    public function __construct()
    {
        $this->request = app(Request::class);
    }

    public function createChat()
    {
        $chat = new Chat();
        $chat->subject = $this->request->get('subject');
        $chat->user_id = auth()->user()->id;
        $chat->save();

        $this->storeMessage($chat);

        return $chat;
    }

    public function changeStatus($chat)
    {
        $chat->status = $this->request->get('status');
        $chat->save();

        return $chat;
    }

    public function markIsReadMessages($chat)
    {
        $user = auth()->user();

        $chat->messages()
            ->where('is_read', 0)
            ->where(function ($query) use($user) {
                if($user->role == User::ROLE_ADMIN) {
                    $query->whereHas('user', function ($q) use ($user) {
                        $q->where('id', '<>', $user->id)->where('role', '<>', User::ROLE_ADMIN);
                    });
                } else {
                    $query->where('user_id', '<>', $user->id);
                }
            })
            ->update([
                'is_read' => 1
            ]);

        return $chat;
    }

    public function storeMessage($chat)
    {
        $chat->messages()->save(
            new Message(['message' => $this->request->get('message'), 'user_id' => auth()->user()->id])
        );

        return $chat;
    }

    public function getMessages($chat)
    {
        return $chat->load(['user','messages.user']);
    }

    public function getUsersChat()
    {
        return auth()->user()->chats()
            ->select('chats.*')
            ->selectRaw("(SELECT m.created_at from messages as m where m.chat_id = chats.id order by m.created_at desc limit 0,1) as last_date")
            ->with('unreadMessages')
            ->orderBy('status')
            ->orderBy('last_date', 'desc')
            ->paginate($this->request->get('perPage'));
    }

    public function getAll()
    {
        return Chat::query()
            ->select('chats.*')
            ->selectRaw("(SELECT m.created_at from messages as m where m.chat_id = chats.id order by m.created_at desc limit 0,1) as last_date")
            ->with(['unreadMessages', 'user'])
            ->orderBy('status')
            ->orderBy('last_date', 'desc')
            ->paginate($this->request->get('perPage'));
    }
}