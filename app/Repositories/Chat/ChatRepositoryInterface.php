<?php

namespace App\Repositories\Chat;

interface ChatRepositoryInterface {

    public function createChat();

    public function changeStatus($chat);

    public function markIsReadMessages($chat);

    public function storeMessage($chat);

    public function getMessages($chat);

    public function getUsersChat();

    public function getAll();

}