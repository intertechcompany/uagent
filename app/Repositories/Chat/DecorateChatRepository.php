<?php

namespace App\Repositories\Chat;

use App\Model\Chat;
use App\Model\Message;
use App\User;

class DecorateChatRepository implements ChatRepositoryInterface {

    protected $chatRepository;

    public function __construct(ChatRepositoryInterface $chatRepository)
    {
        $this->chatRepository = $chatRepository;
    }

    public function createChat()
    {
        return $this->chatRepository->createChat();
    }

    public function changeStatus($chat)
    {
        return $this->chatRepository->changeStatus($chat);
    }

    public function markIsReadMessages($chat)
    {
        return $this->chatRepository->markIsReadMessages($chat);
    }

    public function storeMessage($chat)
    {
        return $this->chatRepository->storeMessage($chat);
    }

    public function getMessages($chat)
    {
        return $this->chatRepository->getMessages($chat);
    }

    public function getUsersChat()
    {
        return $this->chatRepository->getUsersChat();
    }

    public function getAll()
    {
        return $this->chatRepository->getAll();
    }

    public function getMessagesDecorate($chat)
    {
        $this->getMessages($chat);

        $_chat = new \stdClass();
        $_chat->id = $chat->id;
        $_chat->titleQuestions = $chat->subject;
        $_chat->numberOfMessages = $chat->messages->count();

        $_chat->messages = $chat->messages->map(function(Message $message) use($chat) {

            /*if(auth()->user()->role != User::ROLE_ADMIN) {
                $someone = $chat->user_id == $message->user_id;
            } else {
                $someone = $chat->user_id != $message->user_id;
            }*/

            return [
                'messageSomeone' =>  $someone = $chat->user_id == $message->user_id,
                'userAvatar' => $message->user->getAvatar(),
                'userName' => $message->user->role == User::ROLE_ADMIN ? 'Тех поддержка uagent' : $message->user->name,
                'date' => $message->created_at->format('H:i d.m.Y'),
                'messageText' => $message->message,
            ];
        });

        return $_chat;
    }

    public function getUsersChatDecorate()
    {
        $chats = $this->getUsersChat();

        return $chats->setCollection($chats->getCollection()->each(function ($item) {
            $item->unread = $item->unreadMessages->count();
            $item->dt = $item->last_date->format('d.m.Y H:i:s');
            $item->url = route('support.chat', ['id'=>$item->id]);

            unset($item->messages);
            return $item;
        }));
    }

    public function getAllDecorate()
    {
        $chats = $this->getAll();

        return $chats->setCollection($chats->getCollection()->each(function ($item) {
            $item->unread = $item->unreadMessages->count();
            $item->dt = $item->last_date->format('d.m.Y H:i:s');
            $item->url = route('admin.support.chat', ['id'=>$item->id]);
            $item->name = $item->user->name;
            $item->status = $item->status;

            unset($item->messages);
            return $item;
        }));
    }
}