<?php

namespace App\Repositories\Chat;

use App\Model\Chat;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;

class BackendServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ChatRepositoryInterface::class, function() {
            $chatRepository = new DbChatRepository();
            $decorateRepository = new DecorateChatRepository($chatRepository);
            return $decorateRepository;
        });
    }
}
