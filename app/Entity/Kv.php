<?php

namespace App\Entity;

use App\Model\Commission;
use App\Model\Rate;
use App\Model\RateCity;
use App\Model\RateInterface;

class Kv
{
    /**
     * @var string
     */
    private $type;

    /**
     * @var double
     */
    private $value;

    /**
     * @var float
     */
    private $parentValue;

    /**
     * @var mixed
     */
    private $model;

    /**
     * Kv constructor.
     * @param $model
     * @param string|null $type
     * @param float|null $value
     * @param null $parentValue
     */
    public function __construct($model,string $type = null,float $value = null,$parentValue = null)
    {
        $this->type = $type;
        $this->value = $value;
        $this->parentValue = $parentValue;
        $this->model = $model;
    }

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @return float
     */
    public function getValue(): ?float
    {
        return $this->value;
    }

    /**
     * @return float|null
     */
    public function getParentValue(): ?float
    {
        return $this->parentValue;
    }

    /**
     * @return Commission|Rate|RateCity|RateInterface
     */
    public function getModel()
    {
        return $this->model;
    }
}
