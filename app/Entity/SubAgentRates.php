<?php

namespace App\Entity;

use App\Model\RateInterface;

class SubAgentRates
{
    /**
     * @var array|RateInterface[]
     */
    private $cityRates = [];

    /**
     * @var array|RateInterface[]
     */
    private $rates = [];

    /**
     * @return bool
     */
    public function isHaveRates(): bool
    {
        return \count($this->cityRates) > 0 || \count($this->rates) > 0;
    }

    /**
     * @param string $insurance
     * @param RateInterface $rate
     * @return SubAgentRates
     */
    public function addCityRate(string $insurance, RateInterface $rate): self
    {
        $this->cityRates[$insurance][] = $rate;

        return $this;
    }

    /**
     * @param string $insurance
     * @param RateInterface $rate
     * @return SubAgentRates
     */
    public function addRate(string $insurance, RateInterface $rate): self
    {
        $this->rates[$insurance][] = $rate;

        return $this;
    }

    /**
     * @param string $insurance
     * @return iterable|null
     */
    public function getCityRateByInsurance(string $insurance): ?iterable
    {
        if (isset($this->cityRates[$insurance]) && \count($this->cityRates[$insurance]) > 0) {
            return $this->cityRates[$insurance];
        }

        return null;
    }

    /**
     * @param string $insurance
     * @return iterable|null
     */
    public function getRateByInsurance(string $insurance): ?iterable
    {
        if (isset($this->rates[$insurance]) && \count($this->rates[$insurance]) > 0) {
            return $this->rates[$insurance];
        }

        return null;
    }

    /**
     * @param string $insurance
     * @return bool
     */
    public function isHaveOneOfRatesByInsurance(string $insurance): bool
    {
        return $this->isHaveCityRatesByInsurance($insurance) || $this->isHaveRatesByInsurance($insurance);
    }

    /**
     * @param string $insurance
     * @return bool
     */
    public function isHaveCityRatesByInsurance(string $insurance): bool
    {
        return isset($this->cityRates[$insurance]);
    }

    /**
     * @param string $insurance
     * @return bool
     */
    public function isHaveRatesByInsurance(string $insurance): bool
    {
        return isset($this->rates[$insurance]);
    }
}
