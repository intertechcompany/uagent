<?php

namespace App\Traits;

use App\Model\Purpose;
use App\Model\Type;
use App\Model\VehicleModel;
use Carbon\Carbon;

trait AlphaTrait
{
	private $type;
	private $purpose;
	private $category;
	private $car;
	private $vehicleType;
	private $regNumberIsset;
    /**
     * @var Carbon
     */
    private $startDate;

	public function contractData(array $data,$upid,$document,$loadVehicle,$insurer,$owner,$drivers): array 
	{
		$insurerRow = array_get($data,'insurer');

		$regionInsurer = array_get($insurerRow,'address.data.region_with_type');
		$cityInsurer = array_get($insurerRow,'address.data.city_with_type') ?? array_get($insurerRow,'address.data.settlement_with_type');
		$streetInsurer = array_get($insurerRow,'address.data.street');

		$ownerRow = array_get($data,'owner');

		$regionOwner = array_get($ownerRow,'address.data.region_with_type');
		$cityOwner = array_get($ownerRow,'address.data.city_with_type') ?? array_get($ownerRow,'address.data.settlement_with_type');
        $streetOwner = array_get($ownerRow,'address.data.street');

		$dataForRequest =  [
			'PartnerInfo' => [
				'UPID' => $upid,
			],
			'LoadContractRequest' => [
				'DateRevision' => Carbon::now()->format('Y-m-d\Th:i:s'),
				'DateActionBeg' => $this->startDate->format('Y-m-d') . 'T00:00:00',
				'DateActionEnd' => $this->startDate->addYear()->addDays('-1')->format('Y-m-d') . 'T00:00:00',
				'DriversRestriction' => array_get($data,'driver_count') === 'specified',
				'CalcRef' => array_get($document,'RefCalcId'),
				'IsTransCar' => false,
				'IsTrailerAllowed' => array_get($data,'is_vehicle_use_trailer') === false ? 0 : 1,
				'Car' => [
					'IDCheckTS' => array_get($loadVehicle,'IDCheckTS'),
					'CountryCar' => 1,
					'CarIdent' => [
                        $this->vehicleType =>  array_get($data,'vehicle_id'),
					],
					'Mark' => $this->car->make->name,
					'Model' => $this->car->name,
					'YearIssue' => array_get($data,'vehicle_registration_year'),
					'DocumentCar' => array_get($data,'vehicle_document_type') === 'sts' ? 31 : 30,
					'DocCarSerial' => array_get($data,'vehicle_document_serial'),
					'DocCarNumber' => array_get($data,'vehicle_document_number'),
					'DocumentCarDate' => Carbon::createFromFormat('d-m-Y',array_get($data,'vehicle_document_date'))->format('Y-m-d'),
					'EngCap' => array_get($data,'power'),
					'Purpose' => $this->purpose,
				],
				'Insurer' => [
					'PhysicalPerson' => [
						'IDCheckInsurerOwner' => array_get($insurer,'IDCheckInsurerOwner'),
						'CountryCode' => 643,
						'CountryName' => 'Россия',
						'Zip' => array_get($insurerRow,'postal_code'),
						'House' => array_get($insurerRow,'house'),
						'Building' => array_get($insurerRow,'address2'),
						'Apartment' => array_get($insurerRow,'address3'),
						'State' => $regionInsurer,
						'City' => $cityInsurer,
						'Surname' => array_get($insurerRow,'last_name'),
						'Name' => array_get($insurerRow,'first_name'),
						'Patronymic' => array_get($insurerRow,'middle_name'),
						'BirthDate' => Carbon::createFromFormat('d-m-Y',array_get($insurerRow,'dob'))->format('Y-m-d'),
						'Email' => array_get($data,'email'),
						'Phone' => preg_replace("/[^0-9]/", '', array_get($data,'phone')),
						'PersonDocument' => [
							'DocPerson' => 12,
							'Number' => array_get($insurerRow,'personal_document_number'),
							'Serial' => array_get($insurerRow,'personal_document_serial'),
						]
					]
				],
				'Owner' => [
					'PhysicalPerson' => [
						'IDCheckInsurerOwner' => array_get($owner,'IDCheckInsurerOwner'),
						'CountryCode' => 643,
						'CountryName' => 'Россия',
						'Zip' => array_get($ownerRow,'postal_code'),
						'House' => array_get($ownerRow,'house'),
						'Building' => array_get($ownerRow,'address2'),
						'Apartment' => array_get($ownerRow,'address3'),
						'State' => $regionOwner,
						'City' => $cityOwner,
						'Surname' => array_get($ownerRow,'last_name'),
						'Name' => array_get($ownerRow,'first_name'),
						'Patronymic' => array_get($ownerRow,'middle_name'),
						'BirthDate' => Carbon::createFromFormat('d-m-Y',array_get($ownerRow,'dob'))->format('Y-m-d'),
						'PersonDocument' => [
							'DocPerson' => 12,
							'Number' => array_get($ownerRow,'personal_document_number'),
							'Serial' => array_get($ownerRow,'personal_document_serial'),
						]
					]
				],
				'AgentBlock' => [
					'AgentContractId' => 815927,
					'ManagerId' => 185890784,
					'DepartmentId' => 53225815,
					'ChannelSaleId' => 20071,
				]
			]
		];

		if ($streetInsurer !== null) {
            $dataForRequest['LoadContractRequest']['Insurer']['PhysicalPerson']['Street'] = $streetInsurer;
        }

        if ($streetOwner !== null) {
            $dataForRequest['LoadContractRequest']['Owner']['PhysicalPerson']['Street'] = $streetOwner;
        }

        if ($this->regNumberIsset) {
            $dataForRequest['LoadContractRequest']['Car']['LicensePlate'] = array_get($data,'vehicle_plate_number') . array_get($data,'vehicle_plate_region');
        }

		if (!empty($data['diagnostic_until_date'])) {
			$dataForRequest['LoadContractRequest']['Car']['TicketCar'] = 53;
			$dataForRequest['LoadContractRequest']['Car']['TicketCarNumber'] = array_get($data,'diagnostic_number');
			$dataForRequest['LoadContractRequest']['Car']['TicketCarYear'] = Carbon::createFromFormat('d-m-Y',array_get($data,'diagnostic_until_date'))->format('Y');
			$dataForRequest['LoadContractRequest']['Car']['TicketCarMonth'] =  Carbon::createFromFormat('d-m-Y',array_get($data,'diagnostic_until_date'))->format('n');
			$dataForRequest['LoadContractRequest']['Car']['TicketDiagnosticDate'] = Carbon::createFromFormat('d-m-Y',array_get($data,'diagnostic_until_date'))->format('Y-m-d');
		}

		if ($data['driver_count'] !== 'unlimited') {
			foreach ($drivers as $driver) {
				$dataForRequest['LoadContractRequest']['Drivers']['Driver'][] = [
					'IDCheckDriver' => array_get($driver,'IDCheckDriver.IDCheckDriver'),
					'CountryCode' => 643,
					'CountryName' => 'Россия',
					'CategoriesDriverLicense' => [
						'CatDriverLicense' => 2531,
					],
					'Surname' => array_get($driver,'last_name'),
					'Name' => array_get($driver,'first_name'),
					'Patronymic' => array_get($driver,'middle_name'),
					'BirthDate' => Carbon::createFromFormat('d-m-Y',array_get($driver,'dob'))->format('Y-m-d'),
					'DriverDocDate' => Carbon::createFromFormat('d-m-Y',array_get($driver,'start_date'))->format('Y-m-d'),
					'DriverDocument' => [
						'Number' => array_get($driver,'document_number'),
						'Serial' => array_get($driver,'document_serial'),
						'DateIssue' => Carbon::createFromFormat('d-m-Y',array_get($driver,'start_date'))->format('Y-m-d'),
					]
				];
			}
		}

		return $dataForRequest;
	}

    public function formatPolicyStart(string $date): Carbon
    {
        $now = Carbon::now()->startOfDay()->timestamp;
        $dateCarbon = new Carbon($date);

        if ($now === $dateCarbon->startOfDay()->timestamp) {
            return now()->addDay()->startOfDay();
        }

        return new Carbon($date);
    }

	protected function generateData(array $data,$upid): array
	{
        $typeModel = Type::query()->findOrFail(array_get($data,'type'));
        $purposeModel = Purpose::query()->findOrFail(array_get($data,'purpose'));
        $this->vehicleType = $this->getVehicleType(array_get($data,'vehicle_id_type'));

		$this->type = $typeModel->name;
		$this->purpose = $purposeModel->name;
		$this->category = $typeModel->category->name;

		$this->startDate = !empty(array_get($data,'policy_start_date')) ? $this->formatPolicyStart(array_get($data,'policy_start_date')) : now()->addDay();

		$region = array_get($data,'owner.address.data.region_with_type');

		$city = array_get($data,'owner.address.data.city_with_type') ?? array_get($data,'owner.address.data.settlement_with_type');

		$this->car = VehicleModel::query()->find(array_get($data,'model_id'));

		$this->regNumberIsset = array_get($data,'is_vehicle_plate_absent') !== true;

		$array = [
			'PartnerInfo' => [
				'UPID' => $upid,
			],
			'ContractType' => 'Первоначальный',
			'PolicyBeginDate' => $this->startDate->format('Y-m-d\Th:i:s'),
			'InsuranceMonth' => 12,
			'RegionOfUse' => $region,
			'CityOfUse' => $city,
			'DriversRestriction' => array_get($data,'driver_count') === 'specified',
			'FollowToRegistration' => false, // TODO: check
			'Transport' => [
				'Mark' => $this->car->make->name,
				'Model' => $this->car->name,
				'Category' => $this->category,
				'Type' => $this->type,
				'RegistrationRussia' => 'true',
				'Power' => array_get($data,'power'),
                 $this->vehicleType => array_get($data,'vehicle_id'),
				'Purpose' => $this->purpose,
				'IsTaxi' => false,
				'WithUseTrailer' => array_get($data,'is_vehicle_use_trailer'),
				'YearIssue' => array_get($data,'vehicle_registration_year'),
			],
			'TransportInsurer' => [
				'LastName' => array_get($data,'insurer.last_name'),
				'FirstName' => array_get($data,'insurer.first_name'),
				'MiddleName' => array_get($data,'insurer.middle_name'),
				'BirthDate' => Carbon::createFromFormat('d-m-Y',array_get($data,'insurer.dob'))->format('Y-m-d'),
				'email' => array_get($data,'email'),
				'PersonDocument' => [
					'Number' => array_get($data,'insurer.personal_document_number'),
					'Seria' => array_get($data,'insurer.personal_document_serial')
				]
			],
			'TransportOwner' => [
				'LastName' => array_get($data,'owner.last_name'),
				'FirstName' => array_get($data,'owner.first_name'),
				'MiddleName' => array_get($data,'owner.middle_name'),
				'BirthDate' => Carbon::createFromFormat('d-m-Y',array_get($data,'owner.dob'))->format('Y-m-d'),
				'email' => array_get($data,'email'),
				'PersonDocument' => [
					'Number' => array_get($data,'owner.personal_document_number'),
					'Seria' => array_get($data,'owner.personal_document_serial')
				],
			],
			'UnwantedClient' => false,
		];

		if (\count($data['drivers']) > 0 && array_get($data,'driver_count') !== 'unlimited') {
			$array['Drivers']['Driver'] = $this->generateDrivers($data['drivers']);
		}

		if ($this->regNumberIsset) {
            $array['Transport']['LicensePlate'] = array_get($data,'vehicle_plate_number') . array_get($data,'vehicle_plate_region');
        }

		return $array;
	}

	protected function generateDriverForCheck(array $driver): array
	{
		return [
			'DriverRequestValue' => [
				'DriverInfoRequest' => [
					'Surname' => array_get($driver,'last_name'),
					'Name' => array_get($driver,'first_name'),
					'Patronymic' => array_get($driver,'middle_name'),
					'BirthDate' => Carbon::createFromFormat('d-m-Y',array_get($driver,'dob'))->format('Y-m-d'),
					'DriverDocDate' => Carbon::createFromFormat('d-m-Y',array_get($driver,'start_date'))->format('Y-m-d'),
					'DriverDocument' => [
						'Number' => array_get($driver,'document_number'),
						'Serial' => array_get($driver,'document_serial')
					],
				],
				'DateRequest' => Carbon::now()->format('Y-m-d\Th:i:s')
			]
		];
	}

	protected function vechicleData(array $data): array
	{
        $arr =  [
			'TSRequestValue' => [
				'TSInfoRequest' => [
					'CountryCar' => 1,
					'CarIdent' => [
                        $this->vehicleType =>  array_get($data,'vehicle_id'),
					],
					'Mark' => $this->car->make->name,
					'Model' => $this->car->name,
					'YearIssue' => array_get($data,'vehicle_registration_year'),
					'DocumentCar' => array_get($data,'vehicle_document_type') === 'sts' ? 31 : 30,
					'DocCarSerial' => array_get($data,'vehicle_document_serial'),
					'DocCarNumber' => array_get($data,'vehicle_document_number'),
					'DocumentCarDate' => Carbon::createFromFormat('d-m-Y',array_get($data,'vehicle_document_date'))->format('Y-m-d'),
					'EngCap' => array_get($data,'power'),
				],
				'DateRequest' => Carbon::now()->format('Y-m-d\Th:i:s')
			]
		];

        if ($this->regNumberIsset) {
            $arr['TSRequestValue']['TSInfoRequest']['CarIdent']['LicensePlate'] = array_get($data,'vehicle_plate_number') . array_get($data,'vehicle_plate_region');
        }

        return $arr;
	}

	protected function insurerOwnerData(array $data,$type): array
	{
		$data = array_get($data,$type);
		$street = array_get($data,'address.data.street');

        $compactedData =  [
			'InsurerOwnerRequestValue' => [
				'CheckedPerson' => ucfirst($type),
				'PhysicalPersonInfoRequest' => [
					'Country' => 643,
					'Zip' =>  array_get($data,'postal_code'),
					'State' =>  array_get($data,'address.data.region_with_type'),
					'City' =>  array_get($data,'address.data.city_with_type') ?? array_get($data,'address.data.settlement_with_type'),
					'Surname' => array_get($data,'last_name'),
					'Building' => array_get($data,'address2'),
					'Apartment' => array_get($data,'address3'),
					'Name' => array_get($data,'first_name'),
					'Patronymic' => array_get($data,'middle_name'),
					'BirthDate' => Carbon::createFromFormat('d-m-Y',array_get($data,'dob'))->format('Y-m-d'),
					'PersonDocument' => [
						'DocPerson' => 12,
						'Number' => array_get($data,'personal_document_number'),
						'Serial' => array_get($data,'personal_document_serial'),
					],
				],
				'DateRequest' => Carbon::now()->format('Y-m-d\Th:i:s')
			]
		];

        if ($street !== null) {
            $compactedData['InsurerOwnerRequestValue']['PhysicalPersonInfoRequest']['Street'] = $street;
        }

		return $compactedData;
	}

	private function generateDrivers(array $drivers): array
	{
		$result = [];
		foreach ($drivers as $driver) {
			$result[] = [
				'LastName' => array_get($driver,'last_name'),
				'FirstName' => array_get($driver,'first_name'),
				'MiddleName' => array_get($driver,'middle_name'),
				'BirthDate' => Carbon::createFromFormat('d-m-Y',array_get($driver,'dob'))->format('Y-m-d'),
				'ExperienceDate' => Carbon::createFromFormat('d-m-Y',array_get($driver,'start_date'))->format('Y-m-d'),
				'DriverDocument' => [
					'Number' => array_get($driver,'document_number'),
					'Seria' => array_get($driver,'document_serial')
				]
			];
		}

		return $result;
	}

	private function getVehicleType(string $vehicleType): string
    {
        if ($vehicleType === 'body_number') {
            return 'BodyNumber';
        }

        if ($vehicleType === 'chassis_number') {
            return 'ChassisNumber';
        }

        return 'VIN';
    }
}
