<?php

namespace App\Traits;

use App\Helper\DiagnosticCardPeriod;
use App\Service\PolicyService\Client\Kias\Client;
use Carbon\Carbon;

trait KiasNewTrait
{
    protected $docsForSubject;
    protected $driverStart;

    public function getInsurer($data)
    {
        if ((int)array_get($data,'insurer_and_owner_same') === 1){
            return array_get($data,'owner');
        }

        return array_get($data,'insurer');
    }

    public function formatDate(string $date): string
    {
        return (new Carbon($date))->format('d.m.Y') . ' 00:00:00';
    }

    public function formatPolicyStart(string $date): Carbon
    {
        $now = Carbon::now()->startOfDay()->timestamp;
        $dateCarbon = new Carbon($date);

        if ($now === $dateCarbon->startOfDay()->timestamp) {
            return now()->addDay()->startOfDay();
        }

        return new Carbon($date);
    }

    public function getSubject(&$subjects)
    {
        return array_get(array_first($subjects),'ISN');
    }

    public function formatEndDate(string $date): string
    {
        return (new Carbon($date))->addYear()->addDays(-1)->format('d.m.Y') . ' 00:00:00';
    }

    public function formatEndDateDiagnostic(string $date): string
    {
        $year = (int) (new Carbon($date))->format('Y');

        $addYears = DiagnosticCardPeriod::getPeriod($year);

        return (new Carbon($date))->subYear($addYears)->addDays(-1)->format('d.m.Y') . ' 00:00:00';
    }

    public function saveDrivers(&$drivers,Client $client): void
    {
        foreach ($drivers as &$driver) {
            $shortName = $driver['last_name'] . ' ' . mb_substr($driver['first_name'],0,1) . ' ' . mb_substr($driver['middle_name'],0,1);

            $fullName = $driver['last_name'] . ' ' . $driver['first_name'] . ' ' . $driver['middle_name'];

            $dob = $this->formatDate($driver['dob']);

            $subject = $client->subjectSearch($fullName,$dob);

            $subjectISN = $this->getSubject($subject);

            $dataSubject = [
                'CLASSISN' => 2085,
                'SUBJFULLNAME' => $fullName,
                'FIRSTNAME' => $driver['first_name'],
                'LASTNAME' => $driver['last_name'],
                'PARENTNAME' => $driver['middle_name'],
                'SUBJSHORTNAME' => $shortName,
                'SHORTNAME' => $shortName,
                'BIRTHDAY' => $dob,
                'JRESIDENT' => 'Y',
                'JJURIDICAL' => 'N',
                'ISCURRENT' => 1,
                'JURIDICAL' => 'N',
                'RESIDENT' => 'Y',
                'DRIVINGDATEBEG' => $this->driverStart[$fullName],
                'SEX' => (int)array_get($driver,'sex') === 1 ? 'M' : 'W',
                'SUBJECTDOCS' => $this->docsForSubject[$fullName]
            ];

            if (!empty($subjectISN)) {
                $dataSubject['ISN'] = $subjectISN;
            }

            $subject = $client->saveSubject($dataSubject);

            $driver['SubjISN'] = $subjectISN ?? array_get($subject,'ISN');
        }
    }

    public function generateRole(array $drivers,array &$role,bool $add = false): void
    {
        $index = \count($role);
        $driversRow = [];

        if ($add === true) {
            foreach ($drivers as $driver) {
                $driversRow[$index] = [
                    'SubjISN' => $driver['SubjISN'],
                    'ClassISN' => '2085',
                    'ClassName' => 'Лицо допущенное к управлению',
                    'SUBJFULLNAME' => $driver['last_name'] . ' ' . $driver['first_name'] . ' ' . $driver['middle_name'],
                    'SUBJSHORTNAME' => $driver['last_name'] . ' ' . mb_substr($driver['first_name'],0,1) . ' ' . mb_substr($driver['middle_name'],0,1),
                    'SUBJBIRTHDAY' => $this->formatDate($driver['dob']),
                    'SUBJDRIVINGDATEBEG' => $this->formatDate($driver['start_date']),
                    'SUBJRESIDENT' => 'Y',
                    'SUBJJURIDICAL' => 'N',
                    'ISCURRENT' => 1
                ];
                $index++;
            }
        }
        $role[] = [
            'ClassISN' => '2077',
            'ClassName' => 'Агент',
            'SubjISN' => '89240',
            'SUBJJURIDICAL' => 'Y',
            'ISCURRENT' => 'True',
        ];

        $insurerISN = $ownerISN = $this->saveSubject($this->client,array_get($this->data,'owner'),self::CLASS_ISN_OWNER);

        if ((int)array_get($this->data,'insurer_and_owner_same') !== 1) {
            $insurerISN = $this->saveSubject($this->client,array_get($this->data,'insurer'),self::CLASS_ISN_INSURER);
        }

        $role[] = [
            'ClassISN' => self::CLASS_ISN_OWNER,
            'SubjISN'  => $ownerISN,
        ];

        $role[] = [
            'ClassISN' => self::CLASS_ISN_INSURER,
            'SubjISN'  => $insurerISN,
        ];

        $role = array_merge($role,$driversRow);
    }

    public function getVehicleType(string $vehicleType): string
    {
        if ($vehicleType === 'body_number') {
            return 'BODYID';
        }

        if ($vehicleType === 'chassis_number') {
            return 'CHASSISID';
        }

        return 'VIN';
    }

    public function findMakes(array $makes,$originMake)
    {
        $data = [];
        foreach ($makes as $make) {
            if (array_get($make, 'FULLNAME') === $originMake) {
                $data[] = $make;
            }
        }
        return $data;
    }

    public function saveSubject(Client $client,$data,int $roleID): string
    {
        $humanAddress = array_get($data,'address.data');
        $humanFullName = array_get($data,'last_name') . ' ' . array_get($data,'first_name') . ' ' . array_get($data,'middle_name');

        $city = $client->getCityList(null,$humanAddress['city_kladr_id'] ?? $humanAddress['settlement_kladr_id']);

        $streetKladrID = array_get($humanAddress,'street_kladr_id');

        $street = $streetKladrID !== null ?
            $client->getStreetList(array_get($city,'ISN'),array_get($humanAddress,'street'),$streetKladrID) :
            null
        ;

        $postalCode = array_get($data,'postal_code');

        $subject = $client->subjectSearch($humanFullName,$this->formatDate(array_get($data,'dob')));

        $subjectISN = $this->getSubject($subject);

        $shortNameInsurer = $client->makeShortNameAndSex($humanFullName);

        $dataSubject = [
            'FULLNAME' => $humanFullName,
            'SHORTNAME' =>  array_get($shortNameInsurer,'ShortName'),
            'FIRSTNAME' => array_get($data,'first_name') ,
            'PARENTNAME' => array_get($data,'middle_name'),
            'LASTNAME' =>  array_get($data,'last_name'),
            'JURIDICAL' => 'N', // TODO: CHECK
            'CLASSISN' => $roleID, // TODO: CHECK
            'RESIDENT' => 'Y', // TODO: CHECK
            'BIRTHDAY' => $this->formatDate(array_get($data,'dob')),
            'DOCCLASSISN' => self::DOC_CLASS_ISN_RF,
            'DOCSER' => array_get($data,'personal_document_serial'),
            'DOCNO' => array_get($data,'personal_document_number'),
            'DOCDATE' => $this->formatDate(array_get($data,'personal_document_date')),
            'COUNTRYISN1' => 9602,
            'CITYISN1' => array_get($city,'ISN'),
            'HOUSE1' => array_get($data,'house'),
            'BUILDING1' => array_get($data,'address2'),
            'FLAT1' => array_get($data,'address3'),
            'PHONEMOBILE' => array_get($this->data,'phone'),
            'DOCISSUEDBY' => array_get($data,'personal_document_issued_by'),
            'SEX' => array_get($shortNameInsurer,'Sex'),
            'BIRTHPLACE' =>  array_get($data,'place_of_birth'),
            'EMAIL' => array_get($this->data,'email'),
            'SUBJECTDOCS' => $this->docsForSubject[$humanFullName] ?? [],
            'DRIVINGDATEBEG' => $this->driverStart[$humanFullName] ?? null
        ];

        if ($street !== null) {
            $dataSubject['STREETISN1'] = array_get($street,'ISN');
        }

        if (!empty($subjectISN)) {
            $dataSubject['ISN'] = $subjectISN;
        }

        if ($postalCode !== null) {
            $dataSubject['POSTCODE1'] = $postalCode;
        }

        $subject = $client->saveSubject($dataSubject);

        return array_get($subject,'ISN');
    }
}
