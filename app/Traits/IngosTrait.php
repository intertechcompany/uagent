<?php

namespace App\Traits;

use Carbon\Carbon;

trait IngosTrait
{
    /**
     * @var array
     */
    protected $data;

    /**
     * @var Carbon
     */
    protected $dateStart;

    protected $owner;
    protected $insurer;
    protected $insurerAndOwnerSame;
    protected $isUnlimitedDrivers;
    protected $nowSbjType = 1;
    protected $driverPersons = [];

    /**
     * @param string $vehicleType
     * @return string
     */
    public function getVehicleType(string $vehicleType): string
    {
        if ($vehicleType === 'body_number') {
            return 'BodyNum';
        }

        if ($vehicleType === 'chassis_number') {
            return 'ChassisNum';
        }

        return 'VIN';
    }

    /**
     * @param array $data
     */
    protected function initVars(array $data): void
    {
        $this->data = $data;
        $this->dateStart = Carbon::createFromFormat('d-m-Y',$this->data['policy_start_date']);
        $this->owner = array_get($this->data,'owner');

        $this->insurerAndOwnerSame = (int)array_get($this->data,'insurer_and_owner_same') === 1;
        $this->isUnlimitedDrivers = array_get($data,'driver_count') === 'unlimited';

        $this->insurer = $this->insurerAndOwnerSame ?
            $this->owner :
            array_get($this->data,'insurer')
        ;
    }

    /**
     * @return array
     */
    protected function generateDataForAgreement(): array
    {
        $agreement = [];

        $agreement['General'] = [
            'Product' => 753518300,
            'DateBeg' => $this->dateStart->format('Y-m-d') . 'T00:00:00',
            'DateEnd' => $this->dateStart->addYear()->addDays('-1')->format('Y-m-d'),
            'CitySales' => array_get($this->insurer,'address.data.city_kladr_id'),
            'Individual' => 'N',
        ];

        $agreement['Owner'] = [
            'SbjRef' => $this->nowSbjType++,
        ];

        $this->owner['SbjRef'] = $agreement['Owner']['SbjRef'];

        $agreement['Insurer'] = [
            'SbjRef' => $this->insurerAndOwnerSame ? $this->owner['SbjRef'] : $this->nowSbjType++,
            'MobilePhone' => preg_replace("/\D/", '', $this->data['phone']),
            'Email' => $this->data['email'],
        ];

        $this->insurer['SbjRef'] = $agreement['Insurer']['SbjRef'];

        $agreement['SubjectList']['Subject'][] = $this->generatePerson($this->owner,$agreement['Owner']['SbjRef']);

        if ($this->insurerAndOwnerSame === false) {
            $agreement['SubjectList']['Subject'][] = $this->generatePerson($this->insurer,$agreement['Insurer']['SbjRef']);
        }

        $vehicleType = $this->getVehicleType($this->data['vehicle_id_type']);

        $agreement['Vehicle'] = [
            'Model' => '12609916',
            'RegNum' => array_get($this->data,'vehicle_plate_number') . array_get($this->data,'vehicle_plate_region') . 'RUS',
            'Constructed' => $this->data['vehicle_registration_year'] . '-01-01',
            $vehicleType => array_get($this->data,'vehicle_id'),
            'Category' => 'B',
            'EnginePowerHP' => $this->data['power'],
            'EngineType' => 'P',
            'TransmissionType' => 'Р',
            'Document' => [
                'DocType' => 34709116,
                'Serial' => $this->data['vehicle_document_serial'],
                'Number' => $this->data['vehicle_document_number'],
                'DocDate' => $this->formatDate($this->data['vehicle_document_date']),
            ],
            'DocInspection' => [
                'DocType' => 3507627803,
                'Number' => array_get($this->data,'diagnostic_number'),
                'DateEnd' => $this->formatDate(array_get($this->data,'diagnostic_until_date')),
            ],
        ];

        $agreement['Condition'] = [
            'Liability' => [
                'RiskCtg' => 28966116,
                'UsageTarget' => [
                    'Personal' => 'Y'
                ],
                'UseWithTrailer' => (bool)array_get($this->data,'is_vehicle_use_trailer') ? 'Y' : 'N',
                'UsageType' => 1381850903,
                'PeriodList' => [
                    'Period' => [
                        'DateBeg' =>  $this->dateStart->format('Y-m-d'),
                        'DateEnd' =>  $this->dateStart->addYear()->addDays('-1')->format('Y-m-d'),
                    ],
                ],
            ],
        ];

        $agreement['DriverList']['Driver'] = $this->generateDrivers();

        $agreement['SubjectList']['Subject'] = array_merge($agreement['SubjectList']['Subject'],$this->driverPersons);

        return [
            'Agreement' => $agreement,
            'NeedCalc'  => 'Y',
        ];
    }

    /**
     * @param array $person
     * @param int $sbjRef
     * @param bool $isFullProfile = true
     * @return array
     */
    protected function generatePerson(array $person,int $sbjRef,bool $isFullProfile = true): array
    {
        $address = array_get($person,'address.data');

//        dd($person);

        $profile = [
            '@SbjKey' => $sbjRef,
            '#' => [
                'SbjType' => 'Ф',
                'SbjResident' => 'Y',
                'FullName' => array_get($person,'last_name') . ' ' . array_get($person,'first_name') . ' ' . array_get($person,'middle_name'),
                'Gender' => 'М',
                'CountryCode' => 643,
                'BirthDate' => $this->formatDate(array_get($person,'dob')),
            ],
        ];

        if ($isFullProfile === true) {
            $profile['#']['IdentityDocument'] = [
                'DocType' => 30363316,
                'Serial' => array_get($person,'personal_document_serial'),
                'Number' => array_get($person,'personal_document_number'),
                'DocDate' => $this->formatDate(array_get($person,'personal_document_date')),
                'DocIssuedBy' => array_get($person,'personal_document_issued_by'),
            ];

            $location = substr(array_get($address,'settlement_kladr_id'), 0, 11)
                        ?? substr(array_get($address,'city_kladr_id'), 0, 15);
            $street = substr(array_get($address,'street_kladr_id'), 0, 15);

            $profile['#']['Address'] = [
                'CountryCode' => 643,
                'CityCode' => $location,
                'StreetCode' => $street,
                'StreetName' => array_get($address,'street_with_type'),
                'House' => array_get($person,'house'),
                'Flat' => array_get($person,'address3'),
            ];
        }

        return $profile;
    }


    /**
     * @return array
     */
    protected function generateDrivers(): array
    {
        $drivers = [];

        foreach ($this->data['drivers'] as $driver) {
            $drivers[] = [
                'SbjRef' => $this->getSbjRefForDriver($driver),
                'DriverLicense' => [
                    'DocType' => 765912000,
                    'Serial' => $driver['document_serial'],
                    'Number' => 392111,
                    'DocDate' => $this->formatDate($driver['start_date'])
                ],
                'DrvDateBeg' => $this->formatDate($driver['start_date']),
            ];
        }
        return $drivers;
    }

    /**
     * @param array $driver
     * @return int
     */
    protected function getSbjRefForDriver(array $driver): int
    {
        $ownerFullName = strtolower(array_get($this->owner,'last_name') . ' ' . array_get($this->owner,'first_name') . ' ' . array_get($this->owner,'middle_name'));
        $driverFullName = strtolower(array_get($driver,'last_name') . ' ' . array_get($driver,'first_name') . ' ' . array_get($driver,'middle_name'));

        if ($driverFullName === $ownerFullName) {
            return $this->owner['SbjRef'];
        }

        $insurerFullName = strtolower(array_get($this->insurer,'last_name') . ' ' . array_get($this->insurer,'first_name') . ' ' . array_get($this->insurer,'middle_name'));

        if ($driverFullName === $insurerFullName) {
            return $this->insurer['SbjRef'];
        }

        $this->driverPersons[] = $this->generatePerson($driver,$this->nowSbjType,false);

        return $this->nowSbjType++;
    }

    /**
     * @param string $date
     * @return string
     */
    private function formatDate(string $date): string
    {
        return Carbon::createFromFormat('d-m-Y',$date)->format('Y-m-d');
    }
}
