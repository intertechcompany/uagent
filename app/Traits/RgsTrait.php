<?php

namespace App\Traits;

use App\Model\VehicleModel;
use App\User;
use Carbon\Carbon;
use App\Model\Type;
use App\Model\Policy;
use App\Model\Driver;
use App\Model\Purpose;
use App\Model\ModelCodeCar;

/**
 *
 */
use Sabre\Xml\Service as XmlService;
use Spatie\ArrayToXml\ArrayToXml;
use Symfony\Component\Serializer\Encoder\XmlEncoder;

trait RgsTrait
{
	private $type;
	private $purpose;
	private $category;
	private $car;
	private $startDate;
	private $vehicleType;
	private $regNumberIsset;

	/**
	 * @var string
	 */
	private $timeFormat = 'Y-m-d\T';

	/**
	 * Method for final calculation
	 *
	 * @param array $data
	 * @param array $responce
	 * @return array
	 */
	public function generateDataFinal(array $data, array $responce): array
	{
		$generateData = [
			'rgs:calcRequest' => [
				'@xmlns:i' => 'http://www.w3.org/2001/XMLSchema-instance',
				'@xmlns' => 'Rgs.Ufo',
				'#' => [
					'AddServiceStations' => [
						'@i:nil' => 'true',
						'#' => null
					],
					'Agent' => [
						'INN' => config('services.rgs.inn'), // 366401357823
						'LNR' => config('services.rgs.inn'), // 366401357823
						'SubjectTypeId' => '2'
					],
					'Auto' => $this->generateAuto($data, $responce),
					'BranchCode' => '27860170',
					'CalculationDate' => [
						'@xmlns:d2p1' => 'http://schemas.datacontract.org/2004/07/System',
						'd2p1:DateTime' => Carbon::now()->format($this->timeFormat) . '00:00:00.000',
						'd2p1:OffsetMinutes' => '180',
					],
					'CalculationId' => array_get($responce, 's:Body.CalculateResponse.CalculateResult.b:Data.CalculationId'),
					'ClientAccept' => [
						'@i:nil' => 'true',
						'#' => null
					],
					'CorellationId' => [
						'@i:nil' => 'true',
						'#' => null
					],
					'EndDate' => $this->generateEndDate($data),
					'Insurant' => $this->generateInsurant($data),
					'IsAnyone' => 'false',
					'IsExistsKasko' => [
						'@i:nil' => 'true',
						'#' => null
					],
					'IsGrossViolationsOfInsurance' => 'false',
					'IsPolicyPrintedFromEKIS' => [
						'@i:nil' => 'true',
						'#' => null
					],
					'IsPreCalculation' => 'false', // Флаг предварительного расчета true – предварительный, false - окончательный
					'IsRsaChecked' => 'false',
					'IsUpdate' => 'false',
					'LeasingCompanyId' => [
						'@i:nil' => 'true',
						'#' => null
					],
					'NeedKbmRequest' => 'true',
					'OtherPartnerId' => [
						'@i:nil' => 'true',
						'#' => null
					],
					'Owner' => $this->generateOwner($data, $responce),
					'OwnerLicense' => [
						'@i:nil' => 'true',
						'#' => null
					],
					'Periods' => $this->generatePeriods($data, $responce),
					'PolicyCheckSum' => [
						'@i:nil' => 'true',
						'#' => null
					],
					'PolicyRegion' => [
						'@i:nil' => 'true',
						'#' => null
					],
					'PrimaryUseKladr' => array_get($data,'insurer.address.data.region_kladr_id'), // Регион регистрации собственника
					'RegistrationPlace' => 'Rf',
					'SaleChannelType2008Id' => '901', // Всегда передавать 901
					'ServiceStations' => [
						'@i:nil' => 'true',
						'#' => null
					],
					'StartDate' => $this->generateStartDate($data),
					'UpdateRequest' => [
						'@i:nil' => 'true',
						'#' => null
					],
				]
			]
		];

		return $generateData;
	}

	

	protected function generateData(array $data, $upid = null): array
	{
		$typeModel = Type::findOrFail(array_get($data, 'type'));
		$purposeModel = Purpose::findOrFail(array_get($data,  'purpose'));

		$this->vehicleType = $this->getVehicleType(array_get($data, 'vehicle_id_type'));

		$this->type = $typeModel->name;
		$this->purpose = $purposeModel->name;
		$this->category = $typeModel->category->name;

		$this->startDate = !empty(array_get($data,'policy_start_date')) ? Carbon::createFromFormat('d-m-Y',array_get($data,'policy_start_date')) : now();

		$region = array_get($data,'owner.address.data.region_with_type');

		$city = array_get($data,'owner.address.data.city_with_type') ?? array_get($data,'owner.address.data.settlement_type');

		$this->car = VehicleModel::query()->find(array_get($data,'model_id'));

		$this->regNumberIsset = array_get($data,'is_vehicle_plate_absent') !== true;

		/**
		 * Start method calculation
		 */
		return $this->firstFormedData($data);
	}

	/**
	 * Method for first calculation
	 *
	 * @pram array $data
	 * @return array
	 */
	private function firstFormedData(array $data): array
	{
		return [
			'rgs:calcRequest' => [
				'@xmlns:i' => 'http://www.w3.org/2001/XMLSchema-instance',
				'@xmlns' => 'Rgs.Ufo',
				'#' => [
					'AddServiceStations' => [
						'@i:nil' => 'true',
						'#' => null
					],
					'Agent' => [
						'INN' => config('services.rgs.inn'),
						'LNR' => config('services.rgs.inn'),
						'SubjectTypeId' => '2'
					],
					'Auto' => $this->generateAuto($data),
					'BranchCode' => '27860170',
					'CalculationDate' => [
						'@xmlns:d2p1' => 'http://schemas.datacontract.org/2004/07/System',
						'd2p1:DateTime' => Carbon::now()->format($this->timeFormat) . '00:00:00',
						'd2p1:OffsetMinutes' => '180',
					],
					'CalculationId' => [ // Изначально пустое, потом заполнять из ответа для перерасчета
						'@i:nil' => 'true',
						'#' => null
					],
					'ClientAccept' => [
						'@i:nil' => 'true',
						'#' => null
					],
					'CorellationId' => [
						'@i:nil' => 'true',
						'#' => null
					],
					'EndDate' => $this->generateEndDate($data),
					'Insurant' => $this->generateInsurant(),
					'IsAnyone' => 'false',
					'IsExistsKasko' => [
						'@i:nil' => 'true',
						'#' => null
					],
					'IsPreCalculation' => 'true', // Флаг предварительного расчета true – предварительный, false - окончательный
					'IsUpdate' => 'false',
					'LeasingCompanyId' => [
						'@i:nil' => 'true',
						'#' => null
					],
					'NeedKbmRequest' => 'true',
					'OtherPartnerId' => [
						'@i:nil' => 'true',
						'#' => null
					],
					'Owner' => $this->generateOwner($data),
					'OwnerLicense' => [
						'@i:nil' => 'true',
						'#' => null
					],
					'Periods' => $this->generatePeriods($data),
					'PolicyCheckSum' => [
						'@i:nil' => 'true',
						'#' => null
					],
					'PolicyRegion' => [
						'@i:nil' => 'true',
						'#' => null
					],
					'PrimaryUseKladr' => array_get($data,'insurer.address.data.region_kladr_id'), // Регион регистрации собственника
					'RegistrationPlace' => 'Rf',
					'SaleChannelType2008Id' => '901', // Всегда передавать 901
					'ServiceStations' => [
						'@i:nil' => 'true',
						'#' => null
					],
					'StartDate' => $this->generateStartDate($data),
					'UpdateRequest' => [
						'@i:nil' => 'true',
						'#' => null
					],
				]
			]
		];
	}

	public function generateStartDate(array $data): array
	{
		$this->startDate = !empty(array_get($data,'policy_start_date')) ? Carbon::createFromFormat('d-m-Y',array_get($data,'policy_start_date')) : now();

		return [ // Дата начала страхования
			'@xmlns:d2p1' => 'http://schemas.datacontract.org/2004/07/System',
			'd2p1:DateTime' => $this->startDate->format($this->timeFormat).'00:00:00',
			'd2p1:OffsetMinutes' => '180',
		];
	}

	private function generateEndDate(array $data): array
	{
		$this->startDate = !empty(array_get($data,'policy_start_date')) ? Carbon::createFromFormat('d-m-Y',array_get($data,'policy_start_date')) : now();

		return [ // Дата окончания страхования
			'@xmlns:d2p1' => 'http://schemas.datacontract.org/2004/07/System',
			'd2p1:DateTime' => $this->startDate->addYear()->addDays('-1')->format($this->timeFormat).'23:59:59.000',
			'd2p1:OffsetMinutes' => '180',
		];
	}

	private function generateInsurant(array $data = []): array
	{
		$generateData = [
			'Address' => [
				'@i:nil' => 'true',
				'#' => null
			],
			'BirthDate' => [
				'@i:nil' => 'true',
				'#' => null
			],
			'Contacts' => [
				'#' => null
			],
			'Document' => [
				'@i:nil' => 'true',
				'#' => null
			],
			'IsResident' => 'true',
			'Name' => [
				'@i:nil' => 'true',
				'#' => null
			],
			'SubjectTypeId' => 1,
		];

		if (!empty($data) && count($data) > 0) {
			$fullAddress = array_get($data, 'insurer.postal_code') . ', Российская Федерация, ' . array_get($data,'insurer.address.data.city_with_type') . ', ' . array_get($data,'insurer.address.data.street_with_type');

			if (array_get($data, 'insurer.address.data.house') != null) {
				$fullAddress .= ', ' . array_get($data, 'insurer.address.data.house');
			}

			if (array_get($data, 'insurer.address.data.flat') != null) {
				$fullAddress .= ', кв ' . array_get($data, 'insurer.address.data.flat');
			}

			$generateData['Address'] = [
				'Area' => [
					'#' => null
				],
				'Building' => '',
				'City' => array_get($data,'insurer.address.data.city_with_type'),
				'CountryCode' => '643',
				'Flat' => array_get($data, 'insurer.address.data.Flat'),
				'FullAddress' => $fullAddress,
				'House' => array_get($data, 'insurer.data.house'),

				// TODO: need to change insurer city_kladr_id
				'KladrCode' => array_get($data,'insurer.address.data.city_kladr_id'),

				'Place' => [
					'#' => null
				],
				'PostCode' => array_get($data,'insurer.postal_code'),
				'Region' => array_get($data,'insurer.address.data.region_with_type'),
				'Street' => array_get($data,'insurer.address.data.street_with_type'),
			];

			if (array_get($data, 'insurer.address.data.house') != null) {
				$generateData['Address']['House'] = array_get($data, 'insurer.address.data.house');
			}

			if (array_get($data, 'insurer.address.data.flat') != null) {
				$generateData['Address']['Flat'] = array_get($data, 'insurer.address.data.flat');
			}

			if ($dobDate = array_get($data, 'insurer.dob')) {
				$generateData['BirthDate'] = Carbon::parse($dobDate)->format($this->timeFormat).'00:00:00';
			}

			$generateData['Contacts'] = [
				'Email' => array_get($data, 'email'),
				'HomePhone' => [
					'#' => null
				],
				'MobilePhone' => array_get($data, 'phone'),
				'WorkPhone' => [
					'#' => null
				],
			];

			$generateData['Document'] = [
				'Number' => array_get($data, 'insurer.personal_document_number'),
				'Series' => array_get($data, 'insurer.personal_document_serial'),
				'TypeCode' => '012',
			];

			$generateData['Name'] = [
				'FirstName' => array_get($data, 'insurer.first_name'),
				'LastName' => array_get($data, 'insurer.last_name'),
				'SecondName' => array_get($data, 'insurer.middle_name'),
			];
		}

		return $generateData;
	}

	/**
	 * Method for generate auto data
	 *
	 * @param array $data
	 * @return array
	 */
	private function generateAuto(array $data, array $responce = []): array
	{
		$generateData = [
			'AllowWeight' => [
				'@i:nil' => 'true',
				'#' => null
			],
			'ChassisNumber' => [
				'@i:nil' => 'true',
				'#' => null
			],
			'ClassifierVehicleModelCode' => $this->getClassifierVehicleModelCode(), // $this->car->name,
			'Document' => [
				'@i:nil' => 'true',
				'#' => null
			],
			'#' => $this->generateDrivers(array_get($data, 'drivers'), $responce),
			'FrameNumber' => [
				'@i:nil' => 'true',
				'#' => null
			],
			'HasTrailer' => 'false',
			// 'LicensePlate' => array_get($data, 'vehicle_plate_number'),
			// 'LicensePlate' => [
			// 	'#' => null
			// ],
			'ManufactureYear' => array_get($data, 'vehicle_registration_year'),
			'Power' => array_get($data, 'power'),
			'PtsBrand' => [
				'#' => null
			],
			'PtsModel' => [
				'#' => null
			],
			'PurposeUseCode' => 1,
			'Vin' => [
				'#' => null
			],
		];

		if (!empty($responce) && count($responce) > 0) {
			$documentData = Carbon::parse(array_get($data, 'vehicle_document_date'));

			$generateData['Document'] = [
				'IssueDate' => $documentData->format($this->timeFormat).'00:00:00',
				'Number' => array_get($data, 'vehicle_document_number'),
				'Series' => array_get($data, 'vehicle_document_serial'),
				'TypeCode' => array_get($data, 'vehicle_document_type') == 'sts' ? 'СТС' : 'ПТС',
			];

			// $generateData['PtsBrand'] = 'Nissan';
			// $generateData['PtsModel'] = 'Teana';
			// $generateData['Vin'] = 'Z8NBBUJ32DS042385';

			$generateData['PtsBrand'] = $this->car->make->name;
			$generateData['PtsModel'] = $this->car->name;

			if ($vin = array_get($data, 'vehicle_id')) {
				$generateData['Vin'] = $vin;
			}
		}

		if (array_get($data, 'vehicle_plate_number')) {
            $generateData['LicensePlate'] = array_get($data, 'vehicle_plate_number');
        }

		return $generateData;
	}

	/**
	 * Get vehicle code car
	 *
	 * @return int
	 */
	public function getClassifierVehicleModelCode()
	{
		$vehicleCode = ModelCodeCar::byBrand($this->car->make)
			->byModel($this->car)
			->first();

		if (!$vehicleCode) {
			return null;
		}

		return $vehicleCode->code;

		// default code work test query
		// return '261020591';
	}

	private function generateOwner(array $data, array $responce = []): array
	{
		$generateData = [
			'Address' => [
				'Area' => [
					'#' => null
				],
				'City' => array_get($data,'owner.address.data.city_with_type'),
				'CountryCode' => '643',
				'KladrCode' => array_get($data,'owner.address.data.city_kladr_id'),
				'Region' => array_get($data,'owner.address.data.region_with_type'),
			],
			'BirthDate' => [
				'@i:nil' => 'true',
				'#' => null
			],
			'Contacts' => [
				'@i:nil' => 'true',
				'#' => null
			],
			'Document' => [
				'@i:nil' => 'true',
				'#' => null
			],
			'Inn' => [
				'@i:nil' => 'true',
				'#' => null
			],
			'IsResident' => 'true',
			'KK' => [
				'@i:nil' => 'true',
				'#' => null
			],
			'KPP' => [
				'@i:nil' => 'true',
				'#' => null
			],
			'Name' => [
				'@i:nil' => 'true',
				'#' => null
			],
			'SubjectTypeId' => '1',
		];

		if (!empty($responce) && count($responce) > 0) {
			$fullAddress = array_get($data, 'owner.postal_code') . ', Российская Федерация, ' . array_get($data,'owner.address.data.city_with_type') . ', ' . array_get($data,'owner.address.data.street_with_type');

			if (array_get($data, 'owner.address.data.house') != null) {
				$fullAddress .= ', ' . array_get($data, 'owner.address.data.house');
			}

			if (array_get($data, 'owner.address.data.flat') != null) {
				$fullAddress .= ', кв ' . array_get($data, 'owner.address.data.flat');
			}
			

			// For exmaple
			// 143965, Российская Федерация, г. Москва, ул. Бирюлевская, д. 1, к.2, кв. 159

			$generateData['Address'] = [
				'Area' => [
					'#' => null
				],
				'Building' => [
					'#' => null
				],
				'City' => array_get($data,'owner.address.data.city_with_type'),
				'CountryCode' => '643',
				'Flat' => array_get($data, 'owner.address.data.Flat'),
				'FullAddress' => $fullAddress,
				'House' => array_get($data, 'owner.data.house'),
				'KladrCode' => array_get($data,'owner.address.data.city_kladr_id'),
				'Place' => [
					'#' => null
				],
				'PostCode' => array_get($data,'owner.postal_code'),
				'Region' => array_get($data,'owner.address.data.region_with_type'),
				'Street' => array_get($data,'owner.address.data.street_with_type'),
			];

			if (array_get($data, 'owner.address.data.house') != null) {
				$generateData['Address']['House'] = array_get($data, 'owner.address.data.house');
			}

			if (array_get($data, 'owner.address.data.flat') != null) {
				$generateData['Address']['Flat'] = array_get($data, 'owner.address.data.flat');
			}

			if ($dobDate = array_get($data, 'owner.dob')) {
				$generateData['BirthDate'] = Carbon::parse($dobDate)->format($this->timeFormat).'00:00:00';
			}

			$generateData['Document'] = [
				'Number' => array_get($data, 'owner.personal_document_number'),
				'Series' => array_get($data, 'owner.personal_document_serial'),
				'TypeCode' => '012',
			];

			$generateData['Name'] = [
				'FirstName' => array_get($data, 'owner.first_name'),
				'LastName' => array_get($data, 'owner.last_name'),
				'SecondName' => array_get($data, 'owner.middle_name'),
			];
		}

		return $generateData;
	}

	public function generateDrivers(array $drivers = [], array $responce = []): array
	{
		$dataDrivers = [];

		foreach ($drivers as $driver) {
			$dataDrivers[] = $this->generateDriver($driver);
		}

		return [
			'Drivers' => $dataDrivers
		];
	}

	private function generateDriver(array $driver = []): array
	{
		if (empty($driver) || count($driver) == 0) {
			return [];
		}

		$nowTime = Carbon::now();

		$startDate = null;
		if ($date = array_get($driver, 'start_date')) {

			$startDate = Carbon::parse($date)->format($this->timeFormat) . '00:00:00';
		}

		$dob = null;
		if ($dobDate = array_get($driver, 'dob')) {

			$dob = Carbon::parse($dobDate)->format($this->timeFormat).'00:00:00';
		}

		return [
			'Driver' => [
				'BirthDate' => $dob,
				'CountryCode' => [
					'@i:nil' => 'true',
					'#' => null
				],
				'DriverAddedDate' => [
					'@i:nil' => 'true',
					'@xmlns:d5p1' => 'http://schemas.datacontract.org/2004/07/System',
					'#' => null,
				],
				'DriverId' => 0,
				'DrivingStartExperienceDate' => $startDate,
				'IsDriverAdded' => [
					'@i:nil' => 'true',
					'#' => null
				],
				'IsDriverRemoved' => [
					'@i:nil' => 'true',
					'#' => null
				],
				'License' => [
					'Number' => array_get($driver, 'document_number'),
					'Series' => array_get($driver, 'document_serial'),
					'TypeCode' => '017',
				],
				'Name' => [
					'FirstName' => array_get($driver, 'first_name'),
					'LastName' => array_get($driver, 'last_name'),
					'SecondName' => array_get($driver, 'middle_name'),
				],
				'PrevLicense' => [
					'@i:nil' => 'true',
					'#' => null
				],
				'PrevName' => [
					'@i:nil' => 'true',
					'#' => null
				],
			]
		];
	}

	private function generatePeriods(array $data, array $responce = []): array
	{
		$startDate = !empty(array_get($data,'policy_start_date')) ? Carbon::createFromFormat('d-m-Y',array_get($data,'policy_start_date')) : now();
		$endDate = !empty(array_get($data,'policy_start_date')) ? Carbon::createFromFormat('d-m-Y',array_get($data,'policy_start_date')) : now();

		$generateData = [
			'PeriodOfUse' => [
				'EndDate' => [
					'@xmlns:d4p1' => 'http://schemas.datacontract.org/2004/07/System',
					'd4p1:DateTime' => $endDate->addYear()->addDays('-1')->format($this->timeFormat).'23:59:59',
					'd4p1:OffsetMinutes' => '180',
				],
				'Id' => 1,
				'IsUsagePeriodAdded' => [
					'@i:nil' => 'true',
					'#' => null
				],
				'IsUsagePeriodRemoved' => [
					'@i:nil' => 'true',
					'#' => null
				],
				'StartDate' => [
					'@xmlns:d4p1' => 'http://schemas.datacontract.org/2004/07/System',
					'd4p1:DateTime' => $startDate->format($this->timeFormat).'00:00:00',
					'd4p1:OffsetMinutes' => '180',
				],
			],
		];

		return $generateData;
	}

	protected function generateDriverForCheck(array $driver): array
	{
		return [
			'DriverRequestValue' => [
				'DriverInfoRequest' => [
					'Surname' => array_get($driver,'last_name'),
					'Name' => array_get($driver,'first_name'),
					'Patronymic' => array_get($driver,'middle_name'),
					'BirthDate' => Carbon::createFromFormat('d-m-Y',array_get($driver,'dob'))->format('Y-m-d'),
					'DriverDocDate' => Carbon::createFromFormat('d-m-Y',array_get($driver,'start_date'))->format('Y-m-d'),
					'DriverDocument' => [
						'Number' => array_get($driver,'document_number'),
						'Serial' => array_get($driver,'document_serial')
					],
				],
				'DateRequest' => Carbon::now()->format('Y-m-d\Th:i:s')
			]
		];
	}

	private function getVehicleType(string $vehicleType): string
    {
        if ($vehicleType === 'body_number') {
            return 'BodyNumber';
        }

        if ($vehicleType === 'chassis_number') {
            return 'ChassisNumber';
        }

        return 'VIN';
    }
}
