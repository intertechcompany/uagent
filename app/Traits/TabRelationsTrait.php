<?php

namespace App\Traits;

use App\Model\AgentAccrued;
use App\Model\AgentPaidout;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

trait TabRelationsTrait
{
    /**
     * @param string $tab
     * @throws \Throwable
     * @return \Illuminate\Http\JsonResponse
     */
	public function createTab(string $tab): JsonResponse
	{
		return response()->json([
			'view' => view("admin.agents.partials.form-$tab", [
				'tabmodel' => null,
				'type' => $tab,
			])->render()
		]);
	}

    /**
     * @param string $tab
     * @param int $tabModelId
     * @throws \Throwable
     * @return \Illuminate\Http\JsonResponse
     */
	public function editTab(string $tab, int $tabModelId): JsonResponse
	{
		$tabModel = $this->getTabModel($tab, $tabModelId);

		return response()
            ->json([
			'view' => view("admin.agents.partials.form-$tab", [
				'tabmodel' => $tabModel,
				'type' => $tab,
			])->render()
		]);
	}

    /**
     * @param string $tab
     * @param int $tabModelId
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
	public function deleteTab(string $tab, int $tabModelId): RedirectResponse
	{
        /**
         * @var AgentAccrued|AgentPaidout $tabModel
         */
		$tabModel = $this->getTabModel($tab, $tabModelId);

		if (!empty($tabModel->file) && !Storage::delete($tabModel->file)) {
			return redirect()
                ->back()
                ->with('alert',[
                    'status' => 'success',
                    'message' => 'Запись не была удалена!',
                    'tab' => $tab,
                ])
            ;
		}

        $tabModel->delete();

		return redirect()
            ->back()
            ->with('alert',[
                'status' => 'success',
                'message' => 'Запись удалена!',
                'tab' => $tab,
            ]);
	}

    /**
     * @param string $tab
     * @param int $tabModelId
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
	public function getTabFile(string $tab, int $tabModelId): BinaryFileResponse
	{
        /**
         * @var AgentAccrued|AgentPaidout $tabModel
         */
		$tabModel = $this->getTabModel($tab, $tabModelId);

		return response()->download(storage_path('app/' . $tabModel->file));
	}

    /**
     * @param Request $request
     */
	public function processActiveTab(Request $request): void
	{
		Session::put('tab_type', $request->get('tab_type'));
	}

	private function getTabModel(string $tab, int $tabModelId)
	{
		if ($tab === 'paidout') {
			return AgentPaidout::query()->find($tabModelId);
		}

		if ($tab === 'accrued') {
			return AgentAccrued::query()->find($tabModelId);
		}

		return null;
	}
}
