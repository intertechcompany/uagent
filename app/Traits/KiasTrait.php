<?php

namespace App\Traits;

use App\Service\PolicyService\Client\Kias\Client;
use Carbon\Carbon;

trait KiasTrait
{

	public function getInsurer($data)
	{
		if (array_get($data,'insurer_and_owner_same') === '1'){
			return array_get($data,'owner');
		}

		return array_get($data,'insurer');
	}


	protected function saveDrivers(array &$drivers,Client $client): void
	{
		foreach ($drivers as &$driver) {
			$shortName = $driver['last_name'] . ' ' . mb_substr($driver['middle_name'],0,1) . ' ' . mb_substr($driver['first_name'],0,1);
			$dataSubject = [
				'CLASSISN' => 2085,
				'SUBJFULLNAME' => $driver['last_name'] . ' ' . $driver['middle_name'] . ' ' . $driver['first_name'],
				'SUBJSHORTNAME' => $shortName,
				'SHORTNAME' => $shortName,
				'BIRTHDAY' => $this->formatDate($driver['dob']),
				'JRESIDENT' => 'Y',
				'JJURIDICAL' => 'N',
				'ISCURRENT' => 1,
				'JURIDICAL' => 'N',
				'RESIDENT' => 'Y',
				'DOCCLASSISN' => '1145',
				'DOCSER' =>  $driver['document_serial'],
				'DOCNO' => $driver['document_number'],
				'DOCDATE' => $this->formatDate($driver['start_date']),
				'DRIVINGDATEBEG' => $this->formatDate($driver['start_date']),
				'SEX' => array_get($driver,'sex') === 1 ? 'M' : 'W',
			];

			$subject = $client->saveSubject($dataSubject);
			$driver['SubjISN'] = array_get($subject,'ISN');

		}
	}

	protected function getCity(array $parents): array
	{
		foreach ($parents as $parent) {
			if ($parent['contentType'] === 'city') {
				return $parent;
			}
		}

		return [];
	}
	private function formatDate(string $date): string
	{
		return (new Carbon($date))->format('d.m.Y') . ' 00:00:00';
	}

	private function formatEndDate(string $date): string
	{
		return (new Carbon($date))->addYear()->addDays(-1)->format('d.m.Y') . ' 00:00:00';
	}

	private function getDataForSaveAgreement($subject,$insurer,$data, $model, $mark,$newCalc,$cityISN,$ID = null)
	{
		$agroRole = array_get($newCalc,'AGRROLE');
		$this->generateRole(array_get($data,'drivers'),$agroRole['row'],array_get($data,'driver_count') === 'specified');
		$array = [
			'ISN' => $ID,
			'EPOLICY' => 'Y',
			'STATUS' => 'С',
			'MAINDEPTISN' => array_get($newCalc,'MAINDEPTISN'),
			'MAINDEPTFULLNAME' => array_get($newCalc,'MAINDEPTFULLNAME'),
			'EMPLISN' => array_get($newCalc,'EMPLISN'),
			'DEPTISN' => array_get($newCalc,'DEPTISN'),
			'CALCDEPTISN' => array_get($newCalc,'CALCDEPTISN'),
			'CALCDEPTFULLNAME' => array_get($newCalc,'CALCDEPTFULLNAME'),
			'EMPLSHORTNAME' => array_get($newCalc,'EMPLSHORTNAME'),
			'EMPLFULLNAME' => array_get($newCalc,'EMPLFULLNAME'),
			'ISVIP' => array_get($newCalc,'ISVIP'),
			'CLIENTISN' => $subject,
			'PRODUCTISN' => array_get($newCalc,'PRODUCTISN'),
			'PRODUCTNAME' => array_get($newCalc,'PRODUCTNAME'),
			'CLASSISN' => array_get($newCalc,'CLASSISN'),
			'CLASSNAME' => array_get($newCalc,'CLASSNAME'),
			'STATUSISN' => array_get($newCalc,'STATUSISN'),
			'STATUSNAME' => array_get($newCalc,'STATUSNAME'),
			'CLIENTNAME' => array_get($insurer,'last_name') . ' ' . mb_substr(array_get($insurer,'first_name'),0,1) . '. ' . mb_substr(array_get($insurer,'middle_name'),0,1) . '.',
			'DATESIGN' => $this->formatDate(date('d.m.Y')),
			'TIMEBEG'  => $this->formatDate(date('d.m.Y')),
			'DATEBEG'  => $this->formatDate(array_get($data,'policy_start_date')),
			'DATEEND'  => $this->formatEndDate(array_get($data,'policy_start_date')),
			'HASAGRSUM' => 'N',
			'AGREEMENT_ADDATTR' => array_get($newCalc,'AGREEMENT_ADDATTR'),
			'AGROBJECT' => [
				'row' => [
					'ClassISN' => 2118,
					'SubClassISN' => 3366,
					'ObjCount' => 1,
					'AGROBJCAR' => [
						'ModelISN' => array_get($model,'ISN'),
						'MarkaISN' => $mark,
						'ClassISN' => 3366,
						'ClassName' => 'Легковые автомобили',
						'ReleaseDate' => '01.01.' . array_get($data,'vehicle_registration_year') . ' 0:00',
						'PtsClassISN' => 220219,
						'PtsSer' => array_get($data,'vehicle_document_serial'),
						'PtsNo' => array_get($data,'vehicle_document_number'),
						'PtsDate' => $this->formatDate(array_get($data,'vehicle_document_date')),
						'OsmotrClassISN' => 222777,
						'OsmotrDate' => $this->formatDate(array_get($data,'diagnostic_until_date')),
						'OsmotrTalonNo' =>  array_get($data,'diagnostic_number'),
						'DocSer' => array_get($data,'vehicle_document_serial'),
						'DocNo' => array_get($data,'vehicle_document_number'),
						'VIN' => array_get($data,'vehicle_id'),
						'REGNO' => array_get($data,'vehicle_plate_number'),
						'BODYID' => 'ОТСУТСТВУЕТ',
						'CHASSISID' => 'ОТСУТСТВУЕТ',
						'POWER' => array_get($data,'power'),
						'OwnerISN' => $subject,
						'OwnerJuridical' => 'N',
						'CountryISN' => 9602,
						'CarUseISN' => 4978,
						'CarUseName' => 'Личная',
						'BonusMalusISN' => 4930,
						'RefISN' => 4943,
						'MultiDrive' => array_get($data,'driver_count') === 'specified' ? 'N' : 'Y',
						'PeriodBeg' => $this->formatDate(array_get($data,'policy_start_date')),
						'PeriodEnd' => $this->formatEndDate(array_get($data,'policy_start_date')),
						'DurationStr' => '1 г (365)',
						'TerritoryISN' => $cityISN,
						'CategoryISN' => 10830,
						'USESPECIALSIGNAL' => 'N',
					],
					'AGRCOND' => [
						'row' => [
							'RiskName' => array_get($newCalc,'AGROBJECT.row.AGRCOND.row.RiskName'),
							'InsClassName' => array_get($newCalc,'AGROBJECT.row.AGRCOND.row.InsClassName'),
							'RiskISN' => array_get($newCalc,'AGROBJECT.row.AGRCOND.row.RiskISN'),
							'RiskDictiName' => array_get($newCalc,'AGROBJECT.row.AGRCOND.row.RiskDictiName'),
							'InsClassISN' => array_get($newCalc,'AGROBJECT.row.AGRCOND.row.InsClassISN'),
							'DateSign' =>  $this->formatDate(array_get($data,'policy_start_date')),
							'DateBeg' =>  $this->formatDate(array_get($data,'policy_start_date')),
							'DateEnd' =>  $this->formatEndDate(array_get($data,'policy_start_date')),
							'Duration' => 365,
							'CurrISN' => 9788,
							'CurrSumISN' => '9788',
							'LimitSum' => 400000,
							'LimitSumType' => 'Н',
							'Tariff' => 0,
							'FranchType' => '-',
							'FranchTariff' => 0,
							'PaymentKoeff' => 1,
							'AGRCONDOSAGO' => array_get($newCalc,'AGROBJECT.row.AGRCOND.row.AGRCONDOSAGO')
						]
					],
				]
			],
			'AGRROLE' => $agroRole,
		];
		return $array;
	}

	private function generateRole(array $drivers,array &$role,bool $add = false): void
	{
		$index = sizeof($role);
		$driversRow = [];

		if ($add) {
			foreach ($drivers as $driver) {
				$driversRow[$index] = [
					'SubjISN' => $driver['SubjISN'],
					'ClassISN' => '2085',
					'ClassName' => 'Лицо допущенное к управлению',
					'SUBJFULLNAME' => $driver['last_name'] . ' ' . $driver['middle_name'] . ' ' . $driver['first_name'],
					'SUBJSHORTNAME' => $driver['last_name'] . ' ' . mb_substr($driver['middle_name'],0,1) . ' ' . mb_substr($driver['first_name'],0,1),
					'SUBJBIRTHDAY' => $this->formatDate($driver['dob']),
					'SUBJDRIVINGDATEBEG' => $this->formatDate($driver['start_date']),
					'SUBJRESIDENT' => 'Y',
					'SUBJJURIDICAL' => 'N',
					'ISCURRENT' => 1
				];
				$index++;
			}
		}

		$role = array_merge($role,$driversRow);
	}
}