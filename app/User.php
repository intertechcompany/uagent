<?php

namespace App;

use App\Components\Dashboard\Types\CountSalesPoliciesAllType;
use App\Components\Dashboard\Types\CountSalesPoliciesMonthType;
use App\Components\Dashboard\Types\CountSalesPoliciesTodayType;
use App\Components\Dashboard\Types\IncomeMonthType;
use App\Components\Dashboard\Types\IncomeTodayType;
use App\Components\Kv\KvHelper;
use App\Entity\Kv;
use App\Helper\LogHelper;
use App\Model\AgentAccrued;
use App\Model\AgentPaidout;
use App\Model\Chat;
use App\Model\City;
use App\Model\Commission;
use App\Model\Curator;
use App\Model\KvBonus;
use App\Model\Log;
use App\Model\Message;
use App\Model\Policy;
use App\Model\Rate;
use App\Model\RateCity;
use App\Model\SaveUserWithdraw;
use App\Model\Stock;
use App\Model\UserDashboardBlock;
use App\Model\UserEmailConfirm;
use App\Model\UserInformation;
use App\Model\UserPaymentInformation;
use App\Model\UserRequisite;
use App\Model\UserWithdraw;
use App\Traits\TabRelationsTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Laratrust\Traits\LaratrustUserTrait;

/**
 * Class User
 * @package App
 * @property int id
 * @property int parent_user_id
 * @property int is_policy_subagents
 * @property int is_broker
 * @property int is_opportunity_sub_agent
 * @property int is_important
 * @property float balance
 * @property Collection subAgents
 * @property Collection polices
 * @property string last_name
 * @property string name
 * @property string patronymic
 * @property string password
 * @property string type
 * @property string worktime_from
 * @property string worktime_to
 * @property string insurance
 * @property string first_name
 * @property string role
 * @property string fromTime
 * @property string toTime
 * @property string status
 * @property string email
 * @property string phone
 * @property string bonus_type
 * @property string bonus
 * @property string verified_at
 * @property string declined_at
 * @property string full_name
 * @property int city_id
 * @property int is_email_confirmed
 * @property int level
 * @property UserInformation information
 * @property Curator curator
 * @property Collection|Rate[] rates
 * @property Collection accrued
 * @property Collection paidout
 * @property Collection curators
 * @property User|null agent
 * @property UserPaymentInformation paymentInformation
 * @property UserRequisite requisite
 * @method static Builder getAgents()
 * @method static Builder agents()
 * @method static Builder getAgentsAndSubAgents()
 * @method static Builder filter(iterable $filters)
 */
class User extends Authenticatable
{
    use Notifiable, LaratrustUserTrait, TabRelationsTrait;

    public const ROLE_AGENT = 'agent';
    public const ROLE_ADMIN = 'admin';
    public const ROLE_CURATOR = 'curator';
    public const ROLE_GUEST = 'guest';
    public const PAGINATE = 20;
    public const STATUS_ACTIVE = 1;
    public const STATUS_DEACTIVE = 2;
    public const STATUS_ARCHIVE = 3;
    public const STATUS_DELETE = 4;

    public static $bonusTypes = [
        'rub',
        'percent'
    ];

    public $times = ['worktime_from'];

    public static $statuses = [
        self::STATUS_ACTIVE => 'Активный',
        self::STATUS_DEACTIVE => 'Деактивный',
        self::STATUS_ARCHIVE => 'Архиваный',
        self::STATUS_DELETE => 'Удаленый',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'last_name',
        'email',
        'password',
        'phone',
        'city_id',
        'comment',
        'is_policy_subagents',
        'verified_at',
        'is_email_confirmed',
        'is_broker',
        'is_opportunity_sub_agent',
        'role',
        'patronymic',
        'balance',
        'level',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    private $kvs = [];

    public function chats(): HasMany
    {
        return $this->hasMany(Chat::class);
    }

    public function messages(): HasMany
    {
        return $this->hasMany(Message::class);
    }

    public function countUnreadMessages()
    {
        return Message::unread()->count();
    }

    public static function getRole(string $role)
    {
        $roles = [
            'superadministrator' => 'Администратор',
            'curator' => 'Куратор',
            'administrator' => 'Пользователь',
        ];

        return array_get($roles, $role);
    }

    public function getStatusesAttribute()
    {
        return $this->getListStatus();
    }

    public function getStatusNameAttribute()
    {
        return $this->getListStatus($this->status, true);
    }

    public function getListStatus($status = null, $strict = false)
    {
        if ($status) {
            return self::$statuses[$status] ?? null;
        }

        if ($strict) {
            return null;
        }

        return self::$statuses;
    }

    /**
     * @return bool
     */
    public function canCreatePolicy(): bool
    {
        if ($this->isBroker() === true) {
            return false;
        }

        $now = Carbon::now()->format('H:i');

        if ($now < $this->fromTime && $now < $this->toTime) {
            return false;
        }

        if ($now > $this->fromTime && $now > $this->toTime) {
            return false;
        }

        return $this->status !== self::STATUS_DEACTIVE;
    }

    public function isBroker(): bool
    {
        return (bool) $this->is_broker === true;
    }

    public function canCreateSubAgents(): bool
    {
        return (bool) $this->is_opportunity_sub_agent === true;
    }

    public function getFullNameAttribute(): string
    {
        return "$this->name $this->last_name";
    }

    public function canShowStocks(): bool
    {
        $insurances = json_decode($this->insurance,true);

        return !(null === $insurances || \count($insurances) === 0);
    }

    public function stocks(): Collection
    {
        $insurances = json_decode($this->insurance,true) ?? [];

        if (\count($insurances) === 0) {
            return collect([]);
        }

        $now = Carbon::now()->format('Y-m-d H:i:s');

        return Stock::query()
            ->where('from', '<=', $now)
            ->where('to', '>=', $now)
            ->whereIn('insurance',$insurances)
            ->get()
            ;
    }

    public function paidout(): HasMany
    {
        return $this->hasMany(AgentPaidout::class, 'agent_id');
    }

    public function accrued(): HasMany
    {
        return $this->hasMany(AgentAccrued::class, 'agent_id');
    }

    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class);
    }

    public function information(): HasOne
    {
        return $this->hasOne(UserInformation::class);
    }

    public function withdraws(): HasMany
    {
        return $this->hasMany(UserWithdraw::class);
    }

    public function saveWithdraws(): HasMany
    {
        return $this->hasMany(SaveUserWithdraw::class);
    }

    public function requisite(): HasOne
    {
        return $this->hasOne(UserRequisite::class);
    }

    public function emailConfirm(): HasOne
    {
        return $this->hasOne(UserEmailConfirm::class);
    }

    public function paymentInformation(): HasOne
    {
        return $this->hasOne(UserPaymentInformation::class);
    }

    public function dashboardBlocks(): HasMany
    {
        return $this->hasMany(UserDashboardBlock::class);
    }

    /**
     * @return HasMany|User
     */
    public function subAgents(): HasMany
    {
        return $this->hasMany(__CLASS__,'parent_user_id');
    }

    public function allSubAgents(): HasMany
    {
        return $this->subAgents()->with('allSubAgents');
    }

    public function agent(): BelongsTo
    {
        return $this->belongsTo(__CLASS__,'parent_user_id');
    }

    public function polices(): HasMany
    {
        return $this->hasMany(Policy::class);
    }

    public function log(): MorphMany
    {
        return $this->morphMany(Log::class, 'log');
    }

    public function scopeAgents(Builder $query): Builder
    {
        return $query->where('role', self::ROLE_AGENT);
    }

    public function curators(): BelongsToMany
    {
        return $this->belongsToMany(Curator::class, 'curator_agent');
    }

    public function curator(): HasOne
    {
        return $this->hasOne(Curator::class);
    }

    public function rates(): BelongsToMany
    {
        return $this->belongsToMany(Rate::class, 'agent_rate', 'agent_id');
    }

    public function commissions(): HasMany
    {
        return $this->hasMany(Commission::class, 'agent_id', 'id');
    }

    public function kvBonus(): HasMany
    {
        return $this->hasMany(KvBonus::class,'user_id');
    }

    public function calcNewAgents()
    {
        return $this->subAgents()->where('created_at', '>=', Carbon::now()->addDays(-3))->count();
    }

    public function processRates(array $rates)
    {
        /**
         * Get rates this agent
         */
        $listIds = $this->getRelationRateLIsts();

        $diffLeft = $this->realtionDiff($rates, $listIds);
        $diffRight = $this->realtionDiff($listIds, $rates);

        if (\count($diffLeft) > 0) {
            return $this->relationRatesLOgs($diffLeft);
        }

        return $this->relationRatesLOgs($diffRight, false);
    }

    public function processTabRelations(Request $request)
    {
        if (!$request->has('tab_id') || !$request->has('tab_type')) {
            return null;
        }

        $type = $request->get('tab_type');

        $tabmodel = $this->getTabModel($type, $request->get('tab_id'));

        if (!$tabmodel) {
            $tabmodel = $this->$type()->create([
                'sum' => $request->get('tab_sum'),
                'date' => $request->get('tab_date'),
                'balance' => $this->balance,
                'status' => $request->get('tab_status'),
            ]);

            return $this->attachFileToTabModel($request, $tabmodel, true);
        }

        $tabmodel->update([
            'sum' => $request->get('tab_sum'),
            'date' => $request->get('tab_date'),
            'status' => $request->get('tab_status'),
        ]);

        return $this->attachFileToTabModel($request, $tabmodel);
    }

    public function getFormatedBalanceAttribute()
    {
        $sum = $this->paidout->sum('sum');

        $total = $this->balance - $sum;

        return $total > 0 ? $total : 0;
    }

    public function getFormatedPaidoutsAttribute(): string
    {
        return number_format($this->paidout->sum('sum'), 2);
    }

    public function getFormatedAccruedAttribute(): string
    {
        return number_format($this->accrued->sum('sum'), 2);
    }

    public function getInsurance(): array
    {
        if (!empty($this->insurance)) {
            $insurance = json_decode($this->insurance,true);

            return \is_array($insurance) && \count($insurance) > 0 ? $insurance : [];
        }

        return [];
    }

    public function scopeGetAgents(Builder $query): void
    {
        /**
         * @var User $authUser
         */
        $authUser = Auth::user();

        if (self::ROLE_CURATOR === $authUser->role) {
            if($authUser->curator) {
                $ids = $authUser->curator->agents->pluck('id');
                $query
                    ->whereIn('id', $ids)
                    ->where('role', self::ROLE_AGENT)
                    ->whereNull('parent_user_id')
                    ->with(['subAgents.city', 'subAgents.paidout', 'subAgents.accrued', 'polices']);
            } else {
                $query->with('curator.user')
                    ->whereHas('curator.user', function($q) use($authUser) {
                        $q->where('id', $authUser->id);
                    });
            }
        } else {
            $query->where('role', self::ROLE_AGENT)
                ->whereNull('parent_user_id')
                ->with(['subAgents.city', 'subAgents.paidout', 'subAgents.accrued', 'polices']);
        }
    }

    public function scopeGetAgentsAndSubAgents(Builder $query)
    {
        /**
         * @var User $authUser
         */
        $authUser = Auth::user();

        if (self::ROLE_CURATOR === $authUser->role) {
            if($authUser->curator) {
                $ids = $authUser->curator->agents->pluck('id');
                $query
                    ->whereIn('id', $ids)
                    ->where('role', self::ROLE_AGENT);
            } else {
                $query->with('curator.user')
                    ->whereHas('curator.user', function($q) use($authUser) {
                        $q->where('id', $authUser->id);
                    });
            }
        } else {
            $query->where('role', self::ROLE_AGENT);
        }
    }

    public function getFromTimeAttribute(): ?string
    {
        if (empty($this->worktime_from)) {
            return null;
        }

        $time = Carbon::createFromTimeString($this->worktime_from);

        return $time->format('H:i');
    }

    public function getToTimeAttribute(): ?string
    {
        if (empty($this->worktime_to)) {
            return null;
        }

        $time = Carbon::createFromTimeString($this->worktime_to);

        return $time->format('H:i');
    }

    public function processCurators(Request $request)
    {
        if (self::ROLE_CURATOR !== Auth::user()->role) {
            $curators = $request->curators;

            if ($request->has('is_json') && $request->get('is_json') == true) {
                $curators = json_decode($request->get('curators'), true);
            }

            return $this->curators()->sync(
                array_column($curators, 'code')
            );
        }
    }

    public function processPaymentInformation(Request $request)
    {
        $data = $request->only('number_cart', 'cart_date', 'number_qiwi');

        if (!empty($data['number_cart'])) {
            $data['number_cart'] = str_replace('-', '', $data['number_cart']);
        }

        if (!empty($data['cart_date'])) {
            $data['cart_date'] = str_replace('/', '', $data['cart_date']);
        }

        if ($this->paymentInformation) {
            return $this->paymentInformation()->update($data);
        }

        return $this->paymentInformation()->create($data);
    }

    public function canSaveRateMoreSelf(array $rates)
    {
        $errors = [];

        foreach ($rates as $insurance => $rate) {
            if (isset($rate['value']) && $value = $rate['value']) {
                if ($value > $this->balance) {
                    $errors["rates.$insurance.value"] = 'between:0,:' . $this->balance;
                }
            }
        }

        return $errors;
    }

    public function scopeFilter($query, array $filters)
    {
        if (\count($filters) === 0) {
            return $query;
        }

        if (isset($filters['number']) && !empty($filters['number'])) {
            return $query->whereHas('polices', function($querPolicy) use ($filters) {
                return $querPolicy->Where('insurance_id', $filters['number']);
            });
        }

        foreach ($filters as $filter => $value) {
            $this->setFilter($query, $filter, $value);
        }

        return $query;
    }

    public function setFilter(Builder $query, string $filter, $value = null): void
    {
        switch ($filter) {
            case 'city':
                $query->where('city_id', $value);

                break;
            case 'insurance':
                $query->where('insurance', 'like', '%' . $value . '%');

                break;
            case 'statuses':
                if (\is_array($value)) {
                    $query->whereIn('status', $value);

                    break;
                }

                $query->where('status', $value);

                break;
            case 'agents':
                $query->where('id', $value);

                break;
            case 'date':
                $query->where(function (Builder $queryDate) use ($value) {

                    if (!empty($value['from'])) {
                        $queryDate->where('created_at','>=',new Carbon($value['from']));
                    }
                    elseif (!empty($value[0])) {
                        $queryDate->where('created_at','>=',new Carbon($value[0]));
                    }

                    if (!empty($value['to'])) {
                        $queryDate->where('created_at','<=',new Carbon($value['to']));
                    }
                    elseif (!empty($value[1])) {
                        $queryDate->where('created_at','<=',new Carbon($value[1]));
                    }

                    return $queryDate;
                });

                break;
            case 'date_register':
                $query->where('created_at', new Carbon($value));

                break;
            case 'nameLastName':
                $query->where('name', 'like', '%' . $value . '%')
                    ->orwhere('last_name', 'like', '%' . $value . '%');

                break;
            case 'cities':
                if (\is_array($value)) {
                    $query->whereHas('city', function($queryCity) use ($value) {
                        $queryCity->whereIn('id', $value);
                    });

                    break;
                }

                break;
        }
    }

    public function getSuccessPolicies(): int
    {
        return $this
            ->polices()
            ->where('status', Policy::SUCCESS_STATUS)
            ->count()
            ;
    }

    public function getSuccessPoliciesPremiums(): array
    {
        return $this
            ->polices()
            ->where('status', Policy::SUCCESS_STATUS)
            ->pluck('premium')
            ->toArray()
            ;
    }

    public function getAvgPremiumPolicies()
    {
        return $this
            ->polices()
            ->where('status', Policy::SUCCESS_STATUS)
            ->avg('premium')
            ;
    }

    public function getSuccessPoliciesPremiumAttribute(): float
    {
        return array_sum($this->getSuccessPoliciesPremiums());
    }

    public function getAvgPoliciesPremiunAttribute()
    {
        return $this->getAvgPremiumPolicies();
    }

    public function getAvatarAttribute()
    {
        return $this->information ? $this->information->avatar : null;
    }

    public function getAvatar()
    {
        return $this->avatar ? asset('storage/app/avatar/'.$this->avatar) : asset('/img/default_avatar.png');
    }

    public function getCityRate($insurance = 'alpha'): Collection
    {
        if ($this->parent_user_id === null) {
            return RateCity::byInsurance($insurance)->with('city')->get();
        }

        return $this
            ->commissions()
            ->where('insurance',$insurance)
            ->where('rate_type', Commission::TYPE_OF_RATE_RATE_CITY)
            ->get()
            ->map(function (Commission $commission) {
                $rateCity = $commission->rateCity;
                $rateCity->value = $commission->agent;
                return $rateCity;
            })
            ;
    }

    public function getKv($insurance = 'alpha'): ?Kv
    {
        if (!isset($this->kvs[$insurance])) {
            $this->kvs[$insurance] = (new KvHelper($this,$insurance))->getKv();
        }

        return $this->kvs[$insurance];
    }

    public function processSaveUserWithdraw(Request $request, array $fields = []): void
    {
        foreach ($fields as $field) {
            if (empty($field)) {
                continue;
            }

            /**
             * @var SaveUserWithdraw $record
             */
            $record = $this->saveWithdraws()->processSave($field, $request)->first();

            if ($record) {
                $record->update([
                    'value' => $request->get($field)
                ]);

                continue;
            }

            $this->saveWithdraws()->create([
                'field' => $field,
                'value' => $request->get($field),
                'type' => $request->get('type'),
            ]);
        }
    }

    public function pricessRequisite(Request $request)
    {
        $data = $request->only(
            'full_name',
            'kpp',
            'leader_organization',
            'ogrn',
            'based',
            'bank',
            'legal_address',
            'bik',
            'mailing_address',
            'settlement_account',
            'phone',
            'ks',
            'inn',
            'payer_description'
        );

        if ($requisite = $this->requisite) {
            return $requisite->update($data);
        }

        return $this->requisite()->create($data);
    }

    public function getSavedWithraw(string $type = null, string $field = null)
    {
        if ($record = $this->saveWithdraws()->byType($type)->byField($field)->first()) {
            return $record->value;
        }

        return null;
    }

    public function getIncomeForMonth(): string
    {
        return app(IncomeMonthType::class)->getData()->getFormattedValue();
    }

    public function setDefaultValues(): void
    {
        $userBlocks = $this->dashboardBlocks()->get()->keyBy('key');

        $blocks = [
            CountSalesPoliciesAllType::KEY,
            CountSalesPoliciesTodayType::KEY,
            CountSalesPoliciesMonthType::KEY,
            IncomeTodayType::KEY,
            IncomeMonthType::KEY,

        ];

        foreach ($blocks as $key) {
            if (empty($userBlocks[$key])) {
                continue;
            }

            $blockUpdate = $userBlocks[$key] ?? new UserDashboardBlock();
            $blockUpdate->user_id = $this->id;
            $blockUpdate->key = $key;
            $blockUpdate->is_enabled = true;
            $blockUpdate->save();
        }

        $this->worktime_from = '00:00:00';
        $this->worktime_to = '23:59:00';
        $this->insurance = '["nasko","alpha"]';
        $this->save();
    }

    /**
     * @return iterable
     */
    public function getCurators(): iterable
    {
        if ($this->parent_user_id === null) {
            return $this->curators;
        }

        return [$this->agent];
    }

    private function attachFileToTabModel(Request $request, $tabmodel, $context = false)
    {
        if ($request->hasFile('tab_file') && $tabmodel) {
            Storage::delete($tabmodel->file);

            $paidFile = $request->file('tab_file')->storeAs(
                'agents/' . $this->id, $request->file('tab_file')->getClientOriginalName()
            );

            $tabmodel->update([
                'file' => $paidFile,
            ]);
        }

        return $tabmodel;

        $actionName = $context ? 'создана' : 'обновлена';

        return redirect()->back()->withAlert([
            'status' => 'success',
            'message' => "Запись $tabmodel->date $tabmodel->sum была $actionName!",
            'tab' => $request->get('tab_type'),
        ]);
    }

    private function getRelationRateLIsts(string $column = 'id'): array
    {
        if ($this->rates->count() > 0) {
            return $this
                ->rates
                ->pluck($column)
                ->toArray()
                ;
        }

        return [];
    }

    private function realtionDiff(array $left, array $right): array
    {
        return array_diff($left, $right);
    }

    private function relationRatesLOgs(array $lists, bool $add = true)
    {
        if (\count($lists) === 0) {
            return [];
        }

        foreach ($lists as $listId) {
            $this->logRate($listId, $add);
        }
    }

    private function logRate(int $id, bool $add)
    {
        $rate = Rate::query()->find($id);

        if (!$rate || !in_array($rate->type, array_flip(Rate::$types), true)) {
            return null;
        }

        $insurance = Policy::insurance($rate->insurance);
        $process = $add ? 'Изменена ставка на' : 'Удалена ставка';

        $description = "$process $insurance $rate->full_name!";

        LogHelper::factory()->setAction('rates')->setObject($this)->setDescription($description)->save();
    }

    public function scopeDashboard($query)
    {
        return $query;
    }

    public static function getSubAgentsRelationsLevel($addRelations = []) {

        $level = self::max('level');

        $relationName = 'subAgents';
        $relation[] = $relationName;

        if(!empty($addRelations)) {
            foreach($addRelations as $add) {
                $relation[] = $relation[0] . '.' . $add;
            }
        }

        foreach(range(1, $level) as $index) {
            $relation[0] .= ".{$relationName}";

            if(!empty($addRelations)) {
                foreach($addRelations as $add) {
                    $relation[] = $relation[0] . '.' . $add;
                }
            }
        }

        return $relation;
    }

    public function checkBalance($sum)
    {
        return $this->balance >= $sum;
    }
}
