<?php

namespace App\Exceptions;

use App\Helper\SlackApi;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;

class Handler extends ExceptionHandler
{
    /**
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        ValidationException::class,
        \Symfony\Component\HttpKernel\Exception\NotFoundHttpException::class,
        \Illuminate\Session\TokenMismatchException::class
    ];

    /**
     * @param Exception $exception
     * @throws Exception
     * @return mixed|void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Exception $exception
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function render($request, Exception $exception)
    {
        $exceptionClass = \get_class($exception);

        if (!\in_array($exceptionClass,$this->dontReport,true)) {
            $subject = 'UAGENT - ' . $exception->getMessage();
            $message =
                'URL: ' . $request->getRequestUri()
                . "\n ERROR: " . $exception->getMessage()
                . "\n FILE: " . $exception->getFile() . '(' . $exception->getLine() . ')'
                . "\n EXCEPTION: " . $exceptionClass
                . "\n QUERY STRING: " . $request->getQueryString()
                . "\n IP: " . $request->getClientIp()
                . "\n User Agent: " . $request->headers->get('User-Agent')
                . "\n TRACE: \n" . $exception->getTraceAsString()
            ;

            if (config('app.env') == 'production' && !config('app.debug')) {
                mail('claudiapod.intertech@gmail.com', $subject, $message);
            }

            if (config('services.slack.hooks_url')) {
                $subject = $subject . ". <" . config('app.url') . "|" . config('app.url') . "> with code #" . $exception->getCode() . ":";

                $api = new SlackApi(config('services.slack.hooks_url'));
                $api->sendHook($subject, $message);
            }
        }

        return parent::render($request, $exception);
    }


    /**
     * @param \Illuminate\Http\Request $request
     * @param ValidationException $exception
     * @return \Illuminate\Http\Response
     */
	protected function invalid($request, ValidationException $exception)
	{
		session()->flash('error', 'Ошибка в форме');

		return parent::invalid($request, $exception);
    }
}
