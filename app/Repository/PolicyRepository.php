<?php

namespace App\Repository;

use App\Model\Policy;
use App\User;
use Illuminate\Support\Collection;

class PolicyRepository extends Repository
{
    /**
     * @param array $filtersData
     * @param User $user
     * @return Collection
     */
    public function filterPolicyByUser(array $filtersData,User $user): Collection
    {
        return Policy::filter($filtersData)
            ->sortable($filtersData)
            ->where('user_id', $user->id)
            ->with(['owner','kvBonusForOwner'])
            ->get()
            ->map(function (Policy $policy) {
                return [
                    'id' => $policy->id,
                    'owner_full_name' => $policy->owner->first_name . ' ' . $policy->owner->last_name,
                    'insurance_id' => $policy->insurance_id,
                    'insurance' => $policy->insurance,
                    'phone' => $policy->phone,
                    'email' => $policy->email,
                    'policy_start_date' => $policy->policy_start_date,
                    'premium' => $policy->premium,
                    'status' => $policy->status,
                    'kv' => $policy->kvBonusForOwner !== null ? $policy->kvBonusForOwner->sum : 0,
                ];
        });
    }
}
