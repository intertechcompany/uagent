<?php

namespace App\Repository;

use App\User;
use Illuminate\Support\Facades\DB;

class KvBonusRepository extends Repository
{
    /**
     * @param User $user
     * @return float
     */
    public function getIncomeForAgent(User $user): float
    {
       return DB::table('kv_bonuses')
            ->where('user_id', $user->id)
            ->where('is_owner_policy',true)
            ->sum('sum')
       ;
    }

    /**
     * @param User $subAgent
     * @param User $agent
     * @return float
     */
    public function getMyIncome(User $subAgent,User $agent): float
    {
        return DB::table('kv_bonuses as kva')
            ->rightJoin('kv_bonuses as kvs','kvs.policy_id','=','kva.policy_id')
            ->where('kvs.is_owner_policy',true)
            ->where('kva.user_id', $agent->id)
            ->where('kvs.user_id',$subAgent->id)
            ->sum('kva.sum')
        ;
    }
}
