<?php

namespace App\Repository;

class Repository
{
    /**
     * @return static
     */
    public static function factory(): self
    {
        return new static();
    }
}
