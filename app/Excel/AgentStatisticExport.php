<?php

namespace App\Excel;

use App\Components\Statistics\Statistics;
use App\User;
use Maatwebsite\Excel\Facades\Excel;

class AgentStatisticExport implements Export
{
    /**
     * @var User $user
     */
    private $user;

    /**
     * @var iterable
     */
    private $filter;

    /**
     * AgentStatisticExport constructor.
     * @param User $user
     * @param iterable $filter
     */
    public function __construct(User $user, iterable $filter)
    {
        $this->user = $user;
        $this->filter = $filter;
        $this->filter['date'] = [array_get($this->filter,'from'), array_get($this->filter,'to')];
    }

    public function export(): void
    {
        $statistic = new Statistics($this->user, $this->filter);
        $statistic->generate();

        $statisticsArr = $this->user->canCreateSubAgents() === true
            ? $this->generateSheetForAgent($statistic)
            : $this->generateSheetForSubAgent($statistic)
        ;

        Excel::create('Statistic ' . date('Y-m-d H:i:s'), function($excel) use ($statisticsArr): void {

            $excel->sheet('Excel sheet', function($sheet) use ($statisticsArr): void {

                $sheet->setOrientation('landscape');

                $sheet->fromArray($statisticsArr, null, 'A1', false, false);
            });

        })->export('xls');
    }

    /**
     * @param Statistics $statistics
     * @return iterable
     */
    private function generateSheetForAgent(Statistics $statistics): iterable
    {
        $total = $statistics->getTotalStatistic();
        $user = $statistics->getUserStatistic();
        $subAgents = $statistics->getSubAgentStatistic();

        $statisticsArr[] = [
            'name' => '',
            'countSubAgents' => 'Количество субагентов',
            'count_success_policies' => 'Количество оформленных полисов',
            'sum_success_policies_premium' => 'Сумма страховых премий по оформленным полисам',
            'avg_policies_premium' => 'Средняя цена одного полисa',
            'income' => 'Доход субагента',
            'my_income' => 'Мой доход',
        ];

        $statisticsArr[] = [
            'name' => 'Всего',
            'countSubAgents' => $total['countSubAgents'],
            'count_success_policies' => $total['count_success_policies'],
            'sum_success_policies_premium' => $total['sum_success_policies_premium'],
            'avg_policies_premium' => $total['avg_policies_premium'],
            'income' => $total['income'],
            'my_income' => $total['my_income'],
        ];

        $statisticsArr[] = [
            'name' => 'Вы',
            'countSubAgents' => $user['countSubAgents'],
            'count_success_policies' => $user['count_success_policies'],
            'sum_success_policies_premium' => $user['sum_success_policies_premium'],
            'avg_policies_premium' => $user['avg_policies_premium'],
            'income' => '',
            'my_income' => $user['my_income'],
        ];

        foreach ($subAgents as $agent) {
            $statisticsArr[] = [
                'name' => $agent['last_name'] . ' ' . $agent['name'],
                'countSubAgents' => $agent['countSubAgents'],
                'count_success_policies' => $agent['count_success_policies'],
                'sum_success_policies_premium' => $agent['sum_success_policies_premium'],
                'avg_policies_premium' => $agent['avg_policies_premium'],
                'income' => $agent['income'],
                'my_income' => $agent['my_income'],
            ];

        }

        return $statisticsArr;
    }

    /**
     * @param Statistics $statistics
     * @return iterable
     */
    private function generateSheetForSubAgent(Statistics $statistics): iterable
    {
        $total = $statistics->getTotalStatistic();

        $statisticsArr[] = [
            'name' => '',
            'count_success_policies' => 'Количество оформленных полисов',
            'sum_success_policies_premium' => 'Сумма страховых премий по оформленным полисам',
            'avg_policies_premium' => 'Средняя цена одного полисa',
            'my_income' => 'Мой доход',
        ];

        $statisticsArr[] = [
            'name' => 'Всего',
            'count_success_policies' => $total['count_success_policies'],
            'sum_success_policies_premium' => $total['sum_success_policies_premium'],
            'avg_policies_premium' => $total['avg_policies_premium'],
            'my_income' => $total['my_income'],
        ];

        return $statisticsArr;
    }
}
