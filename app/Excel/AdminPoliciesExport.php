<?php

namespace App\Excel;

use App\Model\Policy;
use Maatwebsite\Excel\Facades\Excel;

class AdminPoliciesExport implements Export
{
    /**
     * @var iterable
     */
    private $filters;

    /**
     * AdminPoliciesExport constructor.
     * @param iterable $filters
     */
    public function __construct(iterable $filters)
    {
        $this->filters = $filters;
    }

    public function export(): void
    {
        $polices = Policy::dashboard()
            ->filter($this->filters)
            ->with(['user.curators','owner'])
            ->get()
        ;

        Excel::create('Polices', function($excel) use ($polices): void {

            $excel->sheet('export', function($sheet) use($polices): void {

                $sheet->setOrientation('landscape');

                $columns = [
                    'id' => 'ID',
                    'insurance' => 'Страховая компания',
                    'insurance_number' => 'Номер полиса',
                    'insurance_id' => 'ID полиса',
                    'created_at' => 'Дата оформления',
                    'policy_start_date' => 'Дата начала действия',
                    'status' => 'Статус',
                    'full_name' => 'ФИО собственника',
                    'premium' => 'Премия ₽',
                    'kv' => 'КВ ₽',
                    'agent' => 'Агент/субагент',
                    'curator' => 'Куратор',
                    'city' => 'Город',
                ];

                $data = [$columns];

                foreach($polices as $policy) {

                    $row = [];

                    $user = $policy->user;

                    foreach($columns as $column => $columnName) {
                        switch ($column) {
                            case 'insurance':
                                $row[$column] = $policy->insurance !== null ? Policy::insurance($policy->insurance) : '-';
                                break;
                            case 'status':
                                $row[$column] = $policy->getStatusRus();
                                break;
                            case 'full_name':
                                $row[$column] = $policy->owner->getFullName();
                                break;
                            case 'agent':
                                $row[$column] = $user->full_name;
                                break;
                            case 'kv':
                                $row[$column] = $policy->kvBonusForOwner !== null ? $policy->kvBonusForOwner->sum : 0;
                                break;
                            case 'curator':
                                if ($policy->user->curators->count() > 0) {
                                    $row[$column] = $policy->user->curators->implode('full_name', ', ');
                                }
                                break;
                            case 'city':
                                $row[$column] = $policy->owner ? $policy->owner->address : null;
                                break;
                            default:
                                $row[$column] = $policy->$column;

                        }
                    }

                    $data[] = $row;
                }

                $sheet->fromArray($data, null, 'A1', false, false);
            });
        })->export('xls');
    }
}
