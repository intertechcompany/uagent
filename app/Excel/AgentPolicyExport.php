<?php

namespace App\Excel;

use App\Model\Policy;
use App\Repository\PolicyRepository;
use App\User;
use Maatwebsite\Excel\Facades\Excel;

class AgentPolicyExport implements Export
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var iterable
     */
    private $filter;

    /**
     * AgentPolicyExport constructor.
     * @param User $user
     * @param iterable $filter
     */
    public function __construct(User $user, iterable $filter)
    {
        $this->user = $user;
        $this->filter = $filter;
        $this->filter['date'] = [array_get($this->filter,'from'), array_get($this->filter,'to')];
    }

    /**
     * @inheritdoc
     */
    public function export(): void
    {
        $policies = PolicyRepository::factory()->filterPolicyByUser($this->filter,$this->user);

        $policiesColumns = [
            'id' => '№',
            'owner_full_name' => 'Стахователь',
            'insurance_id' => 'Номер полиса',
            'insurance'=> 'Страховая компания',
            'phone' => 'Телефон',
            'email' => 'E-mail',
            'policy_start_date' => 'Дата и время',
            'premium' => 'Премия',
            'status' => 'Статус',
            'kv' => 'КВ',
        ];

        $policies = $policies->map(function (iterable $policy) {
            $policy['insurance'] = $policy['insurance'] !== null ? Policy::insurance($policy['insurance']) : '-';
            $policy['status'] = Policy::$statuses[$policy['status']];
            return $policy;
        });

        $policies = array_merge([$policiesColumns],$policies->toArray());

        Excel::create('Policies ' . date('Y-m-d H:i:s'), function($excel) use ($policies): void {

            $excel->sheet('Policies sheet', function($sheet) use ($policies): void {

                $sheet->fromArray($policies, null, 'A1', false, false);
            });

        })->export('xls');
    }
}
