<?php

namespace App\Excel;

interface Export
{
    public function export(): void;
}
