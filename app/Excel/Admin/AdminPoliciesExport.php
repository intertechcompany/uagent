<?php

namespace App\Excel\Admin;

use Carbon\Carbon;
use App\Model\Policy;
use App\Excel\Export;
use Maatwebsite\Excel\Facades\Excel;

class AdminPoliciesExport implements Export
{
    /**
     * @var iterable
     */
    private $filters;

    /**
     * AdminPoliciesExport constructor.
     * @param iterable $filters
     */
    public function __construct(iterable $filters)
    {
        $this->filters = $filters;
    }

    public function export(): void
    {
        $polices = Policy::dashboard()
            ->filter($this->filters)
            ->with(['user.curators','owner'])
            ->get()
        ;

        Excel::create('Polices', function($excel) use ($polices): void {

            $excel->sheet('export', function($sheet) use($polices): void {

                $sheet->setOrientation('landscape');

                $columns = [
                    'id' => 'ID',
                    'insurance' => 'Страховая компания',
                    'insurance_number' => 'Номер полиса',
                    'insurance_id' => 'ID полиса',
                    'created_at' => 'Дата оформления',
                    'status' => 'Статус',
                    'policy_start_date' => 'Дата',
                    'full_name' => 'Собственник',
                    'premium' => 'Премия ₽',
                    'kv' => 'КВ',
                    'kv_type' => 'КВ (%, руб.)',
                    'agent' => 'Агент',
                    'level' => 'Уровень Агента',
                    'curator' => 'Куратор',
                    'who_subagent' => 'Чей субагент',
                    'login' => 'Логин',
                    'city' => 'Город',
                    'owner_place_of_birth' => 'Регион оформления (КТ)'
                ];

                $data = [$columns];

                foreach($polices as $policy) {
                    $row = [];

                    $user = $policy->user;

                    foreach($columns as $column => $columnName) {
                        switch ($column) {
                            case 'created_at':
                                $row[$column] = $policy->created_at->format('Y-m-d');

                                break;
                            case 'policy_start_date':
                                $row[$column] = Carbon::parse($policy->policy_start_date)->format('Y-m-d');

                                break;
                            case 'kv':
                                $row[$column] = $policy->kvBonusForOwner !== null ? $policy->kvBonusForOwner->sum : 0;

                                break;
                            case 'kv_type':
                                $row[$column] = $this->getPolicyKv($policy);

                                break;
                            case 'insurance':
                                $row[$column] = $policy->insurance !== null ? Policy::insurance($policy->insurance) : '-';

                                break;
                            case 'status':
                                $row[$column] = $policy->getStatusRus();

                                break;
                            case 'full_name':
                                $row[$column] = $policy->owner->getFullName();

                                break;
                            case 'agent':
                                $row[$column] = $user->full_name;

                                break;
                            case 'level':
                                $row[$column] = $user->level;

                                break;
                            case 'curator':
                                if ($policy->user->curators->count() > 0) {
                                    $row[$column] = $policy->user->curators->implode('full_name', ', ');
                                    break;
                                }

                                $row[$column] = '';

                                break;
                            case 'who_subagent':
                                if ($user->level > 1) {
                                    $row[$column] = $user->agent->full_name;

                                    break;
                                }

                                $row[$column] = '';

                                break;
                            case 'city':
                                $row[$column] = $policy->user && $policy->user->city ? $policy->user->city->name : null;

                                break;
                            case 'login':
                                $row[$column] = $policy->user->email;
                                
                                break;
                            case 'owner_place_of_birth':
                                if ($policy->owner) {
                                    $address = $policy->owner->address;
                                    $addressData = explode(',', $address);

                                    $row[$column] = array_get($addressData, '0');

                                    break;
                                }

                                $row[$column] = '';
                                
                                break;
                            default:
                                $row[$column] = $policy->$column;

                        }
                    }

                    $data[] = $row;
                }

                $sheet->fromArray($data, null, 'A1', false, false);
            });
        })->export('xls');
    }

    private function getPolicyKv(Policy $policy): ?string
    {
        if ($policy && $policy->user && !empty($policy->insurance)) {
            $insuranceCities = $policy->user->getCityRate($policy->insurance);

            if ($insuranceCities && $insuranceCities->count() > 0) {
                $insuranceKv = $insuranceCities->where('city.kladr_id', $policy->kladr_id)->first();

                if ($insuranceKv) {
                    $kv = number_format($insuranceKv->value, 2);
                    $type = $insuranceKv->type === 'percent' ? '%' : '₽';
                    $city = $insuranceKv->city ? $insuranceKv->city->name : null;

                    return "{$kv} {$type} {$city}";
                }
            }

            return $this->getDefaultPolicyKv($policy);
        }

        return null;
    }

    private function getDefaultPolicyKv(Policy $policy): ?string
    {
        $insuranceKv = $policy->user->getKv($policy->insurance);

        if ($insuranceKv) {
            $kv = number_format($insuranceKv->getValue(), 2);
            $type = $insuranceKv->getType() === 'percent' ? '%' : '₽';

            return "{$kv} {$type}";
        }

        return null;
    }
}
