<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class EmailConfirmationNotify extends Notification
{
    use Queueable;

    protected $confirmUrl;

	/**
	 * Create a new notification instance.
	 *
	 * @param string $confirmUrl
	 */
    public function __construct(string $confirmUrl)
    {
		$this->confirmUrl = $confirmUrl;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array
     */
    public function via()
    {
        return ['mail'];
    }

	/**
	 * Get the mail representation of the notification.
	 *
	 * @param User $user
	 * @return MailMessage
	 * @internal param mixed $notifiable
	 */
    public function toMail(User $user)
    {
        return (new MailMessage)
			->greeting("Здравствуйте, $user->full_name")
			->line('The introduction to the notification.')
			->action('Confirm email', $this->confirmUrl)
			->line('Thank you for using our application!');
    }
}
