<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SubAgentEmailConfirmationNotify extends Notification
{
    use Queueable;

    protected $confirmUrl;

    /**
     * Create a new notification instance.
     *
     * @param string $confirmUrl
     */
    public function __construct(string $confirmUrl)
    {
        $this->confirmUrl = $confirmUrl;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array
     */
    public function via()
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param User $user
     * @return MailMessage
     * @internal param mixed $notifiable
     */
    public function toMail(User $user)
    {
        return (new MailMessage)
            ->greeting("Здравствуйте подагент, $user->full_name")
            ->line('Если это вы то нажмите подтвердить E-Mail')
            ->action('Подтвердите E-Mail', $this->confirmUrl)
            ->line('Спасибо за пользование програмой!');
    }
}
