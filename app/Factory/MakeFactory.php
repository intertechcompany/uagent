<?php

namespace App\Factory;

use App\Model\AlphaMake;
use App\Model\Policy;
use App\Model\VehicleMake;

final class MakeFactory
{
	public static function factory(?string $type)
	{
		if ($type === Policy::ALPHA) {
			return new AlphaMake();
		}

		if ($type === Policy::NASKO) {
			return new VehicleMake();
		}

		throw new \InvalidArgumentException('Unknown insurance given');
	}
}