<?php

namespace App\Factory;

use App\Model\AlphaModel;
use App\Model\Policy;
use App\Model\VehicleModel;

final class CarModelFactory
{
	public static function factory(string $type)
	{
		if ($type === Policy::ALPHA) {
			return new AlphaModel();
		}

		if ($type === Policy::NASKO) {
			return new VehicleModel();
		}

		throw new \InvalidArgumentException('Unknown insurance given');
	}
}