<?php

namespace App\Mail;

use App\Model\Question as QuestionModel;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ChangePassword extends Mailable
{
	use Queueable, SerializesModels;

	/**
	 * @var QuestionModel
	 */
	private $password;
	/**
	 * Create a new message instance.
	 *
	 * @param string $link
	 */
	public function __construct(string $password)
	{
		$this->password = $password;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		return $this->view('mail.change-password',[
			'password' => $this->password
		]);
	}
}