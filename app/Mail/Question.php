<?php

namespace App\Mail;

use App\Model\Question as QuestionModel;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Question extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var QuestionModel
     */
    private $question;
    /**
     * Create a new message instance.
     *
     * @param QuestionModel $question
     * @return void
     */
    public function __construct(QuestionModel $question)
    {
        $this->question = $question;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.question',[
            'question' => $this->question
        ]);
    }
}
