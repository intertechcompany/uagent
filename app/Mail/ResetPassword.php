<?php

namespace App\Mail;

use App\Model\Question as QuestionModel;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ResetPassword extends Mailable
{
	use Queueable, SerializesModels;

	/**
	 * @var QuestionModel
	 */
	private $link;
	/**
	 * Create a new message instance.
	 *
	 * @param string $link
	 */
	public function __construct(string $link)
	{
		$this->link = $link;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		return $this->view('mail.reset-password',[
			'link' => $this->link
		]);
	}
}