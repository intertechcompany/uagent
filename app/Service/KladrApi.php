<?php

namespace App\Service;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class KladrApi
{
	private $headers;

	private $url = 'http://kladr-api.ru/api.php';

	public function __construct()
	{
		$this->headers = [
			'Content-Type: application/json'
		];
	}

	/**
	 * Factory method
	 * @return KladrApi
	 */
	public static function factory()
	{
		return new self;
	}

	public function searchCity(string $name,int $regionId = null,int $limit = 1): array
	{
		$data = [
			'query' => $name,
			'contentType' => 'city',
			'regionId' => $regionId,
			'withParent' => 1,
			'limit' => $limit,
		];

		return $this->sendQuery($data);
	}

	public function searchStreet(int $cityKladr,string $name,int $limit = 1): array
	{
		$name = str_replace('-й','',$name);
		
		$data = [
			'query' => $name,
			'contentType' => 'street',
			'withParent' => 1,
			'cityId' => $cityKladr,
			'limit' => $limit,
		];

		return $this->sendQuery($data);

	}

	public function searchRegion(string $name,int $limit = 1): array
	{
		$data = [
			'query' => $name,
			'contentType' => 'region',
			'withParent' => 1,
			'limit' => $limit,
		];

		return $this->sendQuery($data);
	}

	public function getAddress(string $q,$limit = 50)
	{
		$data = [
			'query' => $q,
			'withParent' => 'true',
			'oneString' => 'true',
			'limit' => $limit,
		];

		return $this->sendQuery($data);
	}

	/**
	 * Make request to KLADR
	 * @param array $data
	 * @return array
	 */
	private function sendQuery(array $data): array
	{
		$uri = $this->url.'?'.http_build_query($data);

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		curl_setopt($ch, CURLOPT_URL,$uri);

		$status = json_decode(curl_exec($ch),true);

		curl_close($ch);

		if (!$status) {
			throw new BadRequestHttpException('Check parameters!');
		}
		if ($data['limit'] == 1) {
			return array_first(array_get($status,'result'));
		}
		return array_get($status,'result');
	}

}
