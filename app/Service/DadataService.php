<?php

namespace App\Service;

class DadataService
{
    /**
     * @param $fields
     * @return iterable|null
     */
	public static function suggest($fields): ?iterable
	{
		$result = null;

		if ($ch = curl_init('http://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address')) {
			curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);

			curl_setopt($ch, CURLOPT_HTTPHEADER, [
				'Content-Type: application/json',
				'Accept: application/json',
				'Authorization: Token 20131cc64a9d46aa2ea1d9437478bfcb1c1c66dc'
			]);

			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
			$result = curl_exec($ch);
			$result = json_decode($result, true);
			curl_close($ch);
		}

		return $result;
	}
}
