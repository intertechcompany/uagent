<?php

namespace App\Service;


class SmsService
{
	public const FROM = 'Uagent';

    private $headers;

    private $url = 'https://gate.smsaero.ru/v2/';

    public function __construct()
    {
        $this->headers = [
            'Authorization: Basic ' . base64_encode(config('services.smsaero.email') . ':' . config('services.smsaero.key')),
            'Content-Type: application/json'
        ];
    }

    public function send($number, $message, $channel = 'INFO', $sign = 'SMS Aero')
    {
        $url = $this->url . 'sms/send';
        $ch = curl_init($url);

        $data = [
            'number'  => $number,
            'text'    => $message,
            'channel' => $channel,
            'sign'    => $sign,
        ];

        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

        $status = json_decode(curl_exec($ch));

        curl_close($ch);

        return $status;

    }
}
