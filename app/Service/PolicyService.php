<?php

namespace App\Service;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class PolicyService
{
	public const DRIVER_COUNT_SPECIFIED = 'specified';
    public const DRIVER_COUNT_UNLIMITED = 'unlimited';

    public const VALIDATION_STEP_CAR = 'car';
    public const VALIDATION_STEP_CAR_DRIVERS = 'drivers';
    public const VALIDATION_STEP_CAR_INSURER = 'insurer';
    public const VALIDATION_STEP_CAR_OWNER = 'owner';

    /**
     * @param array $data
     * @throws ValidationException
     * @return bool
     */
	public function checkFullRules(array $data): bool
	{
		$rules = array_merge(
			$this->getValidationRulesByStep(self::VALIDATION_STEP_CAR, $data),
			$this->getValidationRulesByStep(self::VALIDATION_STEP_CAR_DRIVERS, $data),
			$this->getValidationRulesByStep(self::VALIDATION_STEP_CAR_OWNER, $data),
			$this->getValidationRulesByStep(self::VALIDATION_STEP_CAR_INSURER, $data)
		);

        /**
         * @var \Illuminate\Validation\Validator $validator
         */
		$validator = Validator::make($data, $rules);
		$validator->validate();

		return true;
	}

	/**
	 * @param string $step
	 * @param array $data
	 * @throws ValidationException
	 */
	public function validateStepData(string $step, array $data): void
	{
        /**
         * @var \Illuminate\Validation\Validator $validator
         */
		$validator = Validator::make($data, $this->getValidationRulesByStep($step, $data));
		$validator->validate();
	}

    /**
     * @param string $step
     * @param array $data
     * @return array
     */
	private function getValidationRulesByStep(string $step, array $data): array
	{
		switch ($step) {
		    	case self::VALIDATION_STEP_CAR:
				return $this->getCarRules(!empty($data['diagnostic_number']));

			case self::VALIDATION_STEP_CAR_DRIVERS:

				if (array_get($data, 'driver_count') === self::DRIVER_COUNT_SPECIFIED) {
					return $this->getDriversRules();
				}
                return $this->getPersonRulesByKey('owner');

			case self::VALIDATION_STEP_CAR_OWNER:

				return $this->getPersonRulesByKey('owner');

			case self::VALIDATION_STEP_CAR_INSURER:

				$rules = [
					'policy_start_date' => 'required|date|after:' . now()->format('Y-m-d').'|before:'.now()->addDays(60)->format('Y-m-d'),
					'phone' => 'required',
                    'email' => 'required|email'
				];

				if ((int)array_get($data, 'insurer_and_owner_same') === 0) {
					$rules = array_merge($this->getPersonRulesByKey('insurer'), $rules);
				}

				return $rules;
		}

		throw new \InvalidArgumentException('Wrong step');
	}

    /**
     * @param bool $diagnostic
     * @return array
     */
	private function getCarRules($diagnostic = false): array
	{
        $rules =  [
			'make_id' => 'required|exists:vehicle_makes,id',
			'model_id' => 'required|exists:vehicle_models,id',
			'vehicle_registration_year' => 'required|integer|min:1920|max:' . now()->year,
			'vehicle_id' => 'required|string',
			'power' => 'required|integer|min:1|max:9999',
			'vehicle_document_serial' => 'required|string|size:4',
			'vehicle_document_number' => 'required|string|max:6',
			'vehicle_document_date' => 'required|date',
			'is_vehicle_plate_absent' => 'bool',
			'is_vehicle_use_trailer' => 'bool',
		];

		if ($diagnostic) {
            $rules['diagnostic_number'] = 'required|string|max:21';
            $rules['diagnostic_until_date'] = 'required|date';
        }

        return $rules;
	}

    /**
     * @return array
     */
	private function getDriversRules(): array
	{
		return [
			'drivers' => 'array|min:1|max:5',
			'drivers.*.last_name' => 'required|string',
			'drivers.*.first_name' => 'required|string',
			'drivers.*.middle_name' => 'required|string',
			'drivers.*.dob' => 'required|date',
			'drivers.*.document_serial' => 'required|string',
			'drivers.*.document_number' => 'required|string',
			'drivers.*.start_date' => 'required|date',
		];
	}

    /**
     * @param string $key
     * @return array
     */
	private function getPersonRulesByKey(string $key): array
	{
		return [
			"$key.last_name" => 'required|string',
			"$key.first_name" => 'required|string',
			"$key.middle_name" => 'required|string',
			"$key.dob" => 'required|date',
			"$key.personal_document_date" => 'required|date',
			"$key.personal_document_serial" => 'required',
			"$key.personal_document_number" => 'required',
			"$key.address2" => 'required|string',
			"$key.address3" => 'required|string',
            "$key.house" => 'required|string',
			"$key.personal_document_issued_by" => 'required|string',
			"$key.place_of_birth" => 'required|string',
		];
	}
}
