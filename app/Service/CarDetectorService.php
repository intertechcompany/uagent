<?php

namespace App\Service;

use App\Model\VehicleMake;
use Carbon\Carbon;
use GuzzleHttp\{
    Client,
    Exception\ConnectException,
    Exception\RequestException,
    HandlerStack,
    Handler\CurlHandler,
    Middleware,
    Psr7\Response
};
use Illuminate\Http\Response as LaravelResponse;

class CarDetectorService
{
    /**
     * @var Client
     */
	protected $http;

	public function __construct()
	{
		$this->http = $this->getClient();
	}

    /**
     * @param string $plateNumber
     * @return iterable
     */
	public function handle(string $plateNumber): iterable
	{
		$response = $this->http->get("auto/v1.0/carinfo/$plateNumber");

		return $this->transformData(json_decode((string) $response->getBody(),true));
	}

    /**
     * @param iterable $data
     * @return iterable
     */
	private function transformData(iterable $data): iterable
	{
		$make = VehicleMake::query()
            ->where('name', array_get($data, 'brand.name'))
            ->first(['id'])
        ;

		if ($make === null) {
            return [
                'power' => array_get($data, 'power'),
                'vehicle_registration_year' => array_get($data, 'year'),
                'vehicle_id' => array_get($data, 'vin'),
                'diagnostic_number' => array_get($data, 'diagnosticCardNumber'),
                'diagnostic_until_date' => (new Carbon(array_get($data, 'diagnosticCardDateTo')))->format('m/d/Y')
            ];
        }
        $model = $make->models()->where('name', array_get($data, 'model.name'))->first(['id']);

		return [
			'power' => array_get($data, 'power'),
			'make_id' => optional($make)->id,
			'model_id' => optional($model)->id,
			'vehicle_registration_year' => array_get($data, 'year'),
			'vehicle_id' => array_get($data, 'vin'),
			'diagnostic_number' => array_get($data, 'diagnosticCardNumber'),
			'diagnostic_until_date' => (new Carbon(array_get($data, 'diagnosticCardDateTo')))->format('m/d/Y')
		];
	}

    /**
     * @return Client
     */
	private function getClient(): Client
	{
		$handler = HandlerStack::create(new CurlHandler());

		$decider = function (
			$retries,
			Response $response = null,
			RequestException $exception = null
		) {
			if ($retries >= 5) {
				return false;
			}

			if ($exception instanceof ConnectException) {
				return true;
			}

			if ($response && $response->getStatusCode() >= LaravelResponse::HTTP_BAD_GATEWAY) {
                return true;
			}

			return false;
		};

		$retry = function ($numberOfRetries) {
			return 1000 * $numberOfRetries;
		};

		$handler->push(Middleware::retry($decider, $retry));

		return new Client([
			'base_uri' => 'https://api.sravni.ru/',
			'handler' => $handler,
			'headers' => [
				'Accept' => 'application/xml',
				'user-agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36',
				'referer' => 'https://www.sravni.ru/osago/',
				'origin' => 'https://www.sravni.ru',
				'accept-language' => 'uk,ru;q=0.9,en-US;q=0.8,en;q=0.7,de;q=0.6,fr;q=0.5',
				'cache-control' => 'no-cache',
				'accept-encoding' => 'gzip, deflate, br',
				'pragma' => 'no-cache'
			]
		]);
	}
}
