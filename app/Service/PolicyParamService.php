<?php

namespace App\Service;

use App\Model\Policy;
use App\Model\PolicyParam;

class PolicyParamService
{
    /**
     * @param Policy $policy
     * @param string $key
     * @return PolicyParam
     */
    public function getParamOrCreate(Policy $policy, string $key): PolicyParam
    {
        $step = $policy
            ->params()
            ->where('key','=', $key)
            ->first()
        ;

        if ($step === null) {
            /**
             * @var $step PolicyParam
             */
            $step = $policy
                ->params()
                ->create([
                'key' => $key,
            ]);
        }

        return $step;
    }
}
