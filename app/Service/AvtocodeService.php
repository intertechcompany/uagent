<?php

namespace App\Service;

use GuzzleHttp\Client;

class AvtocodeService
{
    /**
     * Token for Api
     */
    private $token;

    private $client;

    private $user;

    private $pass;

    private $date;

    private $age;

    /**
     * @var string
     * URL for avtocad api
     */
    private $url = 'https://b2bapi.avtocod.ru/b2b/api/v1/';

    public function __construct()
    {
        $this->client = new Client();
        $this->user = config('services.avtocode.user');
        $this->pass = config('services.avtocode.pass');
        $this->date = config('services.avtocode.date');
        $this->age = config('services.avtocode.age');

        $this->token = json_decode($this->getToken());
    }

    /**
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getToken(): string
    {
        $response = $this->client->request(
            'GET',
            $this->url . 'dev/token?user='
            .$this->user.'&pass='
            .$this->pass.'&ishash=false&date='.$this->date.'&age='. $this->age
        );

        return (string) $response->getBody();
    }

    /**
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getReports()
    {
        $params = [
            'query' => [
                '_content' => true,
                '_query' => '_all'
            ]
        ];

        return $this->makeRequest($params, 'user/reports', 'GET');
    }

    /**
     * @param string $number
     * @param string $queryType
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function makeReport(string $number = '', string $queryType = 'GRZ')
    {
        $uid = env('AVTOCODE') === 'production' ? config('services.avtocode.uid_prod') :  config('services.avtocode.uid_test');

        $params = [
            'body' => "{'queryType': $queryType, 'query': $number}",
        ];

        $response = $this->makeRequest($params, 'user/reports/' . $uid . '/_make', 'POST');
        
        return $response[0];
    }

    /**
     * @param array $params
     * @param string $path
     * @param string $method
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function makeRequest(array $params, string $path, string $method)
    {
        $params['headers'] = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => $this->token->header,
        ];

        try {
            $response = $this->client->request($method, $this->url . $path, $params);
        } catch (\Exception $exception) {
            return null;
        }

        $result =  (string) $response->getBody();

        return json_decode((string) $result)->data;
    }
}