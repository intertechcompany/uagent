<?php

namespace App\Service;

use App\User;
use Illuminate\Support\Facades\Hash;

class AuthService
{
	public function register($data): User
	{
		$user = new User();

		$user->fill([
            'email' => $data['fileType'] === '2' ? $data['email'] : null,
            'phone' => $data['fileType'] === '1' ? preg_replace('~[^0-9]+~', '', $data['phone']): null,
            'password' => $data['fileType'] === '1' ? Hash::make($data['password']) : Hash::make($data['reg_password']),
            'verified_at' => now(),
            'is_email_confirmed' => 1,
            'role' => User::ROLE_GUEST
		]);

		$user->save();

        $user->setDefaultValues();

		return $user;
	}

	public function registerSubAgent(array $data): User
    {
        $user = new User($data);
        $user->role = User::ROLE_AGENT;
        $user->parent_user_id = auth()->id();
        $user->is_email_confirmed = 1;
        $user->is_opportunity_sub_agent = (int) array_get($data,'is_opportunity_sub_agent') === 1;
        $user->verified_at = now();
        $user->password = Hash::make(array_get($data,'password'));
        $user->level = (auth()->user()->level + 1);
        $user->save();

        $user->setDefaultValues();

       return $user;
    }

    public function createPassForSubAgent($data, User $user): void
    {
        $user->password = Hash::make($data['password']);
        $user->save();
    }
}
