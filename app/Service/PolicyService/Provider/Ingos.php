<?php

namespace App\Service\PolicyService\Provider;

use App\Helper\PolicyCreateHelper;
use App\Model\Policy;
use App\Service\PolicyService\Client\Ingos\Client;
use App\Service\PolicyService\ProviderInterface;
use App\Traits\IngosTrait;

class Ingos implements ProviderInterface
{
    use IngosTrait;

    /**
     * @var Client
     */
    protected $client;

    private const DICTI_ISN = 753518300;

    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * @param array $data
     * @return Policy|null
     */
    public function create(array $data): ?Policy
    {
        $this->initVars($data);

        $agreement = $this->generateDataForAgreement();
//        dd($agreement);
        $dicti = $this->client->getDicti(self::DICTI_ISN);
//        dd($dicti);
        $agr = $this->client->createAgreement($agreement);
        
        dd($agr);

        $data['premium'] = 0;
        $data['insuranceId'] = 0;
        $data['contractNumber'] = 0;

        $policyCreator = new PolicyCreateHelper($data,Policy::INGOS);

        return $policyCreator->createUpdate();
    }
}
