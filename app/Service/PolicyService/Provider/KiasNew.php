<?php

namespace App\Service\PolicyService\Provider;

use App\Helper\PolicyCreateHelper;
use App\Model\Policy;
use App\Model\VehicleMake;
use App\Model\VehicleModel;
use App\Service\PolicyService\Client\Kias\Client as KiasClient;
use App\Service\PolicyService\ProviderInterface;
use App\Traits\KiasNewTrait;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class KiasNew implements ProviderInterface
{
    use KiasNewTrait;

    public const DICT_ISN_CARS = 3366;
    public const DOC_CLASS_ISN_RF = 1165;
    public const STS_ISN = 220220;
    public const PTS_ISN = 220219;

    /**
     * ШТАТНЫЙ СОТРУДНИК
     */
    public const CLASS_ISN_STAFF = 2257;

    /**
     * Страхователь
     */
    public const CLASS_ISN_INSURER = 2103;

    /**
     * Собственник
     */
    public const CLASS_ISN_OWNER = 2099;

    protected $client;
    protected $vehicleType;
    protected $data;

    public function __construct()
    {
        $this->client = new KiasClient();
    }

    public function create(array $data): ?Policy
    {
        $this->data = $data;

        $multiDrive = array_get($data,'driver_count') === 'specified';

        $makes = $this->client->getDictiList(self::DICT_ISN_CARS);

        $makeName = VehicleMake::query()->findOrFail(array_get($data, 'make_id'))->name;

        $makes = $this->findMakes($makes,$makeName);

        $modelName = VehicleModel::query()->findOrFail(array_get($data, 'model_id'))->name;

        $model = null;

        foreach ($makes as $make) {

            $models = $this->client->getDictiList(array_get($make, 'ISN'));

            $model = array_first($models, function ($model) use ($modelName) {
                return array_get($model, 'FULLNAME') === $modelName;
            });

            if ($model !== null) {
                break;
            }
        }

        if ($model === null) {
            throw new \RuntimeException('Could not find VehicleModel.');
        }

        $drivers = [];

        if ($data['driver_count'] !== 'unlimited') {
            $drivers = $data['drivers'];

            foreach ($drivers as $driver) {
                $fullNameDr = $driver['last_name'] . ' ' . $driver['first_name'] . ' ' . $driver['middle_name'];
                $this->docsForSubject[$fullNameDr] = [
                    [
                        'row' => [
                            'DOCSER' =>  $driver['document_serial'],
                            'DOCNO' => $driver['document_number'],
                            'DOCDATE' => $this->formatDate($driver['start_date']),
                            'CLASSISN' => '1145'
                        ]
                    ]
                ];
                $this->driverStart[$fullNameDr] = $this->formatDate($driver['start_date']);
            }
        }

        $this->vehicleType = $this->getVehicleType(array_get($data,'vehicle_id_type'));

        unset($makes,$makeName,$models,$modelName);

        $carCategory = array_get($this->client->getCarClassByModel($model['ISN']),'CategoryISN',2252);

        $insurer = $this->getInsurer($data);

        $subjectISN = $this->saveSubject($this->client,$insurer,self::CLASS_ISN_STAFF);

        $city = $this->client->getCityList(null,$insurer['address']['data']['city_kladr_id'] ?? $insurer['address']['data']['settlement_kladr_id']);

        $newCalc =  $this->client->getAgreement();

        if ($multiDrive) {
            $this->saveDrivers($drivers,$this->client);
        }

        $dataCalc = [
            'EPOLICY' => 'Y',
            'STATUS' => 'С',
            'CLIENTISN' => $subjectISN,
            'PRODUCTISN' => 3387,
            'PRODUCTNAME' => 'ОСАГО',
            'CLASSISN' => 2889,
            'CLASSNAME' => 'Прямой договор',
            'CLIENTNAME' => array_get($insurer,'last_name') . ' ' . mb_substr(array_get($insurer,'first_name'),0,1) . '. ' . mb_substr(array_get($insurer,'middle_name'),0,1) . '.',
            'DATESIGN' => $this->formatDate(date('d.m.Y')),
            'TIMEBEG'  => $this->formatDate(date('d.m.Y')),
            'DATEBEG'  => ($data['policy_start_date'] ? $this->formatPolicyStart($data['policy_start_date']) : now()->addDay())->format('d.m.Y 00:00:00'),
            'DATEEND'  => $this->formatEndDate(array_get($data,'policy_start_date')),
            'DEPTISN' => '881441',
            'HASAGRSUM' => 'N',
            'AGROBJECT' => [
                'row' => [
                    'ClassISN' => 2118,
                    'SubClassISN' => 3366,
                    'ObjCount' => 1,
                    'AGROBJCAR' => [
                        'ModelISN' => array_get($model,'ISN'),
                        'MarkaISN' => array_get($make,'ISN'),
                        'ClassISN' => $carCategory,
                        'ReleaseDate' => '01.01.' . array_get($data,'vehicle_registration_year') . ' 0:00',
                        'PtsClassISN' => array_get($data,'vehicle_document_type') === 'sts' ? self::STS_ISN : self::PTS_ISN,
                        'PtsSer' => array_get($data,'vehicle_document_serial'),
                        'PtsNo' => array_get($data,'vehicle_document_number'),
                        'PtsDate' => $this->formatDate(array_get($data,'vehicle_document_date')),
                        'OsmotrClassISN' => 222777,
                        'OsmotrDate' =>  !empty(array_get($data,'diagnostic_until_date')) ? $this->formatEndDateDiagnostic(array_get($data,'diagnostic_until_date')) : '',
                        'OsmotrNextDate' => !empty(array_get($data,'diagnostic_until_date')) ? $this->formatDate(array_get($data,'diagnostic_until_date')) : '',
                        'OsmotrTalonNo' =>  array_get($data,'diagnostic_number'),
                        'DocSer' => array_get($data,'vehicle_document_serial'),
                        'DocNo' => array_get($data,'vehicle_document_number'),
                        'VIN' => 'ОТСУТСТВУЕТ',
                        'BODYID' => 'ОТСУТСТВУЕТ',
                        'CHASSISID' => 'ОТСУТСТВУЕТ',
                        'DocDate' => $this->formatDate(array_get($data,'vehicle_document_date')),
                        $this->vehicleType => array_get($data,'vehicle_id'),
                        'POWER' => array_get($data,'power'),
                        'OwnerJuridical' => 'N',
                        'CountryISN' => 9602,
                        'CarUseISN' => 4978,
                        'BonusMalusISN' => 4930,
                        'RefISN' => 4943,
                        'MultiDrive' => $multiDrive ? 'N' : 'Y',
                        'PeriodBeg' => $this->formatDate(array_get($data,'policy_start_date')),
                        'PeriodEnd' => $this->formatEndDate(array_get($data,'policy_start_date')),
                        'DurationStr' => '1 г (365)',
                        'TerritoryISN' => array_get($city,'ISN'),
                        'CategoryISN' => 10830,
                        'USESPECIALSIGNAL' => 'N',
                        'USETRAILER' => array_get($data,'is_vehicle_use_trailer') == false ? 'N' : 'Y',
                    ],
                    'AGRCOND' => [
                        'row' => [
                            'RiskName' => array_get($newCalc,'AGROBJECT.row.AGRCOND.row.RiskName'),
                            'InsClassName' => array_get($newCalc,'AGROBJECT.row.AGRCOND.row.InsClassName'),
                            'RiskISN' => array_get($newCalc,'AGROBJECT.row.AGRCOND.row.RiskISN'),
                            'RiskDictiName' => array_get($newCalc,'AGROBJECT.row.AGRCOND.row.RiskDictiName'),
                            'InsClassISN' => array_get($newCalc,'AGROBJECT.row.AGRCOND.row.InsClassISN'),
                            'DateSign' =>  $this->formatDate(date('d-m-Y')),
                            'DateBeg' =>  $this->formatDate(array_get($data,'policy_start_date')),
                            'DateEnd' =>  $this->formatEndDate(array_get($data,'policy_start_date')),
                            'Duration' => 365,
                            'CurrISN' => 9788,
                            'CurrSumISN' => '9788',
                            'LimitSum' => 400000,
                            'LimitSumType' => 'Н',
                            'Tariff' => 0,
                            'FranchType' => '-',
                            'FranchTariff' => 0,
                            'PaymentKoeff' => 1,
                        ]
                    ],
                ]
            ],
        ];

        if (array_get($data,'is_vehicle_plate_absent') !== true){
            $dataCalc['AGROBJECT']['row']['AGROBJCAR']['REGNO'] = array_get($data,'vehicle_plate_number') . array_get($data,'vehicle_plate_region');
        }

        $dataCalc['AGRROLE'] = $newCalc['AGRROLE'];

        $this->generateRole($drivers,$dataCalc['AGRROLE']['row'],$multiDrive);

        $dataCalc['AGROBJECT']['row']['AGROBJCAR']['OwnerISN'] = array_get(array_first($dataCalc['AGRROLE']['row'], function ($role) {
            return array_get($role, 'ClassISN') === self::CLASS_ISN_OWNER;
        }),'SubjISN',$subjectISN);

        $client2 = new KiasClient($this->client->getSession());

        $dataCalc['EPOLICY'] = 'Y';
        $dataCalc['EMPLISN'] = env('NASKO') === 'production' ? config('services.kias.production_curator') : config('services.kias.test_curator');
        $dataCalc['EMPLSHORTNAME'] = 'МИЛОВАНОВ Э.А.';

        unset($dataCalc['EMPLFULLNAME']);
        $agrISN = $client2->saveAgreement($dataCalc);
        $client2->getRsaosagoKbm($agrISN);
        $client2->calcAgreement($agrISN);

        // \Log::info([
        //     'key' => 'response',
        //     'data' => $client2
        // ]);
        
        if ($client2->checkForbidden()) {
            return null;
        }

        $agreement = $client2->getAgreement($agrISN);
        $premiumSum = str_replace(',','.',array_get($agreement,'AGROBJECT.row.AGRCOND.row.PremiumSum'));

        $tb = strtr(array_get($agreement,'AGROBJECT.row.AGRCOND.row.AGRCONDOSAGO.TB'), [','=>'.']);
        $kt = strtr(array_get($agreement,'AGROBJECT.row.AGRCOND.row.AGRCONDOSAGO.KT'), [','=>'.']);
        $kbm= strtr(array_get($agreement,'AGROBJECT.row.AGRCOND.row.AGRCONDOSAGO.KBM'), [','=>'.']);
        $kvs = strtr(array_get($agreement,'AGROBJECT.row.AGRCOND.row.AGRCONDOSAGO.KVS'), [','=>'.']);
        $kt = starts_with($kt, '.') ? "0{$kt}" : $kt;
        $kbm = starts_with($kbm, '.') ? "0{$kbm}" : $kbm;
        $kvs = starts_with($kvs, '.') ? "0{$kvs}" : $kvs;
        $tb = starts_with($tb, '.') ? "0{$tb}" : $tb;

        $data['tb'] = $tb;
        $data['kt'] = $kt;
        $data['kbm'] = $kbm;
        $data['kvs'] = $kvs;
        $data['premium'] = $premiumSum;
        $data['insuranceId'] = $agrISN;

        $policyCreator = new PolicyCreateHelper($data,Policy::NASKO);

        return $policyCreator->create();
    }
}
