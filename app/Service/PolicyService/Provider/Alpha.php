<?php

namespace App\Service\PolicyService\Provider;

use Carbon\Carbon;
use App\Helper\PolicyCreateHelper;
use App\Model\Policy;
use App\Model\PolicyParam;
use App\Service\PolicyService\Client\Alpha\{
	EOsagoRelaunchClient, OsagoCalcRelaunchClient, PartnersInteractionClient
};
use App\Service\PolicyService\ProviderInterface;

use App\Traits\AlphaTrait;
use Illuminate\Support\Facades\Log;

class Alpha implements ProviderInterface
{
	use AlphaTrait;

	public function create(array $data): ?Policy
	{
		$now = new Carbon();
		$policyStart = Carbon::createFromFormat('d-m-Y',$data['policy_start_date']);

		if ($policyStart->diffInDays($now) < 3) {
			throw new \RuntimeException('Must be min 3 days');
		}

		if (array_get($data, 'insurer_and_owner_same')) {
			$data['insurer'] = $data['owner'];
		}

		$partners = new PartnersInteractionClient();
		$upid = $partners->getUpid('E_MILOV');

		unset($partners);

		$generateData = $this->generateData($data,$upid);

		$relaunch = new EOsagoRelaunchClient();

		$checkVehicle = $relaunch->loadVehicle($this->vechicleData($data));

		$checkOwner = $relaunch->loadInsurerOwner($this->insurerOwnerData($data,'owner'));

		$checkInsurer = $relaunch->loadInsurerOwner($this->insurerOwnerData($data,'insurer'));

		$drivers = array_get($data,'drivers');

        if (array_get($data,'driver_count') === 'specified') {
			foreach ($drivers as $key => $driver) {
				$drivers[$key]['IDCheckDriver'] = $relaunch->loadDriver($this->generateDriverForCheck($driver));
			}
		}

		$osagoPrem = new OsagoCalcRelaunchClient();
		$document = $osagoPrem->calcEOsago($generateData);

		 /*Log::info([
             'key' => 'response',
            'data' => $document
         ]);*/
        
		unset($osagoPrem);

		$contract = $relaunch->loadContractEosago($this->contractData($data,$upid,$document,$checkVehicle,$checkInsurer,$checkOwner,$drivers));
        $document = (object)$document;

        if(!empty($document->Drivers)) {
            foreach($data['drivers'] as $key => &$driver) {
                if(!empty($document->Drivers->Driver) && is_array($document->Drivers->Driver)) {
                    $drivers = $document->Drivers->Driver;
                } else {
                    $drivers = $document->Drivers;
                }

                foreach ($drivers as $_driver) {
                    if ($_driver->LastName == $driver['last_name'] && $_driver->FirstName == $driver['first_name'] && $_driver->MiddleName == $driver['middle_name']) {
                        $driver['kbm'] = $_driver->KMBValue;
                    }
                }
            }
        }

        $kt = strtr(@$document->Coefficients->Kt, [',' => '.']);
        $kbm = strtr(@$document->Coefficients->Kbm, [',' => '.']);
        $kvs = strtr(@$document->Coefficients->Kvs, [',' => '.']);
        $tb = strtr(@$document->Coefficients->Tb, [',' => '.']);

		unset($relaunch,$document,$checkVehicle,$checkOwner,$checkInsurer,$drivers);

		$contractId = array_get($contract,'ContractId');
		$contractNumber = array_get($contract,'ContractNumber');

		$premium = array_get($contract,'ContractPremium');

		if ($premium === 0.0) {
			return null;
		}

        $data['tb'] = $tb;
        $data['kt'] = $kt;
        $data['kbm'] = $kbm;
        $data['kvs'] = $kvs;
		$data['premium'] = $premium;
		$data['insuranceId'] = $contractId;
		$data['contractNumber'] = $contractNumber;

		$policyCreator = new PolicyCreateHelper($data,Policy::ALPHA);

		$policy = $policyCreator->create();

		$policy
            ->params()
            ->create([
                'key' => PolicyParam::ALPHA_UPID,
                'value' => $upid,
            ])
        ;

		return $policy;
	}
}
