<?php

namespace App\Service\PolicyService\Provider;

use App\Model\Policy;
use App\Service\PolicyService\ProviderInterface;

final class InsuranceStaticFactory
{
	/**
	 * @param string $type
	 * @return ProviderInterface
	 */
	public static function factory(string $type): ProviderInterface
	{
		if ($type === Policy::ALPHA) {
			return new Alpha();
		}

		if ($type === Policy::NASKO) {
			return new KiasNew();
		}


		if ($type === Policy::INGOS) {
		    return new Ingos();
        }

		if ($type === Policy::RGS) {
			return new Rgs();
		}

		throw new \InvalidArgumentException('Unknown insurance given');
	}
}
