<?php

namespace App\Service\PolicyService\Provider;

use Carbon\Carbon;
use Log;
use Exception;
use App\Model\Policy;
use App\Traits\RgsTrait;
use App\Model\PolicyParam;
use App\Service\DadataService;
use App\Helper\PolicyCreateHelper;
use App\Service\PolicyService\ProviderInterface;
use App\Service\PolicyService\Client\Rgs\CurlClient;

use App\Service\PolicyService\Sign\Rgs as RgsSign;

class Rgs implements ProviderInterface
{
	use RgsTrait;

	public function create(array $data): ?Policy
	{
        $now = new Carbon();
        $policyStart = Carbon::createFromFormat('d-m-Y',$data['policy_start_date']);

        if ($policyStart->diffInDays($now) < 3) {
            throw new \RuntimeException('Must be min 3 days');
        }

		/**
		 * if index insurer_and_owner_same = 1, insurer = owner
		 */ 
		if (array_get($data, 'insurer_and_owner_same') == 1) {
			$data['insurer'] = $data['owner'];
		}

		/**
		 * Generate data array for request to RGS
		 */
		$generateData = $this->generateData($data);
		$result = (new CurlClient('calculate'))->request($generateData);

		/**
		 * Log response from rgs
		 */
		Log::info([
            'key' => 'response calculate',
            'data' => $result
        ]);

		if (array_get($result, 's:Body.CalculateResponse.CalculateResult.b:IsSuccess')  == 'false') {
			return null;
		}

		$finalData = $this->generateDataFinal($data, $result);
		$FinalResult = (new CurlClient('finalCalculate'))->request($finalData);

		Log::info([
            'key' => 'response finalCalculate',
            'data' => $FinalResult
        ]);

		if (array_get($FinalResult, 's:Body.CalculateResponse.CalculateResult.b:IsSuccess') == 'false') {
			return null;
		}

		$premium = array_get($FinalResult, 's:Body.CalculateResponse.CalculateResult.b:Data.Premium');
		$number = array_get($FinalResult, 's:Body.CalculateResponse.CalculateResult.b:Data.Number');
		$calculationId = array_get($FinalResult, 's:Body.CalculateResponse.CalculateResult.b:Data.CalculationId');

		if ($premium == '0.0') {
			return null;
		}

        $kt = array_get($FinalResult, 's:Body.CalculateResponse.CalculateResult.b:Data.K.Kt');
        $kbm = array_get($FinalResult, 's:Body.CalculateResponse.CalculateResult.b:Data.K.Kbm');
        $kvs = array_get($FinalResult, 's:Body.CalculateResponse.CalculateResult.b:Data.K.Kbc');
        $tb = array_get($FinalResult, 's:Body.CalculateResponse.CalculateResult.b:Data.K.Tb');

        $data['tb'] = $tb;
        $data['kt'] = $kt;
        $data['kbm'] = $kbm;
        $data['kvs'] = $kvs;

		/**
		 * Premium policy
		 */
		$data['premium'] = $premium;

		/**
		 * Id policy form rgs
		 */
		$data['insuranceId'] = $number;

		/**
		 * Process create policy and attached to user
		 */
		try {
			$policyCreator = new PolicyCreateHelper($data, Policy::RGS);

			$policy = $policyCreator->create();

	        /**
	         * Attach policy to user
	         */
			$policy->user_id = array_get($data, 'user_id');
	        $policy->insurance = Policy::RGS;
	        $policy->save();

	        /**
			 * Add calculation id param for policy
			 */
			$policy
	            ->params()
	            ->create([
	                'key' => PolicyParam::RGS_CALCULATION_ID,
	                'value' => $calculationId,
	            ])
	        ;

			return $policy;
		} catch (Exception $e) {}

		return null;
	}
}
