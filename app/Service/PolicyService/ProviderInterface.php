<?php

namespace App\Service\PolicyService;

use App\Model\Policy;

interface ProviderInterface
{
	public function create(array $data): ?Policy;
}
