<?php

namespace App\Service\PolicyService\Sign;

use App\Model\Policy;

class Alpha implements SignInterface
{
	public function sign(Policy $policy): bool
	{
		$policy->status = 'success';
		$policy->save();
		return true;
	}
}