<?php

namespace App\Service\PolicyService\Sign;

use App\Model\Policy;
use App\Service\PolicyService\Client\Ingos\Client;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class Ingos implements SignInterface
{
    /**
     * @var Client
     */
    private $client;

    /**
     * Ingos constructor.
     */
    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * @param Policy $policy
     * @return BadRequestHttpException|\Exception
     */
    public function sign(Policy $policy)
    {
        try {
            $insuranceNumber = $this->client->makeEOsago($policy->insurance_id);
        } catch (BadRequestHttpException $exception) {
            return $exception;
        }

        $policy->insurance_number = $insuranceNumber;
        $policy->save();
    }
}
