<?php

namespace App\Service\PolicyService\Sign;

use App\Model\Policy;
use App\Service\PolicyService\Document\Rgs as RgsDocument;
use App\Service\PolicyService\Client\Ingos\Client;
use App\Service\PolicyService\Sign\Rgs\PrintPolicy;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class Rgs implements SignInterface
{
    /**
     * @var Client
     */
    private $client;

    private $document;

    /**
     * Ingos constructor.
     */
    public function __construct()
    {
        $this->document = new RgsDocument();
    }

    /**
     * @param Policy $policy
     * @return \Exception|BadRequestHttpException
     */
    public function sign(Policy $policy)
    {
        if ($document = $this->document->getPolicyFile($policy)) {
            return true;
        }

        return false;

        try {
            $print = new PrintPolicy($policy);

            $result = $print->request();

            \Log::info([
                'key' => 'response Print',
                'data' => $result
            ]);

            $policy->status = 'success';
            $policy->save();
        } catch (BadRequestHttpException $exception) {
            return $exception;
        }

        return true;
    }
}
