<?php

namespace App\Service\PolicyService\Sign\Rgs;

use App\Model\Owner;
use App\Model\Policy;
use App\Service\DadataService;

class Address
{
    /**
     * @var string
     */
    private $address;

    /**
     * Ingos constructor.
     */
    // public function __construct(Policy $policy)
    // {
    //     $this->address = $this->findAddress($policy->owner);
    // }

    public static function factory(): self
    {
        return new self;
    }

    public function setPolicy(Policy $policy, string $type)
    {
        if (!empty($policy->$type)) {
            $this->address = $this->findAddress($policy->$type);
        }

        return $this;
    }

    private function findAddress(Owner $user): array
    {
        $address = DadataService::suggest([
            'query' => $user->address,
            'count' => 1
        ])['suggestions'];

        return !empty($address) && count($address) > 0 ? array_shift($address) : [];
    }

    public function getUserAddress($postCode = null)
    {
        return [
            'rgs1:Area' => [
                '#' => null
            ],
            // 'rgs1:Building' => '2',
            'rgs1:Building' => [
                '#' => null
            ],
            'rgs1:City' => array_get($this->address, 'data.city_with_type'),
            'rgs1:CountryCode' => '643',
            'rgs1:Flat' => array_get($this->address, 'data,Flat'),
            // 'rgs1:Flat' => [
            //     '#' => null
            // ],
            'rgs1:FullAddress' => $this->formedFullAddress($postCode),
            'rgs1:House' => array_get($this->address, 'house'),
            'rgs1:KladrCode' => array_get($this->address, 'data.city_kladr_id'),
            'rgs1:Place' => [
                '#' => null
            ],
            'rgs1:PostCode' => $postCode, // need add column to db
            'rgs1:Region' => array_get($this->address, 'data.region_with_type'),
            'rgs1:Street' => array_get($this->address, 'data.street_with_type'),
        ];
    }

    private function formedFullAddress(string $postCode = null): ?string
    {
        $fullAddress = $postCode . ', Российская Федерация, ' . array_get($this->address, 'data.city_with_type') . ', ' . array_get($this->address, 'data.street_with_type');

        if (array_get($this->address, 'data.house') != null) {
            $fullAddress .= ', ' . array_get($this->address, 'data.house');
        }

        if (array_get($this->address, 'data.flat') != null) {
            $fullAddress .= ', кв ' . array_get($this->address, 'data.flat');
        }

        return $fullAddress;
    }

    public function setUser(Owner $user)
    {
        $this->address = $this->findAddress($user);

        return $this;
    }
}
