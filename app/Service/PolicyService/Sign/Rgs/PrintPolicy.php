<?php

namespace App\Service\PolicyService\Sign\Rgs;

use Carbon\Carbon;
use App\Model\Policy;
use App\Model\PolicyParam;
use App\Service\DadataService;
use App\Service\PolicyService\Sign\Rgs\Address;
use App\Service\PolicyService\Client\Rgs\CurlClient;

class PrintPolicy
{
    /**
     * @var Client
     */
    private $policy;

    /**
     * @var array
     */
    private $address = [];

    /**
     * @var string
     */
    private $timeFormat = 'Y-m-d\T';

    /**
     * Ingos constructor.
     */
    public function __construct(Policy $policy)
    {
        $this->policy = $policy;
        // $this->address = new Address($policy);
    }

    public function request()
    {
        $data = $this->formedPolicy();

        return (new CurlClient('Print'))->request($data);
    }

    /**
     * Method for to write policy after check status policy is paid
     *
     * @param array $data
     * @param array $responce
     * @return array
     */
    public function formedPolicy(): array
    {
        $data = [
            'rgs:printRequest' => [
                'rgs1:CalculationId' => $this->getCalculationId(),
                'rgs1:ContractDate' => [
                    '@xsi:type' => 'dateTime',
                    '@xmlns:xsi' => 'xsi',
                    '#' => Carbon::parse($this->policy->created_at)->format($this->timeFormat) . '00:00:00',
                ],
                'rgs1:Draft' => 'false',
                'rgs1:InspectionDocument' => [
                    'rgs:Series' => [
                        '#' => null
                    ],
                    'rgs:TypeCode' => '53',
                ],
                'rgs1:InsurantAddress' => Address::factory()
                    ->setPolicy($this->policy, 'insurer')
                    ->setUser($this->policy->insurer)
                    ->getUserAddress($this->policy->insurer->post_code),

                // For DK
                'rgs1:InsurantContacts' => [
                    'rgs1:HomePhone' => [
                        '#' => null
                    ],
                    'rgs1:MobilePhone' => $this->generateMobilePhone(),
                    'rgs1:WorkPhone' => [
                        '#' => null
                    ],
                    'rgs1:Email' => $this->policy->email,
                ],
                'rgs1:Insurer' => $this->getInsurerFullName(),
                'rgs:Number' => $this->policy->insurance_id,
                'rgs:OtherInformation' => [
                    '#' => null
                ],
                'rgs1:OwnerAddress' => Address::factory()
                    ->setPolicy($this->policy, 'owner')
                    ->setUser($this->policy->owner)
                    ->getUserAddress($this->policy->owner->post_code),
                'rgs:PrintOnlyDocumentType' => 'Default',
                'rgs:Series' => 'ХХХ',
                'rgs:SpecialNotes' => [
                    '#' => null
                ],
                'rgs:SpecialNotesE' => [
                    '#' => null
                ],
            ]
        ];

        // For DK
        if ($this->policy && $this->policy->diagnostic_number) {
            $data['rgs:printRequest']['rgs1:InspectionDocument'] = [
                'rgs:IssueDate' => [
                    '@xsi:type' => 'dateTime',
                    '@xmlns:xsi' => 'xsi',
                    '#' => Carbon::parse($this->policy->diagnostic_until_date)->format($this->timeFormat) . '00:00:00',
                ],
                'rgs:Number' => $this->policy->diagnostic_number,
                'rgs:Series' => [
                    '#' => null
                ],
                'rgs:TypeCode' => 53,
            ];
        }

        return $data;
    }

    /**
     * Get calculation id in params policy model
     *
     * @return ?string
     */
    private function getCalculationId(): string
    {
        $param = $this->policy->findParam(PolicyParam::RGS_CALCULATION_ID);

        return $param ? $param->value : null;
    }

    private function getInsurerFullName(): array
    {
        if (!$this->policy->insurer) {
            return [];
        }

        return [
            'rgs:FirstName' => 'Д.',
            'rgs:LastName' => 'Пурсанов',
            'rgs:SecondName' => 'В.',
        ];
    }

    private function shorName(string $value = null): ?string
    {
        return mb_strimwidth($value, 0, 1) . '.';
    }

    private function generateMobilePhone()
    {
        return $this->formatPhoneNumber($this->policy->phone);
    }

    public function formatPhoneNumber($phoneNumber)
    {
        $phoneNumber = preg_replace('/[^0-9]/','',$phoneNumber);

        if (strlen($phoneNumber) > 10) {
            $countryCode = substr($phoneNumber, 0, strlen($phoneNumber)-10);
            $areaCode = substr($phoneNumber, -10, 3);
            $nextThree = substr($phoneNumber, -7, 3);
            $lastFour = substr($phoneNumber, -4, 4);

            $phoneNumber = '+'.$countryCode.'('.$areaCode.')'.$nextThree.'-'.$lastFour;
        } else if (strlen($phoneNumber) == 10) {
            $areaCode = substr($phoneNumber, 0, 3);
            $nextThree = substr($phoneNumber, 3, 3);
            $lastFour = substr($phoneNumber, 6, 4);

            $phoneNumber = '('.$areaCode.')'.$nextThree.'-'.$lastFour;
        } else if (strlen($phoneNumber) == 7) {
            $nextThree = substr($phoneNumber, 0, 3);
            $lastFour = substr($phoneNumber, 3, 4);

            $phoneNumber = $nextThree.'-'.$lastFour;
        }

        return trim($phoneNumber);
    }
}
