<?php

namespace App\Service\PolicyService\Sign;

use App\Model\Policy;
use App\Service\PolicyService\Client\Kias\Client;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class Kias implements SignInterface
{
	private $client;

	public function __construct()
	{
		$this->client = new Client();
	}

	public function sign(Policy $policy)
	{
		ini_set('max_execution_time', 300);

		try {

			$this->client->ePolicyCreateAndPrintEPolicy($policy->insurance_id);

			$this->client->setAgrStatus($policy->insurance_id,'П');

			$this->client->rsaEPolicySetProjectStatus($policy->insurance_id);

			$policy->status = 'success';

			$policy->save();
		} catch (BadRequestHttpException $exception) {
			return $exception;
		}

		return true;
	}
}
