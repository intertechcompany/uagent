<?php

namespace App\Service\PolicyService\Sign;

use App\Model\Policy;

final class SignStaticFactory
{
	/**
	 * @param string $type
	 * @return SignInterface
	 */
	public static function factory(string $type): SignInterface
	{
		if ($type === Policy::ALPHA) {
			return new Alpha();
		}

		if ($type === Policy::NASKO) {
			return new Kias();
		}

        if ($type === Policy::INGOS) {
            return new Ingos();
        }

        if ($type === Policy::RGS) {
            return new Rgs();
        }

		throw new \InvalidArgumentException('Unknown insurance given');
	}
}
