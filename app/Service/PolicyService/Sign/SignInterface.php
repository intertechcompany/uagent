<?php

namespace App\Service\PolicyService\Sign;

use App\Model\Policy;

interface SignInterface
{
    /**
     * @param Policy $policy
     * @return mixed/Exception
     */
	public function sign(Policy $policy);
}
