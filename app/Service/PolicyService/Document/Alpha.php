<?php

namespace App\Service\PolicyService\Document;

use App\Model\Policy;
use App\Model\PolicyDocument;
use App\Model\PolicyParam;
use App\Service\PolicyService\Client\Alpha\GetContractSignedClient;
use Illuminate\Support\Facades\Storage;

class Alpha implements DocumentInterface
{
    /**
     * @var GetContractSignedClient
     */
    private $client;

    /**
     * Alpha constructor.
     */
    public function __construct()
    {
        $this->client = new GetContractSignedClient();
    }

    /**
     * @param Policy $policy
     * @return PolicyDocument
     */
    public function getPolicyFile(Policy $policy): PolicyDocument
    {
        $policyDocument = $policy
            ->documents()
            ->where('type',PolicyDocument::POLICY_FILE)
            ->first()
        ;

        if ($policyDocument === null || !$policyDocument->fileExist()) {

            $upid = $policy
                ->params()
                ->where('key',PolicyParam::ALPHA_UPID)
                ->first()
                ->value
            ;

            $params = [
                'UPID' => $upid,
                'ContractId' => $policy->insurance_id,
            ];

            $file = $this->client->getContractSigned($params);

            $fileName = PolicyDocument::POLICY_FILE . "_{$policy->insurance_id}_.pdf";

            Storage::disk(PolicyDocument::STORAGE_DIST)->put($fileName,array_get($file,'Content'));

            if(empty($policyDocument)) {
                $policyDocument = new PolicyDocument();
            }

            $policyDocument->policy_id = $policy->id;
            $policyDocument->type = PolicyDocument::POLICY_FILE;
            $policyDocument->file_name = $fileName;
            $policyDocument->save();
        }

        return $policyDocument;
    }

    public function getInvoiceFile(Policy $policy): PolicyDocument
    {
//        :TODO
    }
}
