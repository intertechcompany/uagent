<?php

namespace App\Service\PolicyService\Document;

use App\Model\Policy;
use App\Model\PolicyDocument;

interface DocumentInterface
{
    /**
     * @param Policy $policy
     * @return PolicyDocument
     */
    public function getPolicyFile(Policy $policy): PolicyDocument;

    /**
     * @param Policy $policy
     * @return PolicyDocument
     */
    public function getInvoiceFile(Policy $policy): PolicyDocument;
}
