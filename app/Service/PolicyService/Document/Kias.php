<?php

namespace App\Service\PolicyService\Document;

use App\Model\Policy;
use App\Model\PolicyDocument;
use App\Service\PolicyService\Client\Kias\Client;
use Illuminate\Support\Facades\Storage;

class Kias implements DocumentInterface
{
    /**
     * @var Client
     */
    private $client;

    /**
     * Kias constructor.
     */
    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * @param Policy $policy
     * @throws DocumentException
     * @return PolicyDocument
     */
    public function getPolicyFile(Policy $policy): PolicyDocument
    {
        $policyDocument = $policy
            ->documents()
            ->where('type',PolicyDocument::POLICY_FILE)
            ->first()
        ;

        if ($policyDocument === null) {
            $file = $this->client->getPolicyDocument($policy->insurance_id);

            if (array_get($file,'FILENAME') === null) {
                throw new DocumentException('Возникла ошибка со стороны страховой компании, попробуйте немного позже.');
            }

            $fileName = PolicyDocument::POLICY_FILE . "_{$policy->id}_" . array_get($file,'FILENAME');

            Storage::disk(PolicyDocument::STORAGE_DIST)->put($fileName,base64_decode(array_get($file,'FILEDATA'), true));

            $policyDocument = new PolicyDocument();
            $policyDocument->policy_id = $policy->id;
            $policyDocument->type = PolicyDocument::POLICY_FILE;
            $policyDocument->file_name = $fileName;
            $policyDocument->save();
        }

        return $policyDocument;
    }

    public function getInvoiceFile(Policy $policy): PolicyDocument
    {
        // TODO: Implement getInvoiceFile() method.
    }
}
