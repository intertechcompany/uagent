<?php

namespace App\Service\PolicyService\Document;

use App\Model\Policy;
use Illuminate\Support\Str;
use App\Model\PolicyDocument;
use Illuminate\Support\Facades\Storage;
use App\Service\PolicyService\Sign\Rgs\PrintPolicy;

class Rgs implements DocumentInterface
{
    public function getPolicyFile(Policy $policy): PolicyDocument
    {
        $policyDocument = $policy
            ->documents()
            ->where('type', PolicyDocument::POLICY_FILE)
            ->orderBy('created_at', 'desc')
            ->first()
        ;

        if ($policyDocument === null) {
            $print = new PrintPolicy($policy);

            $data = $print->request();

            \Log::info([
                'key' => 'response Print',
                'data' => $data
            ]);

            $documents = $this->getDocuments($data);

            $osago = $this->getDocumentOsago($documents);
            $contractOsago = $this->getDocumentContractOsago($documents);
            $notifyOsago = $this->getDocumentNotifyOsago($documents);
            $copyInsurance = $this->getDocumentCopyInsurance($documents);

            $this->storeDocument($policy, $contractOsago);
            $this->storeDocument($policy, $notifyOsago);
            $this->storeDocument($policy, $copyInsurance);

            return $this->storeDocument($policy, $osago, true);
        }

        return $policyDocument;
    }

    private function storeDocument($policy, $file, $isCreate = false): PolicyDocument
    {
        $fileName = PolicyDocument::POLICY_FILE . "_{$policy->id}_" .   Str::snake(array_get($file, 'c:Name'));

        Storage::disk(PolicyDocument::STORAGE_DIST)->put($fileName,base64_decode(array_get($file, 'c:Document')));

        $policyDocument = new PolicyDocument();

        if (true === $isCreate) {
            $policyDocument->policy_id = $policy->id;
            $policyDocument->type = PolicyDocument::POLICY_FILE;
            $policyDocument->file_name = $fileName;
            $policyDocument->save();

            $policy->status = 'success';
            $policy->save();
        }

        return $policyDocument;
    }

    /**
     * Method for get document OSAGO policy
     * PDF file
     *
     * @param array $data
     * @return array
     */
    private function getDocumentOsago(array $data): array
    {
        return !empty($data[0]) ? $data[0] : [];
    }

    /**
     * Application for a contract OSAGO policy
     * PDF file
     * 
     * @param array $data
     * @return array
     */
    private function getDocumentContractOsago(array $data): array
    {
        return !empty($data[1]) ? $data[1] : [];
    }

    /**
     * Document notification
     * PDF file
     * 
     * @param array $data
     * @return array
     */
    private function getDocumentNotifyOsago(array $data): array
    {
        return !empty($data[2]) ? $data[2] : [];
    }

    /**
     * A copy of the insurance policy for the insurer
     * PDF file
     * 
     * @param array $data
     * @return array
     */
    private function getDocumentCopyInsurance(array $data): array
    {
        return !empty($data[3]) ? $data[3] : [];
    }

    private function getDocuments(array $data)
    {
        return $file = array_get($data, 's:Body.PrintResponse.PrintResult.b:Data.c:PrintResult');
    }

    public function getInvoiceFile(Policy $policy): PolicyDocument
    {
        // TODO: Implement getInvoiceFile() method.
    }
}
