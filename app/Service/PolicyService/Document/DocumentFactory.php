<?php

namespace App\Service\PolicyService\Document;

use App\Model\Policy;

final class DocumentFactory
{
    /**
     * @param string $type
     * @return DocumentInterface
     */
	public static function factory(string $type): DocumentInterface
	{
		if ($type === Policy::ALPHA) {
			return new Alpha();
		}

		if ($type === Policy::NASKO) {
            return new Kias();
        }

        if ($type === Policy::RGS) {
            return new Rgs();
        }

		throw new \InvalidArgumentException('Unknown insurance given');
	}
}
