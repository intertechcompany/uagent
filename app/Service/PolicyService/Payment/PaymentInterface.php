<?php

namespace App\Service\PolicyService\Payment;

use App\Model\Policy;
use App\Model\PolicyPayment;

interface PaymentInterface
{
	public function create(Policy $policy): PolicyPayment;

    /**
     * @param Policy $policy
     * @throws PaymentException
     * @return mixed
     */
	public function checkPayment(Policy $policy);
}
