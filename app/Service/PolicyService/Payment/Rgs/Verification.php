<?php

namespace App\Service\PolicyService\Payment\Rgs;

use Exception;
use App\Model\Policy;
use App\Model\PolicyParam;
use App\Model\PolicyPayment;
use Illuminate\Http\Request;
use App\Service\PolicyService\Payment\Rgs;
use App\Service\PolicyService\Client\Rgs\CurlClient;

class Verification
{
    /**
     * @var CurlClient
     */
    private $client;

    private $policy;

    private $method;

    /**
     * @param \App\Model\Policy $policy
     * @param string $method
     */
    public function __construct(Policy $policy, string $method = 'SendVerificationCode')
    {
        $this->policy = $policy;
        $this->method = $method;
    }

    /**
     * @param return bool
     */
    public function SendVerificationCode(): array
    {
        try {
            $client = new CurlClient($this->method);

            $dataConfirmPhone = [
                'rgs:sendVerificationCodeRequest' => [
                    'rgs:ContractId' => $this->getCalculationId($this->policy),
                    'rgs:Mobile' => $this->policy->phone,
                ],
            ];

            $responceCode = $client->request($dataConfirmPhone);

            \Log::info([
                'key' => 'response sendVerificationCodeRequest',
                'data' => $responceCode
            ]);

            if (array_get($responceCode, 's:Body.SendVerificationCodeResponse.SendVerificationCodeResult.b:IsSuccess') == 'true') {
                return ['is_success' => true];
            }

            $keyValidatte = array_get($responceCode, 's:Body.SendVerificationCodeResponse.SendVerificationCodeResult.b:ValidationCodeErrors.c:KeyValuePairOfstringstring.c:key');

            /**
             * If phone number is verified
             */
            if ($keyValidatte == 'OSAGO_4122_3') {
                return [
                    'is_success' => true,
                    'is_verified' => true,
                    'url' => $this->getFormUrl(),
                ];
            }

            return ['is_success' => false];
        } catch (Exception $e) {}

        return false;
    }

    /**
     * @param return array
     */
    public function ConfirmVerificationCode(Request $request): array
    {
        $request->validate([
            'code' => 'required',
        ], [], [
            'code' => 'Код'
        ]);

        if (!$request->has('code')) {
            return ['is_success' => false];
        }

        try {
            $clientConfirm = new CurlClient($this->method);

            $dataConfirmPhoneCode = [
                'rgs:confirmVerificationCodeRequest' => [
                    'rgs:ContractId' => $this->getCalculationId($this->policy),
                    'rgs:Mobile' => $this->policy->phone,
                    'rgs:VerificationCode' => $request->get('code'),
                ],
            ];

            /**
             * Get reslut verify phone form rgs
             */
            $responce = $clientConfirm->request($dataConfirmPhoneCode);

            \Log::info([
                'key' => 'response confirmVerificationCodeRequest',
                'data' => $responce
            ]);

            return $this->getPayment($responce);
        } catch (Exception $e) {}

        return ['is_success' => false];
    }

    private function getPayment(array $responce): array
    {
        $result = array_get($responce, 's:Body.ConfirmVerificationCodeResponse.ConfirmVerificationCodeResult');

        if (array_get($result, 'b:IsSuccess') == true) {
            return $this->getDataPayment();
        }

        $errorKey = array_get($result, 'b:ValidationCodeErrors.c:KeyValuePairOfstringstring.c:key') ? true : false;

        /**
         * If phone number is verified
         */
        if ($errorKey == 'OSAGO_4122_3') {
            return $this->getDataPayment();
        }

        /**
         * If phone number is not corrected
         */
        // if ($keyValidatte == 'not corrected') {
        //     return [
        //         'is_success' => true,
        //         'is_verified' => false,
        //     ];
        // }

        return ['is_success' => false];
    }

    private function getDataPayment(): array
    {
        return [
            'is_success' => true,
            'is_verified' => true,
            'is_confirm' => true,
            'url' => $this->getFormUrl(),
        ];
    }

    /**
     * Get paymeny link for pay policy
     *
     * @return string
     */
    public function getFormUrl(): ?string
    {
        $rgs = new Rgs();

        if ($payment = $rgs->create($this->policy)) {
            return $payment->formUrl;
        }
        
        return null;
    }

    public function saveConfirmPhone(): array
    {
        try {
            $this->policy->is_verify_phone_rgs = true;
            $this->policy->save();

            return [
                'is_success' => true,
                'is_verified' => true,
                'is_confirm' => true,
                'url' => $this->getFormUrl(),
            ];
        } catch (Exception $e) {}

        return ['is_success' => false];
    }

    /**
     * Get calculation id in params policy model
     *
     * @return ?string
     */
    private function getCalculationId(Policy $policy): string
    {
        $param = $policy->findParam(PolicyParam::RGS_CALCULATION_ID);

        return $param ? $param->value : null;
    }
}
