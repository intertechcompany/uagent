<?php

namespace App\Service\PolicyService\Payment\Rgs;

use Exception;
use App\Model\Policy;
use App\Model\PolicyParam;
use App\Service\PolicyService\Client\Rgs\CurlClient;

class VerificationCode
{
    /**
     * @var array
     */
    public $responce = [];

    /**
     * @var CurlClient
     */
    private $client;

    /**
     * Ingos constructor.
     */
    public function __construct()
    {
        $this->client = new CurlClient('SendVerificationCode');
    }

    /**
     * @param Policy $policy
     * @return bool
     */
    public function create(Policy $policy): array
    {
        try {
            $this->responce = $this->client->request([
                'rgs:sendVerificationCodeRequest' => [
                    'rgs:ContractId' => $this->getCalculationId($policy),
                    'rgs:Mobile' => $policy->phone,
                ],
            ]);

            \Log::info([
                'key' => 'response sendVerificationCodeRequest',
                'data' => $this->responce
            ]);
        } catch (Exception $ex) {
            return [];
        }

        return $this->responce;
    }

    public function isSuccess(): boold
    {
        return !empty($this->responce) && array_get($responce, $this->getSuccessAlias());
    }

    private function getSuccessAlias(): string
    {
        return 's:Body.ConfirmVerificationCodeResponse.ConfirmVerificationCodeResult.b:IsSuccess';
    }

    /**
     * Get calculation id in params policy model
     *
     * @return ?string
     */
    private function getCalculationId(Policy $policy): string
    {
        $param = $policy->findParam(PolicyParam::RGS_CALCULATION_ID);

        return $param ? $param->value : null;
    }
}
