<?php

namespace App\Service\PolicyService\Payment;

use App\Model\Policy;
use App\Model\PolicyPayment;
use App\Service\PolicyService\Client\Ingos\Client;

class Ingos implements PaymentInterface
{
    /**
     * @var Client
     */
    private $client;

    /**
     * Ingos constructor.
     */
    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * @param Policy $policy
     * @return PolicyPayment
     */
    public function create(Policy $policy): PolicyPayment
    {
        $bill = $this->client->createBill([$policy->insurance_id]);
        $payment = new PolicyPayment();
        $payment->orderId = $bill['BillISN'];
        $payment->invoice_id = $bill['BillNum'];
        $payment->price = $bill['BillSum'];

        $billData = [
            'BillISN' => $payment->orderId,
            'Client' => [
                'Email'               => $policy->email,
                'DigitalPolicyEmail'  => $policy->email,
                'Phone'               => $policy->phone,
                'PaymentNotification' => 'Y',
                'SendByEmail'         => 'Y',
                'SendBySms'           => 'Y'
            ]
        ];

        $url = $this->client->createOnlineBill($billData);

        $payment->formUrl = $url;

        $payment->save();

        return $payment;
    }

    /**
     * @param Policy $policy
     * @return bool
     */
    public function checkPayment(Policy $policy): bool
    {

    }
}
