<?php

namespace App\Service\PolicyService\Payment;

use App\Model\Policy;

final class PaymentFactory
{
    /**
     * @param string $type
     * @return PaymentInterface
     */
	public static function factory(string $type): PaymentInterface
	{
		if ($type === Policy::ALPHA) {
			return new Alpha();
		}

		if ($type === Policy::NASKO) {
			return new Kias();
		}

        if ($type === Policy::INGOS) {
            return new Ingos();
        }

        if ($type === Policy::RGS) {
            return new Rgs();
        }

		throw new \InvalidArgumentException('Unknown insurance given');
	}
}
