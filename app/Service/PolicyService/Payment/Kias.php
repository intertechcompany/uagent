<?php

namespace App\Service\PolicyService\Payment;

use App\Model\Policy;
use App\Model\PolicyParam;
use App\Model\PolicyPayment;
use App\Service\PolicyParamService;
use App\Service\PolicyService\Client\Kias\Client as KiasClient;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class Kias implements PaymentInterface
{
	public const INVOICE_CLASS = 10254;
	public const ATTR_EMAIL = 1312471;
	/**
	 * @var KiasClient
	 */
	protected $client;

	protected $agreement;

	protected $needRoleToCheck = [
		2103,
		2085,
		2099,
	];

    /**
     * @var PolicyParamService
     */
	private $policyParamService;

    /**
     * Kias constructor.
     */
	public function __construct()
	{
		$this->client = new KiasClient();
		$this->policyParamService = new PolicyParamService();
	}

	public function create(Policy $policy): PolicyPayment
	{
        $paymentDb = $policy->payment;

        if ($paymentDb === null) {

            $isSaveToOsago = $this
                ->policyParamService
                ->getParamOrCreate($policy,PolicyParam::NASKO_IS_SAVE_TO_OSAGO)
            ;

            $this->agreement = $this->client->getAgreement($policy->insurance_id);

            if ($isSaveToOsago->value === null) {
                $client = new KiasClient($this->client->getSession());
                $carISN = array_get($this->agreement,'AGROBJECT.row.ISN');
                $agrRole = array_get($this->agreement,'AGRROLE.row');

                $client->rsaEPolicyCheckAgrForEOsago($policy->insurance_id);
                $client->rsaEPolicyCheckCar($carISN);

                foreach ($agrRole as $role) {
                    if (\in_array((int) array_get($role,'ClassISN'),$this->needRoleToCheck,true)) {
                        $client->rsaEPolicyCheckSubject(array_get($role,'ISN'));
                    }
                }

                $client->rsaEPolicySaveProject($policy->insurance_id);

                $isSaveToOsago->value = true;
                $isSaveToOsago->save();
            }

            $invoiceISN = $this
                ->policyParamService
                ->getParamOrCreate($policy,PolicyParam::NASKO_INVOICE_ISN)
            ;

            $docISN = $invoiceISN->value;

            if ($docISN === null) {
                $docISN = $invoiceISN->value = array_get($this->client->createInvoiceFromagrcond($policy->insurance_id),'DOCISN');
                $invoiceISN->save();
            }

            $document = $this->client->getDocument($docISN,self::INVOICE_CLASS);

            $Doc = $document['Doc']['row'];
            unset($document['Doc']);

            $Doc['STATUSISN'] = '2522';

            $document['Sid'] = $this->client->getSession();

            $document = array_merge($document,$Doc);

            foreach ($document['DocParams']['row'] as $key => $item) {
                if ((int)$item['ATTRISN'] === self::ATTR_EMAIL) {
                    $document['DocParams']['row'][$key]['VAL'] = $policy->email;
                    break;
                }
            }

            $saveDoc = $this->client->saveDocument($document);

            if ($this->client->checkForbidden()) {
                $policy->status = 'forbidden';
                $policy->save();
                throw new BadRequestHttpException('Отказано!');
            }

            $invoiceISN = array_get($saveDoc,'DOCISN');

            $this->client->getAcquiringParams();

            $this->client->checkInvoiceCallback($invoiceISN);

            $transaction = $this->client->acqGetTransactionRequest($invoiceISN);

            $transaction = $this->client->formatParamsForPayment(array_get($transaction,'RESULT'));

            $paymentUrl = array_get($this->client->acqGetPaymentPage($invoiceISN,$transaction),'RESULT.URL');

            preg_match('~OrderId="(.*?)"~',$transaction,$arr);

            $paymentDb = new PolicyPayment();

            $paymentDb->policy_id = $policy->id;
            $paymentDb->formUrl = $paymentUrl;
            $paymentDb->orderId = $arr[1];
            $paymentDb->invoice_id = $invoiceISN;
            $paymentDb->description = "Оплата полиса NASKO OrderId: $arr[1]";
            $paymentDb->save();
        }

		return $paymentDb;
	}

	public function checkPayment(Policy $policy)
	{
		ini_set('max_execution_time', 300);

		$paymentDB = $policy->payment;

        try {
            try {
                $transaction = $this->client->acqGetPaymentResultRequest($paymentDB->invoice_id);
                $result = $this->client->formatParamsForPayment(array_get($transaction,'RESULT'));

                preg_match('~Success="(.*?)"~',$result,$status);

                if ((string)$status[1] !== 'True') {
                    return false;
                }

                $this->client->acqCheckPaymentResult($result);

                preg_match('~OrderId="(.*?)"~',$result,$orderId);

                $paymentDB->orderId = $orderId[1];
                $paymentDB->save();

                $policy
                    ->params()
                    ->create([
                        'key' => PolicyParam::NASKO_TRANSACTION_XML,
                        'value' => $result
                    ]);


            } catch (\Exception $exception) {}

            $this->client->processInvoicePayment($paymentDB->invoice_id);

        } catch (\Exception $exception) {
            return $exception->getMessage();
        }

		return true;
	}
}
