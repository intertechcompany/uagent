<?php

namespace App\Service\PolicyService\Payment;

use App\Model\Policy;
use App\Model\PolicyPayment;
use App\Service\PolicyService\Client\Alpha\MerchantServicesClient;
use Carbon\Carbon;

class Alpha implements PaymentInterface
{

    /**
     * @param Policy $policy
     * @return PolicyPayment
     */
	public function create(Policy $policy): PolicyPayment
	{
        $paymentDb = $policy->payment;

	    if ($paymentDb === null) {
            $merchantService = new MerchantServicesClient();
            $description = 'Оплата страхового полиса ' . $policy->insurance_number;
            $token = md5(str_random(30));

            $payment = $merchantService->registerOrder([
                'merchantOrderNumber' => $policy->insurance_id,
                'description' => $description,
                'expirationDate' => Carbon::now()->addMinutes(20)->format('Y-m-d\Th:i:s'),
                'returnUrl' => route('alpha.status',['status' => 'success','token' => $token]),
                'failUrl' => route('alpha.status',['status' => 'fail','token' => $token]),
                'email' => $policy->email
            ]);

            $paymentDb = new PolicyPayment();
            $paymentDb->policy_id = $policy->id;
            $paymentDb->formUrl = array_get($payment,'formUrl');
            $paymentDb->orderId = array_get($payment,'orderId');
            $paymentDb->description = $description;
            $paymentDb->token = $token;
            $paymentDb->save();
        }

		return $paymentDb;
	}

    /**
     * @param Policy $policy
     * @return bool
     */
	public function checkPayment(Policy $policy): bool
    {
        $merchantService = new MerchantServicesClient();
        $orderStatus = $merchantService->getOrderStatus($policy->payment->orderId);

        return $orderStatus['orderStatus'] === 2;
    }
}
