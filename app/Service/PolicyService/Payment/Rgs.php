<?php

namespace App\Service\PolicyService\Payment;

use Exception;
use Carbon\Carbon;
use App\Model\Policy;
use App\Model\PolicyParam;
use App\Model\PolicyPayment;
use App\Service\PolicyService\Sign\Rgs as RgsSign;
use App\Service\PolicyService\Client\Rgs\CurlClient;

class Rgs implements PaymentInterface
{
    /**
     * @var CurlClient
     */
    private $client;

    /**
     * @var string
     */
    public $paymentStatus = 'inPayment';

    /**
     * @var string
     */
    private $timeFormat = 'Y-m-d\T';

    private $countConnections = 0;

    private $maxConnections = 2;

    public function __construct()
    {
        $this->client = new CurlClient('StartPayment');
    }

    /**
     * @param Policy $policy
     * @return PolicyPayment
     */
    public function create(Policy $policy): PolicyPayment
    {
        $token = md5(str_random(30));

        $response = $this->client->request([
            'rgs:startPaymentRequest' => [
                'rgs:CalculationId' => $this->getCalculationId($policy),
                'rgs:Number' => $policy->insurance_id,
                'rgs:PaymentMethod' => 5,
                'rgs:ReturnUrl' => route('rgs.status', [
                    'status' => 'success',
                    'token' => $token,
                ]),
                'rgs:FailUrl' => route('rgs.status', [
                    'status' => 'fail',
                    'token' => $token,
                ]),
                'rgs:Series' => 'ХХХ',
            ],
        ]);

        \Log::info([
            'key' => 'response startPaymentRequest',
            'data' => $response
        ]);

        $paymentResponseData = array_get($response, 's:Body.StartPaymentResponse.StartPaymentResult.b:Data.PaymentResponse');

        \Log::info([
            'key' => 'var PaymentResponse',
            'data' => $paymentResponseData
        ]);

        try {
            $orderId = array_get($paymentResponseData, 'OrderId');
            $paymentUrl = array_get($paymentResponseData, 'FormUrl');

            $paymentDb = new PolicyPayment();

            $paymentDb->policy_id = $policy->id;
            $paymentDb->formUrl = $paymentUrl;
            $paymentDb->orderId = $orderId;
            $paymentDb->description = "Оплата полиса RGS OrderId: {$orderId}";
            $paymentDb->token = $token;
            $paymentDb->save();

            \Log::info([
                'key' => 'db paymentDb',
                'data' => $paymentDb ? $paymentDb->toArray() : null,
            ]);

            /**
             * Check policy status
             */
            // $this->checkPolicyStatus($policy);

            return $paymentDb;
        } catch (Exception $e) {}

        return null;
    }

    /**
     * @param Policy $policy
     * @return bool
     */
    public function checkPayment(Policy $policy): bool
    {
        try {
            $data = [
                'user' => [
                    '#' => null
                ],
                'checkPaymentRequest' => [
                    '@BankName' => 'RgsBankStartPayment',
                    'BranchCode' => '27860170',
                    'CalculationId' => $this->getCalculationId($policy),
                    'CodeSystemFrom' => 33,
                    'Lnr' => config('services.rgs.inn'),
                    'Payment' => [
                        '@ConfirmCode' => '',
                        'Number' => $policy->insurance_id,
                        'PaymentDate' => [
                            '@xsi:type' => 'dateTime',
                            '#' => Carbon::now()->format($this->timeFormat).'00:00:00',
                        ],
                        'PaymentDocType' => '14',
                        'PaymentMethod' => '5',
                        'PaymentType' => '3',
                        'ReceiptNumber' => [
                            '#' => null
                        ],
                        'ReceiptSeries' => [
                            '#' => null
                        ],
                        'Series' => 'ХХХ',

                    ],
                ],
            ];

            $result = (new CurlClient('CheckPayment'))->request($data);

            \Log::info([
                'key' => 'response CheckPayment',
                'data' => $result
            ]);

            if (array_get($result, 's:Body.CheckPaymentResponse.CheckPaymentResult.b:IsSuccess') == 'true') {
                return true;
            }

            return $this->checkPaymentFall($data);
        } catch (Exception $e) {}

        return false;
    }

    private function checkPaymentFall(array $data): bool
    {
        $result = (new CurlClient('CheckPayment'))->request($data);

        \Log::info([
            'key' => 'response CheckPayment',
            'data' => $result
        ]);

        $this->countConnections++;

        if (array_get($result, 's:Body.CheckPaymentResponse.CheckPaymentResult.b:IsSuccess') == 'true') {
            return true;
        }

        if ($this->countConnections > $this->maxConnections) {
            return false;
        }

        sleep(10);

        return $this->checkPaymentFall($data);
    }

    /**
     * @param \App\Model\Policy $policy
     * @return \App\Model\Policy
     */
    public function checkPolicyStatus(Policy $policy)
    {
        $policy->status = $this->paymentStatus;
        $policy->save();

        return $policy;
    }

    /**
     * Get calculation id in params policy model
     *
     * @return ?string
     */
    private function getCalculationId(Policy $policy): string
    {
        $param = $policy->findParam(PolicyParam::RGS_CALCULATION_ID);

        return $param ? $param->value : null;
    }
}
