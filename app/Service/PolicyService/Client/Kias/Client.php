<?php

namespace App\Service\PolicyService\Client\Kias;

use App\Helper\CacheHelper;
use App\Helper\PolicyLogHelper;
use App\Model\ApiError;
use Carbon\Carbon;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Serializer\Encoder\XmlEncoder;

class Client
{
	protected $soapClient;

	protected $sessionId;

	protected $forbidden = false;

	private $forbiddenCodes = [
		'ORA-20001'
	];

	private $ignoreErrorMethods = [
		'RSA_EPOLICY_CheckCa'
	];

    /**
     * Client constructor.
     * @param null $session
     */
	public function __construct($session = null)
	{
		ini_set('default_socket_timeout', 1200);
		$wsdl = env('NASKO') === 'production' ? config('services.kias.production_endpoint') : config('services.kias.test_endpoint');
		$this->soapClient = new \SoapClient($wsdl . '?WSDL', ['soap_version' => SOAP_1_2,'connection_timeout' => 10000,'cache_wsdl' => WSDL_CACHE_NONE, 'trace' => 1, 'location' => $wsdl]);
		if (empty($session)) {
			$this->checkLocalSession();
		} else {
			$this->sessionId = $session;
		}

	}

	public function checkForbidden(): bool
	{
		return $this->forbidden;
	}


	public function getSession(): string
	{
		return $this->sessionId;
	}

	public function checkSession(string $sId): bool
	{
		$result = $this->makeRequest('CheckSession',[
			'Sid' => $sId
		],true,true);

		return !array_get($result,'code') === '001';
	}

	public function auth(string $login, string $password)
	{
		$params = [
			'Name' => $login,
			'Pwd' => hash('sha512', $password)
		];

		$data = $this->makeRequest('Auth', $params);

		$this->sessionId = array_get($data, 'Sid');

		return $data;
	}

    /**
     * @param int $dictiId
     * @param int|null $mode
     * @return mixed
     */
	public function getDictiList(int $dictiId, int $mode = null)
	{
		$params = [
			'DictiISN' => $dictiId,
			'Mode' => $mode,
			'Sid' => $this->sessionId
		];

		$response = $this->makeRequest('GetDictiList', $params);

		return array_get($response, 'ROWSET.row');
	}

    public function subjectSearch(string $fullName, string $dob, array $additionalCriteria = null)
    {
        $params = [
            'FName' => $fullName,
            'DateBirth' => $dob,
            'Sid' => $this->sessionId
        ];

        if ($additionalCriteria) {
            $params = array_merge($params, $additionalCriteria);
        }

        $response = $this->makeRequest('SubjectSearch', $params);

        return array_get($response, 'ROWSET.row');
	}

    public function getCityList(string $searchMask = null, string $kladrCode = null)
    {
        $params = [
            'SearchMask' => $searchMask,
            'KladrCode' => $kladrCode,
            'Sid' => $this->sessionId
        ];

        $response = $this->makeRequest('GetCityList', $params);

        return array_get($response, 'ROWSET.row');
	}

    public function getStreetList(int $cityISN = null, string $searchMask = null, string $kladrCode = null)
    {
        $params = [
            'CITYISN' => $cityISN,
            'SearchMask' => $searchMask,
            'KladrCode' => $kladrCode,
            'Sid' => $this->sessionId
        ];

        $response = $this->makeRequest('GetStreetList', $params);

        return array_get($response, 'ROWSET.row');
	}

    public function saveSubject(array $data,int $isn = null)
    {
        $params = [
            'Sid' => $this->sessionId,
			'ISN' => $isn,
        ];

        $params = array_merge($params, $data);

        $response = $this->makeRequest('SaveSubject', $params);

        return array_first(array_get($response, 'Subject'));
	}

    public function getAgreementCalc(int $agreementCalcISN = null)
    {
        $params = [
            'AGREEMENTCALCISN' => $agreementCalcISN,
            'PRODUCTISN' => 3387,
            'Sid' => $this->sessionId
        ];

        $response = $this->makeRequest('GetAgreementCalc', $params);

        return array_first(array_get($response, 'AgreementCalc'));
    }

    public function saveAgreementCalc(array $data)
    {
        $params = [
            'Sid' => $this->sessionId
        ];

        $params = array_merge($data, $params);

        $response = $this->makeRequest('SAVEAGREEMENTCALC', $params);

        return array_get($response, 'AGREEMENTCALCISN');
    }

    public function getAgreement(int $agreementISN = null)
    {
        $params = [
            'AgrISN' => $agreementISN,
            'ProductISN' => 3387, // TODO: CHECK
            'Sid' => $this->sessionId,
        ];

        $response = $this->makeRequest('GetAgreement', $params);

        return array_first(array_get($response, 'Agreement'));
    }

    public function saveAgreement(array $data)
    {
        $params = [
            'Sid' => $this->sessionId
        ];

        $params = array_merge($data, $params);

        $response = $this->makeRequest('SaveAgreement', $params);

        return array_get($response, 'AgrISN');
	}

    public function calcAgreement(string $agrISN)
    {
        $params = [
            'AgrISN' => $agrISN,
            'Sid'    => $this->sessionId
        ];

        return $this->makeRequest('CalcAgreement', $params,false);
    }

    public function createGrbyAgreementCalc(int $agreementCalcIsn)
    {
        $params = [
            'Sid'              => $this->sessionId,
            'AGREEMENTCALCISN' => $agreementCalcIsn
        ];

        $response = $this->makeRequest('CREATEAGRBYAGREEMENTCALC', $params);

        return array_get($response, 'AgrISN');
    }

    public function getSubjectInfo(int $SubjISN)
    {
        $params = [
            'Sid'     => $this->sessionId,
            'SubjISN' => $SubjISN
        ];

        return $this->makeRequest('GetSubjectInfo', $params);
    }

    public function getRsaosagoKbm(int $agrISN)
    {
        $params = [
            'AgrISN' => $agrISN,
            'Sid'    => $this->sessionId
        ];
        $response = $this->makeRequest('GETRSAOSAGOKBM', $params);
        
        return array_get($response, 'Remark');
    }
	public function getDocumentForPrint(int $id, int $template)
	{
		$params = [
			'Sid' => $this->sessionId,
			'ISN' => $id,
			'ClassID' => 2,
			'TemplateISN' => $template
		];

        return $this->makeRequest('GETPRINTABLEDOCUMENT', $params,false);
	}

	public function getPrintForms()
	{
		$params = [
			'Sid' => $this->sessionId,
		];

        return $this->makeRequest('GETPRINTFORMSBYRSAKBMREQUEST',$params,false);
	}

	public function wcBankGetTransactionRequest(string $isn)
	{
		$params = [
			'Sid' => $this->sessionId,
			'ISN' => $isn
		];

        return $this->makeRequest('WC_BANK_GETTRANSACTIONREQUEST ',$params,false);
	}

	public function wcBankGetPaymentPage(string $isn)
	{
		$params = [
			'Sid' => $this->sessionId,
			'ISN' => $isn
		];

        return $this->makeRequest('WC_BANK_GETPAYMENTPAGE',$params,false);
	}

	public function wcBankGetPaymentResultRequest(string $isn)
	{
		$params = [
			'Sid' => $this->sessionId,
			'ISN' => $isn
		];

        return $this->makeRequest('WC_BANK_GETPAYMENTRESULTREQUEST',$params,false);
	}

	public function wcBankCheckPaymentResult(string $isn)
	{
		$params = [
			'Sid' => $this->sessionId,
			'ISN' => $isn
		];

        return $this->makeRequest('WC_BANK_CHECKPAYMENTRESULT',$params,false);
	}

	public function rsaEPolicyCheckAgrForEOsago(string $AgrISN)
	{
		$params = [
			'Sid' => $this->sessionId,
			'AgrISN' => $AgrISN
		];

        return $this->makeRequest('RSA_EPOLICY_CheckAgrForEOsago',$params,false);
	}

	public function rsaEPolicyCheckCar(string $AgrObjCarISN)
	{
		$params = [
			'Sid' => $this->sessionId,
			'AGROBJCARISN' => $AgrObjCarISN
		];

        return $this->makeRequest('RSA_EPOLICY_CheckCar',$params,false);
	}

	public function rsaEPolicyCheckSubject(string $AgrRoleISN)
	{
		$params = [
			'Sid' => $this->sessionId,
			'AGRROLEISN' => $AgrRoleISN
		];

        return $this->makeRequest('RSA_EPOLICY_CheckSubject',$params,false);
	}

	public function rsaEPolicySaveProject(string $AgrISN)
	{
		$params = [
			'Sid' => $this->sessionId,
			'AgrISN' => $AgrISN
		];

        return $this->makeRequest('RSA_EPOLICY_SaveProject',$params,false);
	}

	public function createInvoiceFromagrcond(string $AgrISN)
	{
		$params = [
			'Sid' => $this->sessionId,
			'AGRISN' => $AgrISN
		];

        return $this->makeRequest('CREATEINVOICEFROMAGRCOND',$params);
	}

	public function getDocument(string $docIsn,string $classIsn)
	{
		$params = [
			'Sid' => $this->sessionId,
			'DOCISN' => $docIsn,
			'CLASSISN' => $classIsn
		];

        return $this->makeRequest('GetDocument',$params);
	}

	public function saveDocument(array $data)
	{
		$params = [
			'Sid' => $this->sessionId
		];

		$params = array_merge($data, $params);

        return $this->makeRequest('SAVEDOCUMENT',$params);
	}

	public function getAcquiringParams()
	{
		$params = [
			'Sid' => $this->sessionId,
		];

        return $this->makeRequest('GetAcquiringParams',$params);
	}

	public function checkInvoiceCallback(string $invoiceISN)
	{
		$params = [
			'Sid' => $this->sessionId,
			'INVOICEISN' => $invoiceISN
		];

        return $this->makeRequest('CheckInvoiceCallback',$params);
	}

	public function acqGetTransactionRequest(string $isn)
	{
		$params = [
			'Sid' => $this->sessionId,
			'ISN' => $isn
		];

        return $this->makeRequest('Acq_GetTransactionRequest',$params);
	}

	public function acqGetPaymentPage(string $isn,$transaction)
	{
		$params = [
			'Sid' => $this->sessionId,
			'ISN' => $isn,
			'TRANSACTION' => $transaction
		];

        return $this->makeRequest('Acq_GetPaymentPage',$params);
	}

	public function acqGetPaymentResultRequest(string $isn)
	{
		$params = [
			'Sid' => $this->sessionId,
			'ISN' => $isn,
		];

        return $this->makeRequest('Acq_GetPaymentResultRequest',$params);
	}

	public function acqCheckPaymentResult($transaction)
	{
		$params = [
			'Sid' => $this->sessionId,
			'TRANSACTION' => $transaction
		];

        return $this->makeRequest('Acq_CheckPaymentResult',$params);
	}

	public function processInvoicePayment(string $isn)
	{
		$params = [
			'Sid' => $this->sessionId,
			'ISN' => $isn,
		];

        return $this->makeRequest('ProcessInvoicePayment',$params);
	}

	public function setAgrStatus(string $AgrIsn,string $status)
	{
		$params = [
			'Sid' => $this->sessionId,
			'AgrISN' => $AgrIsn,
			'STATUS' => $status,
		];

        return $this->makeRequest('SetAgrStatus',$params,false);
	}

	public function ePolicyCreateAndPrintEPolicy(string $AgrIsn)
	{
		$params = [
			'Sid' => $this->sessionId,
			'AgrISN' => $AgrIsn,
		];

        return $this->makeRequest('EPOLICY_CreateAndPrintEPolicy',$params,false);
	}

	public function rsaEPolicySetProjectStatus(string $AgrIsn)
	{
		$params = [
			'Sid' => $this->sessionId,
			'AgrISN' => $AgrIsn,
		];

		return $this->makeRequest('RSA_EPOLICY_SetProjectStatus',$params,false);
	}

	public function createAgrByAgreementCalc(string $isn):? string
	{
		$params = [
			'Sid' => $this->sessionId,
			'AGREEMENTCALCISN' => $isn,
		];

		$response = $this->makeRequest('CREATEAGRBYAGREEMENTCALC',$params);

		return array_get($response,'AgrISN');
	}

	public function getCarClassByModel(string $modelISN)
	{
		$params = [
			'ModelISN' => $modelISN
		];

		return $this->makeRequest('GetCarClassByModel',$params);
	}

	public function makeShortNameAndSex(string $fullName,string $juridical = 'N')
	{
		$params = [
			'FullName' => $fullName,
			'Juridical' => $juridical
		];
		return $this->makeRequest('MakeShortNameAndSex',$params);
	}

	public function saveAttachment(string $agrIsn,string $filePath,string $filename,string $pictureType = 'A')
	{
		$params = [
			'REFISN' => $agrIsn,
			'PICTTYPE' => $pictureType,
			'FILENAME' => $filename,
			'OLEOBJECT' => base64_encode(file_get_contents($filePath)),
			'CLASSISN' => '1159801',
			'Sid' => $this->sessionId
		];

		return $this->makeRequest('SAVEATTACHMENT',$params);
	}

	public function getPolicyDocument(string $agrIsn)
    {
        $params = [
            'FILETYPE' => 'P',
            'AGRISN' => $agrIsn,
            'Sid' => $this->sessionId,
        ];

        return $this->makeRequest('USER_NaskoDownloadEPolicyPrint',$params);
    }

	public function formatParamsForPayment($response)
	{
		$params['url'] = array_get($response,'URL');

		foreach ($response['GET']['row'] as $param) {
			$params['params'][array_get($param,'NAME')] = array_get($param,'VALUE');
		}

		return $this->makePostRequest($params);
	}

	/**
	 * Make Typical post call
	 * @param mixed $params
	 */
    public function makePostRequest($params)
	{
		$query = \GuzzleHttp\Psr7\build_query($params['params']);
		$url = array_get($params,'url') . '?' . $query;

        if ($ch = curl_init($url)) {
            curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            $result = curl_exec($ch);
            curl_close($ch);
        }

		return $result;
	}

	private function checkLocalSession(): void
	{
		$check = false;
		$sessionId = CacheHelper::get('kias_session');
		if ($sessionId !== null) {
			$check = $this->checkSession($sessionId);
			$this->sessionId = $sessionId;
		}

		if ($check === false) {
			$this->auth(config('services.kias.login'), config('services.kias.password'));
			CacheHelper::set('kias_session',$this->sessionId);
		}
	}


    /**
     * @param string $methodName
     * @param array|null $params
     * @param bool $showResult
     * @param bool $showError
     * @return array|bool|mixed|string
     */
    private function makeRequest(string $methodName, array $params = null, $showResult = true,$showError = false)
    {
        $requestBody = [
            'request' => [
                'RequestIp' => '91.243.20.159',
                'AppId' => config('services.kias.app_id'),
                'reqName' => $methodName,
                'params' => $params
            ]
        ];

        $xmlRequestBody = (new XmlEncoder())->encode($requestBody, 'xml', ['xml_root_node_name' => 'data', 'xml_encoding' => 'utf-8']);


		$response = $this->soapClient->ExecProc(['pData' => $xmlRequestBody]);

        PolicyLogHelper::getInstance('policy')
            ->setStringLog($xmlRequestBody)
            ->setStringLog($response->ExecProcResult->any);

        $responseDecoded = (new XmlEncoder())->decode((string) $response->ExecProcResult->any, 'array');

		if ($showError === true) {
			return array_get($responseDecoded, 'error');
		}

        if (array_get($responseDecoded, 'error') && !\in_array($methodName,$this->ignoreErrorMethods, true)) {

            $error = ApiError::getError(['text' => array_get($responseDecoded, 'error.text')]);

            $logger = PolicyLogHelper::getInstance('policy');

            $logger->saveLog(storage_path('policies/fails/' . Carbon::now()->format('d-m-Y') . '/'.'nasko-' . time() . '.xml'));

            throw new BadRequestHttpException($error);

			if (\in_array(array_get($responseDecoded,'error.code'),$this->forbiddenCodes, true)) {
				return $this->forbidden = true;
			}

			throw new BadRequestHttpException(array_get($responseDecoded, 'error.fulltext') !== null ? array_get($responseDecoded, 'error.fulltext') : array_get($responseDecoded, 'error.text'));

        }

		if ($showResult === true) {
			return array_get($responseDecoded, 'result');
		}

		return $responseDecoded;
    }
}
