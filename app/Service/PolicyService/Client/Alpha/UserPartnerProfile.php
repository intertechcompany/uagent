<?php

namespace App\Service\PolicyService\Client\Alpha;

use GuzzleHttp\Client;

class UserPartnerProfile
{
	private $uri = 'https://alfastrah.ru/api/userPartner/';

	public function register(): void
	{
		$params = [
			'email' => 'test@gmail.com',
			'last_name' => 'test',
			'name' => 'test',
			'second_name' => 'test',
			'password' => 'test',
			'confirm_password' => 'test',
			'phone' => '+7-111-111-11-11',
			'birthday' => '01-11-1980',
			'contract_number' => '11',
			'offer_accepted' => 'Y',
			'partner_name' => 'partner_name',
		];
		$this->makeRequest($params);
	}

	private function makeRequest(array $params = [])
	{
		$client = new Client();
		$result = $client->request('POST',$this->uri,['form_params' => $params]);
		dd($result);
		return 1;
	}
}