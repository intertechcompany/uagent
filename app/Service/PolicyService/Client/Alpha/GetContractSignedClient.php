<?php

namespace App\Service\PolicyService\Client\Alpha;

class GetContractSignedClient extends WsSecurityClient  implements ClientInterface
{
	/**
	 * @var string
	 */
	protected $wsdl = '/cxf/GetContractSigned?wsdl';

	/**
	 * Called to unload the customer's printed policy form
	 * @param array $params
	 * @return array
	 */
	public function getContractSigned(array $params): array
	{
		return $this->makeRequest('GetContractSigned',$params);
	}
}