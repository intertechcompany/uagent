<?php

namespace App\Service\PolicyService\Client\Alpha;

class EOsagoRelaunchClient extends WsSecurityClient implements ClientInterface
{
	/**
	 * @var string
	 */
	protected $wsdl = '/cxf/EOsagoRelaunch?wsdl';


	/**
	 * Verification of data on LDU.
	 * From 01/01/2017, its result should not affect the downloading of the contract in the SAR, but to carry out the check is still mandatory
	 * @param array $params
	 * @return array
	 */
	public function loadDriver(array $params): array
	{
		return $this->makeRequest('loadDriver', $params);
	}

	/**
	 * Inspecting data on the insured / owner
	 * From 01/01/2017, its result should not affect the downloading of the contract in the SAR, but to carry out the check is still mandatory
	 * @param array $params
	 * @return array
	 */
	public function loadInsurerOwner(array $params): array
	{
		return $this->makeRequest('loadInsurerOwner', $params);
	}

	/**
	 * Check of data on the insured vehicle
	 * From 01/01/2017, its result should not affect the downloading of the contract in the SAR, but to carry out the check is still mandatory
	 * @param array $params
	 * @return array
	 */
	public function loadVehicle(array $params): array
	{
		return $this->makeRequest('loadVehicle', $params);
	}

	/**
	 * Downloading the draft contract in the SAR after a successful passing of the calculation and passing the checks
	 * @param array $params
	 * @return array
	 */
	public function loadContractEosago(array $params): array
	{
		return $this->makeRequest('loadContractEosago', $params);
	}

	/**
	 * In case of an error on the side of the insurance company when the contract is drawn up (zero insurance premium in the settlement service answer or a drop in any integration services), it is necessary to call this method to transfer the client to the site of another insurance company so that he continues the purchase there.
	 * Before starting the method, you need to notify the client about the problems that have arisen and ask permission to transfer previously entered information. From its consent depends on the list of input parameters to be filled.
	 * In response, the service provides a hyperlink, according to which it is necessary to transfer the user without the possibility to refuse the transfer.
	 * @param array $params
	 * @return array
	 */
	public function loadStatementEosago(array $params): array
	{
		return $this->makeRequest('loadStatementEosago', $params);
	}
}