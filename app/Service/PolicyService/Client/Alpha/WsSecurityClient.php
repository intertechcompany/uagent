<?php

namespace App\Service\PolicyService\Client\Alpha;

use App\Helper\PolicyLogHelper;
use App\Model\ApiError;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use WsdlToPhp\WsSecurity\WsSecurity;

/**
 * Class WsSecurityClient
 * @package App\Service\PolicyService\Client\Alpha
 * @property string $wsdl
 */
class WsSecurityClient
{
	/**
	 * @var \SoapClient
	 */
	protected $soapClient;

	/**
	 * Login for auth in Alpha
	 * @var string
	 */
	protected $login;

	/**
	 * Password for auth in Alpha
	 * @var string
	 */
	protected $password;

    protected $wsdl = '/cxf/MerchantServices?wsdl';
	/**
	 * WsSecurityClient constructor.
	 */
	public function __construct()
	{
		ini_set('default_socket_timeout', '300');
		$url = env('ALPHA') === 'production' ? config('services.alpha.production_endpoint') : config('services.alpha.test_endpoint');
		$this->login = env('ALPHA') === 'production' ? config('services.alpha.login_production') : config('services.alpha.login');
		$this->password = env('ALPHA') === 'production' ? config('services.alpha.password_production') : config('services.alpha.password');

		$this->soapClient = new \SoapClient($url . $this->wsdl,['connection_timeout' => 10000,'cache_wsdl' => WSDL_CACHE_NONE, 'trace' => 1]);
	}

	/**
	 * Make request with WsSecurity
	 * @param string $method
	 * @param array $params
	 * @return array
	 */
	public function makeRequest(string $method, $params): array
	{
	    try {
            $headers = WsSecurity::createWsSecuritySoapHeader($this->login, $this->password, true);
            $this->soapClient->__setSoapHeaders($headers);
            $result = $this->soapClient->$method($params);

            PolicyLogHelper::getInstance('policy')
                ->setStringLog($this->soapClient->__getLastRequest())
                ->setStringLog($this->soapClient->__getLastResponse());
        } catch(\Exception $e) {
            ApiError::getError(['text'=>$e->getMessage()]);
	        $result = [];
        }

        if(!empty($result->ErrorList)) {
            ApiError::getError($result);
        }
        if(empty($result->InsuranceBonus) && (!empty($result->commentList) || !empty($result->RsaPersonMessageList) || !empty($result->RsaCarMessageList))) {
            ApiError::getError($result);
        }

		return (array) $result;
	}
}
