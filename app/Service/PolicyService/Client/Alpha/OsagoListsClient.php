<?php

namespace App\Service\PolicyService\Client\Alpha;

class OsagoListsClient implements ClientInterface
{
	/**
	 * @var \SoapClient
	 */
	private $soapClient;

	/**
	 * @var string
	 */
	private $wsdl = '/cxf/OSAGOlists?wsdl';

	/**
	 * OsagoListsClient constructor.
	 */
	public function __construct()
	{
		$url = env('ALPHA') === 'production' ? config('services.alpha.production_endpoint') : config('services.alpha.test_endpoint');
		$this->soapClient = new \SoapClient($url . $this->wsdl);
	}

	/**
	 * Get list mark of cars
	 * @param string $login
	 * @return array
	 */
	public function listMark(string $login = '1'): array
	{
		$params = [
			'login' => $login
		];

		return array_get($this->makeRequest('listMark',$params),'markEntry');
	}

	/**
	 * Get list models for cars
	 * @param int $markId
	 * @param string $login
	 * @return array
	 */
	public function listModel(int $markId,string $login = '1'): array
	{
		$params = [
			'login' => $login,
			'markId' => $markId
		];

		return array_get($this->makeRequest('listModel',$params),'modelEntry');
	}

	/**
	 * Get Types for transport
	 * @param string $login
	 * @return array
	 */
	public function listTransportCategory(string $login = '1'): array
	{
		$params = [
			'login' => $login,
		];

		return array_get($this->makeRequest('listTransportCategory',$params),'TransportCategory');
	}

	/**
	 * Get list transport purpose
	 * @param string $login
	 * @return array
	 */
	public function listTransportPurpose(string $login = '1'): array
	{
		$params = [
			'login' => $login,
		];

		return array_get($this->makeRequest('listTransportPurpose',$params),'TransportPurpose');
	}

	/**
	 * Get list transport types
	 * @param string $login
	 * @return array
	 */
	public function listTransportType(string $login = '1'): array
	{
		$params = [
			'login' => $login,
		];

		return array_get($this->makeRequest('listTransportType',$params),'TransportType');
	}

	/**
	 * @param string $method
	 * @param array $params
	 * @return array
	 */
	private function makeRequest(string $method,array $params = []): array
	{
		return (array) $this->soapClient->$method($params);
	}
}