<?php

namespace App\Service\PolicyService\Client\Alpha;

class PartnersInteractionClient extends WsSecurityClient  implements ClientInterface
{
	/**
	 * @var string
	 */
	protected $wsdl = '/cxf/PartnersInteraction?wsdl';

	/**
	 * Called for each attempt to formalize the contract of EOCAGO for obtaining its unique identifier
	 * @param string $callerCode
	 * @return string
	 */
	public function getUpid(string $callerCode): string
	{
		$params = [
			'callerCode' => $callerCode,
		];
		return array_get($this->makeRequest('getUPID',$params),'UPID');
	}

	/**
	 * Called by the partner to determine the status of the contract and retrieve its identifier in the event of a successful conclusion.
	 * @param string $upid
	 * @return array
	 */
	public function getContractId(string $upid): array
	{
		$params = [
			'UPID' => $upid,
		];

		$result = $this->makeRequest('getContractId',$params);

		return $result;
	}
}