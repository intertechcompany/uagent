<?php

namespace App\Service\PolicyService\Client\Alpha;

class OsagoCalcRelaunchClient extends WsSecurityClient  implements ClientInterface
{
	/**
	 * Wsdl link
	 * @var string
	 */
	protected $wsdl = '/cxf/OsagoCalcRelaunch?wsdl';

	/**
	 * Used to calculate the insurance premium under the contract
	 * @param array $params
	 * @return array
	 */
	public function calcEOsago(array $params): array
	{
		return $this->makeRequest('CalcEOsago',$params);
	}
}