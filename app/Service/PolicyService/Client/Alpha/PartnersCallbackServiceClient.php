<?php

namespace App\Service\PolicyService\Client\Alpha;


class PartnersCallbackServiceClient extends WsSecurityClient  implements ClientInterface
{
	/**
	 * @var string
	 */
	protected $wsdl = '/cxf/PartnersCallbackService?wsdl';

	/**
	 * For partners with full API integration, by agreement with Alfa Insurance managers, it may be possible to pay for the EOCAGO contracts through their payment systems, followed by reconciliation of the number of policies sold and the transfer of registers thereof.
	 * This service ensures the fixing of payment in the database AlfaStrahovaniya and sending a letter from the policy to the policyholder.
	 * @param array $params
	 * @return array
	 */
	public function callback(array $params): array
	{
		return $this->makeRequest('callback',$params);
	}
}