<?php

namespace App\Service\PolicyService\Client\Alpha;

class MerchantServicesClient extends WsSecurityClient  implements ClientInterface
{
	/**
	 * @var string
	 */
	protected $wsdl = '/cxf/MerchantServices?wsdl';

	public function registerOrder(array $params)
	{
		return $this->makeRequest('registerOrder',$params);
	}

    /**
     * @param string $orderId
     * @return array
     */
	public function getOrderStatus(string $orderId): array
    {
        return $this->makeRequest('getOrderStatus',[
            'orderId' => $orderId,
        ]);
    }
}
