<?php

namespace App\Service\PolicyService\Client\Makc;

use App\Traits\MakcTrait;
use GuzzleHttp\Cookie\CookieJar;
use Symfony\Component\Serializer\Encoder\XmlEncoder;

class Client
{
    use MakcTrait;

	public $guzzleClient;

	protected $url = 'https://unitest.makc.ru/';

	protected $wsdl = 'https://unitest.makc.ru/XmlDocumExchangeWS/XmlDocumExchangeWebService';

	protected $cookies;

	protected $login = 'IP_MILOVANOV';

	protected $password = '2s41Tsdz';

	public function __construct()
    {
		ini_set('default_socket_timeout', 1200);

        $this->soapClient = new \SoapClient($this->wsdl . '?WSDL', [
            'soap_version' => SOAP_1_1,
            'connection_timeout' => 10000,
            'cache_wsdl' => WSDL_CACHE_NONE,
            'trace' => 1,
            'location' => $this->wsdl,
            'login' => $this->login,
            'password' => $this->password
        ]);
//
//
//            $this->soapClient->__getLastResponse();
//        $this->guzzleClient = new \GuzzleHttp\Client([
//            'auth' => [$this->login, $this->password],
//        ]);

//        $this->cookies = new CookieJar();


	}

    /**
     * @param string $method
     * @param array $params
     * @return array
     */
    protected function makeRequest(string $method, array $params = []): array
    {
        $settingForRequest = [
            'gate' => [
                'action' => $method,
            ]
        ];

        $data = array_merge($settingForRequest, $params);

        $xmlBody = $this->generateXml($data);

        $headers =  [
            'Content-type: text/xml;charset="utf-8"',
            'Accept: text/xml',
            'Cache-Control: no-cache',
            'Pragma: no-cache',
            'SOAPAction: http://unitest.makc.ru:80/XmlDocumExchangeWS/XmlDocumExchangeWebService?xsd=1',
            'Content-length: ' . \strlen($xmlBody),
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_URL, $this->wsdl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_USERPWD, $this->login . ':' . $this->password);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlBody);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($ch);
        curl_close($ch);

        $decodeResponse = (new XmlEncoder())->decode((string) $response, 'json');

        dd($decodeResponse);

        return array_get($decodeResponse,'root');
    }

    /**
     * @param array $params
     * @return string
     */
    private function generateXml(array $params): string
    {
        $data = [
            '@xmlns:soapenv' => 'http://schemas.xmlsoap.org/soap/envelope/',
            '@xmlns:ns' => 'http://aisws.ingos.ru/services/b2b/sales/agents/1.0/',
            '#' => [
                'soapenv:Header' => null,
                'soapenv:Body' => [
                    'root' => $params,
                ],
            ],
        ];

        return (new XmlEncoder())->encode($data, 'xml', [
            'xml_root_node_name' => 'soapenv:Envelope',
            'xml_encoding' => 'utf-8',
        ]);
    }

    /**
     * @return bool|mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function auth()
    {
        $res = $this->guzzleClient->request('get', $this->wsdl, [
            'auth' => [$this->login, $this->password],
        ]);

        if ($res->getStatusCode() == 200) {
            return $res;
        }

        return false;
    }
}