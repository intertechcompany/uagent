<?php

namespace App\Service\PolicyService\Client\Ingos;

use Symfony\Component\Serializer\Encoder\XmlEncoder;

class Client
{
    /**
     * @var \SoapClient
     */
    public $soapClient;

    /**
     * @var string
     */
    private $sessionToken;

    /**
     * @var bool
     */
    private $isProduction;

    /**
     * @var string
     */
    private $wsdl;

    private $soapActionPrefix = 'http://aisws.ingos.ru/services/b2b/sales/agents/1.0/ISalesService/';

    /**
     * Client constructor.
     * @param string|null $sessionToken
     */
    public function __construct(?string $sessionToken = null)
    {
        ini_set('default_socket_timeout', 1200);

        $this->isProduction = env('INGOS') === 'production';
        $this->sessionToken = $sessionToken;

        $this->wsdl = $this->isProduction ?
            config('services.ingos.production_endpoint') :
            config('services.ingos.test_endpoint')
        ;

        $this->soapClient = new \SoapClient($this->wsdl . '?WSDL', [
            'soap_version' => SOAP_1_1,
            'connection_timeout' => 10000,
            'cache_wsdl' => WSDL_CACHE_NONE,
            'trace' => 1,
            'location' => $this->wsdl
        ]);

        if ($this->sessionToken === null) {
            $this->auth();
        }
    }

    /**
     * @return string
     */
    public function getSessionToken(): string
    {
        return $this->sessionToken;
    }

    /**
     * Метод предназначен для получения справочных значений для параметров договора (виды возмещения, список доп. Рисков, классы Бонуса-Малуса итд).
     * @param int $product
     * @return array
     */
    public function getDicti(int $product): array
    {
        $params = [
            'SessionToken' => $this->sessionToken,
            'Product'      => $product
        ];

        return $this->makeRequest('GetDicti',$params);
    }

    /**
     * Метод предназначен для предварительного расчета премии по условиям страхования.
     * Размер премии после сохранения договора может отличаться от поученного на предварительном расчете.
     * @param array $params
     * @return array
     */
    public function getTariff(array $params): array
    {
        $params['SessionToken'] = $this->sessionToken;
        return $this->makeRequest('GetTariff',$params);
    }

    /**
     * Метод предназначен для регистрации нового договора в информационной системе страховой компании Ингосстрах.
     * @param array $params
     * @return mixed
     */
    public function createAgreement(array $params)
    {
//        dd($params);
        $params['SessionToken'] = $this->sessionToken;
        $xmlData = $this->generateXml($params,'ns:CreateAgreementRequest');

        return $this->makeCurlRequest($xmlData,'CreateAgreement');
    }

    /**
     * Метод предназначен для расчета, либо повторного пересчета премии по существующему в компании Ингосстрах договору и формированию начислений.
     * @param string $policyNumber
     * @return array
     */
    public function calcAgreement(string $policyNumber): array
    {
        $params = [
            'SessionToken' => $this->sessionToken,
            'PolicyNumber' => $policyNumber,
        ];

        return $this->makeRequest('CalcAgreement',$params);
    }

    /**
     * Метод предназначен для получения заключенного ранее договора.
     * @param string|null $policyNumber
     * @return array
     */
    public function getAgreement(?string $policyNumber): array
    {
        $params = [
            'SessionToken' => $this->sessionToken,
            'PolicyNumber' => $policyNumber,
            'Original' => 'Y'
        ];

        return $this->makeRequest('GetAgreement',$params);
    }

    /**
     * Производит аннулирование договора, не вступившего в силу.
     * Договор должен быть не оплачен и иметь статус Заявка или Заявление.
     * @param string $policyNumber
     * @return array
     */
    public function terminateAgreement(string $policyNumber): array
    {
        $params = [
            'SessionToken' => $this->sessionToken,
            'PolicyNumber' => $policyNumber,
        ];

        return $this->makeRequest('TerminateAgreement',$params);
    }

    /**
     * Переводит договор в статус “Выпущен”.
     * Внесение изменений после выпуска договора возможно только через создание аддендума.
     * @param string $policyNumber
     * @return array
     */
    public function issueAgreement(string $policyNumber): array
    {
        $params = [
            'SessionToken' => $this->sessionToken,
            'PolicyNumber' => $policyNumber,
        ];

        return $this->makeRequest('IssueAgreement',$params);
    }

    /**
     * Метод позволяет создать счёт на произвольное число договоров для заданного плательщика.
     * @param array $agreementList
     * @return array
     */
    public function createBill(array $agreementList): array
    {
        $params = [
            'SessionToken' => $this->sessionToken,
            'Payer' => 'Customer',
            'AgreementList' => [],
        ];

        foreach ($agreementList as $AgrID) {
            $params['AgreementList'][]['AgrID'] = $AgrID;
        }

        return $this->makeRequest('CreateBill',$params);
    }

    /**
     * Метод предназначен для предоставления клиенту ссылки на страницу онлайн оплаты счета.
     * Клиент, получивший ссылку, при переходе по ней попадет на веб-страницу, где сможет оплатить счет, выставленный на него.
     * Также партнер может указать адрес электронной почты для получения электронного полиса.
     * @param array $bill
     * @return array
     */
    public function createOnlineBill(array $bill): array
    {
        $params = [
            'SessionToken' => $this->sessionToken,
            'Bill' => $bill,
        ];

        $response = $this->makeRequest('CreateOnlineBill ',$params);

        return array_get($response,'PayURL');
    }

    /**
     * Делает полис ОСАГО электронным.
     * Требуется AgrISN.
     * В ответ придут данные о БСО для ЕОСАГО.
     * @param string $agrISN
     * @return string
     */
    public function makeEOsago(string $agrISN): string
    {
        $params = [
            'AgrISN'       => $agrISN,
            'SessionToken' => $this->sessionToken
        ];
        $response = $this->makeRequest('MakeEOsago',$params);

        return array_get($response,'Bso.Serial') . array_get($response,'Bso.Number');
    }

    /**
     * @param string $xmlData
     * @param string $action
     * @return array
     */
    public function makeCurlRequest(string $xmlData,string $action): array
    {
        $headers =  [
            'Content-type: text/xml;charset="utf-8"',
            'Accept: text/xml',
            'Cache-Control: no-cache',
            'Pragma: no-cache',
            'SOAPAction: ' . $this->soapActionPrefix . $action,
            'Content-length: ' . \strlen($xmlData),
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_URL, $this->wsdl . '?wsdl');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlData);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($ch);
        curl_close($ch);

        $decodeResponse = (new XmlEncoder())->decode((string) $response, 'array');

        return array_first(array_get($decodeResponse,'s:Body'));
    }

    /**
     * Auth in service and get SessionToken
     */
    private function auth(): void
    {
        $user = $this->isProduction ?
            config('services.ingos.login_production') :
            config('services.ingos.login')
        ;

        $password = $this->isProduction ?
            config('services.ingos.password_production') :
            config('services.ingos.password')
        ;

        $params = [
            'User'     => $user,
            'Password' => $password
        ];

        $this->sessionToken = array_get($this->makeRequest('Login',$params),'SessionToken');
    }

    /**
     * @param string $method
     * @param $params
     * @return array
     */
    private function makeRequest(string $method, $params): array
    {
        $response = $this->soapClient->$method($params);

        return (array) $response->ResponseData;
    }

    /**
     * @param array $params
     * @param string $rootTag
     * @return string
     */
    private function generateXml(array $params,string $rootTag): string
    {
        $data = [
            '@xmlns:soapenv' => 'http://schemas.xmlsoap.org/soap/envelope/',
            '@xmlns:ns' => 'http://aisws.ingos.ru/services/b2b/sales/agents/1.0/',
            '#' => [
                'soapenv:Header' => null,
                'soapenv:Body' => [
                    $rootTag => $params,
                ],
            ],
        ];

        return (new XmlEncoder())->encode($data, 'xml', [
            'xml_root_node_name' => 'soapenv:Envelope',
            'xml_encoding' => 'utf-8',
        ]);
    }
}
