<?php

namespace App\Service\PolicyService\Client\Rgs;

use App\Model\ApiError;
use GuzzleHttp\Client;
use WsdlToPhp\WsSecurity\WsSecurity;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class CurlClient
{
	/**
     * @var string
     */
    protected $url;

    /**
     * @var string
     */
    protected $wsdl = '/Osago2Service.svc?singlewsdl';

    /**
     * @var string
     */
    public $login;

    /**
     * @var string
     */
    public $password;

    /**
     * @var array
     */
    public $headers = [];

    /**
     * @var string
     */
	public $xml;

    public $soapAction = 'Rgs.Ufo/IOsago2Service/';

    public $messageId = 'urn:uuid:df18b300-909e-4f50-b7c7-50247b4b1c0c';

    /**
     * @var string
     */
    public $method;

	/**
	 * Client constructor.
	 */
	public function __construct(string $method = '')
	{
        $this->method = $method;

		$this->url = env('RGS') === 'production' ? config('services.rgs.production_endpoint') : config('services.rgs.test_endpoint');
        $this->login = env('RGS') === 'production' ? config('services.rgs.login_production') : config('services.rgs.login');
        $this->password = env('RGS') === 'production' ? config('services.rgs.password_production') : config('services.rgs.password');

        $this->headers[] = 'Content-Type: application/soap+xml;charset=UTF-8';
	}

    /**
     * @param array $params
     */
	public function request(array $params = [])
    {
        $xml = $this->getXml($params);

        if ($this->method == 'calculate') {
            \Log::info([
                'key' => 'request calculate',
                'data' => $xml
            ]);
        }

        if ($this->method == 'finalCalculate') {
            \Log::info([
                'key' => 'request finalCalculate',
                'data' => $xml
            ]);
        }

        if ($this->method == 'CheckPayment') {
            \Log::info([
                'key' => 'request CheckPayment',
                'data' => $xml
            ]);
        }

        if ($this->method == 'SendVerificationCode') {
            \Log::info([
                'key' => 'request SendVerificationCode',
                'data' => $xml
            ]);
        }

        if ($this->method == 'sendVerificationCodeRequest') {
            \Log::info([
                'key' => 'request sendVerificationCodeRequest',
                'data' => $xml
            ]);
        }

        if ($this->method == 'ConfirmVerificationCode') {
            \Log::info([
                'key' => 'request ConfirmVerificationCode',
                'data' => $xml
            ]);
        }

        if ($this->method == 'confirmVerificationCodeRequest') {
            \Log::info([
                'key' => 'request confirmVerificationCodeRequest',
                'data' => $xml
            ]);
        }

        if ($this->method == 'startPaymentRequest') {
            \Log::info([
                'key' => 'request startPaymentRequest',
                'data' => $xml
            ]);
        }

        if ($this->method == 'StartPayment') {
            \Log::info([
                'key' => 'request StartPayment',
                'data' => $xml
            ]);
        }

        if ($this->method == 'Print') {
            \Log::info([
                'key' => 'request Print',
                'data' => $xml
            ]);
        }

        try {
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $this->url.$this->wsdl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_USERPWD, $this->login . ':' . $this->password);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);

            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                throw new BadRequestHttpException(curl_error($ch));
                //echo 'Error:' . curl_error($ch);
            }

            curl_close ($ch);
        } catch (ServerException $e) {
            throw new BadRequestHttpException($e->getMessage());
            //dd($e->getMessage());
        }

        $result = str_replace('Rgs.Ufo', 'http://Rgs.Ufo', (string) $result);
        $result = (new XmlEncoder())->decode((string) $result, 'array');

        //print_r(array_get($result, 's:Body.CalculateResponse.CalculateResult.b:ValidationCodeErrors'));exit;

        if(!empty(array_get($result, 's:Body.CalculateResponse.CalculateResult.b:ValidationCodeErrors'))) {
            ApiError::getError(array_get($result, 's:Body.CalculateResponse.CalculateResult.b:ValidationCodeErrors'));
        }
        elseif(!empty(array_get($result, 's:Body.s:Fault.s:Reason'))) {
            ApiError::getError(array_get($result, 's:Body.s:Fault'));
        }

        return $result;
    }

    /**
     * Method for genearate xml doom
     * Formed header, action
     *
     * @param array @param
     * @return string
     */
    public function getXml(array $params)
    {
        if ($this->method == 'Print') {
            return (new XmlEncoder())->encode($this->getMainScheme($params), 'xml', [
                'xml_root_node_name' => 'soap:Envelope',
                'xml_encoding' => 'utf-8',
            ]);
        }

        return (new XmlEncoder())->encode($this->getMainScheme($params), 'xml', [
            'xml_root_node_name' => 'env:Envelope',
            'xml_encoding' => 'utf-8',
        ]);
	}

    private function getMainScheme(array $params): array
    {
        if ($this->method == 'Print') {
            return [
                '@xmlns:soap' => 'http://www.w3.org/2003/05/soap-envelope',
                '@xmlns:rgs' => 'Rgs.Ufo',
                '@xmlns:rgs1' => 'Rgs.Ufo',
                '#' => [
                    'soap:Header' => [
                        '@xmlns:wsa' => 'http://www.w3.org/2005/08/addressing',
                        '#' => $this->generateHeader(),
                    ],
                    'soap:Body' => $this->formedBody($params),
                ],
            ];
        }

        return [
            '@xmlns:env' => 'http://www.w3.org/2003/05/soap-envelope',
            '@xmlns:ns1' => 'http://tempuri.org/',
            '@xmlns:ns2' => 'http://www.w3.org/2005/08/addressing',
            '@xmlns:soap' => 'http://www.w3.org/2003/05/soap-envelope',
            '@xmlns:rgs' => 'Rgs.Ufo',
            '@xmlns:xsi' => 'xsi',
            '#' => [
                'env:Header' => $this->generateHeader(),
                'env:Body' => $this->formedBody($params),
            ],
        ];
    }

    private function formedBody(array $params)
    {
        if ($this->method === 'StartPayment') {
            return [
                'rgs:StartPayment' => $params,
            ];
        }

        if ($this->method == 'SendVerificationCode') {
            return [
                'rgs:SendVerificationCode' => $params
            ];
        }

        if ($this->method == 'ConfirmVerificationCode') {
            return [
                'rgs:ConfirmVerificationCode' => $params
            ];
        }

        if ($this->method == 'CheckPayment') {
            return [
                'CheckPayment' => [
                    '@xmlns' => 'Rgs.Ufo',
                    '#' => $params,
                ],
            ];
        }

        if ($this->method == 'Print') {
            return [
                'rgs:Print' => [
                    '#' => $params
                ],
            ];
        }

        return [
            'rgs:Calculate' => $params,
        ];
    }

    public function generateHeader(): array
    {
        if ($this->method == 'Print') {
            return [
                'wsse:Security' => [
                    '@xmlns:wsse' => 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd',
                    '@xmlns:wsu' => 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd',
                    '#' => [
                        'wsse:UsernameToken' => [
                            'wsse:Username' => $this->login,
                            'wsse:Password' => [
                                '@Type' => 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText',
                                '#' => $this->password,
                            ],
                        ],
                    ],
                ],
                'wsa:Action' => [
                    '@soap:mustUnderstand' => '1',
                    '#' => $this->getAction(),
                ],
                'wsa:MessageID' => [
                    '@soap:mustUnderstand' => '1',
                    '#' => $this->messageId,
                ],
                'wsa:To' => [
                    '@soap:mustUnderstand' => '1',
                    '#' => $this->url . '/Osago2Service.svc',
                ],
            ];
        }

        return [
            'ns2:MessageID' => $this->messageId,
            'ns2:ReplyTo' => [
                'ns2:Address' => 'http://www.w3.org/2005/08/addressing/anonymousfaCalculate',
            ],
            'ns2:To' => [
                '@s:mustUnderstand' => '1',
                '@xmlns:s' => 's',
                '#' => $this->url . '/Osago2Service.svc?singlewsdl',
            ],
            'o:Security' => [
                '@s:mustUnderstand' => '1',
                '@xmlns:o' => 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd',
                '@xmlns:s' => 's',
                '#' => [
                    'o:UsernameToken' => [
                        '@u:Id' => 'uuid-728f6038-1949-4f93-b5ea-447271427c01-1',
                        '@xmlns:u' => 'u',
                        '#' => [
                            'o:Username' => $this->login,
                            'o:Password' => [
                                '@Type' => 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText',
                                '#' => $this->password,
                            ],
                        ],
                    ],
                ],
            ],
            'ns2:Action' => $this->getAction(),
        ];
    }

    private function getAction()
    {
        if ($this->method == 'StartPayment') {
            return $this->soapAction . 'StartPayment';
        }

        if ($this->method == 'SendVerificationCode') {
            return $this->soapAction . 'SendVerificationCode';
        }

        if ($this->method == 'ConfirmVerificationCode') {
            return $this->soapAction . 'ConfirmVerificationCode';
        }

        if ($this->method == 'CheckPayment') {
            return $this->soapAction . 'CheckPayment';
        }

        if ($this->method == 'Print') {
            return $this->soapAction . 'Print';
        }

        return $this->soapAction . 'Calculate';
    }
}
