<?php

namespace App\Service\PolicyService\Client\Rgs;

use SoapVar;
use SoapClient;
use GuzzleHttp\Client;
use App\Helper\PolicyLogHelper;
use WsdlToPhp\WsSecurity\WsSecurity;
use Artisaninweb\SoapWrapper\SoapWrapper;
use Symfony\Component\Serializer\Encoder\XmlEncoder;

/**
 * Class WsSecurityClient
 * @package App\Service\PolicyService\Client\Rgs
 * @property string $wsdl
 */
class WsSecurityClient extends \SoapClient
{
	/**
	 * @var \SoapClient
	 */
	protected $soapClient;

	/**
	 * Login for auth in Rosgostrah
	 * @var string
	 */
	protected $login;

	/**
	 * Password for auth in Rosgostrah
	 * @var string
	 */
	protected $password;

	protected $url;

	/**
	 * @var string
	 */
	protected $correlationId = null;

	/**
	 * WsSecurityClient constructor.
	 */
	public function __construct()
	{
		$this->url = env('RGS') === 'production' ? config('services.rgs.production_endpoint') : config('services.rgs.test_endpoint');

		ini_set('default_socket_timeout', 1200);
		ini_set('soap.wsdl_cache_enabled', 0);
		ini_set('soap.wsdl_cache_ttl', 0);

		$url = env('RGS') === 'production' ? config('services.rgs.production_endpoint') : config('services.rgs.test_endpoint');
		$this->login = env('RGS') === 'production' ? config('services.rgs.login_production') : config('services.rgs.login');
		$this->password = env('RGS') === 'production' ? config('services.rgs.password_production') : config('services.rgs.password');
	}

	/**
	 * Make request with WsSecurity
	 * @param string $method
	 * @param array $params
	 * @return array
	 */
	protected function makeRequest(string $method, array $params): array
	{
		$this->guzzleClient($params);

		$headers = WsSecurity::createWsSecuritySoapHeader($this->login, $this->password);

		$this->soapClient->__setSoapHeaders($headers);

		try {
			dd($this->soapClient->FindServiceStations());die;

			// dd($this->soapClient->__getTypes());

			$result = $this->soapClient->CheckPayment($params);
		} catch (\SoapFault $sf) {
			dd($sf);
		} catch (\Exception $e) {
			dd($e->getMessage());
		}

		PolicyLogHelper::getInstance('policy')
            ->setStringLog($this->soapClient->__getLastRequest())
            ->setStringLog($this->soapClient->__getLastResponse());

		return (array) $result;
	}

	public function guzzleClient($params)
	{
		$client = new CurlClient();
		$result = $client->request($params);

		dd($result);

		die();
	}

    public function getResult()
    {
    	return '';
    }
}
