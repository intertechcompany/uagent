<?php

namespace App\Service;

use App\Model\UserDashboardBlock;
use App\User;
use Illuminate\Contracts\Auth\Guard;

class UserSettingsService
{
    /**
     * @var Guard
     */
    private $guard;

    public function __construct(Guard $guard)
    {
        $this->guard = $guard;
    }

    /**
     * @param array $blocks
     * @return bool
     */
    public function saveBlocks(array $blocks): bool
    {
        /**
         * @var User $user
         */
        $user = $this->guard->user();
        $userBlocks = $user->dashboardBlocks()->get()->keyBy('key');

        foreach ($blocks as $key => $block) {
            $blockUpdate = $userBlocks[$key] ?? new UserDashboardBlock();
            $blockUpdate->user_id = $user->id;
            $blockUpdate->key = $key;
            $blockUpdate->is_enabled = (int)$block;
            $blockUpdate->save();
        }

        return true;
    }
}
