<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

	public function verify(User $parentUser, User $user)
	{
		if ($parentUser->role !== User::ROLE_ADMIN) {
			return false;
		}

		if ($user->role !== User::ROLE_AGENT) {
			return false;
		}

		return !$user->isVerificationProcessed();
    }

	public function decline(User $parentUser, User $user)
	{
		if ($parentUser->role !== User::ROLE_ADMIN) {
			return false;
		}

		if ($user->role !== User::ROLE_AGENT) {
			return false;
		}

		return !$user->isVerificationProcessed();
    }
}
