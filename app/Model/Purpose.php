<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Purpose
 * @package App\Model
 * @property string name
 * @property int id
 */
class Purpose extends Model
{
    public $timestamps = false;

	public static function importPurpose(array $purposes): array
	{
		$collection = [];
		foreach ($purposes as $purpose) {
			$check = self::where('name',$purpose->purposeName)->first();
			if (empty($check)) {
				$newSelf = new self();
				$newSelf->name = $purpose->purposeName;
				$newSelf->save();
				$collection[] = $newSelf;
			}
		}
		return $collection;
	}
}
