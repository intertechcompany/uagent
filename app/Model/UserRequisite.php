<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserRequisite
 * @package App\Model
 * @property string full_name
 * @property string kpp
 * @property string leader_organization
 * @property string ogrn
 * @property string based
 * @property string bank
 * @property string legal_address
 * @property string bik
 * @property string mailing_address
 * @property string settlement_account
 * @property string phone
 * @property string ks
 * @property string inn
 * @property string payer_description
 */
class UserRequisite extends Model
{
	public $table = 'user_requisites';
	
	protected $fillable = [
		'full_name',
        'kpp',
        'leader_organization',
        'ogrn',
        'based',
        'bank',
        'legal_address',
        'bik',
        'mailing_address',
        'settlement_account',
        'phone',
        'ks',
        'inn',
        'payer_description',
	];
}
