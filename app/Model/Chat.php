<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Chat extends Model
{
    protected $dates = ['created_at', 'last_date'];

    CONST STATUS_NEW = 1;
    CONST STATUS_IN_WORK = 2;
    CONST STATUS_CLOSED = 3;

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function messages(): HasMany
    {
        return $this->hasMany(Message::class);
    }

    public function lastMessage(): HasOne
    {
        return $this->hasOne(Message::class)->orderBy('id', 'desc');
    }

    public function unreadMessages()
    {
        return $this->messages()->unread();
    }

    public function countUnreadMessages()
    {
        $user = auth()->user();

        if($user->role == User::ROLE_ADMIN) {
            $count = $this->messages()->where('is_read', 0)->whereHas('user', function($q) use($user) {
                $q->where('id', '<>', $user->id)->where('role', '<>', User::ROLE_ADMIN);
            })->count();
        }
        else {
            $count = $this->messages()->where('is_read', 0)->where('user_id', '<>', $user->id)->count();
        }

        return $count;
    }

    public function showStatus()
    {
        return trans('chat.status.'.$this->status);
    }
}
