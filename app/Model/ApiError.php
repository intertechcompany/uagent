<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ApiError extends Model
{
    public $timestamps = false;
    public $fillable = ['code','original'];

    public static function getError($data)
    {
        $errors = [];

        if(is_array($data) && !empty($data['c:KeyValuePairOfstringstring'])) {

            $tmp = isset($data['c:KeyValuePairOfstringstring']['c:key']) ? [$data['c:KeyValuePairOfstringstring']] : $data['c:KeyValuePairOfstringstring'];

            foreach($tmp as $error) {
                $er = self::firstOrCreate(['code' => $error['c:key']], ['original' => $error['c:value']]);
                session()->push('apiErrors.rgs', $er);
                $errors[] = $er;
            }
        }
        elseif(is_array($data) && !empty($data['s:Reason'])) {
            $text = trim(array_get($data,'s:Reason.s:Text.#'));
            $data = [];
            $data['text'] = $text;
        }

        if(!empty($errors)) {
            return $errors;
        }

        if(isset($data->ErrorList) && !empty($data->ErrorList)) {
            foreach((array)$data->ErrorList as $error) {
                if(!is_array($error)) {
                    $error = [$error];
                }

                foreach($error as $e) {
                    $er = self::firstOrCreate(['code' => $e->Code], ['original' => $e->Message]);
                    session()->push('apiErrors.alpha', $er);
                    $errors[] = $er;
                }
            }
        }
        if(!empty($data->RsaPersonMessageList) && empty($data->InsuranceBonus)) {
            $er = self::firstOrCreate(['code' => hash('md5', $data->RsaPersonMessageList->RsaPersonMessage->Message)], ['original' => $data->RsaPersonMessageList->RsaPersonMessage->Message]);
            session()->push('apiErrors.alpha', $er);
            $errors[] = $er;
        }
        if(!empty($data->RsaCarMessageList) && empty($data->InsuranceBonus)) {
            $er = self::firstOrCreate(['code' => hash('md5', $data->RsaCarMessageList->RsaCarMessage->Message)], ['original' => $data->RsaCarMessageList->RsaCarMessage->Message]);
            session()->push('apiErrors.alpha', $er);
            $errors[] = $er;
        }
        if(!empty($data->commentList) && empty($data->InsuranceBonus)) {

            if(is_array($data->commentList)) {
                $comments = $data->commentList;
            } else {
                $comments[] = $data->commentList;
            }

            foreach($comments as $comment) {
                $er = self::firstOrCreate(['code' => hash('md5', $comment)], ['original' => $comment]);
                session()->push('apiErrors.alpha', $er);
                $errors[] = $er;
            }
        }

        if(!empty($errors)) {
            return $errors;
        }

        $data['text'] = trim($data['text']);
        $data['text'] = mb_strlen($data['text']) > 255 ? mb_substr($data['text'], 0, 255) : $data['text'];

        if(empty($data['code'])) {
            $data['code'] = hash('md5', $data['text']);
        }

        return self::firstOrCreate(['code'=>$data['code']], ['original'=>$data['text']]);
    }
}
