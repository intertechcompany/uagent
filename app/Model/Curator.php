<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Collection;

/**
 * Class Curator
 * @package App\Model
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $email
 * @property iterable $insurance
 * @property string $password
 * @property int $user_id
 * @property string $full_name
 * @property User user
 * @property Collection agents
 */
class Curator extends Model
{
	protected $fillable = [
		'first_name',
		'last_name',
		'phone',
		'email',
		'insurance',
		'password',
		'user_id',
	];

	protected $casts = [
	    'insurance' => 'array'
    ];

    /**
     * @return BelongsTo
     */
	public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return mixed
     */
	public function agents()
	{
		return $this->belongsToMany(User::class, 'curator_agent')->agents();
	}

    /**
     * @return string
     */
	public function getFullNameAttribute(): string
	{
		return $this->first_name . ' ' . $this->last_name;
	}

    /**
     * @return iterable
     */
	public function getInsurance(): iterable
	{
		return $this->insurance;
	}
}
