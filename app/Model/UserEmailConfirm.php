<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserEmailConfirm extends Model
{
    /**
     * @return BelongsTo
     */
	public function user(): BelongsTo
	{
		return $this->belongsTo(User::class);
	}
}
