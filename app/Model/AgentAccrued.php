<?php

namespace App\Model;

use Auth;
use Illuminate\Database\Eloquent\Model;

class AgentAccrued extends Model
{
	public $table = 'agent_accrued';
	
	protected $fillable = [
		'sum',
		'date',
		'file',
		'agent_id',
		'balance',
		'status',
	];
}
