<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Type
 * @package App\Model
 * @property string name
 * @property int id
 */
class Type extends Model
{
	public $timestamps = false;

    /**
     * @return HasMany
     */
    public function policies(): HasMany
	{
		return $this->hasMany(Policy::class);
	}

    /**
     * @return BelongsTo
     */
	public function category(): BelongsTo
	{
		return $this->belongsTo(Category::class);
	}

	/**
	 * Import Types
	 * @param array $types
	 * @param $alphaCategories
	 * @return array
	 */
	public static function importType(array $types,$alphaCategories): array
	{
		$collection = [];
		$categories = Category::all()->keyBy('name');

		foreach ($types as $type) {
			$checkTemp = self::where('name',$type->typeName)->first();
			if (empty($checkTemp)) {
				$categoryTemp = $categories[$alphaCategories[$type->categoryId]->categoryName];
				$newTemp = new self();
				$newTemp->name = $type->typeName;
				$newTemp->category_id = $categoryTemp->id;
				$newTemp->save();
				$collection[] = $newTemp;
			}
		}
		return $collection;
	}
}
