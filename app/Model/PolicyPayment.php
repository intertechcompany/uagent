<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class PolicyPayment
 * @package App\Model
 * @property int policy_id
 * @property string orderId
 * @property string formUrl
 * @property string description
 * @property string token
 * @property string invoice_id
 * @property string price
 * @property Policy policy
 */
class PolicyPayment extends Model
{
    // only for pay from balance
    CONST STATUS_NEW = 1;
    CONST STATUS_PAID = 2;
    CONST STATUS_CANCELED = 3;

    /**
     * @return BelongsTo
     */
	public function policy(): BelongsTo
	{
		return $this->belongsTo(Policy::class);
	}
}
