<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserPaymentInformation
 * @package App\Model
 * @property string number_cart
 * @property string cart_date
 * @property string number_qiwi
 */
class UserPaymentInformation extends Model
{
	public $table = 'user_payment_information';

	protected $fillable = [
		'number_cart',
		'cart_date',
		'number_qiwi',
	];
}
