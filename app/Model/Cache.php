<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Cache
 * @package App\Model
 * @property string key
 * @property string value
 */
class Cache extends Model
{
    public $timestamps = false;
	public $incrementing = false;
}
