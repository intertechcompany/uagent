<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Category
 * @package App\Model
 * @property string name
 * @property int id
 */
class Category extends Model
{
    public $timestamps = false;

    /**
     * @return HasMany
     */
	public function types(): HasMany
	{
		return $this->hasMany(Type::class);
	}

	public static function importCategory(array $categories): array
	{
		$collection = [];
		foreach ($categories as $category) {
			$check = self::query()->where('name',$category->categoryName)->first();
			if (empty($check)) {
				$newSelf = new self();
				$newSelf->name = $category->categoryName;
				$newSelf->save();
				$collection[] = $newSelf;
			}
		}
		return $collection;
	}
}
