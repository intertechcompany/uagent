<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class KvBonus
 * @package App\Model
 * @property int policy_id
 * @property int user_id
 * @property float sum
 * @property float kv
 * @property string type
 * @property bool is_sub_agent
 * @property bool is_owner_policy
 */
class KvBonus extends Model
{
    /**
     * @return BelongsTo
     */
    public function policy(): BelongsTo
    {
        return $this->belongsTo(Policy::class);
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
