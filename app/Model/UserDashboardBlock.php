<?php

namespace App\Model;

use App\Components\Dashboard\Types\CountSalesPoliciesAllType;
use App\Components\Dashboard\Types\CountSalesPoliciesMonthType;
use App\Components\Dashboard\Types\CountSalesPoliciesTodayType;
use App\Components\Dashboard\Types\IncomeAllType;
use App\Components\Dashboard\Types\IncomeMonthType;
use App\Components\Dashboard\Types\IncomeTodayType;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class UserDashboardBlock
 * @package App\Model
 * @property string key
 * @property int sort_order
 * @property int user_id
 * @property int is_enabled
 */
class UserDashboardBlock extends Model
{

    /**
     * RUS BLOCKS
     */
    public const BLOCKS = [
        CountSalesPoliciesTodayType::KEY => 'Продано полисов (сегодня)',
        CountSalesPoliciesMonthType::KEY => 'Продано полисов (за этот месяц)',
        CountSalesPoliciesAllType::KEY => 'Продано полисов за все время',
        IncomeTodayType::KEY => 'Доход за день',
        IncomeMonthType::KEY => 'Доход за месяц',
        IncomeAllType::KEY => 'Доход за все время',
    ];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
