<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ModelCodeCar
 * @package App\Model
 * @property string code
 * @property string brand
 * @property string model
 */
class ModelCodeCar extends Model
{
	public $table = 'rgs_brand_directory_cars';
	
	protected $fillable = [
		'code',
		'brand',
		'model',
	];

	public function scopeByBrand($query, $brand)
	{
		return $query->where($this->table . '.brand', $brand->name);
	}

	public function scopeByModel($query, $model)
	{
		return $query->where($this->table . '.model', $model->name);
	}
}
