<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Owner
 * @package App\Model
 * @property string $last_name
 * @property string $first_name
 * @property string middle_name
 * @property string dob
 * @property string personal_document_serial
 * @property string personal_document_number
 * @property string personal_document_issued_by
 * @property string personal_document_date
 * @property string place_of_birth
 * @property string house
 * @property string address
 * @property string address2
 * @property string address3
 * @property string post_code
 * @property int type
 * @property int id
 */
class Owner extends Model
{
    /**
     *ВЛАДЕЛЕЦ
     */
    public const OWNER = 1;
    /**
     * страхователь
     */
    public const INSURER = 2;

    /**
     * @return BelongsTo
     */
    public function policy()
    {
        return $this->hasOne(Policy::class);
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->first_name . ' ' . $this->last_name;
    }
}
