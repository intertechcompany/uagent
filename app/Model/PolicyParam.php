<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PolicyParam
 * @package App\Model
 * @property string key
 * @property string value
 */
class PolicyParam extends Model
{
    /**
     * ALPHA
     */
    public const ALPHA_UPID = 'upid';

    /**
     * NASKO
     */
    public const NASKO_TRANSACTION_XML = 'transaction_xml';
    public const NASKO_PAYMENT_STEP = 'payment_step';
    public const NASKO_IS_SAVE_TO_OSAGO = 'is_save_to_osago';
    public const NASKO_INVOICE_ISN = 'nasko_invoice_isn';

    /**
     * RGS
     */
    public const RGS_CALCULATION_ID = 'rgs_calculation_id';

    protected $fillable = ['key', 'value'];
}
