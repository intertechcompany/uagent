<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * Class File
 * @package App\Model
 * @property string dir
 * @property string name
 */
class File extends Model
{
	public function file(): MorphTo
	{
		return $this->morphTo();
	}

	public function getPath(): string
	{
		return '/media/' . $this->dir . '/' . $this->name;
	}
}
