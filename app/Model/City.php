<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class City
 * @package App\Model
 * @property string name
 * @property int id
 */
class City extends Model
{
	public function user(): HasOne
	{
		return $this->hasOne(User::class);
	}

	public function users(): HasMany
	{
		return $this->hasMany(User::class);
	}

	public function scopeWithAgents(Builder $query): Builder
	{
		return $query->whereHas('users')->orderBy('name', 'asc');
	}
}
