<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Street
 * @package App\Model
 * @property int id
 * @property string name
 */
class Street extends Model
{
    public $timestamps = false;

    /**
     * @return BelongsTo
     */
	public function city(): BelongsTo
	{
		return $this->belongsTo(City::class);
	}
}
