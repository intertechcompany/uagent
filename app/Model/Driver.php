<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Driver
 * @package App\Model
 * @property string first_name
 * @property string last_name
 * @property string middle_name
 * @property string dob
 * @property string document_serial
 * @property string document_number
 * @property string start_date
 * @property string id
 */
class Driver extends Model
{
    public function policies(): BelongsToMany
    {
        return $this->belongsToMany(Policy::class,'policy_driver');
    }
}
