<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 * Class AgentFrontendRate
 * @package App\Model
 * @property string from
 * @property string to
 * @property string value
 * @property string type_name
 * @property string type
 */
class AgentFrontendRate extends Model
{
	public $table = 'agent_frontend_rates';
	
	protected $fillable = [
		'type',
		'value',
		'insurance',
	];

	public static $types = [
		'rub' => 'руб.',
		'percent' => '%',
	];

    /**
     * @return null|string
     */
	public function getTypeNameAttribute(): ?string
	{
		return self::$types[$this->type] ?? null;
	}

    /**
     * @return string
     */
	public function getFullNameAttribute(): string
	{
		return "$this->from-$this->to $this->value $this->type_name";
	}

	public function scopePageAgent($query)
	{
        /**
         * @var User $authUser
         */
        $authUser = Auth::user();

		if (User::ROLE_CURATOR === $authUser->role && $authUser->curator !== null) {
            $query->whereIn('insurance', $authUser->curator->getInsurance());
		}

		return $query;
	}
}
