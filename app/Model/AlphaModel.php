<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class AlphaModel extends Model
{
	public $timestamps = false;

    /**
     * @return HasMany
     */
	public function policy(): HasMany
	{
		return $this->hasMany(Policy::class);
	}

    /**
     * @return BelongsTo
     */
	public function make(): BelongsTo
	{
		return $this->belongsTo(AlphaMake::class,'mark_id');
	}

	public static function importModelFromAlpha(array $models,int $makeId): Collection
	{
		$alphaModels = self::where('mark_id',$makeId)->get()->keyBy('name');
		foreach ($models as $model) {
			if (!isset($alphaModels[$model->value])) {
				$newMake = new self();
				$newMake->name = $model->value;
				$newMake->mark_id = $makeId;
				$newMake->save();
				$alphaModels[$model->value] = $newMake;
				echo $model->value . "\n";
			}
		}
		return $alphaModels;
	}
}
