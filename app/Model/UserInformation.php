<?php

namespace App\Model;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class UserInformation
 * @package App\Model
 * @property string address
 * @property string birthday
 * @property string avatar
 */
class UserInformation extends Model
{

	public static $fields = [
		'address',
		'birthday',
		'gender',
		'avatar',
		'is_two_step_auth',
        'is_remind_change_pass',
        'is_send_email_suspiciou_login',
	];

    public function user(): BelongsTo
	{
		return $this->belongsTo(User::class);
	}

    /**
     * @return string|null
     */
	public function getBirthdayDateAttribute(): ?string
    {
        return $this->birthday !== null ? Carbon::parse($this->birthday)->format('d-m-Y') : null;
    }
}
