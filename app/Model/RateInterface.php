<?php

namespace App\Model;

interface RateInterface
{
    /**
     * @return float
     */
    public function getValue(): float;

    /**
     * @return string
     */
    public function getType(): string;

    /**
     * @return string
     */
    public function getTypeOfRate(): string;

    /**
     * @return bool
     */
    public function isCommission(): bool;

    /**
     * @return int
     */
    public function getRateId(): int;

    /**
     * @return bool
     */
    public function isHaveParent(): bool;
}
