<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * Class SaveUserWithdraw
 * @package App\Model
 * @property string field
 * @property string value
 * @property string type
 */
class SaveUserWithdraw extends Model
{
	public $table = 'save_user_withdraw';
	
	protected $fillable = [
		'field',
		'value',
		'type',
	];

	public function scopeProcessSave($query, string $field, Request $request)
	{
		return $query->where('type', $request->get('type'))
			->where('field', $field);
	}

	public function scopeByType($query, string $type = null)
	{
		return $query->where('type', $type);
	}

	public function scopeByField($query, string $field = null)
	{
		return $query->where('field', $field);
	}
}
