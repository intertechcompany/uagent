<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Message extends Model
{
    public $fillable = ['message', 'user_id'];
    protected $dates = ['created_at'];

    public function chat(): BelongsTo
    {
        return $this->belongsTo(Chat::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function scopeUnread($query)
    {
        $user = auth()->user();

        if($user->role == User::ROLE_ADMIN) {
            $query->where('is_read', 0)->whereHas('user', function($q) use($user) {
                    $q->where('id', '<>', $user->id)->where('role', '<>', User::ROLE_ADMIN);
                });
        } else {
            $query->whereHas('chat', function($q) use($user) {
                $q->where('user_id', $user->id);
            })->where('is_read', 0)->where('user_id', '<>', $user->id);
        }
    }
}
