<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class VehicleMake extends Model
{
    /**
     * @var bool
     */
	public $timestamps = false;

    /**
     * @return HasMany
     */
	public function models(): HasMany
	{
		return $this->hasMany(VehicleModel::class);
	}

	/**
	 * @return HasOne
	 */
	public function getModel(): HasOne
	{
		return $this->hasOne(VehicleModel::class);
	}
}
