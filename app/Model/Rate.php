<?php

namespace App\Model;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Rate
 * @package App\Model
 * @property int id
 * @property string from
 * @property string to
 * @property string type
 * @property float value
 * @property string type_name
 * @property string full_name
 * @property string insurance
 * @property boolean is_default
 * @method static Builder pageAgent()
 */
class Rate extends Model implements RateInterface
{

	public static $types = [
		'rub' => 'руб.',
		'percent' => '%',
	];

	public static $rules = [
        'from' => 'required|integer|min:0',
        'to' => 'required|integer|min:1',
        'type' => 'required|string|max:255',
        'value' => 'required|integer',
        'insurance' => 'required',
    ];

	protected $fillable = [
		'from',
		'to',
		'type',
		'value',
		'insurance',
        'is_default',
	];

    public function agents(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'agent_rate', 'rate_id', 'agent_id');
    }

    /**
     * @inheritdoc
     */
	public function getType(): string
    {
        return $this->type;
    }

    /**
     * @inheritdoc
     */
    public function getValue(): float
    {
        return (float) $this->value;
    }

    /**
     * @return string
     */
    public function getTypeOfRate(): string
    {
        return Commission::TYPE_OF_RATE_RATE;
    }

    /**
     * @return bool
     */
    public function isCommission(): bool
    {
        return false;
    }

    /**
     * @return bool
     */
    public function isHaveParent(): bool
    {
        return false;
    }

    /**
     * @return int
     */
    public function getRateId(): int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
	public function getTypeNameAttribute(): ?string
	{
        return self::$types[$this->type] ?? null;
	}

    /**
     * @return string
     */
	public function getFullNameAttribute(): string
	{
		return "$this->from-$this->to $this->value $this->type_name";
	}

    /**
     * @param Builder $query
     * @return Builder
     */
	public function scopePageAgent(Builder $query): Builder
	{
        /**
         * @var User $authUser
         */
        $authUser = Auth::user();

		if (User::ROLE_CURATOR === $authUser->role && $authUser->curator) {
		    $query->whereIn('insurance', $authUser->curator->getInsurance());

		}

		return $query;
	}

    public function scopeFilter($query, Request $request)
    {
        $filters = array_filter($request->all());

        if (\count($filters) === 0) {
            return $query;
        }

        foreach ($filters as $filter => $value) {
            $this->setFilter($query, $filter, $value);
        }

        return $query;
    }

    public function setFilter(Builder $query, string $filter, $value = null): void
    {
        switch ($filter) {
            case 'insurances':
                if (is_array($value)) {
                    $query->whereIn('insurance', array_column($value, 'code'));

                    break;
                }

                break;
            case 'cities':
                if (is_array($value)) {
                    $query->whereIn('insurance', array_column($value, 'code'));

                    break;
                }

                break;
        }
    }
}
