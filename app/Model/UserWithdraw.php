<?php

namespace App\Model;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class UserWithdraw
 * @package App\Model
 * @property string requisites
 * @property float amount
 * @property float current_balance
 * @property string email
 * @property string type
 * @property string status
 */
class UserWithdraw extends Model
{

	public const BANK_CARD = 'bank_card';
	public const YANDEX = 'yandex';
	public const REQUISITES = 'requisites';

	public $table = 'user_withdraw';

	protected $fillable = [
		'requisites',
		'amount',
		'email',
		'card_number',
		'type',
		'current_balance',
	];

	public static $statuses = [
		'in_progress' => 'В обработке',
		'approved' => 'Одобрено',
		'canceled' => 'Аннулировано',
	];

	/**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

	public static function getPaymentMethods(): array
	{
		return [
			'bank_card' => 'Банковская карта',
			'yandex' => 'Яндекс деньги',
			'requisites' => 'Реквизиты ИП / Юр. лица',
		];
	}

    /**
     * @return string|null
     */
	public function getTypeName(): ?string
	{
	    switch ($this->type) {
            case self::BANK_CARD:
                return 'Банковская карта';
            case self::YANDEX:
                return 'Яндекс деньги';
            case self::REQUISITES:
                return 'Реквизиты ИП / Юр. лица';
            default:
                return null;
        }
	}

	/**
	 * @return array
	 */
	public static function getRequisitesFields()
	{
		return [
			'full_name' => 'Полное наименование юр. лица',
			'leader_organization' => 'Представитель/руководитель организации',
			'based' => 'На основании',
			'legal_address' => 'Юридический адрес',
			'mailing_address' => 'Почтовый адрес',
			'phone' => 'Телефон',
			'inn' => 'ИНН',
			'kpp' => 'КПП',
			'ogrn' => 'ОГРН',
			'bank' => 'Банк',
			'bik' => 'БИК',
			'settlement_account' => 'Р/с',
			'ks' => 'К/с',
			'payer_description' => 'Описание плательщик',
		];
	}

	public static function getVueStatuses(array $statuses = []): array
	{
		foreach (self::$statuses as $key => $status) {
			$statuses[] = [
				'key' => $key,
				'value' => $key,
				'text' => $status,
			];
		}

		return $statuses;
	}

	public function getVueStatus()
	{
		foreach (self::getVueStatuses() as $status) {
			if ($this->status == array_get($status, 'key')) {
				return $status;
			}
		}

		return array_first(self::getVueStatuses());
	}

	public function getStatusNameAttribute()
	{
		return !empty(self::$statuses[$this->status]) ? self::$statuses[$this->status] : array_get(
            array_first(self::getVueStatuses()),
            'text'
        );;
	}

	public function scopeFilter($query, Request $request)
	{
		$filters = array_filter($request->all());
		$filters['date'] = $request->only('from', 'to');
		$filters['balance'] = $request->only('balance_agent_from', 'balance_agent_to');
		$filters['amount'] = $request->only('get_summ_from', 'get_summ_to');

		if (isset($filters['page'])) {
			unset($filters['page']);
		}

		if (\count($filters) === 0) {
    		return $query;
    	}

        foreach ($filters as $filter => $value) {
            $this->setFilter($query, $filter, $value);
        }

        return $query;
	}

	public function setFilter(Builder $query, string $filter, $value = null): void
    {
        switch ($filter) {
            case 'transaction_id':
            	if (is_string($value)) {
	                $query->where('id', $value);

	                break;
                }

                break;
            case 'statuses':
            	if (is_array($value) && !empty($value) && count($value) > 0) {
	            	$query->whereIn('status', array_column($value, 'code'));

	            	break;
            	}
            	break;
        	case 'payment_method':
        		if (is_array($value) && !empty($value) && count($value) > 0) {
	            	$query->whereIn('type', array_column($value, 'code'));

	            	break;
            	}

            	break;
            case 'agents':
                $query->whereHas('user', function($queryUser) use ($value) {
                	$queryUser->where('id', $value);
                });

                break;

            /**
        	 * Filter for get query by current_balance column from, to balance
        	 */
            case 'balance':
            	$query->where(function (Builder $queryBalance) use ($value) {

                    if (!empty($value['balance_agent_from'])) {
                        $queryBalance->where('current_balance','>=', array_get($value, 'balance_agent_from'));
                    }

                    if (!empty($value['balance_agent_to'])) {
                        $queryBalance->where('current_balance','<=', array_get($value, 'balance_agent_to'));
                    }

                    return $queryBalance;
                });
            	break;

        	/**
        	 * Filter for get query by amount column from, to amount
        	 */
        	case 'amount':
            	$query->where(function (Builder $queryAmount) use ($value) {

                    if (!empty($value['get_summ_from'])) {
                        $queryAmount->where('amount','>=', array_get($value, 'get_summ_from'));
                    }

                    if (!empty($value['get_summ_to'])) {
                        $queryAmount->where('amount','<=', array_get($value, 'get_summ_to'));
                    }

                    return $queryAmount;
                });
            	break;
            case 'date':
                $query->where(function (Builder $queryDate) use ($value) {

                    if (!empty($value['from'])) {
                        $queryDate->where('created_at','>=',new Carbon($value['from']));
                    }

                    if (!empty($value['to'])) {
                        $queryDate->where('created_at','<=',new Carbon($value['to']));
                    }

                    return $queryDate;
                });

                break;
        }
    }
}
