<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Storage;

/**
 * Class PolicyDocument
 * @package App\Model
 * @property string file_name
 * @property string type
 * @property int policy_id
 */
class PolicyDocument extends Model
{
    public const POLICY_FILE = 'policy';
    public const STORAGE_DIST = 'policyDocuments';

    /**
     * @return BelongsTo
     */
    public function policy(): BelongsTo
    {
        return $this->belongsTo(Policy::class);
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return storage_path('policies/documents/' . $this->file_name);
    }

    public function fileExist()
    {
        return  Storage::disk(PolicyDocument::STORAGE_DIST)->exists($this->file_name);
    }
}
