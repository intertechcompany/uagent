<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * Class Log
 * @package App\Model
 * @property string action
 * @property string log_type
 * @property string description
 * @property int log_id
 */
class Log extends Model
{
    /**
     * @return MorphTo
     */
	public function log(): MorphTo
	{
		return $this->morphTo();
	}
}
