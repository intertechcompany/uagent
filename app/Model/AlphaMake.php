<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class AlphaMake extends Model
{
    /**
     * @var bool
     */
	public $timestamps = false;

    /**
     * @return BelongsTo
     */
    public function policy(): BelongsTo
	{
		return $this->belongsTo(Policy::class);
	}

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
	public function models(): HasMany
	{
		return $this->hasMany(AlphaModel::class,'mark_id');
	}

	public static function importFromAlpha(array $makes): Collection
	{
		$alphaMakes = self::all()->keyBy('name');
		foreach ($makes as $make) {
			if (!isset($alphaMakes[$make->value])) {
				$newMake = new self();
				$newMake->name = $make->value;
				$newMake->alpha_id = $make->id;
				$newMake->save();
				$alphaMakes[$make->value] = $newMake;
				echo $make->value;
			}
		}

		return $alphaMakes->keyBy('alpha_id');
	}
}
