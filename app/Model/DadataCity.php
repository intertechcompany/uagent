<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class DadataCity
 * @package App\Model
 * @property string name
 * @property string kladr_id
 * @method static self|Builder findByKladr(string $kladr)
 */
class DadataCity extends Model
{
	protected $fillable = [
		'name',
		'kladr_id',
	];

    /**
     * @return HasOne
     */
	public function rate(): HasOne
	{
		return $this->hasOne(RateCity::class, 'city_id', 'kladr_id');
	}

    /**
     * @param Builder $query
     * @param $kladr
     */
	public function scopeFindByKladr(Builder $query, $kladr): void
	{
	    $query->where('kladr_id', $kladr);
	}
}
