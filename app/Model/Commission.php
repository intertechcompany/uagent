<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Commission
 * @package App\Model
 * @property float all
 * @property float you
 * @property float agent
 * @property string type
 * @property string insurance
 * @property string rate_type
 * @property int rate_id
 * @property int parent_commission_id
 * @property int agent_id
 * @property int id
 * @property Rate rate
 * @property User subAgent
 * @property Commission parentCommission
 */
class Commission extends Model implements RateInterface
{
    public const RUB = 'rub';
    public const PERCENT = 'percent';
    public const TYPE_OF_RATE_RATE = 'rate';
    public const TYPE_OF_RATE_RATE_CITY = 'rate_city';

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'all',
        'you',
        'agent',
        'type',
        'rate_id',
        'insurance',
        'rate_type',
    ];

    /**
     * @inheritdoc
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @inheritdoc
     */
    public function getValue(): float
    {
        return (float) $this->agent;
    }

    /**
     * @return string
     */
    public function getTypeOfRate(): string
    {
        return $this->rate_type;
    }

    /**
     * @return bool
     */
    public function isCommission(): bool
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isHaveParent(): bool
    {
        return $this->parent_commission_id !== null;
    }

    /**
     * @return int
     */
    public function getRateId(): int
    {
        return $this->rate_id;
    }

    /**
     * @return BelongsTo
     */
    public function rateCity(): BelongsTo
    {
        return $this->belongsTo(RateCity::class,'rate_id');
    }

    /**
     * @return BelongsTo
     */
    public function rate(): BelongsTo
    {
        return $this->belongsTo(Rate::class);
    }

    /**
     * @return BelongsTo
     */
    public function parentCommission(): BelongsTo
    {
        return $this->belongsTo(__CLASS__,'parent_commission_id');
    }

    /**
     * @return HasMany
     */
    public function childrenCommission(): HasMany
    {
        return $this->hasMany(__CLASS__,'parent_commission_id');
    }

    /**
     * @return BelongsTo
     */
    public function subAgent(): BelongsTo
    {
        return $this->belongsTo(User::class,'agent_id');
    }
}
