<?php

namespace App\Model;

use App\User;
use Carbon\Carbon;
use App\Model\PolicyParam;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

/**
 * Class Policy
 * @package App\Model
 * @property int|null id
 * @property float premium
 * @property int user_id
 * @property int owner_id
 * @property int insurer_id
 * @property int make_id
 * @property int model_id
 * @property int version
 * @property string diagnostic_number
 * @property string diagnostic_until_date
 * @property string driver_count
 * @property string email
 * @property string insurer_and_owner_same
 * @property string is_electronic
 * @property string $insurance
 * @property string phone
 * @property string policy_start_date
 * @property float power
 * @property string vehicle_document_date
 * @property string vehicle_document_number
 * @property string vehicle_document_serial
 * @property string vehicle_document_type
 * @property string vehicle_id
 * @property string vehicle_id_type
 * @property string vehicle_plate_number
 * @property string vehicle_plate_region
 * @property string vehicle_registration_year
 * @property string is_vehicle_plate_absent
 * @property string is_vehicle_use_trailer
 * @property int type_id
 * @property int purpose_id
 * @property int is_save_to_osago
 * @property string insurance_id
 * @property string insurance_number
 * @property string status
 * @property string kladr_id
 * @property string region_kladr_id
 * @property Owner insurer
 * @property Owner $owner
 * @property User user
 * @property PolicyPayment payment
 * @property Collection documents
 * @property KvBonus kvBonusForOwner
 * @method static Builder|self dashboard
 * @method static Builder|self filter(array $filters)
 * @method static Builder|self search(Request $request)
 */
class Policy extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public const PAGINATE = 20;

	public const NASKO = 'nasko';
	public const ALPHA = 'alpha';
	public const INGOS = 'ingos';
	public const RGS = 'rgs';

	public const OLD_VERSION = 1;
	public const NEW_VERSION = 2;

	public const SUCCESS_STATUS = 'success';
	public const PAID_STATUS = 'paid';
	public const DECORATED_STATUS = 'decorated';
	public const DRAFT_STATUS = 'draft';
	public const FORBIDDEN_STATUS = 'forbidden';
	public const IN_PAYMENT_STATUS = 'inPayment';

	public const VIN_TYPE = 'vin';
	public const BODY_NUMBER_TYPE = 'body_number';
	public const CHASSIS_NUMBER_TYPE = 'chassis_number';

	public static $activeInsurances = [
	    self::NASKO,
        self::ALPHA,
        self::RGS,
    ];

	public static $activeInsurancesWithRus = [
        self::NASKO => 'Наско',
        self::ALPHA => 'АльфаСтрахование',
        self::RGS => 'Росгосстрах',
    ];

	public static $statuses = [
		self::SUCCESS_STATUS => 'Оформлен',
		self::PAID_STATUS => 'Оплачен',
		self::DECORATED_STATUS => 'Аннулирован',
		self::DRAFT_STATUS => 'Черновик',
		self::FORBIDDEN_STATUS => 'Отказан',
        self::IN_PAYMENT_STATUS => 'Ожидается оплата'
	];

	public static $types = [
	    self::VIN_TYPE => 'VIN номер',
        self::BODY_NUMBER_TYPE => 'Номер кузова',
        self::CHASSIS_NUMBER_TYPE => 'Номер шасси',
    ];

	public $searchableFields = [
		'insurance_id',
		'diagnostic_number',
		'driver_count',
		'email',
		'phone',
		'power',
		'vehicle_document_number',
		'vehicle_document_serial',
		'vehicle_document_type',
		'vehicle_plate_number',
		'vehicle_plate_region',
		'vehicle_registration_year',
		'premium',
		'insurance',
		'status',
		'insurance_number',
		'kladr_id',
        'region_kladr_id',
        'kt',
        'kbm',
        'kvs',
	];

	public static function insurance($insurance = null, $forBlade = false)
	{
		$data = [
			//self::NASKO => 'Наско',
			self::ALPHA => 'АльфаСтрахование',
            self::RGS => 'Росгосстрах',
		];

		if (null === $insurance && !$forBlade) {
			return $data;
		}

		return $data[$insurance] ?? null;
	}

	public function user(): BelongsTo
	{
		return $this->belongsTo(User::class);
	}

    public function owner(): BelongsTo
    {
        return $this->belongsTo(Owner::class,'owner_id');
    }

    public function insurer(): BelongsTo
    {
        return $this->belongsTo(Owner::class,'insurer_id');
    }

    public function drivers(): BelongsToMany
    {
        return $this->belongsToMany(Driver::class,'policy_driver');
    }

    public function make(): BelongsTo
	{
		if ($this->insurance === self::ALPHA && $this->version === self::OLD_VERSION) {
			return $this->belongsTo(AlphaMake::class);
		}
		return $this->belongsTo(VehicleMake::class);
	}

	public function model(): BelongsTo
	{
		if ($this->insurance === self::ALPHA && $this->version === self::OLD_VERSION) {
			return $this->belongsTo(AlphaModel::class);
		}
		return $this->belongsTo(VehicleModel::class);
	}

	public function file(): MorphMany
	{
		return $this->morphMany(File::class, 'file');
	}

	public function type(): BelongsTo
	{
		return $this->belongsTo(Type::class);
	}

	public function payment(): HasOne
	{
		return $this->hasOne(PolicyPayment::class);
	}

    public function kvBonus(): HasMany
    {
        return $this->hasMany(KvBonus::class);
    }

    public function kvBonusForOwner(): HasOne
    {
        return $this->hasOne(KvBonus::class,'policy_id','id');
    }

    public function params(): HasMany
    {
        return $this->hasMany(PolicyParam::class);
    }

    public function documents(): HasMany
    {
        return $this->hasMany(PolicyDocument::class);
    }

	// methods

	public function getDateTimeStartPolicy(): Carbon
	{
		if (\is_string($this->policy_start_date)) {
			$this->policy_start_date = new Carbon($this->policy_start_date);
		}

		return $this->policy_start_date;
	}

	public function getStatusRus(): string
	{
		return self::$statuses[$this->status];
	}

	public function getVehicleType(): string
    {
        return self::$types[$this->vehicle_id_type];
    }

    /**
     * @return array
     */
    public function getKladers(): array
    {
        return [
            'city_kladr_id' => $this->kladr_id,
            'region_kladr_id' => $this->region_kladr_id
        ];
    }

	public function scopeDashboard(Builder $query): void
	{
        /**
         * @var User $authUser
         */
        $authUser =  $authUser = Auth::user();

        $query
            ->whereNotNull('user_id')
            ->orderBy('id', 'DESC')
        ;

		if (User::ROLE_CURATOR === $authUser->role) {
            $query->whereHas('user.curators.user', function($q) use($authUser) {
                $q->where('id', $authUser->id);
            });
        }
	}
	
	public function scopeSearch(Builder $query, Request $request): void
	{
		if (!$request->has('term')) {
			return;
		}

		$query->where('id', 'like', '%' . $request->get('term'));

		foreach ($this->searchableFields as $column) {
			$query->orWhere($column, 'like', '%' . $request->get('term'));
		}

		$query->orWhere(function(Builder $query) use ($request): void {
			$query->whereHas('owner', function(Builder $queryPolicy) use ($request): void {
                $queryPolicy->where('last_name', 'like', '%' . $request->get('term') . '%')
				->orWhere('first_name', 'like', '%' . $request->get('term') . '%')
				->orWhere('middle_name', 'like', '%' . $request->get('term') . '%');

				if (\is_int($request->get('term'))) {
                    $queryPolicy->orWhere('personal_document_date', $request->get('term'))
					->orWhere('personal_document_serial', 'like', '%' . $request->get('term') . '%')
					->orWhere('personal_document_number', 'like', '%' . $request->get('term') . '%')
					;
				}
                $queryPolicy->orWhere('personal_document_issued_by', 'like', '%' . $request->get('term') . '%')
				->orWhere('personal_document_issued_by', 'like', '%' . $request->get('term') . '%')
				->orWhere('place_of_birth', 'like', '%' . $request->get('term') . '%')
				->orWhere('address', 'like', '%' . $request->get('term') . '%')
				->orWhere('address2', 'like', '%' . $request->get('term') . '%')
				->orWhere('address3', 'like', '%' . $request->get('term') . '%')
				->orWhere('phone', 'like', '%' . $request->get('term') . '%')
				->orWhere('email', 'like', '%' . $request->get('term') . '%')
				;
			});
		});
	}

    /**
     * @param Builder $query
     * @param array $filters
     */
	public function scopeFilter(Builder $query, array $filters): void
    {
    	if (\count($filters) === 0) {
    		return;
    	}

        if (isset($filters['number']) && !empty($filters['number'])) {
            $query->where('insurance_number', $filters['number']);
            return;
        }

        foreach ($filters as $filter => $value) {
            $this->setFilter($query, $filter, $value);
        }
    }

    /**
     * @param Builder $query
     * @param string $filter
     * @param null $value
     */
    public function setFilter(Builder $query, string $filter, $value = null): void
    {
        switch ($filter) {
    		case 'city':
                $query->whereHas('user.city', function($queryCity) use ($value) {
                	$queryCity->where('cities.id', $value);
                });

                break;
            case 'insurance':
            	if (\is_array($value)) {
                	$query->whereIn('insurance', $value);

                	break;
                }

                $query->where('insurance', $value);

                break;
            case 'insurances':
                $query->whereIn('insurance', $value);

                break;
            case 'statuses':
                if (\is_array($value)) {
                	$query->whereIn('status', $value);

                	break;
                }

                $query->where('status', $value);

                break;
            case 'agents':
            	if (\is_array($value)) {
                	$query->whereIn('user_id', $value);

                	break;
                }

                $query->where('user_id', $value);

                break;
            case 'curators':
                if (\is_array($value)) {
                    $query->whereHas('user.curators', function($queryCurator) use ($value) {
                        $queryCurator->whereIn('curators.id', $value);
                    });

                    break;
                }

                break;
            case 'cities':
                if (\is_array($value)) {
                    $query->whereHas('user.city', function($queryCity) use ($value) {
                        $queryCity->whereIn('cities.id', $value);
                    });

                    break;
                }

                $cities = array_map(function ($item){
                    return explode('#', $item)[0];
                }, $value);

                $query->whereIn('kladr_id', $cities);

                break;
            case 'ownercities':
                if (\is_array($value)) {
                    $query->whereHas('owner', function($queryOwner) use ($value) {

                        $index = 0;
                        foreach ($value as $item) {

                            if ($index == 0) {
                                $queryOwner->where('address', 'like', '%' . $item . '%');
                            }

                            if ($index > 0) {
                                $queryOwner->orwhere('address', 'like', '%' . $item . '%');
                            }

                            $index++;
                        }
                    });

                    break;
                }

                break;
            case 'date':
                $query->where(function (Builder $queryDate) use ($value) {
                    if (!empty($value['from'])) {
                        $queryDate->where('created_at', '>=', new Carbon($value['from']));
                    }
                    elseif (!empty($value[0])) {
                        $queryDate->where('created_at', '>=', new Carbon($value[0]));
                    }

                    if (!empty($value['to'])) {
                        $queryDate->where('created_at', '<=', Carbon::parse($value['to'])->addDay());
                    }
                    elseif (!empty($value[1])) {
                        $queryDate->where('created_at', '<=', Carbon::parse($value[1])->addDay());
                    }

                    return $queryDate;
                });

                break;
        }
    }

    /**
     * @param Builder $query
     * @param array $sort
     */
    public function scopeSortable(Builder $query, array $sort): void
    {
    	$position = array_get($sort,'position') === 'up' ? 'ASC' : 'DESC';
    	
		if (array_get($sort,'sort') === 'words') {

			$query->select('*');
	        $query->addSelect('policies.*');
	        $query->addSelect('owners.first_name')
	        ->join('owners', 'policies.owner_id', '=', 'owners.id')
	        ->orderBy('owners.first_name', $position);

	        return;
    	}

		if (array_get($sort,'sort') === 'date') {
            $query->orderBy('policy_start_date', $position);
    		return;
    	}

        $query->orderBy('policies.id','DESC');
    }

    /**
     * @param string $param
     * @return 
     */
    public function findParam(string $param): PolicyParam
    {
    	return $this->params()
    		->where('key', $param)
    		->first() ?? null;
    }
}
