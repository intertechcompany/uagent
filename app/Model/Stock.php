<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Stock
 * @package App\Model
 * @property string title
 * @property string from
 * @property string to
 * @property string insurance
 * @property int is_enabled
 * @property string text
 * @property string information
 */
class Stock extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'from',
        'to',
        'insurance',
        'is_enabled',
        'text',
        'information',
    ];

    public function getInsurance(): array
	{
		if (!empty($this->insurance)) {
			$insurance = json_decode($this->insurance,true);

			return \is_array($insurance) && \count($insurance) > 0 ? $insurance : [];
		}

		return [];
	}

    /**
     * @return string|null
     */
	public function getFromTimeAttribute(): ?string
    {
    	return $this->getTimeFormat('from');
    }

    /**
     * @return string|null
     */
    public function getToTimeAttribute(): ?string
    {
    	return $this->getTimeFormat('to');
    }

    /**
     * @return string
     */
    public function getEnabledAttribute(): string
    {
    	return $this->is_enabled ? 'Да' : 'Нет';
    }

    /**
     * @param Builder $query
     */
    public function scopeEnabled(Builder $query): void
    {
        $query->where('is_enabled' . true);
    }

    /**
     * @param $column
     * @param string $format
     * @return string|null
     */
    private function getTimeFormat($column, $format = 'Y-m-d H:i'): ?string
    {
    	if (empty($this->$column)) {
    		return null;
    	}

    	$time = Carbon::createFromTimeString($this->$column);

    	return $time->format($format);
    }
}
