<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AgentPaidout
 * @package App\Model
 * @property float sum
 * @property string date
 * @property string file
 * @property int agent_id
 * @property float balance
 */
class AgentPaidout extends Model
{
	public $table = 'agent_paidout';
	
	protected $fillable = [
		'sum',
		'date',
		'file',
		'agent_id',
		'balance',
	];
}
