<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Question
 * @package App
 * @property string $name
 * @property string $phone
 * @property string $message
 * @property string $ip
 * @property string $user_agent
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Question extends Model
{
    public $fillable = [
        'name',
        'phone',
        'ip',
        'user_agent',
        'message'
    ];

    public static $emails = [
        '6550011@mail.ru',
        '288776@mail.ru'
    ];
}
