<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class VehicleModel extends Model
{
    /**
     * @var bool
     */
	public $timestamps = false;

    /**
     * @return BelongsTo
     */
	public function make(): BelongsTo
	{
		return $this->belongsTo(VehicleMake::class,'vehicle_make_id');
	}
}
