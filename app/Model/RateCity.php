<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Http\Request;

/**
 * Class RateCity
 * @package App\Model
 * @property int id
 * @property string city_id
 * @property float value
 * @property string name
 * @property string type
 * @property string insurance
 * @property DadataCity city
 * @method static self|Builder byInsurance(string $insurance)
 */
class RateCity extends Model implements RateInterface
{
	protected $fillable = [
		'insurance',
		'city_id',
		'type',
		'value',
	];

    /**
     * @return HasOne
     */
	public function city(): HasOne
	{
		return $this->hasOne(DadataCity::class, 'kladr_id', 'city_id');
	}
    
    /**
     * @inheritdoc
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @inheritdoc
     */
    public function getValue(): float
    {
        return (float) $this->value;
    }

    /**
     * @return string
     */
    public function getTypeOfRate(): string
    {
        return Commission::TYPE_OF_RATE_RATE_CITY;
    }

    /**
     * @return bool
     */
    public function isCommission(): bool
    {
        return false;
    }

    /**
     * @return bool
     */
    public function isHaveParent(): bool
    {
        return false;
    }

    /**
     * @return int
     */
    public function getRateId(): int
    {
        return $this->id;
    }

    /**
     * @param Request $request
     * @return Builder|DadataCity|Model|object|null
     */
	public function processCity(Request $request)
	{
		if (!$request->has('city_id')) {
			return null;
		}

		$kladr = DadataCity::findByKladr($request->get('city_id'))->first();

		if ($kladr !== null) {
			return $kladr;
		}

		return DadataCity::query()->create([
			'name' => $request->get('address'),
			'kladr_id' => $request->get('city_id'),
		]);
	}

    /**
     * @return string
     */
	public function getFullNameAttribute(): ?string
	{
		return $this->city ? $this->city->name : null;
	}

    /**
     * @return string
     */
	public function getInsuranceNameAttribute(): string
	{
		$insurance = Policy::insurance($this->insurance);

		return \is_string($insurance) ? $insurance : '';
	}

    /**
     * @return string|null
     */
	public function getTypeNameAttribute(): ?string
	{
		return Rate::$types[$this->type] ?? null;
	}

    /**
     * @param Builder $query
     * @param $insurance
     */
	public function scopeByInsurance(Builder $query, $insurance): void
	{
	    $query->where('insurance', $insurance);
	}
}
