<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAgent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required',
            'last_name' => 'required',
            'phone' => 'required',
        ];

        if (!$this->route('id')) {
            $rules['password'] = 'string|min:6';
          //  $rules['password_confirmation'] = 'required_with:password|same:password|min:6';
            $rules['email'] = 'required|email|unique:users,email';
        }

        return $rules;
    }
}
