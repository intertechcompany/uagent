<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Curator;
use App\Role;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class CuratorController extends Controller
{

//    public function roles($data, $id)
//    {
//        $data = push
//        Validator::make($data, [
//            'email' => [
//                'required',
//                Rule::unique('users')->where(function ($query) {
//                    $query->where('id','!=',);
//                }),
//            ],
//        ]);
//    }


    /**
     * @param Request $request
     * @param Curator|null $curator
     * @param string $action
     * @return array
     */
    public function rules(Request $request, Curator $curator = null, $action = 'create'): array
    {
        $rules = [
            'first_name' => 'required|alpha|max:255',
            'last_name' => 'required|alpha|max:255',
            'phone' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
        ];

        if ($action === 'create' || $request->get('password') !== null) {
            $rules['password'] = 'required|string|min:6';
        }

        if ($curator !== null && $curator->user) {
            $rules['email'] = 'required|string|email|max:255|unique:users,email' . $curator->user->id;
        }

        return $rules;
    }

	/**
	 * @return JsonResponse
	 */
    public function index(): JsonResponse
    {
        $curator = Curator::all();

        return response()->json($curator);
    }


    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        /**
         * @var Curator $curator
         */
        $curator = Curator::query()->with('agents')->find($id);

        $curator->agents = $curator->agents->map(function (User $user) {
            return [
                'code' => $user->id,
                'name' => $user->name,
                'last_name' => $user->last_name,
            ];
        });

        return response()->json($curator);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id)
    {
        $curator = Curator::query()->find($id);
        $dataCurator = $request->get('curator');

        $this->validate($request, [
            'curator.first_name' => 'required|alpha|max:255',
            'curator.last_name' => 'required|alpha|max:255',
            'curator.phone' => 'required|string|max:255',
            'curator.email' => 'required|string|email|max:255|unique:users,email,'.$curator->user_id,
        ]);

        $curator->first_name = $dataCurator['first_name'];
        $curator->last_name = $dataCurator['last_name'];
        $curator->phone = $dataCurator['phone'];
        $curator->email = $dataCurator['email'];


        $agents = collect($request->get('agents',[]))->pluck('id');

        $insurance = $dataCurator['insurance'];
        $insurance = array_column($insurance,'name');

        if ($dataCurator['password'] !== null && $dataCurator['password'] !== '') {
            $curator->password = Hash::make($dataCurator['password']);
        }

        $curator->agents()->sync($agents);

		if ($insurance !== null) {
			$curator->insurance = $insurance;
		}

        $curator->save();

        return response()->json([
        	'url' => '/admin/curators',
            'status' => 'success',
            'message' => 'Куратор ' . $curator->full_name . ' обновлен!',
        ]);
    }

    public function store(Request $request): JsonResponse
    {
        $this->validate($request, $this->rules($request));

		$insurances = $this->checkInsurances($request);

        /**
         * @var Curator $curator
         */
        $curator = Curator::query()->create([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'phone' => $request->get('phone'),
            'email' => $request->get('email'),
            'insurance' => $insurances,
            'password' => Hash::make($request->get('password')),
        ]);

        if ($curator !== null) {

            $curator->agents()->sync(collect($request->get('agents',[]))->pluck('id'));

            if ($user = $this->processUser($request)) {

                $curator->update([
                    'user_id' => $user->id
                ]);

                $role = Role::query()
                    ->where('name', 'curator')
                    ->first()
                ;

                if ($role !== null) {
                    $user->syncRoles($role);
                }

            }
        }

        return response()->json([
            'url' => '/admin/curators',
            'status' => 'success',
            'message' => 'Куратор ' . $curator->full_name . ' создан!',
        ], 200);
    }

    /**
     * @param Request $request
     * @param User|null $user
     * @return User
     */
    private function processUser(Request $request, ?User $user = null): User
    {
        if ($user === null) {
            $user = new User();
        }

        $user->name = $request->get('first_name');
        $user->email = $request->get('email');
        $user->role = User::ROLE_CURATOR;
        $user->is_email_confirmed = true;
        $user->verified_at = now();


        $password = $request->get('password');

        if ($password !== null) {
            $user->password = Hash::make($password);
        }

        $user->save();

        $user->setDefaultValues();

        return $user;
    }

    private function checkInsurances(Request $request): ?array
	{
		if ($request->has('insurance')) {
			$insurances = [];

			foreach ($request->get('insurance',[]) as $insurance) {
				$insurances[] = $insurance['name'];
			}
		} else {
			$insurances = null;
		}

		return $insurances;
	}
}