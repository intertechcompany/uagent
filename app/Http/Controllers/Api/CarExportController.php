<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\VehicleMake;
use App\Model\VehicleModel;
use Excel;

class CarExportController extends Controller
{
    public function download()
    {
        return Excel::create('CarData', function($excel) {

            $excel->sheet('car data', function($sheet) {

                $sheet->with([
                    ['КОЛИЧЕСТВО ЛЕТ ВОЗРАСТА'],
                    ['КБМ (0,5-1)'],
                    [],
                    ['4 ВОДИТЕЛЬ ФИО'],
                    ['ДАТА РОЖДЕНИЯ'],
                    ['В/У СЕРИЯ, НОМЕР'],
                    ['КОЛИЧЕСТВО ЛЕТ СТАЖА'],
                    ['КОЛИЧЕСТВО ЛЕТ ВОЗРАСТА'],
                    ['КОЛИЧЕСТВО ЛЕТ ВОЗРАСТА'],
                    ['КБМ (0,5-1)'],
                    ['МАРКА ТС', VehicleMake::findOrFail(request('make_id'))->name],
                    ['МОДЕЛЬ ТС', VehicleModel::findOrFail(request('model_id'))->name],
                    ['VIN', request('vehicle_id')],
                    ['РЕГ. НОМЕР', request('vehicle_plate_number') . request('vehicle_plate_region')],
                    ['МОЩНОСТЬ (лс)', request('power')],
                    ['ГОД ВЫПУСКА', request('vehicle_registration_year')],
                    ['ВИД ДОКУМЕНТА', request('vehicle_document_type')],
                    ['НОМЕР ДОКУМЕНТА', request('vehicle_document_number')],
                    ['ДАТА ВЫДАЧИ', request('vehicle_document_date')],
                    ['ДИАГНОСТИЧЕСКАЯ КАРТА, НОМЕР', request('diagnostic_number')],
                    ['ДАТА ОКОНЧАНИЯ', request('diagnostic_until_date')],
                    [],
                    ['СУММА СТРАХОВОЙ ПРЕМИИ'],
                ]);

            });

        })->export('xlsx');
    }
}
