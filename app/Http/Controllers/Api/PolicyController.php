<?php

namespace App\Http\Controllers\Api;

use App\Components\Kv\ChargeBonusKvHelper;
use App\Components\Kv\KvHelper;
use App\Helper\PolicyCreateHelper;
use App\Helper\PolicyLogHelper;
use App\Http\Controllers\Controller;
use App\Model\ApiError;
use App\Model\Policy;
use App\Service\PolicyService;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;

class PolicyController extends Controller
{
    /**
     * @var PolicyService
     */
    protected $policyService;

    /**
     * PolicyController constructor.
     * @param PolicyService $policyService
     */
    public function __construct(PolicyService $policyService)
    {
        $this->policyService = $policyService;
    }

    /**
     * @param string $step
     * @throws \Illuminate\Validation\ValidationException
     * @return JsonResponse
     */
    public function validateStep(string $step): JsonResponse
    {
        $this->policyService->validateStepData($step, request()->all());

        return response()->json();
    }

    /**
     * @return JsonResponse
     */
    public function checkAllRules(): JsonResponse
    {
        $data = [
            'errors' => []
        ];

        try {
            $this->policyService->checkFullRules(request()->all());
        } catch (ValidationException $exception) {
            $data['errors'] = $exception->errors();
        }

        return new JsonResponse($data);
    }

    /**
     * @param string $insurance
     * @param User $user
     * @param Request $request
     * @return JsonResponse
     */
    public function calc(string $insurance,User $user,Request $request): JsonResponse
    {
        $data = $request->all();

        $kv = (new KvHelper($user,$insurance,array_get($data,'owner.address.data')))->getKv();

        if (array_get($data, 'insurer_and_owner_same')) {
            $data['insurer'] = $data['owner'];
        }

        $exceptionErr = null;
        $premium = 0;
        $tb = 0;
        $kt = 0;
        $kbm = 0;
        $kvs = 0;
        $policyId = null;
        $error = '';

        try {

            PolicyLogHelper::getInstance('policy')
                ->setStringLog(json_encode($data))
                ->setStringLog("--------\n")
            ;


            $policy = PolicyService\Provider\InsuranceStaticFactory::factory($insurance)->create($data);

            if ($policy !== null) {
                $premium = !empty($policy->premium) ? $policy->premium : 0;
                $tb = $policy->tb;
                $kt = $policy->kt;
                $kbm = $policy->kbm;
                $kvs = $policy->kvs;
                $policyId = $policy->id;
            }

        } catch (\Exception $exception) {
            $exceptionErr = $exception;
        }

        if(session()->has('apiErrors.'.$insurance)) {
            $error = session()->pull('apiErrors.'.$insurance);
            $tempArr = array_unique(array_column($error, 'id'));
            $error = array_intersect_key($error, $tempArr);
            sort($error);
        }

        if ($exceptionErr !== null) {
            if(empty($error)) {
                $error = json_decode($exceptionErr->getMessage());

                if (json_last_error()) {
                    $msg = trim($exceptionErr->getMessage());
                    $msg = mb_strlen($msg) > 255 ? mb_substr($msg, 0, 255) : $msg;
                    $error = ApiError::getError(['text'=>$msg]);
                }
            }

            PolicyLogHelper::getInstance('policy')
                ->setStringLog($exceptionErr->getMessage())
                ->setStringLog($exceptionErr->getFile())
                ->setStringLog($exceptionErr->getTraceAsString())
                ->saveLog(storage_path('policies/provider/fails/' . Carbon::now()->format('d-m-Y') . '/' . $insurance . '-' . (!empty($policy->id) ? $policy->id : @$data['diagnostic_number']) . '.xml'));

            Log::debug($exceptionErr);
        }
        elseif(empty($premium)) {
            PolicyLogHelper::getInstance('policy')
                ->saveLog(storage_path('policies/provider/fails/' . Carbon::now()->format('d-m-Y') . '/' . $insurance . '-' . (!empty($policy->id) ? $policy->id : @$data['diagnostic_number']) . '.xml'));
        }

        return new JsonResponse([
            'drivers' => $insurance == 'alpha' ? @$policy->drivers : [],
            'premium' => $premium,
            'tb' => $tb,
            'kt' => $kt,
            'kbm' => $kbm,
            'kvs' => $kvs,
            'kv' => [
                'percent'  => $kv ? $kv->getValue() : 0,
                'type'     => $kv ? $kv->getType() : null,
                'sum'      => number_format($kv !== null ? ChargeBonusKvHelper::calcSum($kv->getType(),$kv->getValue(),$premium) : 0,2),
            ],
            'policyID'  => $policyId,
            'insurance' => $insurance,
            'err' => $error
        ], 200, [], JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
    }

    /**
     * @param User $user
     * @param Policy $policy
     * @param string $insurance
     * @return JsonResponse
     */
    public function savePolicy(User $user,Policy $policy,string $insurance): JsonResponse
    {
        if ($policy->user_id === null) {
            $policy->user_id = $user->id;
            $policy->insurance = $insurance;
            $policy->save();
        }

        return new JsonResponse([
            'link' => route('policies.show',['policy' => $policy->id])
        ]);
    }

    /**
     * @param Request $request
     * @param Policy $policy
     * @return JsonResponse
     */
    public function createUpdatePolicy(Request $request, ?Policy $policy): JsonResponse
    {
        $data = [
            'errors' => [],
            'link'   => null,
        ];

        $formData = $request->all();

        try {
            $this->policyService->checkFullRules($formData);
        } catch (ValidationException $exception) {
            $data['errors'] = $exception->errors();

            return new JsonResponse($data);
        }

        $formData['premium'] = 0;

        $policyCreator = new PolicyCreateHelper($formData);

        $policy = $policy->status === 'draft' ? $policyCreator->createUpdate($policy) : $policyCreator->createUpdate();

        $policy->user_id = $formData['user_id'];

        $policy->save();

        $data['link'] = route('policies.show',['policy' => $policy->id]);

        return new JsonResponse($data);
    }

	/**
	 * @return JsonResponse
	 */
    public function getAllInsurance()
	{
		$insurances = Policy::$activeInsurancesWithRus;

		if (!$insurances) {
			return response()->json("Активных страховых компаний не найдено", 400);
		}

		$response = [];

		$index = 0;
		foreach ($insurances as $name => $insurance) {
			array_push($response, [
				'code' => $index,
				'name' => $name,
                'lable' => $insurance
			]);
			$index++;
		}

		return response()->json($response, 200);
	}
}
