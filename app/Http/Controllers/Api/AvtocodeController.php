<?php

namespace App\Http\Controllers\Api;

use App\Helper\StringHelper;
use App\Http\Controllers\Controller;
use App\Model\VehicleMake;
use App\Model\VehicleModel;
use App\Service\AvtocodeService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class AvtocodeController extends Controller
{

    public $service;

    public function __construct()
    {
        $this->service = new AvtocodeService();
    }

    /**
     * @return JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function user(): JsonResponse
    {
        $params = ['_detailed' => false];

        $response = $this->service->makeRequest($params, 'user', 'GET');

        return Response::json($response);
    }

    /**
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getReports()
    {
        $params = [
            'query' => [
                '_content' => true,
                '_query' => '_all'
            ]
        ];

        return $this->service->makeRequest($params, 'user/reports', 'GET');
    }



    public function getReport(Request $request)
    {
        $makeReport = $this->service->makeReport($request->get('number'));

        if ($makeReport === null) {
            return response()->json([
                'status' => false,
                'error' => 'Произошла ошибка при формировании отчёта',
            ]);
        }

        $params = [
            'query' => [
                '_content' => true,
            ]
        ];

        for ($count = 0; $count < 15; $count++) {
            $report = $this->service->makeRequest($params, 'user/reports/' . $makeReport->uid, 'GET');
            sleep(1);

            if (isset($report[0]->content->tech_data)) {
                break;
            }
        }

        if (!isset($report[0]->content->tech_data)) {
            return response()->json([
                'status' => false,
                'error' => 'Произошла ошибка при формировании полиса',
            ]);
        } elseif (!isset($report[0]->content->tech_data->brand)) {
            return response()->json([
                'status' => false,
                'error' => 'Произошла ошибка при формировании связаная с маркой машины',
            ]);
        } elseif (!isset($report[0]->content->tech_data->model)) {
            return response()->json([
                'status' => false,
                'error' => 'Произошла ошибка при формировании связаная с моделью машины',
            ]);
        }

        $report = $this->service->makeRequest($params, 'user/reports/' . $makeReport->uid, 'GET');

        if ($report === null) {
            return response()->json([
                'status' => false,
                'error' => 'Произошла ошибка не позволяющая просмотреть отчёт',
            ]);
        }

        $report = $report[0]->content;

        $markName = StringHelper::multiexplode([' ', '.', '|', ':', ',', '(', ')', '/'], $report->tech_data->brand->name->normalized);
        $modelName = StringHelper::multiexplode([' ', '.', '|', ':', ',', '(', ')', '/'], $report->tech_data->model->name->normalized);

        $mark = VehicleMake::where('name', 'like', '%' . strtolower($markName) . '%')->whereHas('getModel', function ($query) use ($modelName) {
            $query->where('name', 'like', '%' . strtolower($modelName) . '%');
        })->first();

		$mark === null ? $model = null : $model = VehicleModel::query()->where('vehicle_make_id', $mark->id)->where('name', 'like', '%' . strtolower($modelName) . '%')->first();

        return response()->json([
            'report' => $report,
            'mark' => $mark,
            'model' => $model,
        ], 200);
    }

    /**
     * @param $report
     * @return JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getInfo($report): JsonResponse
    {
        $params = [
            '_detailed'=> 'true',
            '_content'=> 'true',
        ];

        $response = $this->service->makeRequest($params, 'user/reports/' . $report->uid, 'GET');

        return Response::json($response);
    }
}