<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\City;
use App\Model\Street;
use Illuminate\Http\JsonResponse;

class CitiesController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCities(): JsonResponse
	{
		$cities = City::query()
            ->orderBy('name')
            ->get()
            ->map(function (City $city) {
                return [
                    'text' => $city->name,
                    'value' => $city->id,
                    'key' => $city->id,
                ];
		});

		return response()->json($cities);
	}

    /**
     * @param City $city
     * @return JsonResponse
     */
	public function getStreets(City $city): JsonResponse
	{
		$streets = Street::query()
            ->orderBy('name')
            ->where('city_id',$city->id)
            ->get()
            ->map(function (Street $street) {
                return [
                    'text' => $street->name,
                    'value' => $street->id,
                    'key' => $street->id,
                ];
            })
        ;

		return response()->json($streets);
	}
}
