<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Service\SmsService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class SendMessageController extends Controller
{
    /**
     * @param Request $request
     * @throws \Exception
     * @return JsonResponse
     */
    public function send(Request $request): JsonResponse
    {
        $phone = $request->get('phone');

        if ($phone === null) {
            return new JsonResponse([
                'error' => 'Phone number is invalid!',
            ]);
        }

        $generateCode = random_int(10000,99999);

        $phone = $request->get('phone');
        $message = 'Ваш проверочный код: ' . $generateCode;

        $result = null;

		if (env('MESSAGES') === 'production') {
			$result = (new SmsService)->send($phone,$message);
		}

        return response()->json([
            'code' => $generateCode,
			'result' => $result,
        ]);
    }
}
