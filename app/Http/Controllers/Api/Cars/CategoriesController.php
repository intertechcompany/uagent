<?php

namespace App\Http\Controllers\Api\Cars;

use App\Http\Controllers\Controller;
use App\Model\Category;

class CategoriesController extends Controller
{
	public function index()
	{
		$categories = Category::query()->orderBy('name')->get()->map(function (Category $model) {
			return [
				'text' => $model->name,
				'key' => $model->id,
				'value' => $model->id
			];
		});

		return response()->json($categories);
	}
}
