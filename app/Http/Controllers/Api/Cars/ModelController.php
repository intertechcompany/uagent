<?php

namespace App\Http\Controllers\Api\Cars;

use App\Http\Controllers\Controller;
use App\Model\VehicleMake;

class ModelController extends Controller
{
	public function index(int $makeId)
	{
        /**
         * @var VehicleMake $make
         */
		$make = VehicleMake::query()->findOrFail($makeId);

		$models = $make
            ->models()
            ->orderBy('name')
            ->get()
            ->map(function ($model) {
                return [
                    'text' => $model->name,
                    'key' => $model->id,
                    'value' => $model->id
                ];
		});

		return response()->json($models);
	}

}
