<?php

namespace App\Http\Controllers\Api\Cars;

use App\Http\Controllers\Controller;
use App\Model\Type;

class TypesController extends Controller
{
	public function index()
	{
		$types = Type::query()->orderBy('name')->get()->map(function (Type $model) {
			return [
				'text' => $model->name,
				'key' => $model->id,
				'value' => $model->id
			];
		});

		return response()->json($types);
	}
}
