<?php

namespace App\Http\Controllers\Api\Cars;

use App\Http\Controllers\Controller;
use App\Model\Policy;
use App\Service\CarDetectorService;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Response;

class DetectController extends Controller
{
	public function show(CarDetectorService $carDetectorService)
	{
		$data = request()->all();

		$vehiclePlateRegionRule = 'required|integer|max:999';

		if (isset($data['vehicle_plate_region'])) {
            if ($data['vehicle_plate_region'][0] === 0) {
                $vehiclePlateRegionRule = 'required|integer|max:99';
                $data['vehicle_plate_region'] = (integer)$data['vehicle_plate_region'];
            }
        }


		$validator = \Validator::make($data, [
			'vehicle_plate_number' => 'required|string',
			'vehicle_plate_region' => $vehiclePlateRegionRule,
		]);

		$validator->validate();
		$insurance = array_get($data,'insurance');

//		if ($insurance === Policy::ALPHA) {
			return response()->json([], Response::HTTP_NOT_FOUND);
//		}

		$plateNumber = array_get($data, 'vehicle_plate_number') . array_get($data, 'vehicle_plate_region');



		try {
			$data = $carDetectorService->handle($plateNumber);
		} catch (ClientException $e) {
			return response()->json([], Response::HTTP_NOT_FOUND);
		}

		return response()->json($data);
	}
}
