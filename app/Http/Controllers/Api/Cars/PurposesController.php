<?php

namespace App\Http\Controllers\Api\Cars;

use App\Http\Controllers\Controller;
use App\Model\Purpose;

class PurposesController extends Controller
{
	public function index()
	{
		$purposes = Purpose::query()->orderBy('name')->get()->map(function (Purpose $model) {
			return [
				'text' => $model->name,
				'key' => $model->id,
				'value' => $model->id
			];
		});

		return response()->json($purposes);
	}
}
