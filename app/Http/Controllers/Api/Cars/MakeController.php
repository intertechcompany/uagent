<?php

namespace App\Http\Controllers\Api\Cars;

use App\Http\Controllers\Controller;
use App\Model\VehicleMake;

class MakeController extends Controller
{
	public function index()
	{
		$makes = VehicleMake::query()
            ->orderBy('name')
            ->get()
            ->map(function ($make) {
                return [
                    'text' => $make->name,
                    'value' => $make->id,
                    'key' => $make->id,
                ];
		});

		return response()->json($makes);
	}
}
