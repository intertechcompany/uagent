<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Service\DadataService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class KladrController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getAddress(Request $request): JsonResponse
	{
		$result = [];
		$q = $request->get('q');

		if (!empty($q)) {

			$result = DadataService::suggest([
			    'query' => $q,
                'count' => 20
            ])['suggestions'];

			foreach ($result as $key => $item) {
				$result[$key]['label'] = $item['value'];
 			}
		}

		return new JsonResponse($result);
	}
}
