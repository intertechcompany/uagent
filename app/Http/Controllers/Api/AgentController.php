<?php

namespace App\Http\Controllers\Api;

use App\User;

class AgentController
{
    public function index()
    {
        $agents = User::where('role', 'agent')->get();

        $agents = $agents->map(function (User $user) {
            return [
                'id' => $user->id,
                'name' => $user->name,
                'last_name' => $user->last_name,
                'full_name' => $user->full_name,
            ];
        });

        return response()->json([
            'agents' => $agents,
        ], 200);
    }
}