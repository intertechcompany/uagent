<?php

namespace App\Http\Controllers\Auth;

use App\Helper\UserHelper;
use App\Http\Controllers\Controller;
use App\Service\AuthService;
use App\Service\SmsService;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/#modal_in';

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function register(Request $request)
    {
		$request['phone'] = UserHelper::format_phone($request['phone']);

        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'error' => $validator->getMessageBag(),
            ], 400);
        } elseif ($request->get('fileType') === '1' && $request->get('password') !== (string) session()->get('code')) {
            return response()->json([
                'status' => false,
                'error' => 'Код с смс не совпадает с вашим',
            ]);
        }

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        return $this->registered($request, $user) ?: response()->json([
            'status' => true,
            'path' => $this->redirectPath(),
        ]);
    }

    public function redirectPath()
    {
        switch (auth()->user()->role) {
            case User::ROLE_GUEST:
                return '/settings';
            case User::ROLE_AGENT:
                return '/settings';
            case User::ROLE_ADMIN;
                return '/admin/home';
            case User::ROLE_CURATOR;
                return '/admin/home';
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $params = [];

        if ($data['fileType'] === '1') {
            $params['phone'] = 'required|string|unique:users';
            $params['password'] = 'required|string|min:9|regex:/[a-z]/|regex:/[A-Z]/|regex:/[0-9]/';
			env('RECAPCHA') === 'production' ? $params['g-recaptcha-response'] = 'required' : false;
        } else {
            $params['email'] = 'required|string|email|max:255|unique:users';
            $params['password_confirmation'] = 'sometimes|required_with:reg_password';
            $params['reg_password'] = 'required|string|min:9|confirmed|regex:/[a-z]/|regex:/[A-Z]/|regex:/[0-9]/';
			env('RECAPCHA') === 'production' ? $params['g-recaptcha-response'] = 'required' : false;
        }

        return Validator::make($data, $params);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return app(AuthService::class)->register($data);
    }

    public function sendPassword(Request $request)
    {
        $phone = UserHelper::format_phone($request->get('phone'));

        $user = User::where('phone',$phone)->first();

        if (strlen($phone) !== 11) {
            return [
                'error' => 'Phone number is invalid!',
            ];
        } elseif(isset($user)) {
            return [
                'error' => 'Такой телефон уже зарегистрирован!',
            ];
        }

        $generateCode = random_int(100000000,999999999);

        $session = session()->put('code', $generateCode);

        $message = 'Ваш пароль: ' . $generateCode;

        $result = null;

        $result = (new SmsService)->send($phone,$message);

        return [
            'result' => true,
        ];
    }
}
