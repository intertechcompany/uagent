<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Service\AuthService;
use App\User;

class EmailConfirmController extends Controller
{
	public function __invoke($id, $code)
	{
	    if (auth()->check()) {
            auth()->logout();
        }

		$user = User::withoutGlobalScope('email_confirmed')->findOrFail($id);

		app(AuthService::class)->confirmEmail($user, $code);

		if ($user->parent_user_id === null) {
            session()->flash('success', 'Вы успешно подтвердили свою почту. Ваш аккаунт будет верифицирован администрацией на протяжении 48 часов');
        } else {
		    auth()->login($user);
        }

		return redirect('/');
	}
}