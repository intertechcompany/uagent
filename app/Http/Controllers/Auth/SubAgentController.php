<?php

namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreSubAgent;
use App\Service\AuthService;


class SubAgentController extends Controller
{
	public function index()
	{
        return view('agent.agents.createpass');
	}

        public function store(StoreSubAgent $request)
    {
        $user = auth()->user();

        $data = $request->only('password');
        app(AuthService::class)->createPassForSubAgent($data, $user);

        return redirect('/logout');
    }
}
