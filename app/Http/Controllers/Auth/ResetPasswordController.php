<?php

namespace App\Http\Controllers\Auth;

use App\Helper\UserHelper;
use App\Http\Controllers\Controller;
use App\Mail\ResetPassword;
use App\Service\SmsService;
use App\User;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Illuminate\View\View;

class ResetPasswordController extends Controller
{
    const PHONE = 1;
    const EMAIL = 2;

    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    public function send(Request $request): JsonResponse
    {
        if ((int) $request->get('fileType') === self::EMAIL) {
            $email = $this->sendToEmail($request);

            if ($email['status'] === false) {
                return response()->json($email);
            }

            return response()->json([
                'status' => 'ok',
                'message' => 'Ссылка для восстановление отосланна на данный email ' . $request->get('email'),
            ], 200);
        }

        //if fileType equal self::EMAIL
        $phoneResult = $this->sendToPhone($request);

        if (isset($phoneResult->success) && $phoneResult->success === true) {
			return response()->json([
				'status' => 'ok'
			], 200);

        }

		return response()->json($phoneResult, 400);
    }

    public function showResetForm(string $token = null): View
    {
        return view('agent.home-new', [
            'token' => $token
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function reset(Request $request): JsonResponse
    {
		$rules = [
			'reset_password' => 'required|string|min:9|confirmed|regex:/[a-z]/|regex:/[A-Z]/|regex:/[0-9]/',
			'reset_password_confirmation'=>'sometimes|required_with:reset_password',
			'token' => 'required',
		];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'error' => $validator->getMessageBag(),
            ]);
        }

        if(empty($request->get('token'))) {
            abort(403);
        }

        $user = User::query()->where('token', '=', $request->get('token'))->first();

        if ($user) {
            $user->password = Hash::make($request->get('reset_password'));
            $user->token = null;
            $user->save();

            $this->guard()->login($user);

			$response = [
				'status' => true,
				'path' => $user->role === 'admin'? '/admin/home' : '/home',
			];

            return response()->json($response);
        }

        return response()->json([
            'status' => false,
            'error' => 'Пользователь не найден',
        ]);
    }

    private function sendToEmail(Request $request)
    {
        $email = $request->get('email');
        $user = User::query()->where('email','=',$email)->first();

        if ($user === null) {
            return [
                'status' => false,
                'error' => 'Пользователь с таким email не найден',
            ];
        } else {
            $token = Str::random(32);
            $user->token = $token;
            $user->save();

            $link = route('showResetForm', $token);

            Mail::to($email)->send(new ResetPassword($link));

            return [
                'status' => 'ok'
            ];
        }
    }

    private function sendToPhone(Request $request)
    {
        $phone = UserHelper::format_phone($request->get('phone'));

        if (strlen($phone) !== 11) {
            return [
                'status' => false,
                'error' => 'Введите коректный телефон!',
            ];
        }

        $user = User::query()->where('phone', '=', $phone)->first();

        if ($user === null) {
            return [
                'status' => false,
                'error' => 'Пользователь с таким телефоном не найден',
            ];
        } else {

            $password = Str::random(6);

            $user->password = Hash::make($password);
            $user->save();

            $message = "Ваш новый пароль " . $password . ' (пожалуйста поменяйте его после авторизации).';

            $result = (new SmsService)->send($phone, $message);

            return $result;
        }
    }

}