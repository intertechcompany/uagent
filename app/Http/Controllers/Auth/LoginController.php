<?php

namespace App\Http\Controllers\Auth;

use App\Events\LoginEvent;
use App\Helper\UserHelper;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        $validCredentials = $this->checkCredentials($request);

        if ($validCredentials['status'] == false) {
            return response()->json($validCredentials);
        }


        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            die();

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);
        return $this->sendFailedLoginResponse($request);
    }

    public function sendLoginResponse(Request $request)
    {

        $request->session()->regenerate();


        $this->clearLoginAttempts($request);

        $credentials = [
            'password' => $request->get('fileType') === '1' ? $request->get('password') : $request->get('password2'),
        ];

        if ($request->get('fileType') === '1') {
            $phone = UserHelper::format_phone($request->get('phone'));
            $credentials['phone'] = $phone;
        }else {
            $credentials['email'] = $request->get('email');
        }

        if($auth = Auth::attempt($credentials, true)) {
            $auth = Auth::authenticate();
        }

        if($auth) {
            Auth::login($auth, true);
            event(new LoginEvent($auth));

            return response()->json([
                'path' => $this->redirectPath(),
            ], 200);
        }


        return redirect()->back();
    }

    public function sendFailedLoginResponse(Request $request)
    {
        return redirect()->route('html')
            ->withInput($request->only($this->loginName($request), 'remember'))
            ->withErrors([
                $this->username() => \Lang::get('auth.failed'),
            ]);

    }

    public function redirectPath()
    {
        switch (auth()->user()->role) {
            case User::ROLE_GUEST:
                return '/settings';
            case User::ROLE_AGENT:
                return '/home';
            case User::ROLE_ADMIN;
                return '/admin/home';
            case User::ROLE_CURATOR;
                return '/admin/home';
        }
    }

    protected function attemptLogin(Request $request)
    {
        $loginName = $this->loginName($request);
        $user = User::where($loginName, $request->get($loginName))->first();

        if ($user && \Hash::check($request->get('fileType') === '1' ? $request->get('password') : $request->get('password2'), $user->password) && !$user->verified_at) {
            // throw ValidationException::withMessages([
            // 	$this->username() => 'Ваш аккаунт еще не верифицирован',
            // ]);

            return redirect()->route('html')->withErrors([
                $this->username() => 'Ваш аккаунт еще не верифицирован',
            ]);
        }


        if (!$user || $user->status === User::STATUS_ARCHIVE || $user->status === User::STATUS_DELETE) {
            // throw ValidationException::withMessages([
            // 	$this->username() => 'Не верный логин или пароль!',
            // ]);

            return response()->json(['error' => 'Не верный логин или пароль!']);
        }


        $isSuccess = \Hash::check($request->get('fileType') === '1' ? $request->get('password') : $request->get('password2'), $user->password);

        return $isSuccess;
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/');
    }

    public function validateLogin(Request $request)
    {
        $login = $this->loginName($request);

        $password = $request->get('fileType') === '2' ? 'password2' : 'password';

        $roles = [
            $login => 'required|string',
            $password => 'required|string',
        ];

        env('RECAPCHA') === 'production' ? $roles['g-recaptcha-response'] = 'required|recaptcha' : false;

        $this->validate($request, $roles);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @param Request $request
     * @return string
     */
    public function loginName(Request $request)
    {
        if ($request->get('fileType') === '2') {
            $login =  'email';
        } else {
            $login = 'phone';
        }

        return $login;
    }

    private function checkCredentials(Request $request)
    {
        if ($request->get('fileType') == '1') {
            $user = User::where('phone', UserHelper::format_phone($request->get('phone')))->first();

            if ($user === null) {
                return [
                    'status' => false,
                    'error' => 'Пользователь с таким телефоном не найден',
                ];
            }

            if ($user) {
                if (\Hash::check($request->get('password') , $user->password) === false) {
                    return [
                        'status' => false,
                        'error' => 'Не верный пароль',
                    ];
                }
            }
        } elseif($request->get('fileType') == '2') {
            $user = User::where('email', $request->get('email'))->first();

            if ($user === null) {
                return [
                    'status' => false,
                    'error' => 'Пользователь с такой почтой не найден',
                ];
            }

            if (\Hash::check($request->get('password2') , $user->password) === false) {
                return [
                    'status' => false,
                    'error' => 'Не верный пароль',
                ];
            }
        }

        return ['status' => true];
    }
}
