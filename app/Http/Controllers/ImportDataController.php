<?php

namespace App\Http\Controllers;

use App\Model\City;
use App\Model\Street;
use App\Model\VehicleMake;
use App\Model\VehicleModel;
use App\Service\PolicyService\Provider\Kias\Client;

class ImportDataController extends Controller
{
	private $client;

	public function __construct(Client $client)
	{
		$this->client = $client;
	}

	public function import()
	{
		$this->importCars();
	}

	private function importCars():bool
	{
		$makes = $this->client->getDictiList(3366);
		foreach ($makes as $make) {
			$item = new  VehicleMake();
			$item->name = array_get($make,'FULLNAME');
			$item->save();
			$models = $this->client->getDictiList(array_get($make,'ISN'));

			foreach ($models as $model) {
				if ($name = array_get($model,'FULLNAME')) {
					$itemModel = new VehicleModel();
					$itemModel->name = $name;
					$itemModel->vehicle_make_id = $item->id;
					$itemModel->vehicle_type = 'B';
					$itemModel->save();
				}

			}
		}
		return true;
	}

	private function importCities(string $symbol):bool
	{
		$cities = $this->client->getCityList($symbol.'%');

		foreach ($cities as $city) {
			$item = new  City();
			$item->name = array_get($city,'CITYSHORTNAME');
			$item->kladr_code = array_get($city,'KLADRCODE');
			$item->save();
			$streets = $this->client->getStreetList(array_get($city,'ISN'));
			if ($streets) {
				foreach ($streets as $street) {
					if ($name = array_get($street,'NAME')) {
						$itemModel = new Street();
						$itemModel->name = $name;
						$itemModel->city_id = $item->id;
						$itemModel->kladr_code =  array_get($street,'KLADRCODE');;
						$itemModel->save();
					}

				}
			}
		}
		return true;
	}
}
