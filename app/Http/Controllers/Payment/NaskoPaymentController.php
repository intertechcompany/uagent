<?php

namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use App\Model\Policy;
use App\Service\PolicyService\Payment\Kias;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\RedirectResponse;

class NaskoPaymentController extends Controller
{
    /**
     * @param string $orderId
     * @throws \App\Service\PolicyService\Payment\PaymentException
     * @return \Illuminate\Http\RedirectResponse
     */
    public function status(string $orderId): RedirectResponse
	{
		$orderId  = explode('=',$orderId)[1];

        /**
         * @var Policy $policy
         */
		$policy = Policy::query()
            ->whereHas('payment',function (Builder $q) use ($orderId): void {
			$q->where('orderId',$orderId);
		})->first();

		$status = (new Kias())->checkPayment($policy);

		if ($status === true) {
			$policy->status = 'paid';
			$policy->save();

			session()->flash('success', 'Оплата прошла успешно!');
		} else {
			session()->flash('error', 'Оплата не прошла успешно!');
		}

		return redirect()->route('policies.show',[
			'policy' => $policy->id
		]);
	}
}
