<?php

namespace App\Http\Controllers\Payment;

use App\Components\PolicyStatus\Statuses\StatusFactory;
use App\Http\Controllers\Controller;
use App\Model\PolicyPayment;
use Illuminate\Http\RedirectResponse;

class AlphaPaymentController extends Controller
{
	public function status(string $status,string $token): RedirectResponse
	{
        /**
         * @var PolicyPayment $payment
         */
		$payment = PolicyPayment::query()->where('token',$token)->first();

		if ($status === 'success') {
            StatusFactory::factory($status)->setPolicy($payment->policy)->execute();
		}

        return redirect()->route('policies.show',[
            'policy' => $payment->policy
        ]);
	}
}
