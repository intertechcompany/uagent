<?php

namespace App\Http\Controllers\Payment;

use App\Model\PolicyPayment;
use App\Service\PolicyService\Payment\Ingos;
use Illuminate\Http\RedirectResponse;

class IngosPaymentController
{
    /**
     * @param string $token
     * @return RedirectResponse
     */
    public function status(string $token): RedirectResponse
    {
        /**
         * @var PolicyPayment $payment
         */
        $payment = PolicyPayment::query()
            ->where('token',$token)
            ->first()
        ;

        $policy = $payment->policy;

        $status = (new Ingos())->checkPayment($policy);

        if ($status === true) {
            $policy->status = 'paid';
            $policy->save();
            session()->flash('success', 'Оплата прошла успешно!');
        } else {
            session()->flash('error', 'Оплата не прошла успешно! ');
        }

        return redirect()->route('policies.show',[
            'policy' => $payment->policy
        ]);
    }
}
