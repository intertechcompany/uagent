<?php

namespace App\Http\Controllers\Payment;

use App\Model\PolicyPayment;
use Illuminate\Http\RedirectResponse;

class RgsPaymentController
{
    /**
     * @param string $token
     * @return RedirectResponse
     */
    public function status(string $status, string $token): RedirectResponse
    {
        /**
         * @var PolicyPayment $payment
         */
        $payment = PolicyPayment::query()
            ->where('token', $token)
            ->first()
        ;

        if ($status === 'success') {
            $payment->policy->status = 'inPayment';
            $payment->policy->save();
        }

        return redirect()->route('policies.show', [
            'policy' => $payment->policy->id
        ]);
    }
}
