<?php

namespace App\Http\Controllers;

use App\Model\Question;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomePageController extends Controller
{
    public function index()
    {
        if (auth()->check()) {
            switch (auth()->user()->role) {
                case \App\User::ROLE_AGENT:
                    return redirect('/home');
                case \App\User::ROLE_ADMIN || \App\User::ROLE_CURATOR:
                    return redirect('/admin/home');
            }
        }

        return view('agent.home-new');
    }

    public function question(Request $request): JsonResponse
    {
        $this->validate($request,[
            'name'    => 'required|string',
            'phone'   => 'required|string',
            'message' => 'required|string',
			'g-recaptcha-response' => 'required',
        ]);

        $data = $request->all();
        $data['ip'] = $request->ip();
        $data['user_agent'] = $request->userAgent();

        /**
         * @var Question $question
         */
        $question = Question::query()->create($data);

        foreach (Question::$emails as $email) {
            Mail::to($email)->send(new \App\Mail\Question($question));
        }

        return new JsonResponse([
            'success' => true
        ]);
    }
}
