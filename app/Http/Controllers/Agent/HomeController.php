<?php

namespace App\Http\Controllers\Agent;

use App\Components\Dashboard\DashboardFactory;
use App\Http\Controllers\Controller;
use App\Model\Policy;
use App\Model\UserDashboardBlock;
use App\User;
use Carbon\Carbon;
use Illuminate\View\View;

class HomeController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(): View
    {
        /**
         * @var User $user
         */
		$user = auth()->user();
        $userId = $user->id;
        $today = Carbon::now()->startOfDay();

        $userDashBoards = $user
            ->dashboardBlocks()
            ->where('is_enabled',true)
            ->pluck('key')
            ->toArray()
        ;
        $dashboards = [];

        foreach (array_keys(UserDashboardBlock::BLOCKS) as $dashBoard) {
            if (\in_array($dashBoard,$userDashBoards,true)) {
                $dashboards[$dashBoard] = DashboardFactory::factory($dashBoard)->getData();
            }
        }

        $polices = Policy::query()
            ->where('user_id',$userId)
            ->where('created_at','>=',$today)
            ->orderBy('id','DESC')
            ->with(['owner','file'])
            ->get()
        ;

        return view('agent.home',[
            'polices' => $polices,
            'user' => $user,
            'dashboards' => $dashboards,
        ]);
    }
}
