<?php

namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Controller;
use App\Model\Policy;
use App\User;
use Illuminate\Http\Request;
use Illuminate\View\View;

class SearchController extends Controller
{
    /**
     * @param Request $request
     * @return View
     */
	public function index(Request $request): View
	{
        /**
         * @var User $user
         */
		$user = auth()->user();

		$polices = Policy::search($request)
            ->where('user_id',$user->id)
			->orderBy('id','DESC')
			->with(['owner','file'])
			->with('user')
			->get()
        ;


		$term = $request->get('term');

		return view('agent.search', [
			'term' => $term,
			'polices' => $polices,
		]);
	}
}
