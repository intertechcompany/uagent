<?php

namespace App\Http\Controllers\Agent;

use App\Components\Settings\SettingsFactory;
use App\Http\Controllers\Controller;
use App\Model\UserDashboardBlock;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class SettingController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function index()
	{
        /**
         * @var User $user
         */
		$user = auth()->user();

		return view('agent.settings', [
			'user' => $user,
			'dashboardBlocks' => $user->dashboardBlocks()->get(['key','is_enabled'])->keyBy('key'),
			'blocks' => UserDashboardBlock::BLOCKS,
		]);
	}

    /**
     * @param Request $request
     * @return RedirectResponse
     */
	public function update(Request $request): RedirectResponse
	{
        /**
         * @var User $user
         */
		$user = auth()->user();

		$flashStatus = $flashText = null;

        if ($request->hasFile('avatar')) {

            $user = SettingsFactory::factory('avatar')
                ->setUser($user)
                ->setValue($request->file('avatar'))
                ->saveSetting()
                ->getUser()
            ;

            $flashStatus = 'success';
            $flashText = 'Аватарка успешно загружена!';

            $user->information->save();
		}


        $newPassword = $request->get('password_confirmation');

		if ($newPassword !== null) {

		    if (Hash::check($request->get('password'),$user->password)) {

                $user = SettingsFactory::factory('password')
                    ->setUser($user)
                    ->setValue($newPassword)
                    ->saveSetting()
                    ->getUser()
                ;

                $flashStatus = 'success';
                $flashText = 'Сброс пароля завершен';

            } else {
                $flashStatus = 'error';
                $flashText = 'Старый пароль неверный!';
            }
		}

        $user->save();

        if ($flashStatus !== null) {
            session()->flash($flashStatus, $flashText);
        }


		return back();
	}

    /**
     * @param Request $request
     * @return JsonResponse
     */
	public function updateField(Request $request): JsonResponse
	{
        if (!$request->has('name') || !$request->has('value')) {

            return new JsonResponse([
                'error' => true
            ]);
        }

        /**
         * @var User $user
         */
		$user = auth()->user();

        $user = SettingsFactory::factory($request->get('name'))
            ->setUser($user)
            ->setValue($request->get('value'))
            ->saveSetting()
            ->getUser()
        ;

        $user->save();
        $user->information->save();

        return new JsonResponse([
            'success' => true
        ]);
	}
}
