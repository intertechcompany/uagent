<?php

namespace App\Http\Controllers\Agent;

use App\Components\Statistics\Statistics;
use App\Excel\AgentStatisticExport;
use App\Helper\UserSubAgents;
use App\Http\Controllers\Controller;
use App\Model\City;
use App\Model\Policy;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class StatisticController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function index(): View
	{
        /*$user = auth()->user();

        $statistic = new Statistics($user, []);
        $statistic->generate();*/

        //dd($statistic);

		return view('agent.statistic');
	}

    /**
     * @param Request $request
     * @return JsonResponse
     */
	public function ajaxFilter(Request $request): JsonResponse
	{
        /**
         * @var User $user
         */
		$user = auth()->user();
		$requestAll = $request->all();

		$insurances = [];
		foreach (Policy::insurance() as $key => $insurance) {
			$insurances[] = [
				'key' => $key,
				'value' => $key,
				'text' => $insurance,
			];
		}

		$statuses = [];
		foreach (Policy::$statuses as $key => $status) {
			$statuses[] = [
				'key' => $key,
				'value' => $key,
				'text' => $status,
			];
		}

        $subAgents = new UserSubAgents($user,[],['name','id','parent_user_id']);

        $subAgents = $subAgents
            ->getSubAgents()
            ->map(function ($agent) {
                return [
                    'key'   => $agent->id,
                    'value' => $agent->id,
                    'text'  => $agent->name,
                ];
            })
        ;

        $cities = City::query()
            ->get(['id','name'])
            ->map(function (City $city){
                return [
                    'key'   => $city->id,
                    'value' => $city->id,
                    'text'  => $city->name,
                ];
            })
        ;

        $statistic = new Statistics($user, $requestAll);
        $statistic->generate();

		return response()->json([
			'cities' => $cities,
			'insurances' => $insurances,
			'subagents' => $subAgents,
			'statuses' => $statuses,
			'statusesNames' => Policy::$statuses,
			'statistics' => [
                'user' => $statistic->getUserStatistic(),
                'subAgents' => $statistic->getSubAgentStatistic(),
                'total' => $statistic->getTotalStatistic(),
                'chart' => $statistic->getChartStatistic(),
            ],
			'url_params' => '/statistic/export?' . http_build_query($requestAll),
		]);
	}

    /**
     * @param Request $request
     */
	public function export(Request $request): void
	{
        /**
         * @var User $user
         */
		$user = auth()->user();

		$export = new AgentStatisticExport($user,$request->all());

		$export->export();
	}
}
