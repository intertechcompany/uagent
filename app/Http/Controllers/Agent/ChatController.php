<?php

namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Controller;
use App\Model\Chat;
use App\Repositories\Chat\ChatRepositoryInterface;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    private $chat;

    public function __construct(ChatRepositoryInterface $chat)
    {
        $this->chat = $chat;
    }

    public function index()
    {
        return view('agent.support.tech-support');
    }

    public function chats(Request $request)
    {
        return response()->json([
            'chats'=>$this->chat->getUsersChatDecorate(),
        ]);
    }

    public function chat(Chat $chat)
    {
        if(!$chat || $chat->user_id != auth()->user()->id) {
            abort(404);
        }

        return view('agent.support.chat-support', [
            'chat' => $chat
        ]);
    }

    public function messages(Chat $chat)
    {
        $this->chat->markIsReadMessages($chat);
        $chat = $this->chat->getMessagesDecorate($chat);

        return response()->json([
            'chat'=>$chat,
        ]);
    }

    public function newMessage(Request $request, Chat $chat)
    {
        $this->chat->storeMessage($chat);

        return $this->messages($chat);
    }

    public function newChat(Request $request)
    {
        $chat = $this->chat->createChat();

        return response()->json([
            'redirect' => route('support.chat', ['chat' => $chat]),
        ]);
    }
}
