<?php

namespace App\Http\Controllers\Agent;

use App\User;
use Carbon\Carbon;
use App\Model\Policy;
use App\Model\UserWithdraw;
use Illuminate\Http\Request;
use App\Helper\UserSubAgents;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

class WithdrawController extends Controller
{
	public function index()
	{
		return view('agent.withdraw');
	}

	public function funds()
	{
		return view('agent.withdraw-funds', [
			'user' => auth()->user()
		]);
	}

	public function edit(Request $request)
	{
		$user = auth()->user();

		if ($request->get('type') === 'requisites') {
			$this->validate($request, [
	            'amount' => 'required|numeric|between:0,' . $user->balance,
	            // 'email' => 'required|email',
	            'full_name' => 'required',
                'kpp' => 'required',
                'leader_organization' => 'required',
                'ogrn' => 'required',
                'based' => 'required',
                'bank' => 'required',
                'legal_address' => 'required',
                'bik' => 'required',
                'mailing_address' => 'required',
                'settlement_account' => 'required',
                'phone' => 'required',
                'ks' => 'required',
                'inn' => 'required',
                'payer_description' => 'required',
	        ], [
	        	'amount.between' => 'Значение должно бить не больше ' . $user->balance,
	        ], [
	        	'amount' => 'Сума',
	        	'email' => 'E-mail',
	        ]);

	        $data = ['current_balance' => $user->balance];

	        $withdraw = $user->withdraws()->create(
        		array_merge($request->only('amount', 'email', 'requisites', 'type'), $data)
    		);

			if ($withdraw) {
    			$user->update([
    				'balance' => ($user->balance - (float) $withdraw->amount),
				]);

				$withdraw->update(['current_balance' => $user->balance]);

				/**
				 * Process create or update requisite for user
				 */
				$user->pricessRequisite($request);

				/**
				 * Process create or update card number
				 * for save to user
				 * for get this record by type and sohw in form 
				 */
				$user->processSaveUserWithdraw($request, [
					'full_name',
	                'kpp',
	                'leader_organization',
	                'ogrn',
	                'based',
	                'bank',
	                'legal_address',
	                'bik',
	                'mailing_address',
	                'settlement_account',
	                'phone',
	                'ks',
	                'inn',
	                'payer_description',
				]);
    		}
		}

		if ($request->get('type') === 'bank_card' || $request->get('type') === 'yandex') {
			$this->validate($request, [
	            'amount' => 'required|numeric|between:0,' . $user->balance,
	            // 'email' => 'required|email',
	            'card_number' => 'required',
	            'current_balance' => $user->balance,
	        ], [
	        	'amount.between' => 'Значение должно бить не больше ' . $user->balance,
	        ], [
	        	'amount' => 'Сума',
	        	'email' => 'E-mail',
	        ]);

	        $data = [
	        	'card_number' => str_replace(' ', '', trim($request->get('card_number'))),
	        	'current_balance' => (float) $user->balance,
	        	'amount' => (float) $request->get('amount'),
	        ];

	        $withdraw = $user->withdraws()->create(
        		array_merge($request->only('email', 'card_number', 'type'), $data)
    		);

    		if ($withdraw) {
    			$user->update([
    				'balance' => ($user->balance - (float) $withdraw->amount)
				]);

				$withdraw->update(['current_balance' => $user->balance]);

				/**
				 * Process create or update card number
				 * for save to user
				 * for get this record by type and sohw in form 
				 */
				$user->processSaveUserWithdraw($request, [
					'card_number'
				]);
    		}
		}

		session()->flash('success', 'Вывод средств успешно создан!');

		return redirect()->back()->withAlert([
			'status' => 'success',
			'message' => 'Вывод средств создан!'
		]);
	}

	public function ajaxFilter(Request $request)
	{
        /**
         * @var User $user
         */
		$user = auth()->user();

		$filters = array_filter($request->all());
		$filters['date'] = $request->only('from', 'to');

		$insurances = [];
		foreach (Policy::insurance() as $key => $insurance) {
			$insurances[] = [
				'key' => $key,
				'value' => $key,
				'text' => $insurance,
			];
		}

        $subAgents = new UserSubAgents($user,[],['id','name','parent_user_id']);

		$subagents = $subAgents->getSubAgents()->map(function (User $agent) {
		    return [
		        'key' => $agent->id,
                'value' => $agent->id,
                'text' => $agent->name,
            ];
        });

        $subAgents = new UserSubAgents($user,$filters);

		$agents = $subAgents->getSubAgents()->each(function($agent) use ($request) {
			return $agent['wuthdraw'] = $this->getWithdraws($request, $agent);
		});

		if ($request->has('yoursPayment') && $request->get('yoursPayment') === 1 && !$request->has('agents')) {
			$agents = null;
		}

		$statuses = [];
        foreach (UserWithdraw::$statuses as $key => $status) {
            $statuses[] = [
                'code' => $key,
                'name' => $status,
            ];
        }

		$response = array_merge([
			'user' => $user,
			'insurances' => $insurances,
			'subagents' => $subagents,
			'paidouts' => $user->paidout,
			'accrueds' => $user->accrued,
			'listInsurances' => $this->getInsurances(),
			'agents' => $agents,
			'url_params' => '/withdraws/export?' . http_build_query($filters),
			'statuses' => $statuses,
			
		], $this->getWithdraws($request, $user));

		return response()->json($response);
	}

	public function getInsurances()
	{
		$insurances = [];
		$userInsurance = json_decode(auth()->user()->insurance) ?? [];
		foreach ($userInsurance as $insurance) {
			$insurances[$insurance] = Policy::insurance($insurance);
		}

		return implode(', ', $insurances);
	}

	public function getWithdraws(Request $request, $user)
	{
		$paidouts = $user->paidout->each(function($paidout): void {
			$paidout['is_paidout'] = true;
			$paidout['is_accrued'] = false;
			$paidout['percent_accrued'] = 0;
		});

		$accrueds = $user->withdraws->each(function($accrued): void {
			$accrued['is_accrued'] = true;
			$accrued['is_paidout'] = false;
			$paidout['percent_accrued'] = 0;
			$accrued['date'] = $accrued->created_at->format('Y-m-d H:i');
			$accrued['sum'] = $accrued->amount;
			$accrued['balance'] = $accrued->current_balance;
			$accrued['type_name'] = $accrued->getTypeName();
		});;

		$data = $paidouts->toArray();
		$data = array_merge($data, $accrueds->toArray());

		/**
		 * Sort by date
		 */
		$data =  array_values(array_sort($data, function ($value) {
		    return $value['date'];
		}));

		if ($request->has('from') || $request->has('to')) {
            $from = Carbon::parse($request->get('from'))->format('Y-m-d');
			$to = Carbon::parse($request->get('to'))->format('Y-m-d');

			foreach ($data as &$item) {
				if ($item['date'] < $from && $item['date'] > $to) {
					$item = [];
				}
			} unset($item);
			$data = array_filter($data);
		}

		$balance = 0;
		$index=1;
		foreach ($data as $key => &$item) {
			if ($index === 1) {
				if ($item['is_paidout']) {
					$item['accrueds'] = (int) $item['balance'];
					$item['paidout'] = 0;
				}

				if ($item['is_accrued']) {
					$item['balance'] = $item['balance'];
					$item['accrueds'] = 0;
					$item['paidout'] = $item['sum'];
				}

				$index++;
				$balance = $item['balance'];
				continue;
			}

			if ($item['is_paidout']) {
				$item['accrueds'] = (int) $item['balance'];
				$item['paidout'] = 0;

				$index++;
				$balance = $item['balance'];
				continue;
			}

			if ($item['is_accrued']) {
				$item['balance'] = $item['balance'];
				$item['accrueds'] = 0;
				$item['paidout'] = (int) $item['sum'];

				$index++;
				$balance = $item['balance'];
				continue;
			}

			$index++;
		} unset($item);

		$accrued = 0;
		foreach ($data as &$item) {
			if ($item['is_accrued']) {
				$accrued += $item['sum'];
			}
		} unset($item);

		foreach ($data as &$item) {
			$item['balance'] = number_format($item['balance'], 2);

			if ($item['is_paidout']) {
				$item['percent_accrued'] = $item['sum'] * 100 / $accrued;
				$item['accrueds'] = number_format($item['sum'], 2);
				$item['paidout'] = number_format($item['paidout'], 2);

				continue;
			}

			if ($item['is_accrued']) {
				$item['paidout'] = '-' . number_format($item['paidout'], 2);
				$item['accrueds'] = number_format($item['accrueds'], 2);
			}

			$item['percent_accrued'] = 0;
		} unset($item);

		$totalAccrued = 0;
		$totalPaidout = 0;
		foreach ($data as $item) {
			if ($item['is_paidout']) {
				$totalAccrued += number_format($item['sum'], 2);
			}

			if ($item['is_accrued']) {
				$totalPaidout += $item['sum'];
			}
		}

		// $userLists = (array_values(array_sort($data, function ($value) {

		//     return $value['is_accrued'];
		// })));

		$userLists = array_sort($data, function ($value): void {
		    // return $value['is_paidout'];
		});

		$userLists = array_reverse($userLists);

		return [
			'userLists' => $userLists,
			'totalPaidout' => number_format($totalPaidout, 2),
			'totalAccrued' => number_format($totalAccrued, 2),
			'totalBalance' => number_format($balance, 2),
		];
	}

	public function export(Request $request): void
	{
        /**
         * @var User $user
         */
		$user = auth()->user();

		$filters = array_filter($request->all());
		$filters['date'] = $request->only('from', 'to');

        $subAgents = new UserSubAgents($user,$filters);

		$agents = $subAgents
            ->getSubAgents()
            ->each(function($agent) use ($request) {
			return $agent['wuthdraw'] = $this->getWithdraws($request, $agent);
		});

		$response = array_merge([
			'user' => $user,
			'paidouts' => $user->paidout,
			'accrueds' => $user->accrued,
			'listInsurances' => $this->getInsurances(),
			'agents' => $agents,
		], $this->getWithdraws($request, $user));

		Excel::create('Laravel Excel', function($excel) use ($response, $request): void {

		    $excel->sheet('Excel sheet', function($sheet) use ($response, $request): void {

		        $sheet->setOrientation('landscape');

		        $data = $this->generateRowXls($response);

		        if (!$request->has('yoursPayment') || $request->get('yoursPayment') !== 1) {
			        $dataAgents = [];
			        foreach ($response['agents'] as $key => $agent) {
						$data = $this->generateRowXls($agent['wuthdraw'], false, $agent, $data);
			        }
		        }

                $sheet->fromArray($data, null, 'A1', false, false);
		    });

		})->export('xls');

	}

	private function generateRowXls($response, $isUser = true, $agent = null, $newData = [])
	{
		$data = [];

		array_push($data, [
			'id' => !$isUser && $agent ? $agent->full_name : 'Вы',
		]);

		$columns = [
        	'id' => '№',
            'name'=> 'Наименование',
            'insurance' => 'Источник',
            'date' => 'Дата и время',
            'balance' => 'Баланс',
            'paid' => 'Списание',
            'accrued' => 'Начисление',
        ];

        $dataColumns = [
            $columns
        ];

        $data = array_merge($data, $dataColumns);

        $row = [
        	'id' => '',
            'name'=> '',
            'insurance' => '',
            'date' => 'Всего',
            'balance' => number_format((float) $response['totalBalance'], 2),
            'paid' => number_format((float) $response['totalPaidout'], 2),
            'accrued' => number_format((float) $response['totalAccrued'], 2),
        ];

        array_push($data, $row);

        foreach($response['userLists'] as $key => $userList) {
            $row = [];

            $row = [
            	'id' => $userList['id'],
                'name'=> $userList['is_accrued'] ? 'Запрос на вывод средств' : 'Начисление',
                'insurance' => !empty($userList['type_name']) ? $userList['type_name'] : '',
                'date' => $userList['date'],
                'balance' => number_format((float) $userList['balance'], 2),
                'paid' => number_format((float) $userList['paidout'], 2),
                'accrued' => number_format((float) $userList['accrueds'], 2),
            ];

            array_push($data, $row);
        }

        return count($newData) > 0 ? array_merge($newData, $data) : $data;
	}
}
