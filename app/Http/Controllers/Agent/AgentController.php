<?php

namespace App\Http\Controllers\Agent;

use App\Components\Kv\SetCommission;
use App\Components\RatesForSubAgent\AgentCommissions;
use App\Components\RatesForSubAgent\RatesForSubAgentsHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAgent;
use App\Model\City;
use App\Model\Rate;
use App\Service\AuthService;
use App\User;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

/**
 * Excel export agents
 */
use Maatwebsite\Excel\Facades\Excel;

class AgentController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function index(): View
	{
        $users = User::query()
            ->where('parent_user_id', auth()->id())
            ->withoutGlobalScope('email_confirmed')
            ->latest()
            ->paginate()
        ;

        return view('agent.agents.index', [
            'users' => $users,
        ]);
	}

    /**
     * @return \Illuminate\Contracts\View\Factory|RedirectResponse|View
     */
    public function create()
    {
        /**
         * @var User $user
         */
        $user = auth()->user();

        if ($user->canCreateSubAgents() === false) {
            session()->flash('error','Ви не можете создавать агентов, обратитесь к куратору чтобы уточнить все детели!');
            return redirect()->route('agents');
        }

        $ratesForSubAgentHelper = new RatesForSubAgentsHelper($user);
        $ratesForSubAgent = $ratesForSubAgentHelper->getRates();

        return view('agent.agents.create', [
            'ratesForSubAgent' => $ratesForSubAgent,
            'agentCommissions' => optional(),
        ]);
	}

    /**
     * @param StoreAgent $request
     * @return RedirectResponse
     */
    public function store(StoreAgent $request): RedirectResponse
    {
        /**
         * @var User $user
         */
        $user = auth()->user();

        if ($user->canCreateSubAgents() === false) {
            session()->flash('error','Ви не можете создавать агентов, обратитесь к куратору чтобы уточнить все детели!!');
            return redirect()->route('agents');
        }

        $this->validate($request,[
            'name' => 'required|alpha|max:255',
            'last_name' => 'required|alpha|max:255',
            'phone' => 'required|max:255',
            'email' => 'required|email|max:255',
            'password' => 'required|min:6',
            'city_id' => 'required|exists:cities,id',
        ]);

        $data = $request->only('email', 'name', 'last_name', 'phone', 'comment','password', 'rates', 'is_opportunity_sub_agent');

        $rates = $request->get('rates', []);

        $agent = app(AuthService::class)->registerSubAgent($data);

        foreach ($rates as $insurance => $rateType) {
            foreach ($rateType as $rateData) {
                foreach ($rateData as $rateID => $data) {

                    if ($data['you'] === null) {
                        continue;
                    }

                    $rateType = $data['type'];
                    $rateId = (int) $rateID;
                    $youCommission = (float) $data['you'];
                    $agentCommission = (float) $data['agent'];
                    $isCommission = (int) $data['is_commission'] === 1;

                    $setCommission = new SetCommission(
                        $agent,
                        $rateId,
                        $rateType,
                        $isCommission,
                        $agentCommission,
                        $youCommission,
                        0
                    );

                    $setCommission->calcCommission();
                }
            }
        }

        return redirect('agents');
    }

    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function edit(int $id): View
    {
        /**
         * @var User $agent
         */
        $agent = User::query()->findOrFail($id);

        /**
         * @var User $user
         */
        $user = auth()->user();

        $ratesForSubAgentHelper = new RatesForSubAgentsHelper($user);
        $ratesForSubAgent = $ratesForSubAgentHelper->getRates();

        $agentCommissions = new AgentCommissions($agent);

        return view('agent.agents.edit', [
            'agent' => $agent,
            'ratesForSubAgent' => $ratesForSubAgent,
            'agentCommissions' => $agentCommissions->renderCommissions(),
        ]);
    }

    /**
     * @param StoreAgent $request
     * @param int $id
     * @return \Illuminate\Routing\Redirector|RedirectResponse
     */
    public function update(StoreAgent $request, int $id): RedirectResponse
    {
        /**
         * @var User $user
         */
        $user = User::query()->findOrFail($id);
        $user->fill($request->only('email', 'name', 'last_name', 'phone', 'comment'));
        $user->save();

        /**
         * Append rates to agent
         */
        try {
            if ($errors = $user->canSaveRateMoreSelf($request->get('rates',[]))) {
                $validator = Validator::make($request->all(), $errors, [
                    'rates.nasko.value.max' => 'Значение должно бить не больше ' . $user->balance,
                    'rates.alpha.value.max' => 'Значение должно бить не больше ' . $user->balance,
                ]);

                return redirect()->back()->withErrors($validator);
            }
        } catch (Exception $ex) {}

        return redirect('agents');
    }

    /**
     * @param User $agent
     * @param Request $request
     * @return JsonResponse
     */
    public function updateRates(User $agent, Request $request): JsonResponse
    {
        $rateId = (int) $request->get('rate_id');
        $rateType = $request->get('rate_type');
        $commissionId = (int) $request->get('id');
        $agentCommission = (float) $request->get('agent');
        $youCommission = (float) $request->get('you');
        $isCommission =  (int) $request->get('is_commission',0) === 1;

        $setCommission = new SetCommission(
            $agent,
            $rateId,
            $rateType,
            $isCommission,
            $agentCommission,
            $youCommission,
            $commissionId
        );

        $commission = $setCommission->calcCommission();

        return new JsonResponse([
            'commission' => $commission->id,
        ]);
    }

    /**
     * @param Request $request
     * @param User $agent
     * @return JsonResponse
     */
    public function updateCreateAgent(Request $request, User $agent): JsonResponse
    {
        $agent->is_opportunity_sub_agent = (int) $request->get('value') === 1;
        $agent->save();

        return new JsonResponse([
            'success' => true,
        ]);
    }

    public function updateField(Request $request, $user)
    {
        /**
         * @var User $user
         */
        $user = User::query()->find($user);

        if (!$request->has('name') || !$request->has('value')) {
            return false;
        }

        if ($request->get('name') === 'password') {

            $user->password = Hash::make($request->get('value'));
            $user->save();

            return;
        }

        $validator = \Validator::make([
            $request->get('name') => $request->get('value')
        ], [
            'email' => 'email',
        ]);

        if ($validator->fails()) {
            return redirect('post/create')
                ->withErrors($validator)
                ->withInput();
        }

        if ($password = $request->get('password')) {
            
            $user->password = Hash::make($password);
            $user->save();

            session()->flash('success', 'Смена пароля завершена!');

            return back();
        }
        
        $user->update([
            $request->get('name') => $request->get('value')
        ]);
    }

    /**
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        User::query()->where('id',$id)->delete();

        return redirect('agents');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function ajaxFilter(Request $request): JsonResponse
    {
        $filters = array_filter($request->all());
        $filters['date'] = $request->only('from', 'to');

        $cities = City::query()->get(['id','name'])->map(function (City $city){
            return [
                'key'   => $city->id,
                'value' => $city->id,
                'text'   => $city->name,
            ];
        });

        $rates = Rate::query()->get(['id','from'])->map(function (Rate $rate) {
            return [
                'key'   => $rate->id,
                'value' => $rate->id,
                'text'  => $rate->from,
            ];
        });

        /**
         * @var User $user
         */
        $user = auth()->user();

        $agents = $user
            ->subAgents()
            ->withoutGlobalScope('email_confirmed')
            ->filter(array_filter($filters))
            ->with('city')
            ->latest()
            ->get()
        ;

        return response()->json([
            'cities'        => $cities,
            'rates'         => $rates,
            'statusesNames' => (new User)->getListStatus(),
            'agents'        => $agents,
            'url_params'    => '/agents/export?' . http_build_query($filters),
        ]);
    }

    public function export(Request $request): void
    {
        $user = auth()->user();
        $filters = array_filter($request->all());
        $filters['date'] = $request->only('from', 'to');

        Excel::create('Laravel Excel', function($excel) use ($user, $filters): void {

            $excel->sheet('Excel sheet', function($sheet) use ($user, $filters): void {

                $sheet->setOrientation('landscape');

                $columns = [
                    'id' => '№',
                    'full_name'=> 'Ф.И.О субагента',
                    'kv' => 'КВ',
                    'city' => 'Населенный пункт',
                    'created_at' => 'Дата рег-и',
                    'phone' => 'Телефон',
                    'email' => 'E-mail',
                    'status' => 'Статус',
                    'comment' => 'Комментарий',
                ];

                $data = [
                    $columns
                ];

                $agents = $user->subagents()->filter(array_filter($filters))->get();

                foreach($agents as $key => $agent) {

                    $row = [
                        'id' => $agent->id,
                        'full_name'=> $agent->full_name,
                        'kv' => $agent->balance,
                        'city' => $agent->city ? $agent->city->name : null,
                        'created_at' => $agent->created_at,
                        'phone' => $agent->phone,
                        'email' => $agent->email,
                        'status' => $agent->statusname,
                        'comment' => $agent->comment,
                    ];

                    array_push($data, $row);
                }

                $sheet->fromArray($data, null, 'A1', false, false);
            });

        })->export('xls');

    }

}
