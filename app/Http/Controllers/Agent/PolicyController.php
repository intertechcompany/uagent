<?php

namespace App\Http\Controllers\Agent;

use App\Helper\PolicyLogHelper;
use App\Helper\StringHelper;
use App\Model\PolicyPayment;
use App\Service\PolicyService\Payment\Rgs\Verification;
use App\Components\PolicyStatus\Statuses\StatusFactory;
use App\Excel\AgentPolicyExport;
use App\Helper\SerializeDataForPolicy;
use App\Http\Controllers\Controller;
use App\Model\Policy;
use App\Repository\PolicyRepository;
use App\Service\PolicyService\Document\DocumentFactory;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;


class PolicyController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function index(): View
	{
		return view('agent.policies.index');
	}

    /**
     * @return \Illuminate\Contracts\View\Factory|View
     */
	public function create(): View
	{
		$userId = auth()->user()->id;

		return view('agent.policies.create',compact('userId'));
	}


    /**
     * @param Policy $policy
     * @param string $file
     * @return Response
     */
	public function printDocument(Policy $policy,string $file)
	{
        $userId = auth()->user()->id;

        if ($userId !== (int)$policy->user_id) {
            session()->flash('error','Недостаточно прав на просмотр полиса: #' . $policy->id . '!');
            return redirect()->route('home');
        }

        if($policy->status != Policy::SUCCESS_STATUS) {
            session()->flash('error','Полис должен быть в статусе "Оформлен!"');
            return redirect()->route('home');
        }

        try {
            $policyDocument = DocumentFactory::factory($policy->insurance)->getPolicyFile($policy);
        } catch (\Exception $exception) {
            session()->flash('error',$exception->getMessage());
            return redirect()->route('policies.show',['policy' => $policy->id]);
        }

        PolicyLogHelper::getInstance('policy')
            ->saveLog(storage_path('policies/provider/documents/' . Carbon::now()->format('d-m-Y') . '/' . $policy->insurance . '-' . $policy->id . '.xml'));

        return response()->file($policyDocument->getPath(),[
            'Content-Type' => 'application/pdf',
            'Content-Transfer-Encoding' => 'binary',
            'Cache-Control' => 'must-revalidate',
            'Content-Length' => filesize($policyDocument->getPath()),
            'Content-Disposition' => 'filename=' . $policyDocument->file_name
        ]);
	}

    /**
     * @param Policy $policy
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|View
     */
	public function show(Policy $policy)
	{
		$userId = auth()->user()->id;

		if ($userId !== (int)$policy->user_id) {
            session()->flash('error','Недостаточно прав на просмотр полиса: #' . $policy->id . '!');

            return redirect()->route('home');
		}

		return view('agent.policies.show',[
		    'policy' => $policy
        ]);
	}

    public function payment(Policy $policy)
    {
        if($policy->status == 'inPayment') {
            return redirect()->to(route('policies.show', ['policy'=>$policy]));
        }

        $userId = auth()->user()->id;

        if ($userId !== (int)$policy->user_id) {
            session()->flash('error','Недостаточно прав на просмотр полиса: #' . $policy->id . '!');
            return redirect()->route('home');
        }

        $result = StatusFactory::factory(Policy::PAID_STATUS)->setPolicy($policy)->execute(false);
        $sessionMessage = $result->getSessionMessage();
        $policy->load('payment');

        if ($sessionMessage !== null) {

            $error = json_decode($sessionMessage->getMessage());

            if (!json_last_error() && is_object($error)) {
                if($error->is_show) {
                    session()->flash($sessionMessage->getStatus(), $error->custom ?? $error->original);
                }
            } else {
                session()->flash($sessionMessage->getStatus(), $sessionMessage->getMessage());
            }
        }

        if(empty($policy->payment->formUrl)) {
            if(!$sessionMessage) {
                session()->flash('error', 'Не удалось получить ссылку на оплату!');
            }

            return redirect()->back();
        }

        return view('agent.policies.pay-of-balance',[
            'policy' => $policy
        ]);

    }

    public function paymentFromBalance(Policy $policy,Request $request)
    {
        if($policy->status == 'inPayment') {
            return redirect()->to(route('policies.show', ['policy'=>$policy]));
        }

        $user = auth()->user();

        if ($user->id !== (int)$policy->user_id) {
            session()->flash('error','Недостаточно прав на просмотр полиса: #' . $policy->id . '!');
            return redirect()->route('home');
        }

        $user->balance -= $policy->premium;
        $user->save();

        $policy->payment->is_from_balance = 1;
        $policy->payment->save();

        $policy->status = 'inPayment';
        $policy->save();

        return redirect()->to(route('policies.show', ['policy'=>$policy]));
    }

    /**
     * @param Policy $policy
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|mixed
     */
	public function setStatus(Policy $policy,Request $request)
	{
		$userId = auth()->user()->id;

        if ($userId !== (int)$policy->user_id) {
            session()->flash('error','Недостаточно прав на просмотр полиса: #' . $policy->id . '!');
            return redirect()->route('home');
        }

		$status = $request->get('status');

        $statusCommand = StatusFactory::factory($status)->setPolicy($policy)->execute();
        $sessionMessage = $statusCommand->getSessionMessage();
        $return = $statusCommand->return();

        if ($sessionMessage !== null) {

            $error = json_decode($sessionMessage->getMessage());

            if (!json_last_error()) {
                if($error->is_show) {
                    session()->flash($sessionMessage->getStatus(), $error->custom ?? $error->original);
                }
            } else {
                session()->flash($sessionMessage->getStatus(), $sessionMessage->getMessage());
            }
        }

        if ($return !== null) {
            return $return;
        }

		return redirect()->route('policies.show',[
			'policy' => $policy->id
		]);
	}

    /**
     * Pricess verification agent phone
     *
     * @param Policy $policy
     * @param Request $request
     */
    public function verifyPhone(Policy $policy, Request $request)
    {
        /**
         * If phone number is press to input
         * make request for confirm phone number
         */
        if ($request->has('is_apply') && $request->get('is_apply') == true) {
            $verifyCode = new Verification($policy, 'ConfirmVerificationCode');

            $responseData = $verifyCode->ConfirmVerificationCode($request);

            return response()->json($responseData);
        }

        /**
         * Class verify phone number agent
         */
        $verify = new Verification($policy);

        return response()->json($verify->SendVerificationCode());
        
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
	public function ajaxFilter(Request $request): JsonResponse
	{
        /**
         * @var User $user
         */
		$user = auth()->user();

		$filtersData = array_filter($request->all());

		$filters['date'] = $request->only('from', 'to');

		$policies = Policy::filter($filtersData)
            ->sortable($filtersData)
            ->where('user_id', $user->id)
            ->with(['owner','kvBonusForOwner'])
            ->paginate($filtersData['perPage']);

		$insurances = [];
		foreach (Policy::insurance() as $key => $insurance) {
			$insurances[] = [
				'key' => $key,
				'value' => $key,
				'text' => $insurance,
			];
		}

		$statuses = [];
		foreach (Policy::$statuses as $key => $status) {
			$statuses[] = [
				'key' => $key,
				'value' => $key,
				'text' => $status,
			];
		}

        $subAgents = $user->subAgents()->get(['id','name'])->map(function (User $agent) {
            return [
                'key'   => $agent->id,
                'value' => $agent->id,
                'text'  => $agent->name,
            ];
        });

        $total = $policies->total();
        $current = StringHelper::pages($request, $total);

        $data = [
            'polices' => $policies->setCollection($policies->getCollection()->each(function($policy) {
                $policy->owner_full_name = $policy->owner->first_name . ' ' . $policy->owner->last_name;
                $policy->kv = $policy->kvBonusForOwner !== null ? $policy->kvBonusForOwner->sum : 0;
            }))->toArray(),
            'user' => $user,
            'insurances' => $insurances,
            'subagents' => $subAgents,
            'statuses' => $statuses,
            'exportUrl' => route('policies.filter.export') . '?' . http_build_query($filtersData),
            'totalItems' => $total,
            'currentItems' => $current
        ];

		return response()->json($data);
	}

    /**
     * @param Request $request
     * @return void
     */
	public function ajaxFilterExport(Request $request): void
    {
        /**
         * @var User $user
         */
        $user = auth()->user();

        $export = new AgentPolicyExport($user, $request->all());

        $export->export();
    }

    /**
     * @param Policy $policy
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|View
     */
	public function reCalcForm(Policy $policy)
    {
        $userID = auth()->user()->id;

        if ($userID !== (int)$policy->user_id) {
            session()->flash('error','Недостаточно прав на просмотр полиса: #' . $policy->id . '!');
            return redirect()->route('home');
        }

        return view('agent.policies.recalc',[
            'policy' => $policy
        ]);
    }

    /**
     * @param Policy $policy
     * @return \Illuminate\Http\RedirectResponse|JsonResponse
     */
	public function reCalc(Policy $policy)
    {
        $userID = auth()->user()->id;

        if ($userID !== (int)$policy->user_id) {
            session()->flash('error','Недостаточно прав на просмотр полиса: #' . $policy->id . '!');
            return redirect()->route('home');
        }

        $serializePolicy = new SerializeDataForPolicy($policy);

        $formData = $serializePolicy->getData();

        $formData['user_id'] = $userID;

        return new JsonResponse([
            'formData' => $formData
        ]);
    }

    public function delete(Request $request)
    {
        if($request->has('id')) {
            Policy::destroy($request->get('id'));
        }

        return new JsonResponse([
            'status' => 'ok'
        ]);
    }
}
