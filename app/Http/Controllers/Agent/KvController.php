<?php

namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Controller;
use App\Model\Policy;
use Illuminate\Http\Request;

class KvController extends Controller
{
    public function index()
    {
        $insurances = Policy::insurance();
        return view('agent.kv.index', [
            'insurances' => $insurances
        ]);
    }
}
