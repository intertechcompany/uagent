<?php

namespace App\Http\Controllers;

use App\Helper\MenuHelper;
use Illuminate\Http\Request;
use App\Service\DadataService;
use Illuminate\Http\JsonResponse;

class AjaxController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function cities(Request $request): JsonResponse
    {
        $term = trim($request->get('q'));
        $result = [];

        if ($term !== null) {
            $result = DadataService::suggest(['query' => $term, 'count' => 20])['suggestions'];
            $result = array_map(function ($item){
                return [
                    'id' => $item['data']['kladr_id'] . '#' . $item['value'],
                    'text' => $item['value']
                    ];
            },$result);
        }
        return new JsonResponse($result);
    }

    /**
     * Fixed menu
     *
     * @param \Illuminate\Http\Request $request
     * @param string $type
     * @return \Illuminate\Http\JsonResponse
     */
    public function fixedMenu(Request $request, string $type): JsonResponse
    {
        return new JsonResponse(
            MenuHelper::factory()->setType($type)->makePosition()
        );
    }
}
