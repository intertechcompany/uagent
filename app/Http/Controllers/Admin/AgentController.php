<?php

namespace App\Http\Controllers\Admin;

use App\Components\Kv\UpdateRateForAgent;
use App\Helper\LogHelper;
use App\Helper\StringHelper;
use App\Http\Controllers\Controller;
use App\Model\City;
use App\Model\Curator;
use App\Model\Policy;
use App\Model\Rate;
use App\Traits\TabRelationsTrait;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;
use App\Model\UserWithdraw;
use Illuminate\Http\JsonResponse;
use App\Helper\Admin\VueAgentHelper;
use Illuminate\Validation\ValidationException;

class AgentController extends Controller
{
	use TabRelationsTrait;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function index(): View
	{
		/*$agents = User::getAgents()
			->with(['paidout','accrued','city'])
			->paginate(User::PAGINATE)
        ;*/

		return view('admin.agents.index', [
		 //   'agents' => $agents,
        ]);
	}

    /**
     * @param User $agent
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function edit(User $agent): View
	{
		$rateInsurance = Rate::pageAgent()
            ->get()
            ->groupBy('insurance')
        ;

		$logs = LogHelper::factory()
            ->setAction('rates')
            ->setObject($agent)
            ->getLog()
        ;

		return view('admin.agents.edit', [
			'logs' => $logs,
			'agent' => $agent,
			'cities' => City::query()->orderBy('name', 'asc')->get(),
			'curators' => Curator::query()->get(),
			'rateInsurance' => $rateInsurance,
			'agentCuratorListIds' => $agent->curators->pluck('id')->toArray(),
			// 'agentRatesIds' => $agent->rates->pluck('id')->toArray(),
			'insurance' => Policy::insurance(),
		]);
	}

    /**
     * @return View
     */
	public function create(): View
    {
        $rateInsurance = Rate::pageAgent()
            ->get()
            ->groupBy('insurance')
        ;

        return view('admin.agents.create', [
            'cities' => City::query()->orderBy('name', 'asc')->get(),
            'curators' => Curator::query()->get(),
            'rateInsurance' => $rateInsurance,
            'insurance' => Policy::insurance(),
        ]);
    }

    public function store(Request $request): RedirectResponse
    {
        $this->validate($request,[
            'name' => 'required|alpha|max:255',
            'last_name' => 'required|alpha|max:255',
            'phone' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'city_id' => 'required|exists:cities,id'
        ]);

        $agent = new User();
        $agent->email = $request->get('email');
        $agent->name = $request->get('name');
        $agent->last_name = $request->get('last_name');
        $agent->phone = $request->get('phone');
        $agent->city_id = $request->get('city_id');
        $agent->role = User::ROLE_AGENT;
        $agent->is_email_confirmed = 1;
        $agent->is_policy_subagents = $request->get('is_policy_subagents', 0);
        $agent->is_important = $request->get('is_important', 0);
        $agent->is_broker = $request->get('is_broker',0);
        $agent->password = Hash::make($request->get('password'));
        $agent->verified_at = Carbon::now();
        $agent->save();

        /**
         * Process sync detach agents to curators
         */
        $agent->processCurators($request);

        /**
         * Log rates
         */
        $agent->processRates($request);

        /**
         * Append rates to agent
         */
        $agent->rates()->sync($request->get('rates', []));

        return redirect()
            ->route('admin.agent.index')
            ->with('alert', [
                    'status' => 'success',
                    'message' => 'Агент ' . $agent->name . ' создан!',
                ]
            )
        ;
    }

    /**
     * @param Request $request
     * @param User $agent
     * @return \Illuminate\Http\RedirectResponse|null
     */
	public function update(Request $request, User $agent)
	{
        $data = [
            'errors' => []
        ];

        // try {
    		// $this->processActiveTab($request);

    		$request->validate([
    			'name' => 'required|alpha|max:255',
    			'last_name' => 'required|alpha|max:255',
    			'phone' => 'required|string|max:255',
    			'password' => 'nullable|string|min:6',
    			'city_id' => 'required|exists:cities,id',
    			'tab_date' => 'nullable|date_format:Y-m-d',
    			'tab_sum' => 'nullable|required_with:tab_date,tab_id',
    		]);

    		$agent->name = $request->get('name');
    		$agent->last_name = $request->get('last_name');
    		$agent->phone = $request->get('phone');
    		$agent->city_id = $request->get('city_id');
    		$agent->is_policy_subagents = $request->get('is_policy_subagents', 0);
    		$agent->is_broker = $request->get('is_broker', 0);
    		$agent->status = $request->get('status');
    		$agent->worktime_from = $request->get('worktime_from');
    		$agent->worktime_to = $request->get('worktime_to');
            $agent->is_important = $request->get('is_important', 0);

            if ($request->has('insurance')) {

                $insurance = $request->get('insurance');
                if ($request->has('is_json') && $request->get('is_json') == true) {
                    $insurance = json_decode($request->get('insurance'), true);
                }

                $agent->insurance = json_encode(
                    array_column($insurance, 'code')
                );
            }

            if (!$request->has('insurance')) {
                $agent->insurance = null;
            }

    		$password = $request->get('password');

    		if ($password !== null) {
    			$agent->password = Hash::make($password);
    		}

    		if (empty($request->get('active'))) {
    			$agent->declined_at = Carbon::now();
    		} else {
    			$agent->declined_at = null;
    		}

    		$agent->save();

    		/**
    		 * Process sunct detach agents to curators
    		 */
    		$agent->processCurators($request);

    		/**
    		 * Log rates
    		 */
            $ratesArray = $request->get('rates', []);
            if ($request->has('is_json') && $request->get('is_json') == true) {
                $ratesArray = json_decode($request->get('rates'), true);
            }
            $rates = array_column($ratesArray, 'code');

    		$agent->processRates($rates);
    		$updateRates = new UpdateRateForAgent($agent, $rates);
    		$updateRates->update();

    		/**
    		 * Process tab relation model
    		 */
    		if ($response = $agent->processTabRelations($request)) {
    			
    		}

            $data['accrueds'] = $this->getAgentAccrued($agent);

    		// return redirect()
      //           ->route('admin.agent.index')
      //           ->with('alert', [
      //               'status' => 'success',
      //               'message' => 'Агент ' . $agent->name . ' обновлен!',
      //               ]
      //           )
      //       ;
        // } catch (\Exception $exception) {
        //     $data['errors'] = $exception;
        // }

        return new JsonResponse($data);
	}

    /**
     * @param User $agent
     * @return \Illuminate\Contracts\View\Factory|View
     */
	public function bonus(User $agent): View
	{
		$logs = LogHelper::factory()
            ->setAction('bonus')
            ->setObject($agent)
            ->getLog()
        ;

		return view('admin.agents.bonus',[
		    'agent' => $agent,
            'logs' => $logs,
        ]);
	}

    /**
     * @param Request $request
     * @param User $agent
     * @return RedirectResponse
     */
	public function bonusStore(Request $request, User $agent): RedirectResponse
	{
		$rules = [
			'bonus' => 'required|numeric',
			'type' => 'required',
		];

		$this->validate($request,$rules);

		$bonus = $request->get('bonus');
		$type = $request->get('type');

		if (!\in_array($type,User::$bonusTypes,true)) {
			throw new \InvalidArgumentException('Not found bonus type!');
		}

		$agent->bonus_type = $type;
		$agent->bonus = $bonus;

		$description = 'Изменен бонус на ' . $bonus . ($type === 'rub' ? ' руб.' : '%') . ' !';

		LogHelper::factory()
            ->setAction('bonus')
            ->setObject($agent)
            ->setDescription($description)
            ->save()
        ;

		$agent->save();

		return redirect()
            ->route('admin.agent.index')
            ->with('alert',[
                'status' => 'success',
                'message' => 'Бонус для ' . $agent->name . ' .' . $description,
            ])
        ;
	}

    public function ajaxAgents(Request $request)
    {
        $filters = VueAgentHelper::factory()->formedFilters($request);

        $agents = User::getAgents()
            ->with(User::getSubAgentsRelationsLevel(['paidout','accrued','city']))
            ->with(['paidout','accrued','city','polices'])
            ->filter($filters)
            ->paginate($request->get('perPage'))
        ;

        $total = $agents->total();
        $current = StringHelper::pages($request, $total);

        $data = array_merge([
            'agents' => $agents->setCollection($agents->getCollection()->each(function($item) {
                $item->count_agents = $item->subAgents->count();
                $item->policesCount = $item->polices->count();
                
                /**
                 * Statistic agent url
                 */
                $item->statisticUrl = route('admin.dashboard.index') . '/?agent=' . $item->id;

                if ($item->city) {
                    $item->city_name = $item->city->name;
                }

                $item->subAgents = $this->generateThreeSubAgents($item);

                $item->formated_paidouts = $item->formated_paidouts;
                $item->formated_accrued = $item->formated_accrued;
                $item->formated_balance = $item->formated_balance;
                $item->show = false;
            })),
            'totalItems' => $total,
            'currentItems' => $current
        ], VueAgentHelper::factory()->dashboard($request, 'agents'));

        return response()->json($data);
    }

    public function generateThreeSubAgents(User $agent)
    {
        if ($agent->subAgents->count() == 0) {
            return null;
        }

        return $agent->subAgents->each(function($subAgent) {
            $subAgent->formated_paidouts = $subAgent->formated_paidouts;
            $subAgent->formated_accrued = $subAgent->formated_accrued;
            $subAgent->formated_balance = $subAgent->formated_balance;
            $subAgent->show = false;
            $subAgent->count_agents = $subAgent->subAgents->count();

            /**
             * Statistic agent url
             */
            $subAgent->statisticUrl = route('admin.dashboard.index') . '/?agent=' . $subAgent->id;

            if ($subAgent->city) {
                $subAgent->city_name = $subAgent->city->name;
            }

            if ($subAgent->subAgents->count() == 0) {
                return null;
            }

            $subAgent->subAgents = $this->generateThreeSubAgents($subAgent);
        });
    }

    public function ajaxAgentData(Request $request, User $agent)
    {
        $statuses = [];
        foreach (User::$statuses as $key => $status) {
            $statuses[] = [
                'key' => $key, 
                'value' => $key, 
                'text' => $status,
            ];
        }

        $cities = [];
        foreach (City::withAgents()->get() as $city) {
            $cities[] = [
                'key' => $city->id,
                'value' => $city->id,
                'text' => $city->name,
            ];
        }

        /**
         * Set date for date period
         */
        $agent->fromTime = $agent->fromTime;
        $agent->toTime = $agent->toTime;

        $logs = LogHelper::factory()
            ->setAction('rates')
            ->setObject($agent)
            ->getLog()
            ->each(function($log) {
                $log->created_at_format = $log->created_at->format('d-m-Y');
            })
        ;

        $selectedCurators = [];
        foreach ($agent->curators as $curator) {
            $selectedCurators[] = [
                'code' => $curator->id,
                'name' => "({$curator->id}) $curator->full_name",
            ];
        }

        $data = array_merge([
            'agent' => $agent,
            'statuses' => $statuses,
            'cities' => $cities,
            'logs' => $logs,

            'insurances' => $this->getInsurances($agent),
            'agentInsurances' => $this->getInsurances($agent, [], true),

            'rateInsurance' => $this->getInsuranceRates($agent),
            'agentRatesIds' => $this->getInsuranceRates($agent, [], true),

            'curators' => Curator::query()->get()->each(function($curator) {
                $curator->code = $curator->id;
                $curator->name = "({$curator->id}) $curator->full_name";
            })->toArray(),
            'selectedCurators' => $selectedCurators,

            'saveAgentUrl' => route('admin.agent.update', $agent),
            'saveAgentWithdrawUrl' => route('admin.agent.edit.withdraw.ajax'),

            'withdraws' => $agent->withdraws->each(function($withdraw) {
                $withdraw->card_number_mask = wordwrap($withdraw->card_number , 4 , '-' , true );
                $withdraw->icon = $this->getWithdrawPathIcon($withdraw);
                $withdraw->type_name = $withdraw->getTypeName();

                if (empty($withdraw->status)) {
                    $withdraw->status = array_get(
                        array_first(UserWithdraw::getVueStatuses()),
                        'value'
                    );
                }

                $withdraw->status_name = $withdraw->status_name;
            }),
            'withdrawStatuses' => UserWithdraw::getVueStatuses(),

            'accrueds' => $this->getAgentAccrued($agent),
            'accruedStatuse' => UserWithdraw::$statuses,

            'paymentInfo' => [
                'bank' => $agent->saveWithdraws()
                    ->byType('bank_card')
                    ->byField('card_number')
                    ->first(),
                'yandex' => $agent->saveWithdraws()
                    ->byType('yandex')
                    ->byField('card_number')
                    ->first(),
                'requisites' => $agent->saveWithdraws()
                    ->byType('requisites')
                    // ->byField('payer_description')
                    ->get(),
            ],
            'requisiteTypes' => UserWithdraw::getRequisitesFields(),
        ]);

        return response()->json($data);
    }

    public function getAgentAccrued(User $agent)
    {
        return $agent->accrued->each(function($accrued) {
            $accrued->sum = number_format($accrued->sum, 2);
        });
    }

    public function updateWithdraw(Request $request)
    {
        if ($withdraw = UserWithdraw::find($request->get('id'))) {
            $withdraw->status = $request->get('status');
            $withdraw->save();

            return response()->json([
                'status' => $withdraw->getVueStatus()
            ]);
        }
    }

    private function getWithdrawPathIcon(UserWithdraw $withdraw): string
    {
        if ($withdraw->type == 'bank_card') {
            return '/img/icons/icon-card.svg';
        }

        if ($withdraw->type == 'yandex') {
            return '/img/icons/icon-ym.svg';
        }

        return '/img/icons/icon-yr.svg';
    }

    public function getInsuranceRates(User $agent, $rateInsuranceData = [], $selected = false)
    {
        $rateInsurance = Rate::pageAgent()
            ->get()
            ->groupBy('insurance')
        ;

        foreach ($rateInsurance as $insurance => $rates) {
            $insuranceName = Policy::insurance($insurance);

            foreach ($rates as  $rate) {
                $rateInsuranceData[] = [
                    'code' => $rate->id,
                    'name' => "{$insuranceName} {$rate->full_name}",
                ];
            }
        }

        if ($selected === true) {
            $rates = $agent->rates->pluck('id')->toArray();

            $newRateInsuranceData = [];
            foreach ($rateInsuranceData as $rateInsurance) {
                if ($rateInsurance && !empty($rateInsurance['code']) && in_array($rateInsurance['code'], $rates)) {
                    $newRateInsuranceData[] = $rateInsurance;
                }
            }

            return $newRateInsuranceData;
        }

        return $rateInsuranceData;
    }

    public function getInsurances(User $agent, $insurances = [], $selected = false)
    {
        foreach (Policy::insurance() as $key => $insurance) {
            $insurances[] = [
                'code' => $key,
                'name' => $insurance,
            ];
        }

        if ($selected === true) {
            $data = array_map(function($insurance) use ($agent) {
                return $insurance && !empty($insurance['code']) && in_array($insurance['code'], $agent->getInsurance()) ? $insurance : [];
            }, $insurances);

            return array_filter($data);
        }

        return $insurances;
    }
}
