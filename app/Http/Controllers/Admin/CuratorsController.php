<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Curator;
use App\Model\Policy;
use App\Role;
use App\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;

class CuratorsController extends Controller
{
    /**
     * @param Request $request
     * @param Curator|null $curator
     * @param string $action
     * @return array
     */
    public function rules(Request $request, Curator $curator = null, $action = 'create'): array
    {
        $rules = [
            'first_name' => 'required|alpha|max:255',
            'last_name' => 'required|alpha|max:255',
            'phone' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
        ];

        if ($action === 'create' || $request->get('password') !== null) {
            $rules['password'] = 'required|string|min:6|confirmed';
        }

        if ($curator !== null && $curator->user) {
            $rules['email'] = 'required|string|email|max:255|unique:users,email' . $curator->user->id;
        }

        return $rules;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(): View
    {
        $curators = Curator::query()->paginate(20);

        return view('admin.curators.index', [
            'curators' => $curators,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function create(): View
    {
        return view('admin.curators.create', [
            'agents' => User::agents()->get(),
            'insurance' => Policy::insurance(),
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $this->validate($request, $this->rules($request));

        /**
         * @var Curator $curator
         */
        $curator = Curator::query()->create([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'phone' => $request->get('phone'),
            'email' => $request->get('email'),
            'insurance' => $request->has('insurance') ? json_encode($request->get('insurance',[])) : null,
            'password' => Hash::make($request->get('password')),
        ]);

        if ($curator !== null) {
            $curator->agents()->sync($request->get('agents',[]));

            if ($user = $this->processUser($request)) {

                $curator->update([
                    'user_id' => $user->id
                ]);

                $role = Role::query()
                    ->where('name', 'curator')
                    ->first()
                ;

                if ($role !== null) {
                    $user->syncRoles($role);
                }
            }
        }

        return redirect()
            ->route('admin.curators.index')
            ->with('alert',[
                'status' => 'success',
                'message' => 'Куратор ' . $curator->full_name . ' создан!'
            ])
        ;
    }

    /**
     * @param Curator $curator
     * @throws \Exception
     * @return RedirectResponse
     */
    public function delete(Curator $curator): RedirectResponse
    {
        $curator->user()->delete();
        $curator->delete();

        return redirect()->route('admin.curators.index');
    }

    /**
     * @param Curator $curator
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function edit(Curator $curator): View
    {
        return view('admin.curators.edit', [
            'curator' => $curator,
            'agents' => User::agents()->get(),
            'curatorAgentListIds' => $curator->agents->pluck('id')->toArray(),
            'insurance' => Policy::insurance(),
        ]);
    }

    /**
     * @param Request $request
     * @param Curator $curator
     * @return RedirectResponse
     */
    public function update(Request $request, Curator $curator): RedirectResponse
    {
        $this->validate($request, $this->rules($request,$curator,'update'));

        $dataForUpdate = [
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'phone' => $request->get('phone'),
            'email' => $request->get('email'),
            'insurance' => $request->has('insurance') ? json_encode($request->get('insurance',[])) : null,
        ];

        $password = $request->get('password');

        if ($password !== null) {
            $dataForUpdate['password'] = Hash::make($password);
        }

        $curator->update($dataForUpdate);

        $curator->agents()->sync($request->get('agents',[]));

        if ($user = $this->processUser($request, $curator->user)) {

            $curator->update([
                'user_id' => $user->id
            ]);

            $role = Role::query()
                ->where('name', 'curator')
                ->first()
            ;

            if ($role) {
                $user->syncRoles($role);
            }
        }

        return redirect()
            ->route('admin.curators.index')
            ->with('alert',[
                'status' => 'success',
                'message' => 'Куратор ' . $curator->full_name . ' обновлен!'
            ])
            ;
    }

    /**
     * @param Request $request
     * @param User|null $user
     * @return User
     */
    private function processUser(Request $request, ?User $user = null): User
    {
        if ($user === null) {
            $user = new User();
        }

        $user->name = $request->get('first_name');
        $user->email = $request->get('email');
        $user->role = User::ROLE_CURATOR;
        $user->is_email_confirmed = true;
        $user->verified_at = now();

        $password = $request->get('password');

        if ($password !== null) {
            $user->password = Hash::make($password);
        }

        $user->save();

        $user->setDefaultValues();

        return $user;
    }
}
