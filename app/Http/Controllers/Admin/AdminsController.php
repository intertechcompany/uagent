<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\ChangePassword;
use App\Mail\ResetPassword;
use App\Model\City;
use App\Role;
use App\User;
use Carbon\Carbon;
use Hackzilla\PasswordGenerator\Generator\ComputerPasswordGenerator;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class AdminsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $admins = User::query()
            ->where('role',User::ROLE_ADMIN)
            ->with('city')
            ->with('roles')
            ->paginate(20)
        ;

        return view('admin.admins.index',[
            'admins' => $admins,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $cities = City::query()->orderBy('name','asc')->get();

        return view('admin.admins.create', [
            'roles' => Role::query()->get(),
            'cities' => $cities,
        ]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): JsonResponse
    {
        $data = [
            'errors' => []
        ];

        $request->validate([
            'name' => 'required|alpha|max:255',
            'last_name' => 'required|alpha|max:255',
            'phone' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'city_id' => 'required|exists:cities,id'
        ]);

        $user = new User();
        $user->email = $request->get('email');
        $user->name = $request->get('name');
        $user->last_name = $request->get('last_name');
        $user->phone = $request->get('phone');
        $user->city_id = $request->get('city_id');
        $user->role = User::ROLE_ADMIN;
        $user->is_email_confirmed = 1;
        $user->password = Hash::make($request->get('password'));
        $user->verified_at = Carbon::now();
        $user->save();

        /**
         * Attach roles to updated user
         */
        if ($user && $request->has('roles')) {
            $user->syncRoles(
                array_column($request->get('roles', []), 'code')
            );
        }

        $data['redirectUrl'] = route('admin.admins.edit', $user);

        return new JsonResponse($data);

        return redirect()
            ->route('admin.admins.index')
            ->with('alert',[
                'status' => 'success',
                'message' => 'Администратор ' . $user->name . ' создан!',
            ])
        ;

    }

    /**
     * @param User $user
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(User $user): RedirectResponse
    {
        if ($user->role === User::ROLE_ADMIN) {
            $user->delete();
        }

        return redirect()->route('admin.admins.index');
    }

    /**
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(User $user)
    {
        $cities = City::query()
            ->orderBy('name','asc')
            ->get()
        ;

        return view('admin.admins.edit', [
            'user' => $user,
            'cities' => $cities,
            'roles' => Role::query()->get(),
        ]);
    }

    public function onChangePass(Request $request, User $user): JsonResponse
    {
        $generator = new ComputerPasswordGenerator();
        $generator->setLength(16)
            ->setAvoidSimilar()
            ->setLowercase()
            ->setUppercase()
            ->setNumbers()
            ->setSymbols();

        $password = $generator->generatePassword();

        Mail::to($user->email)->send(new ChangePassword($password));

        $user->password = Hash::make($password);
        $user->remember_token = null;
        $user->save();

        session()->flush();

        return response()->json(['status'=>'ok']);
    }

    /**
     * @param Request $request
     * @param User $user
     * @return RedirectResponse
     */
    public function update(Request $request, User $user): JsonResponse
    {
        $data = [
            'errors' => []
        ];

        $request->validate([
            'name' => 'required|alpha|max:255',
            'last_name' => 'required|alpha|max:255',
            'phone' => 'required|string|max:255',
            'password' => 'nullable|string|min:6',
            'city_id' => 'required|exists:cities,id',
            'email' => 'required|string|email|max:255|unique:users,email,' . $user->id,
        ]);

        $user->name = $request->get('name');
        $user->last_name = $request->get('last_name');
        $user->phone = $request->get('phone');
        $user->city_id = $request->get('city_id');

        $password = $request->get('password');

		if ($password !== null) {
			$user->password = Hash::make($password);
		}

        $user->save();
        
        /**
         * Attach roles to updated user
         */
        if ($user && $request->has('roles')) {
            $user->syncRoles(
                array_column($request->get('roles', []), 'code')
            );
        }

        $data['user'] = $user;

        return new JsonResponse($data);

        return redirect()
            ->route('admin.admins.index')
            ->with('alert',[
                'status' => 'success',
                'message' => 'Администратор ' . $user->name . ' обновлен!',
            ])
        ;
    }

    public function loadAjax(Request $request)
    {
        $admins = User::query()
            ->where('role',User::ROLE_ADMIN)
            ->paginate(20)
        ;

        return response()->json([
            'admins' => $admins->setCollection($admins->getCollection()->each(function($admin) {
                if ($admin->city) {
                    $admin->city_name = $admin->city->name;
                }
            })),
        ]);
    }

    public function createAjax(Request $request)
    {
        $cities = [];
        foreach (City::get() as $city) {
            $cities[] = [
                'key' => $city->id,
                'value' => $city->id,
                'text' => $city->name,
            ];
        }

        $roles = [];
        foreach (Role::query()->get() as $role) {
            $roles[] = [
                'code' => $role->id,
                'key' => $role->name,
                'name' => User::getRole($role->name),
            ];
        }

        return response()->json([
            'cities' => $cities,

            'roles' => $roles,

            'storeUrl' => route('admin.admins.store.ajax'),
        ]);
    }

    public function editAjax(Request $request, User $user)
    {
        $cities = [];
        foreach (City::get() as $city) {
            $cities[] = [
                'key' => $city->id,
                'value' => $city->id,
                'text' => $city->name,
            ];
        }

        return response()->json([
            'user' => $user,
            'cities' => $cities,

            'roles' => $this->getUserRoles($user),
            'selectedRoles' => $this->getUserRoles($user, [], true),

            'updateUrl' => route('admin.admins.update', $user),
        ]);
    }

    public function getUserRoles(User $agent, $roles = [], $selected = false)
    {
        foreach (Role::query()->get() as $role) {
            $roles[] = [
                'code' => $role->id,
                'key' => $role->name,
                'name' => User::getRole($role->name),
            ];
        }

        if ($selected === true) {
            $data = array_map(function($role) use ($agent) {
                return is_array($role) && !empty($role['code']) && $agent->hasRole($role['key']) ? $role : [];
            }, $roles);

            return array_filter($data);
        }

        return $roles;
    }
}
