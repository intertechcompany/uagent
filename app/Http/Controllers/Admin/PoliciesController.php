<?php

namespace App\Http\Controllers\Admin;

use App\Model\Policy;
use Illuminate\View\View;

class PoliciesController
{
    /**
     * @param Policy $policy
     * @return View
     */
    public function show(Policy $policy): View
    {
        return view('admin.policies.show',[
            'policy' => $policy
        ]);
    }
}
