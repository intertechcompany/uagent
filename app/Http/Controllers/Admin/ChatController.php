<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Chat;
use App\Repositories\Chat\ChatRepositoryInterface;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    private $chat;

    public function __construct(ChatRepositoryInterface $chat)
    {
        $this->chat = $chat;
    }

    public function index()
    {
        return view('admin.support.tech-support');
    }

    public function chats(Request $request)
    {
        $statuses = [];

        foreach(trans('chat.status') as $k => $v) {
            array_push($statuses, [
               'key' => $k,
               'value' => $k,
               'text' => $v,
            ]);
        }

        return response()->json([
            'chats'=>$this->chat->getAllDecorate(),
            'statuses' => $statuses
        ]);
    }

    public function update(Request $request, Chat $chat)
    {
        $this->chat->changeStatus($chat);

        return response()->json(['status'=>'ok']);
    }

    public function chat(Chat $chat)
    {
        if(!$chat) {
            abort(404);
        }

        return view('admin.support.chat-support', [
            'chat' => $chat
        ]);
    }

    public function messages(Chat $chat)
    {
        $this->chat->markIsReadMessages($chat);
        $chat = $this->chat->getMessagesDecorate($chat);

        return response()->json([
            'chat'=>$chat,
        ]);
    }

    public function newMessage(Request $request, Chat $chat)
    {
        $this->chat->storeMessage($chat);

        return $this->messages($chat);
    }
}
