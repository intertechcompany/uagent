<?php

namespace App\Http\Controllers\Admin;

use App\Components\Kv\ReCalcRates;
use App\Http\Controllers\Controller;
use App\Model\Policy;
use App\Model\City;
use App\Model\Rate;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Helper\Admin\RatesHelper;
use App\Helper\Admin\VueRatesHelper;

class RatesController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(): View
    {
        /*$rates = Rate::query()
            ->where('is_default',false)
            ->paginate(20)
        ;*/

        return view('admin.rates.index', [
           // 'rates' => $rates,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(): View
    {
        return view('admin.rates.create', [
            'types' => Rate::$types,
            'insurance' => Policy::insurance(),
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request): JsonResponse
    {
        /**
         * Validate rate form
         */
        $this->validate($request, Rate::$rules);

        /**
         * Process store rate
         */
         $result = RatesHelper::factory()
            ->setData($request)
            ->storeRate();

        return response()->json($result);

        return redirect()
            ->route('admin.rates.index')
            ->with('alert',[
                'status' => 'success',
                'message' => 'Ставка ' . $rate->full_name . ' создана!'
            ])
        ;
    }

    /**
     * @param Rate $rate
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Rate $rate): RedirectResponse
    {
        /*if ((bool)$rate->is_default === true) {
            throw new NotFoundHttpException('Not found!');
        }*/

        ReCalcRates::factory()->clearCommissions($rate);

        $rate->delete();

        return redirect()->route('admin.rates.index');
    }

    /**
     * @param Rate $rate
     * @return View
     */
    public function edit(Rate $rate): View
    {
        /*if ((bool) $rate->is_default === true) {
            throw new NotFoundHttpException('Not found!');
        }*/

        return view('admin.rates.edit', [
            'rate' => $rate,
            //'types' => Rate::$types,
            //'insurance' => Policy::insurance(),
        ]);
    }

    /**
     * @param Request $request
     * @param Rate $rate
     * @return RedirectResponse
     */
    public function update(Request $request, Rate $rate): RedirectResponse
    {
        /*if ((bool)$rate->is_default === true) {
            throw new NotFoundHttpException('Not found!');
        }*/

        $this->validate($request, Rate::$rules);

        $oldFrom = (int) $rate->from;
        $newFrom = (int) $request->get('from');
        $oldInsurance = $rate->insurance;
        $newInsurance = $request->get('insurance');

        $rate->update([
            'from' => $newFrom,
            'to' => $request->get('to'),
            'type' => $request->get('type'),
            'value' => $request->get('value'),
            'insurance' => $newInsurance,
        ]);

        if (
            $oldFrom !== $newFrom ||
            $oldInsurance !== $newInsurance
        ) {
            ReCalcRates::factory()->clearCommissions($rate);
        } else {
            ReCalcRates::factory()->calc($rate);
        }

        return redirect()
            ->route('admin.rates.index')
            ->with('alert',[
                'status' => 'success',
                'message' => 'Ставка ' . $rate->full_name . ' обновлена!',
            ])
        ;
    }

    public function editAjax(Request $request)
    {
        return response()->json(
            VueRatesHelper::factory()->getData($request)
        );
    }

    public function editRateAjax(Request $request, int $id)
    {
        $rate = RatesHelper::factory()
            ->setData($request)
            ->setRateId($id)
            ->getRate();

        if (!$rate) {
            return response()->json([]);
        }

        $data = array_merge(
            [
                'rate' => $rate,
                'updateUrl' => route('admin.rates.update', $rate)
            ],
            VueRatesHelper::factory()->getData($request)
        );

        return response()->json($data);
    }
}
