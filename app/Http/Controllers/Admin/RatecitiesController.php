<?php

namespace App\Http\Controllers\Admin;

use App\Components\Kv\ReCalcRates;
use App\Http\Controllers\Controller;
use App\Model\Policy;
use App\Model\Rate;
use App\Model\RateCity;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

use App\Helper\Admin\RatesHelper;
use App\Helper\Admin\VueRatesHelper;

class RatecitiesController extends Controller
{
    /***
     * @return array
     */
    public function rules(): array
    {
        return [
            'insurance' => 'required',
            'city_id' => 'required',
            'address' => 'required',
            'type' => 'required',
            'value' => 'required',
        ];
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(): View
    {
        $rateCities = RateCity::query()->paginate(20);

        return view('admin.ratecities.index', [
            'rateCities' => $rateCities,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function create()
    {
        return view('admin.ratecities.create', [
            'types' => Rate::$types,
            'insurance' => Policy::insurance(),
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $this->validate($request, $this->rules());

        /**
         * @var RateCity $rateCity
         */
        $rateCity = RateCity::query()->create([
            'city_id' => $request->get('city_id'),
            'type' => $request->get('type'),
            'value' => $request->get('value'),
            'insurance' => $request->get('insurance'),
        ]);

        $rateCity->processCity($request);

        return redirect()
            ->route('admin.ratecities.index')
            ->with('alert', [
                'status' => 'success',
                'message' => 'Ставка для города ' . $rateCity->name . ' создана!'
            ])
        ;
    }

    /**
     * @param RateCity $rateCity
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(RateCity $rateCity): RedirectResponse
    {
        ReCalcRates::factory()->clearCommissions($rateCity);

        $rateCity->delete();

        return redirect()->route('admin.ratecities.index');
    }

    /**
     * @param RateCity $rateCity
     * @return View
     */
    public function edit(RateCity $rateCity): View
    {
        return view('admin.ratecities.edit', [
            'rateCity' => $rateCity,
            'types' => Rate::$types,
            'insurance' => Policy::insurance(),
        ]);
    }

    /**
     * @param Request $request
     * @param RateCity $rateCity
     * @return RedirectResponse
     */
    public function update(Request $request, RateCity $rateCity)
    {
        $this->validate($request, $this->rules());

        // echo "<pre>"; print_r($request->all()); die();

        $rateCity->update([
            'city_id' => $request->get('city_id'),
            'type' => $request->get('type'),
            'value' => $request->get('value'),
            'insurance' => $request->get('insurance'),
        ]);

        $rateCity->processCity($request);

        ReCalcRates::factory()->calc($rateCity);

        $data = array_merge(
            [
                'rate' => $rateCity,
                'updateUrl' => route('admin.ratecities.update', $rateCity)
            ],
            VueRatesHelper::factory()->getData($request)
        );

        return response()->json($data);

        return redirect()
            ->route('admin.ratecities.index')
            ->with('alert',[
                'status' => 'success',
                'message' => 'Ставка для города ' . $rateCity->name . ' обновлена!',
            ])
        ;
    }

    public function editRateAjax(Request $request, int $id)
    {
        $rateCity = RatesHelper::factory()
            ->setData($request)
            ->setRateId($id)
            ->getRate();

        if (!$rateCity) {
            return response()->json([]);
        }

        $data = array_merge(
            [
                'rate' => $rateCity,
                'city' => $rateCity->city ? $rateCity->city : null,
                'updateUrl' => route('admin.ratecities.update', $rateCity)
            ],
            VueRatesHelper::factory()->getData($request)
        );

        return response()->json($data);
    }
}
