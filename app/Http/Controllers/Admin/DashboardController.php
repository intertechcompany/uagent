<?php

namespace App\Http\Controllers\Admin;

use App\Helper\StringHelper;
use App\User;
use App\Model\Policy;
use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Excel\AgentPolicyExport;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;
use App\Repository\PolicyRepository;
use App\Helper\Admin\VuePolicyHelper;
use App\Excel\Admin\AdminPoliciesExport;

class DashboardController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request): View
    {
        /*$filters = $request->get('filter', []);

        $polices = Policy::dashboard()
            ->filter($filters)
            ->with('kvBonusForOwner')
            ->paginate(Policy::PAGINATE)
            ->appends(request()->query())
        ;


        if ($request->get('export')) {
            $this->exportPolices($request);
        }

		$premiumSum = array_sum($polices->pluck('premium')->toArray());

		$statistic = new \stdClass();
		$statistic->allAgents = User::getAgents()->count();
		$statistic->allPolices = Policy::dashboard()->count();
		$statistic->allPolicesSumPremium = Policy::dashboard()->sum('premium');
		$statistic->avgPolicesPremium = Policy::dashboard()->avg('premium');

        $agentsAndSubAgents = User::getAgentsAndSubAgents()->get();*/

        return view('admin.home',[
            'isCurator' => User::ROLE_CURATOR === auth()->user()->role
           /* 'polices' => $polices,
            'premiumSum' => $premiumSum,
            'statistic' => $statistic,
            'agentsAndSubAgents' => $agentsAndSubAgents,
            'filters' => $filters*/
        ]);
    }

    public function ajaxDashboard(Request $request)
    {
        $filters = VuePolicyHelper::factory()->formedFilters($request);

        /**
         * @var Collection $polices
         */
        $policesQuery = Policy::dashboard()
            ->filter($filters)
            ->with([
                'kvBonusForOwner',
                'user.curators.user',
                'user.city',
                'owner',
            ])
        ;

        $formedData = VuePolicyHelper::factory()->dashboard($request, 'polices', $policesQuery);

        $polices = $policesQuery->paginate($request->get('perPage'));
        $total = $polices->total();
        $current = StringHelper::pages($request, $total);

        $data = array_merge([
            'polices' => $polices->setCollection($polices->getCollection()->each(function($item) {
                if (@$item->owner) {
                    $address = explode(',', $item->owner->address);
                    $item->city_name = array_get($address, '0');
                }
            })),
            'premiumSum' => number_format(
                array_sum($policesQuery->get()->pluck('premium')->toArray()),
                2
            ),
            'exportUrl' => route('dashboard.policies.filter.export') . '?' . http_build_query($filters),
            'totalItems' => $total,
            'currentItems' => $current
        ], $formedData);

        return response()->json($data);
    }

    /**
     * @param Request $request
     * @return void
     */
    public function ajaxFilterExport(Request $request): void
    {
        $filters = VuePolicyHelper::factory()->formedFilters($request);
        
        $export = new AdminPoliciesExport($filters);

        $export->export();
    }
}
