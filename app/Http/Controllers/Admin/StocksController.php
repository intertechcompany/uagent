<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Policy;
use App\Model\Stock;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Model\City;

class StocksController extends Controller
{
    /**
     * @param Request $request
     */
    public function stockValidate(Request $request): void
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'from' => 'date_format:Y-m-d H:i',
            'to' => 'date_format:Y-m-d H:i',
            'insurance' => 'required',
        ], [
            'from.date_format' => 'Поле Дата начала акции не соответствует формату гггг-мм-дд чч-мм',
            'to.date_format' => 'Поле Дата конца акции не соответствует формату гггг-мм-дд чч-мм',  
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(): View
    {
        $stocks = Stock::query()->paginate(20);

        return view('admin.stocks.index', [
            'stocks' => $stocks,
        ]);
    }

    public function ajaxIndex()
    {
        $stocks = Stock::query()->paginate();

        return response()->json([
            'stocks' => $stocks->setCollection($stocks->getCollection()->each(function($item) {
                $item->from_time = $item->from_time;
                $item->to_time = $item->to_time;
                $item->enabled = $item->enabled;
                $item->text = $item->text;
            })),
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function create(): View
    {
        return view('admin.stocks.create', [
            'insurance' => Policy::insurance(),
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $this->stockValidate($request);

        /**
         * @var Stock $stock
         */
        $stock = Stock::query()->create([
            'title' => $request->get('title'),
            'from' => $request->get('from'),
            'to' => $request->get('to'),
            'insurance' => $request->has('insurance') ? json_encode($request->get('insurance',[])) : null,
            'is_enabled' => $request->get('is_enabled', 0),
            'text' => $request->get('text'),
            'information' => $request->get('information'),
        ]);

        return redirect()
            ->route('admin.stocks.index')
            ->with('alert',[
                'status' => 'success',
                'message' => 'Акция ' . $stock->title . ' создан!'
            ])
        ;

    }

    /**
     * @param Stock $stock
     * @throws \Exception
     * @return RedirectResponse
     */
    public function delete(Stock $stock): RedirectResponse
    {
        $stock->delete();

        return redirect()->route('admin.stocks.index');
    }

    /**
     * @param Stock $stock
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function edit(Stock $stock): View
    {
        return view('admin.stocks.edit', [
            'stock' => $stock,
            'insurance' => Policy::insurance(),
        ]);
    }

    public function editAjax(Stock $stock)
    {
        // $stock->from_time_stock = $stock->from_time;
        // $stock->to_time_stock = $stock->to_time;

        $cities = [];
        foreach (City::get() as $city) {
            $cities[] = [
                'code' => $city->id,
                'name' => $city->name,
            ];
        }

        $insurances = [];
        foreach (Policy::insurance() as $key => $insurance) {
            $insurances[] = [
                'code' => $key,
                'name' => $insurance,
            ];
        }

        return response()->json([
            'stock' => $stock,
            'insurance' => $insurances,
            'cities' => $cities,
            'routeUpdate' => route('admin.stocks.update.ajax', $stock),
        ]);
    }

    /**
     * @param Request $request
     * @param Stock $stock
     * @return RedirectResponse
     */
    public function update(Request $request, Stock $stock): RedirectResponse
    {
        $this->stockValidate($request);

        $stock->update([
            'title' => $request->get('title'),
            'from' => $request->get('from'),
            'to' => $request->get('to'),
            'insurance' => $request->has('insurance') ? json_encode($request->get('insurance',[])) : null,
            'is_enabled' => $request->get('is_enabled', 0),
            'text' => $request->get('text'),
            'information' => $request->get('information'),
        ]);

        return redirect()
            ->route('admin.stocks.index')
            ->with('alert',[
                'status' => 'success',
                'message' => 'Акция ' . $stock->title . ' обновлен!'
            ])
        ;
    }

    public function updateAjax(Request $request, Stock $stock)
    {
        // echo "<pre>"; print_r($request->all()); die();
        $this->stockValidate($request);

        $stock->update([
            'title' => $request->get('title'),
            'from' => $request->get('from'),
            'to' => $request->get('to'),
            'is_enabled' => $request->get('is_enabled', 0),
            'text' => $request->get('text'),
            // 'information' => $request->get('information'),
        ]);

        

        // 'insurance' => $request->has('insurance') ? json_encode($request->get('insurance',[])) : null,
    }
}
