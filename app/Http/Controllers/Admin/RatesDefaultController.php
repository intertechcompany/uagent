<?php

namespace App\Http\Controllers\Admin;

use App\Components\Kv\ReCalcRates;
use App\Http\Controllers\Controller;
use App\Model\Policy;
use App\Model\City;
use App\Model\Rate;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RatesDefaultController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(): View
    {
        $rates = Rate::query()
            ->where('is_default',true)
            ->paginate(20)
        ;

        return view('admin.rates_default.index', [
            'rates' => $rates,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(): View
    {
        return view('admin.rates_default.create', [
            'types' => Rate::$types,
            'insurance' => Policy::insurance(),
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $this->validate($request, Rate::$rules);

        /**
         * @var Rate $rate
         */
        $rate = Rate::query()
            ->create([
                'from' => $request->get('from'),
                'to' => $request->get('to'),
                'type' => $request->get('type'),
                'value' => $request->get('value'),
                'insurance' => $request->get('insurance'),
                'is_default' => true,
            ])
        ;

        return redirect()
            ->route('admin.rates_default.index')
            ->with('alert',[
                'status' => 'success',
                'message' => 'Ставка по умолчанию ' . $rate->full_name . ' создана!',
            ])
        ;
    }

    /**
     * @param Rate $rate
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Rate $rate): RedirectResponse
    {
        if ((bool) $rate->is_default === false) {
            throw new NotFoundHttpException('Not found!');
        }

        ReCalcRates::factory()->clearCommissions($rate);

        $rate->delete();

        return redirect()->route('admin.rates_default.index');
    }

    /**
     * @param Rate $rate
     * @return View
     */
    public function edit(Rate $rate): View
    {
        if ((bool) $rate->is_default === false) {
            throw new NotFoundHttpException('Not found!');
        }

        return view('admin.rates_default.edit', [
            'rate' => $rate,
            'types' => Rate::$types,
            'insurance' => Policy::insurance(),
        ]);
    }

    /**
     * @param Request $request
     * @param Rate $rate
     * @return RedirectResponse
     */
    public function update(Request $request, Rate $rate): RedirectResponse
    {
        if ((bool) $rate->is_default === false) {
            throw new NotFoundHttpException('Not found!');
        }

        $this->validate($request, Rate::$rules);

        $oldFrom = (int) $rate->from;
        $newFrom = (int) $request->get('from');
        $oldInsurance = $rate->insurance;
        $newInsurance = $request->get('insurance');

        $rate->update([
            'from' => $newFrom,
            'to' => $request->get('to'),
            'type' => $request->get('type'),
            'value' => $request->get('value'),
            'insurance' => $newInsurance,
            'is_default' => true,
        ]);

        if (
            $oldFrom !== $newFrom ||
            $oldInsurance !== $newInsurance
        ) {
            ReCalcRates::factory()->clearCommissions($rate);
        } else {
            ReCalcRates::factory()->calc($rate);
        }

        return redirect()
            ->route('admin.rates_default.index')
            ->with('alert',[
                'status' => 'success',
                'message' => 'Ставка по умолчанию ' . $rate->full_name . ' обновлена!'
            ])
        ;
    }

    public function loadDataAjax(Request $request)
    {
        $ratesDefault = Rate::query()
            ->filter($request)
            ->where('is_default', true)
            ->get()
        ;

        $rates = Rate::query()
            ->filter($request)
            ->where('is_default', false)
            ->get()
        ;

        $cities = [];
        foreach (City::get() as $city) {
            $cities[] = [
                'code' => $city->id,
                'name' => $city->name,
            ];
        }

        return response()->json([
            'ratesDefault' => $ratesDefault,
            'rates' => $rates,
            'insurances' => $this->getInsurances(),
            'cities' => $cities,
            'types' => Rate::$types,
        ]);
    }

    public function getInsurances($insurances = [])
    {
        foreach (Policy::insurance() as $key => $insurance) {
            $insurances[] = [
                'code' => $key,
                'name' => $insurance,
            ];
        }

        return $insurances;
    }
}
