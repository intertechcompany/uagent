<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;

class VerificationController extends Controller
{
	public function index()
	{
		$query = User::where('role', User::ROLE_AGENT)->whereNull('verified_at')->orderBy('created_at');

		if (request('declined')) {
			$query->whereNotNull('declined_at');
		} else {
			$query->whereNull('declined_at');
		}

		$agents = $query->paginate();

		return view('admin.verifications.index', compact('agents'));
	}

	public function verify($id)
	{
		$user = User::where('role', User::ROLE_AGENT)->findOrFail($id);

		$this->authorize('verify', $user);

		$user->verify();

		return back();
	}

	public function decline($id)
	{
		$user = User::where('role', User::ROLE_AGENT)->findOrFail($id);

		$this->authorize('verify', $user);

		$user->decline();

		return back();
	}
}