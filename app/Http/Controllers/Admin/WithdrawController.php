<?php

namespace App\Http\Controllers\Admin;

use App\Helper\StringHelper;
use App\Model\PolicyPayment;
use App\User;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Model\UserWithdraw;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class WithdrawController extends Controller
{
    /**
     * @param Request $request
     */
    public function userWithdrawValidate(Request $request): void
    {
        $this->validate($request, [
            'card_number' => 'numeric',
            'amount' => 'required|numeric',
            'email' => 'required|email',
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(): View
    {
        return view('admin.withdraws.index', [
            'withdraws' => UserWithdraw::query()->paginate(),
        ]);
    }

    /**
     * @param UserWithdraw $stock
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(UserWithdraw $stock): RedirectResponse
    {
        $stock->delete();

        return redirect()->route('admin.withdraws.index');
    }

    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function edit(int $id): View
    {
        $withdraw = UserWithdraw::query()->find($id);

        return view('admin.withdraws.edit', [
            'withdraw' => $withdraw,
        ]);
    }

    public function editAjax(Request $request, int $id)
    {
        $withdraw = $this->getWithdraw(UserWithdraw::find($id));

        $paymentMethods = [];
        foreach (UserWithdraw::getPaymentMethods() as $key => $method) {
            $paymentMethods[] = [
                'code' => $key,
                'name' => $method,
            ];
        }

        $statuses = [];
        foreach (UserWithdraw::$statuses as $key => $status) {
            $statuses[] = [
                'code' => $key,
                'name' => $status,
            ];
        }

        return response()->json([
            'withdraw' => $withdraw,
            'paymentMethods' => $paymentMethods,
            'statuses' => $statuses,
            'changeStatusUrl' => route('admin.admins.change.status'),
        ]);
    }

    private function getWithdraw(UserWithdraw $withdraw): UserWithdraw
    {
        $withdraw->date = Carbon::parse($withdraw->created_at)->format('d.m.Y H:i:s');
        $withdraw->updated = Carbon::parse($withdraw->updated_at)->format('d.m.Y H:i:s');
        $withdraw->balance = number_format(!empty($balance->current_balance) ? $balance->current_balance : 0, 2);
        $withdraw->amount = number_format(!empty($balance->amount) ? $balance->amount : 0, 2);
        $withdraw->user_name = $withdraw->user->name;
        $withdraw->user_last_name = $withdraw->user->last_name;

        return $withdraw;
    }

    /**
     * @param Request $request
     * @param UserWithdraw $withdraw
     * @return RedirectResponse
     */
    public function update(Request $request, UserWithdraw $withdraw): RedirectResponse
    {
        $this->userWithdrawValidate($request);

        $withdraw->update([
            'card_number' => $request->get('card_number'),
            'requisites' => $request->get('requisites'),
            'amount' => $request->get('amount'),
            'email' => $request->get('email'),
        ]);

        return redirect()
            ->route('admin.withdraws.index')
            ->with('alert',[
                'status' => 'success',
                'message' => 'Выплата ' . $withdraw->getTypeName() . ' обновлен!'
            ])
        ;
    }

    public function loadAjax(Request $request)
    {
        $agents = [];
        foreach (User::whereHas('withdraws')->get() as $agent) {
            $agents[] = [
                'code' => $agent->id,
                'name' => $agent->full_name,
            ];
        }

        $paymentMethods = [];
        foreach (UserWithdraw::getPaymentMethods() as $key => $method) {
            $paymentMethods[] = [
                'code' => $key,
                'name' => $method,
            ];
        }

        $statuses = [];
        foreach (UserWithdraw::$statuses as $key => $status) {
            $statuses[] = [
                'code' => $key,
                'name' => $status,
            ];
        }

        $singleStatuses = [];
        foreach (UserWithdraw::$statuses as $key => $status) {
            $singleStatuses[] = [
                'key' => $key,
                'value' => $key,
                'text' => $status,
            ];
        }

        $items = UserWithdraw::query()
            ->filter($request)
            ->with('user')
            ->paginate($request->get('perPage'));

        $total = $items->total();
        $current = StringHelper::pages($request, $total);

        $payments = PolicyPayment::with('policy.user')->where('is_from_balance', 1)->get();

        $paymentStatuses = [];

        foreach(trans('admin.payments.status') as $k => $v) {
            array_push($paymentStatuses, [
                'key' => $k,
                'value' => $k,
                'text' => $v,
            ]);
        }

        return response()->json([
            'agents' => $agents,
            'paymentStatuses' => $paymentStatuses,
            'statuses' => $statuses,
            'singleStatuses' => $singleStatuses,
            'paymentMethods' => $paymentMethods,
            'withdraws' => $items,
            'totalItems' => $total,
            'currentItems' => $current,
            'changeStatusUrl' => route('admin.admins.change.status'),
            'payments' => $payments,
        ]);
    }

    public function updatePayment(Request $request, PolicyPayment $payment)
    {
        $payment->status = $request->get('status');
        $payment->save();

        return response()->json(['status'=>'ok']);
    }

    public function changeStatus(Request $request)
    {
        if ($request->has('is_many')) {
            foreach ($request->get('withdraws') as $withdrawId) {
                if ($withdraw = UserWithdraw::find($withdrawId)) {
                    $this->changeWithdrawStatus($request, $withdraw, true);
                }
            }

            return response()->json([
                'withdraws' => UserWithdraw::query()
                    ->filter($request)
                    ->with('user')
                    ->paginate(20)
            ]);
        }

        if ($withdraw = UserWithdraw::find($request->get('id'))) {
            return $this->changeWithdrawStatus($request, $withdraw);
        }
    }

    public function changeWithdrawStatus(Request $request, UserWithdraw $withdraw, bool $isMany = false)
    {
        $withdraw->status = $request->get('status');
        $withdraw->save();

        if (true === $isMany) {
            return;
        }

        return response()->json([
            'withdraw' => $this->getWithdraw($withdraw),
        ]);
    }
}
