<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Auth\Access\AuthorizationException;

class CanHaveAgents
{
    /**
     * @param $request
     * @param Closure $next
     * @throws AuthorizationException
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /**
         * @var User|null $user
         */
        $user = auth()->user();

        if ($user === null || $user->canCreateSubAgents() === false) {
            throw new AuthorizationException('You can not create agents!');
        }

        return $next($request);
    }
}
