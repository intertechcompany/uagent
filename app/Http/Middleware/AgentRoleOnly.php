<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Auth\Access\AuthorizationException;

class AgentRoleOnly
{
    /**
     * @param $request
     * @param Closure $next
     * @throws AuthorizationException
     * @return \Illuminate\Http\RedirectResponse|mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->role === User::ROLE_GUEST) {
            return redirect()->route('settings');
        }

        if (auth()->user()->role !== User::ROLE_AGENT) {
            throw new AuthorizationException('Your role must be AGENT for this section!');
        }

        if (auth()->user()->status === User::STATUS_ARCHIVE || auth()->user()->status === User::STATUS_DELETE) {
            auth()->logout();

            return redirect()->to('/');
        }

        return $next($request);
    }
}
