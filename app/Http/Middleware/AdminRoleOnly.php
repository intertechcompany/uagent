<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Auth\Access\AuthorizationException;

class AdminRoleOnly
{
    /**
     * @param $request
     * @param Closure $next
     * @throws AuthorizationException
     * @return mixed
     */
	public function handle($request, Closure $next)
	{
		if (auth()->user()->role !== User::ROLE_ADMIN && auth()->user()->role !== User::ROLE_CURATOR) {
			throw new AuthorizationException('Your Role must be ADMIN for this section');
		}

		return $next($request);
	}
}
