<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Auth\Access\AuthorizationException;

class CanShowSectionPolicies
{
    /**
     * @param $request
     * @param Closure $next
     * @throws AuthorizationException
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /**
         * @var User|null $user
         */
        $user = auth()->user();

        if ($user === null || $user->isBroker() === true) {
            throw new AuthorizationException('You can\'t see policies section!');
        }

        return $next($request);
    }
}
