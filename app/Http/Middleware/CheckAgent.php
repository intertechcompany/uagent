<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Access\AuthorizationException;

class CheckAgent
{
    /**
     * Check user is agent
     *
     * @param $request
     * @param Closure $next
     * @return mixed
     * @throws AuthorizationException
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->parent_user_id !== null) {
            throw new AuthorizationException();
        }

        return $next($request);
    }
}
