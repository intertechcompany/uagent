<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

	'kias' => [
		'app_id' => '1320381',
		'login' => 'ЦЕПЛЯЕВА Л.П.',
		'password' => 'klinich11',
		'production_endpoint' => 'https://kiassvc.nasko.ru/kiassvc/KiasSVC.asmx',
		'test_endpoint' => 'https://kiassvctest.nasko.ru/kiassvctest/KiasSVC.asmx',
        'production_curator' => 5757614,
        'test_curator' => 5692049
	],
    'smsaero' => [
        'email' => 'lovecubesupport@yandex.ru',
        'key'   => 'vYUuHcj8YuHWTAXcdGxpwAUlukzb',
    ],
	'alpha' => [
        'login' => 'E_PARTNER',
        'login_production' => 'E_MILOV',
        'password' => 'ALFAE313',
        'password_production' => 'Rw947MKd94',
        'production_endpoint' => 'https://b2b.alfastrah.ru',
        'test_endpoint' => 'https://b2b-test.alfastrah.ru',
    ],

    'ingos' => [
        'login' => 'ИП Шильников WS',
        'login_production' => 'ИП Шильников WS',
        'password' => 'jxpl2amr',
        'password_production' => 'jxpl2amr',
        'production_endpoint' => 'https://aisws.ingos.ru/sales-test/SalesService.svc',
        'test_endpoint' => 'https://aisws.ingos.ru/sales-test/SalesService.svc',
    ],

    'rgs' => [
        'login' => 'uagent_online',
        'login_production' => 'uagent_online',
        'password' => 'rKk7qSR5A62E',
        'password_production' => 'vj4qi2t1d3Vm',
        'production_endpoint' => 'https://ufo.rgs.ru',
        'test_endpoint' => 'https://ufot.rgs.ru',
        'inn' => '3666203760', // 366401357823
        'case' => 2,
    ],
    'avtocode' => [
        'user' => 'Windyurin%40IP_Shilnikov',
        'pass' => 'atUKaNAd',
        'date' => '2019-02-04T14%3A55%3A23Z',
        'age' => 999999999,
        'uid_prod' => 'IP_Shilnikov_Kharakteristiki_TS_plus_normalizatsiya_report',
        'uid_test' => 'IP_Shilnikov_Kharakteristiki_TS_plus_normalizatsiya_report_test',
    ],
    'slack' => [
        'hooks_url' => env('SLACK_HOOKS_URL')
    ]
];
